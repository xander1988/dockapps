/*
 *    WMMemLoad - A dockapp to monitor memory usage
 *    Copyright (C) 2002  Mark Staggs <me@markstaggs.net>
 *
 *    Based on work by Seiichi SATO <ssato@sh.rim.or.jp>
 *    Copyright (C) 2001,2002  Seiichi SATO <ssato@sh.rim.or.jp>

 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.

 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define ALARM_LENGTH 60 /* this value / 4 = length in sec (default: 15) */

static char *light_color; /* back-light color */
static char *border_dark;
static char *border_light;
static char *alarm_time;
static char *command;

static bool backlight;
static bool alarm_on;
static bool am_pm_time;
static bool show_seconds;
static bool blink;
static bool config_scr = false;
static bool do_alarm = false;

static const char day_of_week[7][4] = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
static const char mon_of_year[12][4] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

static int alarm_h;
static int alarm_m;
static int alarm_s;
static int alarm_counter = ALARM_LENGTH;
static int blink_counter = 2;

struct tm *time_struct;

static long curtime;

time_t last_update = 0;

/* prototypes */
static void update_time(void);
static void update_ui(void);
static void parse_alarm_time(void);
static void do_blink(void);
static void show_main_scr(void);
static void show_conf_scr(void);
static void switch_light(void);
static void draw_time(int, int, int);
static void draw_text(const char *, int, int);
static void draw_day_num(int);
static void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a nice digital clock.\n\n"

    "Clicking various parts of the window with left mouse button switches backlight or turns some options on and off, like changing between 24- and 12-hour time display, activating alarm, dots blinking and so on. Right mouse button click shows configuration screen, where alarm time can be set.");

    da_init_string(&light_color, "front_color", "-f", "--front-color", "#6ec63b", "Main color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&backlight, "backlight", "-b", "--backlight", false, "Turn on LCD backlight");
    da_init_string(&alarm_time, "alarm_time", "-a", "--alarm-time", "00:00:00", "Time to display alarm (always in the 24-hour format)");
    da_init_string(&command, "command", "-c", "--command", NULL, "Command to execute on alarm");
    da_init_bool(&alarm_on, "alarm_on", NULL, "--alarm-on", false, "Activate alarm or not");
    da_init_bool(&am_pm_time, "12_hour_time", NULL, "--12-hour-time", false, "Show time in the 12-hour format (does not affect alarm time)");
    da_init_bool(&show_seconds, "show_seconds", "-S", "--show-seconds", true, "Display seconds counter or not");
    da_init_bool(&blink, "blink", "-B", "--blink", true, "Blink the dots or not");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

static void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(light_color, "led_color_high");
    da_add_blended_color(light_color, -24, -24, -24, 1.0, "led_color_med");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    da_add_color("#020202", "almost_black");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
    }

    parse_alarm_time();

    /* Clickable regions */
    /* Main screen */
    da_add_mouse_region(0,  8, 29, 19, 45); /* 24-hour/12-hour     */
    da_add_mouse_region(1, 21, 29, 42, 36); /* alarm on/off        */
    da_add_mouse_region(2, 44, 29, 57, 40); /* show seconds on/off */
    da_add_mouse_region(3, 30, 12, 35, 26); /* blink on/off        */
    /* Config screen */
    da_add_mouse_region(4,   8, 30, 19, 36); /* hour+ button */
    da_add_mouse_region(5,  20, 30, 31, 36); /* min+ button  */
    da_add_mouse_region(6,  32, 30, 43, 36); /* sec+ button  */
    da_add_mouse_region(7,   8, 40, 19, 46); /* hour- button */
    da_add_mouse_region(8,  20, 40, 31, 46); /* min- button  */
    da_add_mouse_region(9,  32, 40, 43, 46); /* sec- button  */
    da_add_mouse_region(10,  8, 51, 29, 58); /* cancel       */
    da_add_mouse_region(11, 41, 51, 57, 58); /* ok           */

    update_time();

    show_main_scr();

    /* Main loop */
    while (!da_conf_changed(argc, argv)) {
        if (time(0) - last_update >= 1) {
            update_time();
        }

        if (!config_scr)
            update_ui();

        waitpid(0, NULL, WNOHANG);

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch (da_check_mouse_region()) {
            case 0:
                am_pm_time ^= true;
                
                update_ui();
                break;

            case 1:
                alarm_on ^= true;
                
                update_ui();
                break;

            case 2:
                show_seconds ^= true;
                
                update_ui();
                break;

            case 3:
                blink ^= true;
                
                update_ui();
                break;

            case 4:
                alarm_h++;

                if (alarm_h > 23)
                    alarm_h = 0;

                draw_time(alarm_h, alarm_m, alarm_s);
                break;
            
            case 5:
                alarm_m++;

                if (alarm_m > 59)
                    alarm_m = 0;

                draw_time(alarm_h, alarm_m, alarm_s);
                break;
            
            case 6:
                alarm_s++;

                if (alarm_s > 59)
                    alarm_s = 0;

                draw_time(alarm_h, alarm_m, alarm_s);
                break;
            
            case 7:
                alarm_h--;

                if (alarm_h < 0)
                    alarm_h = 23;

                draw_time(alarm_h, alarm_m, alarm_s);
                break;
            
            case 8:
                alarm_m--;

                if (alarm_m < 0)
                    alarm_m = 59;

                draw_time(alarm_h, alarm_m, alarm_s);
                break;
            
            case 9:
                alarm_s--;

                if (alarm_s < 0)
                    alarm_s = 59;

                draw_time(alarm_h, alarm_m, alarm_s);
                break;
            
            case 10:
                parse_alarm_time();
                show_main_scr();
                break;
            
            case 11:
                show_main_scr();
                break;

            default:
                switch_light();

                if (!config_scr)
                    update_ui();

                da_redraw_window();
                break;
            }
            
            break;

        case MOUSE_3_PRS:
            if (!config_scr) {
                show_conf_scr();
                draw_time(alarm_h, alarm_m, alarm_s);
            
            } else {
                show_main_scr();
                update_ui();
            }
            break;
        }

        da_redraw_window();
    
        usleep(250000L);
    }

    routine(argc, argv);
}

static void show_main_scr(void) {
    int i;

    for (i = 0; i < 4; i++) {
        da_enable_mouse_region(i);
    }
    for (i = 4; i < 12; i++) {
        da_disable_mouse_region(i);
    }
    
    config_scr = false;
    
    if (backlight) {
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }

    da_redraw_window();
}

static void show_conf_scr(void) {
    int i;

    for (i = 0; i < 4; i++) {
        da_disable_mouse_region(i);
    }
    for (i = 4; i < 12; i++) {
        da_enable_mouse_region(i);
    }
    
    config_scr = true;
    
    if (backlight) {
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);
        da_copy_xpm_area(130, 93, 49, 29, 8, 29);

        da_set_mask_xy(128, 0);
    
    } else {
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);
        da_copy_xpm_area(130, 64, 49, 29, 8, 29);

        da_set_mask_xy(64, 0);
    }

    draw_time(alarm_h, alarm_m, alarm_s);

    draw_text("RESET", 9, 52);
    draw_text("SET", 42, 52);

    da_redraw_window();
}

static void update_time(void) {
    curtime = time(0);

    time_struct = localtime(&curtime);

    time(&last_update);
}

static void update_ui(void) {
    int hour;

    bool pm;

    hour = time_struct->tm_hour;

    da_set_foreground(4);

    if (am_pm_time) {
        pm = (hour >= 12) ? true : false;
        
        if (hour == 0) {
            hour = 12;
        } else {
            hour = (hour > 12) ? hour - 12 : hour;
        }

        if (pm) {
            da_fill_rectangle(8, 38, 11, 7);

            draw_text("PM", 9, 39);
        
        } else {
            da_fill_rectangle(8, 29, 11, 7);

            draw_text("AM", 9, 30);
        }
    } else {
        if (backlight) {
            da_copy_xpm_area(136, 29, 11, 16, 8, 29);
        } else {
            da_copy_xpm_area(72, 29, 11, 16, 8, 29);
        }
    }

    if (alarm_on) {
        /* alarm */
        if (time_struct->tm_hour == alarm_h && time_struct->tm_min == alarm_m && time_struct->tm_sec == alarm_s) {
            do_alarm = true;
            
            if (command)
                da_exec_command(command);
        }
    
        da_fill_rectangle(21, 29, 21, 7);

        draw_text("ALRM", 22, 30);
    
    } else {
        if (backlight) {
            da_copy_xpm_area(149, 29, 21, 7, 21, 29);
        } else {
            da_copy_xpm_area(85, 29, 21, 7, 21, 29);
        }
    }

    if (do_alarm) {
        if (alarm_counter <= 0) {
            do_alarm = false;
            
            alarm_counter = ALARM_LENGTH;
        
        } else {
            alarm_counter--;
            
            switch_light();
        }
    }

    if (blink) {
        if (blink_counter <= 0) {
            blink_counter = 2;
        }

        if (blink_counter == 2) {
            do_blink();
        }
        
        blink_counter--;
    
    } else {
        if (backlight) {
            da_copy_xpm_area(104, 76, 3, 12, 31, 13);
        } else {
            da_copy_xpm_area(104, 64, 3, 12, 31, 13);
        }
    }

    if (!show_seconds) {
        if (backlight) {
            da_copy_xpm_area(172, 29, 13, 11, 44, 29);
        } else {
            da_copy_xpm_area(108, 29, 13, 11, 44, 29);
        }
    }

    draw_time(hour, time_struct->tm_min, time_struct->tm_sec);

    draw_day_num(time_struct->tm_mday);
    
    draw_text(day_of_week[time_struct->tm_wday], 9, 52);
    draw_text(mon_of_year[time_struct->tm_mon], 42, 52);

    da_redraw_window();
}

/* called when mouse button pressed */
static void switch_light(void) {
    if (!backlight) {
        backlight = true;
        
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        if (config_scr) {
            da_copy_xpm_area(130, 93, 49, 29, 8, 29);

            draw_time(alarm_h, alarm_m, alarm_s);

            draw_text("RESET", 9, 52);
            draw_text("SET", 42, 52);
        }

        da_set_mask_xy(128, 0);
    
    } else {
        backlight = false;
        
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        if (config_scr) {
            da_copy_xpm_area(130, 64, 49, 29, 8, 29);

            draw_time(alarm_h, alarm_m, alarm_s);

            draw_text("RESET", 9, 52);
            draw_text("SET", 42, 52);
        }

        da_set_mask_xy(64, 0);
    }

    da_redraw_window();
}

static void draw_text(const char *text, int x, int y) {
    int length;
    int i = 104, ii;

    if (backlight) {
        i = 110;
    }

    length = strlen(text);

    for (ii = 0; ii < length; ii++) {
        da_copy_xpm_area((int)(text[ii] - 'A') * 5, i, 5, 5, x, y);

        x += 5;
    }

    da_redraw_window();
}

static void draw_day_num(int day_num) {
    char day_str[4];
    
    int i = 115;

    if (backlight) {
        i = 125;
    }

    sprintf(day_str, "%02d", day_num);

    da_copy_xpm_area((int)(day_str[0] - 48) * 6, i, 6, 11, 27, 47);
    da_copy_xpm_area((int)(day_str[1] - 48) * 6, i, 6, 11, 33, 47);

    da_redraw_window();
}

static void draw_time(int hr, int min, int sec) {
    char time_str[16];
    
    int i = 64;
    int ii = 115;

    if (backlight) {
        i = 84;
        ii = 125;
    }

    sprintf(time_str, "%02d%02d%02d", hr, min, sec);

    da_copy_xpm_area((int)(time_str[0] - 48) * 10, i, 10, 20, 8, 8);
    da_copy_xpm_area((int)(time_str[1] - 48) * 10, i, 10, 20, 20, 8);
    /* : */
    da_copy_xpm_area((int)(time_str[2] - 48) * 10, i, 10, 20, 35, 8);
    da_copy_xpm_area((int)(time_str[3] - 48) * 10, i, 10, 20, 47, 8);

    if (show_seconds) {
        da_set_foreground(4);
        
        da_fill_rectangle(44, 29, 13, 11); /* forgot a thin line on the left */
        da_copy_xpm_area((int)(time_str[4] - 48) * 6, ii, 6, 11, 45, 29);
        da_copy_xpm_area((int)(time_str[5] - 48) * 6, ii, 6, 11, 51, 29);
    }

    da_redraw_window();
}

static void do_blink(void) {
    static bool dots_on = true;

    int i = 64;

    if (backlight) {
        i = 76;
    }

    if (dots_on) {
        dots_on = false;
        
        da_copy_xpm_area(107, i, 3, 12, 31, 13);

    } else {
        dots_on = true;
        
        da_copy_xpm_area(104, i, 3, 12, 31, 13);
    }
}

static void parse_alarm_time(void) {
    char time_tmp[3];
    
    if (!alarm_time || (strlen(alarm_time) != 8)) {
        printf("ERR: wrong time set, aborting.\n");
        da_log(ERR, "wrong time set, aborting.\n");

        exit(0);
    }
    
    /* hour */
    time_tmp[0] = alarm_time[0];
    time_tmp[1] = alarm_time[1];
    alarm_h = atoi(time_tmp);
    /* min */
    time_tmp[0] = alarm_time[3];
    time_tmp[1] = alarm_time[4];
    alarm_m = atoi(time_tmp);
    /* sec */
    time_tmp[0] = alarm_time[6];
    time_tmp[1] = alarm_time[7];
    alarm_s = atoi(time_tmp);
}
