#include <libdockapp4/dockapp.h>
#include <time.h>

#include "config.h"

#include "bitmaps/mask.xbm"
#include "bitmaps/mask2.xbm"
#include "bitmaps/mask3.xbm"
#include "bitmaps/mask4.xbm"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

int posx[4] = {8, 17, 26, 35};

static struct tm *clk;

time_t actualtime;

char *front_color;
char *back_color;
char *border_dark;
char *border_light;

void Beat(void);
void InsertTime(void);
void routine(int, char **);

int main(int argc,char *argv[]) {
	da_init_main(PROG_NAME, PROG_VERSION, "a beats clock dockable in the windowmaker dock.\n\n"

    "Credits go out to Beat Christen, the author of asclock, which most of the code that handles X comes from. Also to Alfredo Kojima, who has written the code for docking the application in the afterstep and windowmaker dock.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc"); 

    da_parse_all(argc, argv);

    routine(argc, argv);
    
    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, mask4_bits, mask4_width, mask4_height);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, mask3_bits, mask3_width, mask3_height);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, mask2_bits, mask2_width, mask2_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, mask_bits, mask_width, mask_height);
        break;
    }
    
	InsertTime();
    
	da_redraw_window();
    
	while (!da_conf_changed(argc, argv)) {
		if (actualtime != time(0)) {
			InsertTime();
            
			da_redraw_window();

		}
        
		da_watch_xevent();
        
		usleep(50000L);
	}
    
	routine(argc, argv);
}

void Beat() {
	float fTime;

	fTime=(clk->tm_hour*3600+clk->tm_min*60+clk->tm_sec);
	fTime=fTime+timezone+3600;
	if (clk->tm_isdst)
		fTime-=3600;
	fTime=(fTime*1000)/86400;
	if (fTime >= 1000)
		fTime-=1000;
	else
		if (fTime < 0)
			fTime+=1000;

	da_copy_xpm_area(64 + 90, 0, 9, 11, posx[0], 15);
	da_copy_xpm_area(64 + 9*(((int)fTime)/100), 0, 9, 11, posx[1], 15);
	da_copy_xpm_area(64 + 9*((((int)fTime) / 10) % 10), 0, 9, 11, posx[2], 15);
	da_copy_xpm_area(64 + 9*((int)fTime % 10), 0, 9, 11, posx[3], 15);

	fTime = abs(((fTime - abs(fTime)) * 1000) + 0.5);

	da_copy_xpm_area(64 + 99, 0, 9, 11, posx[0], 32);
	da_copy_xpm_area(64 + 9*(((int)fTime)/100), 0, 9, 11, posx[1], 32);
	da_copy_xpm_area(64 + 9*((((int)fTime) / 10) % 10), 0, 9, 11, posx[2], 32);
	da_copy_xpm_area(64 + 9*((int)fTime % 10), 0, 9, 11, posx[3], 32);

	return;
}

void InsertTime() {
	actualtime = time(0);
    
	clk = localtime(&actualtime);
    
	//da_copy_xpm_area(0, 0, 64, 64, 0, 0);
    
	Beat();
    
	return;
}
