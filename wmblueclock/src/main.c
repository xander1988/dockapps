/*
 *  WMBlueClock - a clock dockapp
 *
 *  Copyright (C) 2003 Draghicioiu Mihai Andrei <misuceldestept@go.ro>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/select.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "mwm.h"
#include "menu.h"

#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define DIGIT_X 0
#define DIGIT_Y 72
#define DIGIT_WIDTH 8
#define DIGIT_HEIGHT 12
#define MERIDIAN_AM_X 80
#define MERIDIAN_AM_Y 64
#define MERIDIAN_PM_X 98
#define MERIDIAN_PM_Y 64
#define MERIDIAN_WIDTH 18
#define MERIDIAN_HEIGHT 8
#define NUMBER_X 0
#define NUMBER_Y 64
#define NUMBER_WIDTH 8
#define NUMBER_HEIGHT 8
#define OPT_MILISECS 1000

struct timeval tv = {0, 0};

menu_t *m;

int opt_milisecs;

char *opt_bgcolor;
char *opt_oncolor;
char *opt_offcolor;

bool opt_ampm;
bool show_sec;

void draw_digit(int, int, int);
void draw_number(int, int, int);
void draw_meridian(int, int, int);
void draw_window(void);
void proc(void);
void switch_ampm(menuitem_t *);
void routine(int, char **);
void process_events(void);

void process_events(void) {
    /* Process any pending X events */
    switch (da_watch_xevent()) {
    case MOUSE_2_PRS:
        exit(0);
        break;
        
    case MOUSE_3_PRS:
        if (menu_pop(m) == 0)
            exit(0);
        break;
    }
}

void draw_digit(int x, int y, int n) {
    if (n >= 0)
        da_copy_xpm_area(DIGIT_X + n * DIGIT_WIDTH, DIGIT_Y, DIGIT_WIDTH, DIGIT_HEIGHT, x, y);
    else
        da_fill_rectangle(x, y, DIGIT_WIDTH, DIGIT_HEIGHT);
}

void draw_number(int x, int y, int n) {
    if (n >= 0)
        da_copy_xpm_area(NUMBER_X + n * NUMBER_WIDTH, NUMBER_Y, NUMBER_WIDTH, NUMBER_HEIGHT, x, y);
    else
        da_fill_rectangle(x, y, NUMBER_WIDTH, NUMBER_HEIGHT);
}

void draw_meridian(int x, int y, int pm) {
    if (pm == -1)
        da_fill_rectangle(x, y, MERIDIAN_WIDTH, MERIDIAN_HEIGHT);
    else if (pm == 0)
        da_copy_xpm_area(MERIDIAN_AM_X, MERIDIAN_AM_Y, MERIDIAN_WIDTH, MERIDIAN_HEIGHT, x, y);
    else if (pm == 1)
        da_copy_xpm_area(MERIDIAN_PM_X, MERIDIAN_PM_Y, MERIDIAN_WIDTH, MERIDIAN_HEIGHT, x, y);
}

void draw_window(void) {
    struct tm *atm;
    
    time_t atime;

    atime = time(NULL);
    
    atm = localtime(&atime);

    if (opt_ampm) {
        if (atm->tm_hour > 12) {
            atm->tm_hour -= 12;
            
            draw_meridian(19, 30, 1);
        } else
            draw_meridian(19, 30, 0);
    } else
        draw_meridian(19, 30, -1);

    draw_digit(19, 10, (atm->tm_hour > 9) ? atm->tm_hour / 10 : -1);
    draw_digit(29, 10, atm->tm_hour % 10);
    draw_digit(41, 10, atm->tm_min / 10);
    draw_digit(51, 10, atm->tm_min % 10);

    if (show_sec) {
        draw_number(41, 30, atm->tm_sec / 10);
        draw_number(51, 30, atm->tm_sec % 10);
    } else {
        da_copy_xpm_area(4, 4, 10, 8, 41, 30);
        da_copy_xpm_area(4, 4, 10, 8, 51, 30);
    }

    draw_number(19, 51, (atm->tm_mday >= 10) ? atm->tm_mday / 10 : atm->tm_mday % 10);
    draw_number(29, 51, (atm->tm_mday >= 10) ? atm->tm_mday % 10 : -1);
    draw_number(41, 51, ((atm->tm_mon + 1) / 10) % 10);
    draw_number(51, 51, (atm->tm_mon + 1) % 10);
}

void proc(void) {
    int i;
    
    fd_set fs;
    
    int fd = ConnectionNumber(da_display);

    process_events();
    
    FD_ZERO(&fs); 
    FD_SET(fd, &fs);
    
    i = select(fd + 1, &fs, 0, 0, &tv);
    
    if (i == -1) {
        fprintf(stderr, "Error with select(): %s", strerror(errno));
        exit(1);
    }
 
    if (!i) {
        draw_window();
        
        da_redraw_window();
        
        tv.tv_sec = opt_milisecs / 1000;
        tv.tv_usec = (opt_milisecs % 1000) * 1000;
    }
}

void switch_ampm(menuitem_t *i) {
    opt_ampm = i->checked;
    
    draw_window();
    
    da_redraw_window();
}

void switch_sec(menuitem_t *i) {
    show_sec = i->checked;
    
    draw_window();
    
    da_redraw_window();
}

void routine(int argc, char *argv[]) {
    menuitem_t *i;

    da_init_xwindow();

    da_add_color(opt_bgcolor, "bg_color");
    da_add_color(opt_oncolor, "on_color");
    da_add_color(opt_offcolor, "off_color");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 128 * da_scale, 84 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 128 * da_scale, 84 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 128 * da_scale, 84 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 128 * da_scale, 84 * da_scale);
        break;
    }

    menu_init(da_display);
    
    m = menu_new();

    i = menu_append(m, "12 Hour mode");
    i->i = -1;
    i->checked = opt_ampm;
    i->callback = switch_ampm;
    
    i = menu_append(m, "Display seconds");
    i->i = -1;
    i->checked = show_sec;
    i->callback = switch_sec;

    i = menu_append(m, "Exit");
    i->i = 0;

    while (!da_conf_changed(argc, argv))
        proc();
    
    routine(argc, argv);
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp which displays time.\n\n"

    PROG_NAME" displays hour, minute, second (can optionally be off), day of month, and number of month. It can run in 12 and 24 hour mode. Right-click for a menu.");

    da_init_string(&opt_bgcolor, "bg_color", "-B", "--bgcolor", "#000000", "Background color");
    da_init_string(&opt_oncolor, "on_color", "-N", "--oncolor", "#87d7ff", "Color for On leds");
    da_init_string(&opt_offcolor, "off_color", "-F", "--offcolor", "#0095e0", "Color for Off leds");
    da_init_bool(&opt_ampm, "12_hour_time", "-t", "--12-hour-time", false, "Use 12-hour time");
    da_init_bool(&show_sec, "display_seconds", "-S", "--display-seconds", true, "Display seconds");
    da_init_integer(&opt_milisecs, "milisecs", "-m", "--milisecs", 0, 0, OPT_MILISECS, "The number of milisecs between updates");   
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}
