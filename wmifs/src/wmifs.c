/*  WMiFS - a complete network monitoring dock.app
    Copyright (C) 1997, 1998 Martijn Pieterse <pieterse@xs4all.nl>
    Copyright (C) 1997, 1998 Antoine Nulle <warp@xs4all.nl>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

    Best viewed with vim5, using ts=4

    This code was mainly put together by looking at the
    following programs:

    asclock
        A neat piece of equip, used to display the date
        and time on the screen.
        Comes with every AfterStep installation.

        Source used:
            How do I create a not so solid window?
            How do I open a window?
            How do I use pixmaps?

    pppstats
        A program that prints the amount of data that
        is transferred over a ppp-line.

        Source used:
            How do I read the ppp device?
    ------------------------------------------------------------

    Author: Martijn Pieterse (pieterse@xs4all.nl)

    This program was hacked together between the 7th of March
    and the 14th of March 1998.

    This program might be Y2K resistant. We shall see. :)

    This program is distributed under the GPL license.
    (as were asclock and pppstats)

    Known Features: (or in non M$ talk, BUGS)
        * only ppp0 will be read
            use wmifs if you want to read more than one ppp connection
            not sure about this.
        * the leds won't be reliable with
          more than 1 ppp connection
        * there is an iconwin, and win variable.
          I have no clue why only win shouldn't
          be enough. Will check it out later.
        * The afterstep what seems the shift the
          pixmap a bit. Don't know how and why.
          It works in the WindowManager.

    Things to do:
        Split up main()

    ----
    Thanks
    ----

    Most of the ideas, and jumpstarting it:

    #linuxnl, without this irc-channel wmppp would've never seen the light!

    CCC (Constructive Code Criticism):

    Marcelo E. Magallon
        Thanks a LOT! It takes a while to get me convinced... :)


    Minor bugs and ideas:

    Marc De Scheemaecker / David Mihm / Chris Soghoian /
    Alessandro Usseglio Viretta

    and ofcourse numerous ppl who send us bug reports.
    (numerous? hmm.. todo: rephrase this :) )


    ----
    Changes:
    ---
    02/29/2004 (Tom Marshall, tommy@home.tig-grr.com)
        * Patch to add a special interface name "auto" for the -i
          option. "wmifs -i auto" will automatically select the
          first up interface.
    01/08/2004 (Peter Samuelson, peter@samba-tng.org)
        * Patch to make make sampling and scrolling intervals
          customizable, adds new options -I and -s.
    01/15/2002 (Matyas Koszik, koszik@debijan.lonyay.edu.hu)
        * Patch that fixes segfaults on long interface names.
    08/31/2001 (Davi Leal, davileal@terra.es)
        * Patch that cuts long interface names, so they look
          good in wmifs. For example, "dummy0" gets displayed
          as "dumm0", "vmnet10" as "vmn10", etc.
    06/16/2001 (Jorge García, Jorge.Garcia@uv.es)
        * Added the LockMode, so wmifs doesn't swap to another
          interface if the one requested with "-i" isn't up.
    05/06/2001 (Jordi Mallach, jordi@sindominio.net)
        * Integrated many patches, fixing issues with suspended
          wmifs.
    07/21/1999 (Stephen Pitts, smpitts@midsouth.rr.com)
        * Added new constant: BUFFER_SIZE to determine the size
          of the buffer used in fgets() operations. Right now,
          its at 512 bytes. Fixed crashing on my system when
          one line of /proc/net/dev was longer than 128 bytes
    04/05/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Changed the "middle of the waveform" line color
        * Moved the da_redraw_window out of the main loop.
          Lightens the system load
    02/05/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Torn wmppp and wmifs apart.
          This is gonna be wmifs
        * Added parse_rcfile
        * Added waitpid, to get rid of the zombies, spotteb by Alessandro Usseglio Viretta
    30/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Used the DrawStats routine from wmifs in wmppp
        * I decided to add a list in this source file
          with name of ppl who helped me build this code better.
        * I finally removed the /proc/net/route dependency
          All of the connections are taken from /proc/net/dev.
          /proc/net/route is still used for checking if it is on-line.
    27/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * WMIFS: stats scrolled, while red led burning
        * WMPPP: changed positions of line speed
    25/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Changed the checknetdevs routine, a lot!
    23/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added line speed detection. via separate exec. (this has to be suid root!)
          Speed has to be no more than 99999
        * Added speed and forcespeed in ~/.wmppprc and /etc/wmppprc
        * wmifs: added on-line detection scheme, update the bitmap coordinates
        * wmppp: the x-button now allways disconnects.
    22/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added /etc/wmppprc support, including "forced" mode.
        * Added /etc/wmifsrc support, including "forced" mode.
    21/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Moved the stats one pixel down.
        * Added status led in wmifs.
        * Changed RX/TX leds of wmifs to resemble wmppp
        * Added the "dot" between eth.0 ppp.0 etc.
        * Changed to wmifs stats to match wmppp stats (only pppX changed)
        * Made sure that when specified -t 1, it stayed that way, even
          when longer than 100 minutes online
        * With -t 1, jump from 59:59 to 01:00 instead of 99:59 to 01:40
    16/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added "all" devices in wmifs
        * Added "lo" support only if aked via -i
        * Added on-line time detection (using /var/run/ppp0.pid)
        * Added time-out for the orange led. (one minute)
    15/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Another wmppp-master.xpm.
            Line speed detection being the main problem here.. :(
        * Moved START_COMMAND / STOP_COMMAND to ~/.wmppprc
            Return 0, everything went ok.
            Return 10, the command did not work.
            Please note, these functions are ran in the background.
        * Removed the ability to configure
        * When "v" is clicked, an orange led will appear.
          if the connect script fails (return value == 10)
          it will become dark again. Else the on-line detection will
          pick it up, and "green" it.
    14/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added "-timer"
        * Added "-display" support
        * Changed pixmap to a no-name pixmap.
            + Changed LED positions
            + Changed Timer position
            + Changed Stats Size
    05/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added ~/.wmifsrc support.
        * Put some code in DrawStats
        * Check devices when pressing "device change"
    03/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added code for wmifs
    28/03/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * forgot what i did.. :)
    27/03/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added on-line detection
            Scan through /proc/net/route and check everye line
            for "ppp".
        * A bit of code clean-up.
*/

#define _DEFAULT_SOURCE
#include <linux/ppp_defs.h>            /* for ppp_stats, pppstat */
#include <net/if_ppp.h>                /* for ifpppstatsreq, ifr__name, etc */
#include <stddef.h>                    /* for size_t */
#include <sys/ioctl.h>                 /* for ioctl */
#include <sys/socket.h>                /* for socket, AF_INET */
#include <sys/time.h>                  /* for gettimeofday */
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "XPM/wmifs-mask64.xbm"
#include "XPM/wmifs-mask128.xbm"
#include "XPM/wmifs-mask192.xbm"
#include "XPM/wmifs-mask256.xbm"
#include "XPM/wmifs-master64.xpm"
#include "XPM/wmifs-master128.xpm"
#include "XPM/wmifs-master192.xpm"
#include "XPM/wmifs-master256.xpm"

  /***********/
 /* Defines */
/***********/

#ifndef ifr__name
#define ifr__name ifr_name
#endif

#ifndef stats_ptr
#define stats_ptr stats.p.FIXME
#endif

/* Defines voor alle coordinate */

#define LED_NET_RX          (4)
#define LED_NET_TX          (5)
#define LED_NET_POWER       (6)

/* the size of the buffer read from /proc/net/ */
#define BUFFER_SIZE 512

#define MAX_STAT_DEVICES 16

typedef struct {
    char    name[IFNAMSIZ];
    int his[55][2];
    long    istatlast;
    long    ostatlast;

} stat_dev;

stat_dev stat_devices[MAX_STAT_DEVICES];

  /********************/
 /* Global Variables */
/********************/

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *line_color;
char *active_interface;
char *left_action;
char *right_action;
char *middle_action;

bool WaveForm;
bool LockMode;

double SampleInt;

int ScrollSpeed;

  /*****************/
 /* PPP variables */
/*****************/

#define     PPP_UNIT        0
int         ppp_h = -1;

#define     PPP_STATS_HIS   54

  /***********************/
 /* Function Prototypes */
/***********************/

void DrawTime(int, int);
void DrawStats(int *, int, int, int, int);
void SetOnLED(int);
void SetErrLED(int);
void SetWaitLED(int);
void SetOffLED(int);
void ButtonUp(int);
void ButtonDown(int);
void routine(int, char **);
void get_ppp_stats(struct ppp_stats *);
int checknetdevs(void);
int get_statistics(char *, long *, long *, long *, long *);
int stillonline(char *);
void DrawActiveIFS(char *);

  /********/
 /* Main */
/********/

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockable network traffic monitor.\n\n"

    PROG_NAME" is a dockable applet for X11 that can monitor all your network interfaces. It's designed for WindowMaker's Dock and AfterStep's Wharf, but it's not dependent on these window managers and should work with any other.\n\n"

    PROG_NAME" is a complete network monitoring dock.app, it's mainly designed for usage in WindowMaker's dock and gives you some nice & nifty features like: autosensing of *ALL* active network interfaces; integrated autoscaling (per interface) transfer statistics, tested up to 100Mbit; displays a 'normal' xload style graph or our new 'waveform' like load graph; realtime cycling through active interfaces by simply clicking on the eth0/ppp0 (interface) gadget; integrated RX/TX interface activity LEDs; integrated interface status LED; commandline options to force monitoring a particular interface, even 'lo' is supported (see note); user-definable scripts for left/middle/right mouse buttons which are read from ~/."PROG_NAME"rc (optional); fixed rc file option, useful for sites where users are not allowed to mess with pppd.\n\n"

    PROG_NAME" has a special -I option, this way you can force "PROG_NAME" to monitor a particular interface like:\n\n"

    PROG_NAME" -I eth0 &\n"
    PROG_NAME" -I ppp0 &\n"
    PROG_NAME" -I lo &\n\n"

    "Without the -I option ("PROG_NAME" &) "PROG_NAME" automagicly grabs the default interface and will display the name and statistics of that interface.\n\n"

    "You can cycle in realtime through all available active interfaces by simply clicking with the left mousebutton on the interface name gadget in the upperleft corner of "PROG_NAME".\n\n"

    "Note: The 'lo' interface is an exception, 'lo' ONLY works when invoked from the commandline ("PROG_NAME" -I lo), lo was mainly build in for testing purposes.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&line_color, "line_color", NULL, "--line-color", "#71E371", "Separator line color in the waveform mode");
    da_init_string(&left_action, "left_action", NULL, "--left-action", NULL, "Left action");
    da_init_string(&middle_action, "middle_action", NULL, "--middle-action", NULL, "Middle action");
    da_init_string(&right_action, "right_action", NULL, "--right-action", NULL, "Right action");
    da_init_string(&active_interface, "interface", "-I", "--interface", NULL, "Interface that should come up initially when executing "PROG_NAME"; if you use \"auto\" as interface name, the first active (\"up\") interface will be used");
    da_init_float(&SampleInt, "interval", "-i", "--interval", 0.0, 0.0, 0.05, "Sampling interval, in seconds");
    da_init_bool(&LockMode, "lock", NULL, "--lock", false, "Starts "PROG_NAME" in lock mode; useful if combined with -I, if you want to monitor a, for example, ppp device which isn't up when "PROG_NAME" is started, it won't switch to the next available interface");
    da_init_bool(&WaveForm, "waveform", NULL, "--waveform", false, "Use the waveform graph instead of the classic one");
    da_init_integer(&ScrollSpeed, "speed", NULL, "--speed", 0, 0, 5, "Scrolling interval, in seconds");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc"); 
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    da_add_color(line_color, "line_color");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, wmifs_master256_xpm, wmifs_mask256_bits, wmifs_mask256_width, wmifs_mask256_height);
        break;

    case 3:
        da_open_xwindow(argc, argv, wmifs_master192_xpm, wmifs_mask192_bits, wmifs_mask192_width, wmifs_mask192_height);
        break;

    case 2:
        da_open_xwindow(argc, argv, wmifs_master128_xpm, wmifs_mask128_bits, wmifs_mask128_width, wmifs_mask128_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, wmifs_master64_xpm, wmifs_mask64_bits, wmifs_mask64_width, wmifs_mask64_height);
        break;
    }

     /* > Button */
    da_add_mouse_region(0, 5, 5, 35, 15);
    da_add_mouse_region(1, 5, 20, 58, 58);

/*******************************************************************************\
|* wmifs_routine                                                               *|
\*******************************************************************************/
    int         i, j;

    int         stat_online;
    int         stat_current;
    int         first_time = 1;

    unsigned int    curtime;
    unsigned int    nexttime;
    
    struct timeval  tv, tv2;

    long        ipacket, opacket, istat, ostat;

    char        temp[BUFFER_SIZE];

    for (i = 0; i < MAX_STAT_DEVICES; i++) {
        stat_devices[i].name[0] = 0;
        
        for (j = 0; j < 48; j++) {
            stat_devices[i].his[j][0] = 0;
            stat_devices[i].his[j][1] = 0;
        }
    }

    stat_online = checknetdevs();

    stat_current = 0;
    
    if (active_interface) {
        int isauto = !strcmp(active_interface, "auto");
        
        for (i = 0; i < stat_online; i++) {
            if ((isauto && stillonline(stat_devices[i].name)) || !strcmp(stat_devices[i].name, active_interface)) {
                stat_current = i;
                
                break;
            }
        }
    }

    gettimeofday(&tv2, NULL);

    ScrollSpeed *= 1000;
    
    nexttime = ScrollSpeed;

    DrawActiveIFS(stat_devices[stat_current].name);

    while (!da_conf_changed(argc, argv)) {
        struct timespec ts;

        gettimeofday(&tv, NULL);
        
        curtime = (tv.tv_sec - tv2.tv_sec) * 1000 + (tv.tv_usec - tv2.tv_usec) / 1000;

        waitpid(0, NULL, WNOHANG);

        for (i = 0; i < stat_online; i++) {
            get_statistics(stat_devices[i].name, &ipacket, &opacket, &istat, &ostat);

            if (first_time) {
                first_time = 0;
                
            } else {
                stat_devices[i].his[53][0] += istat - stat_devices[i].istatlast;
                stat_devices[i].his[53][1] += ostat - stat_devices[i].ostatlast;
            }

            if (i == stat_current) {
                if (!stillonline(stat_devices[i].name))
                    SetErrLED(LED_NET_POWER);
                else
                    SetOnLED(LED_NET_POWER);

                if (stat_devices[i].istatlast == istat)
                    SetOffLED(LED_NET_RX);
                else
                    SetOnLED(LED_NET_RX);

                if (stat_devices[i].ostatlast == ostat)
                    SetOffLED(LED_NET_TX);
                else
                    SetOnLED(LED_NET_TX);
            }

            stat_devices[i].istatlast = istat;
            stat_devices[i].ostatlast = ostat;
        }
        
        da_redraw_window();

        if (curtime >= nexttime) {
            nexttime = curtime + ScrollSpeed;

            DrawStats(&stat_devices[stat_current].his[0][0], 54, 40, 5, 58);
            
            for (i = 0; i < stat_online; i++) {
                if (stillonline(stat_devices[i].name)) {
                    for (j = 1; j < 54; j++) {
                        stat_devices[i].his[j-1][0] = stat_devices[i].his[j][0];
                        stat_devices[i].his[j-1][1] = stat_devices[i].his[j][1];
                    }
                    
                    stat_devices[i].his[53][0] = 0;
                    stat_devices[i].his[53][1] = 0;
                }
            }
            
            da_redraw_window();
        }

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch (da_check_mouse_region()) {
            case 0:
                /* re-read the table */
                strcpy(temp, stat_devices[stat_current].name);
                stat_online = checknetdevs();
                stat_current = 0;
                for (i = 0; i < stat_online; i++) {
                    if (!strcmp(temp, stat_devices[i].name))
                        stat_current = i;
                }

                stat_current++;
                if (stat_current == stat_online)
                    stat_current = 0;

                DrawActiveIFS(stat_devices[stat_current].name);

                DrawStats(&stat_devices[stat_current].his[0][0], 54, 40, 5, 58);

                da_redraw_window();
                break;

            case 1:
                if (left_action)
                    da_exec_command(left_action);
                        
                break;
            }
            
            break;

        case MOUSE_2_PRS:
            if (da_check_mouse_region() == 1) {
                if (middle_action)
                    da_exec_command(middle_action);
            }
            break;
            
        case MOUSE_3_PRS:
            if (da_check_mouse_region() == 1) {
                if (right_action)
                    da_exec_command(right_action);
            }
            break;

        default:
            break;
        }
        
        ts.tv_sec = 0;
        ts.tv_nsec = SampleInt * 1000000;
        nanosleep(&ts, NULL);
    }
    
    routine(argc, argv);
}

/*******************************************************************************\
|* void DrawActiveIFS(char *)                                                  *|
\*******************************************************************************/

void DrawActiveIFS(char *real_name) {

    /* Cijfers op: 0,65
       Letters op: 0,75
       Alles 9 hoog, 6 breedt

       Destinatie: 5,5
    */

    size_t      i;
    int     k;
    size_t      len;
    char        name[256];


    da_copy_xpm_area(5, 84, 30, 10, 5, 5);


    strcpy(name, real_name);
    len = strlen(name);
    if (len > 5) {
        for (i = len-5; i < len && !(name[i] >= '0' && name[i] <= '9'); i++)
            ;
        for (; i <= len; i++) /* '=' to get the '\0' character moved too \*/
            name[i-(len-5)] = name[i];
    }

    k = 5;
    for (i = 0; name[i]; i++) {
        int c;

        if (i == strlen(name)-1 && strlen(name) <= 4 && name[strlen(name)-1] >= '0' &&
            name[strlen(name)-1] <= '9') {
            da_copy_xpm_area(61, 64, 4, 9, k, 5);
            k += 4;
        }
        c = toupper(name[i]);
        if (c >= 'A' && c <= 'Z') {
            c -= 'A';
            da_copy_xpm_area(c * 6, 74, 6, 9, k, 5);
            k += 6;
        } else {
            c -= '0';
            da_copy_xpm_area(c * 6, 64, 6, 9, k, 5);
            k += 6;
        }
    }

}

/*******************************************************************************\
|* get_statistics                                                              *|
\*******************************************************************************/

int get_statistics(char *devname, long *ip, long *op, long *is, long *os)
{

    FILE                *fp;
    char                temp[BUFFER_SIZE];
    char                *p, *saveptr;
    char                *tokens = " |:\n";
    int                 input, output;
    int                 i;
    int                 found;
    struct ppp_stats    ppp_cur;


    if (!strncmp(devname, "ppp", 3)) {
        static int ppp_opened;

        if (!ppp_opened) {
            /* Open the ppp device. */
            memset(&ppp_cur, 0, sizeof(ppp_cur));
            ppp_h = socket(AF_INET, SOCK_DGRAM, 0);
            if (ppp_h < 0)
                return -1;
            get_ppp_stats(&ppp_cur);
            ppp_opened = 1;
        }

        get_ppp_stats(&ppp_cur);

        *op = ppp_cur.p.ppp_opackets;
        *ip = ppp_cur.p.ppp_ipackets;

        *is = ppp_cur.p.ppp_ibytes;
        *os = ppp_cur.p.ppp_obytes;

        return 0;
    }

    /* Read from /proc/net/dev the stats! */
    fp = fopen("/proc/net/dev", "r");
    if (!fp)
        return -1;
    if (!fgets(temp, BUFFER_SIZE, fp)) {
        fclose(fp);
        return -1;
    }
    if (!fgets(temp, BUFFER_SIZE, fp)) {
        fclose(fp);
        return -1;
    }

    input = -1;
    output = -1;
    i = 0;
    found = -1;

    p = strtok_r(temp, tokens, &saveptr);
    do {
        if (!(strcmp(p, "packets"))) {
            if (input == -1)
                input = i;
            else
                output = i;
        }
        i++;
        p = strtok_r(NULL, tokens, &saveptr);
    } while (input == -1 || output == -1);

    while (fgets(temp, BUFFER_SIZE, fp)) {
        if (strstr(temp, devname)) {
            found = 0;
            p = strtok_r(temp, tokens, &saveptr);
            i = 0;
            do {
                if (i == input) {
                    *ip = *is = atoi(p);
                    input = -1;
                }
                if (i == output) {
                    *op = *os = atoi(p);
                    output = -1;
                }
                i++;
                p = strtok_r(NULL, tokens, &saveptr);
            } while (input != -1 || output != -1);
        }
    }
    fclose(fp);

    return found;
}

/*******************************************************************************\
|* stillonline                                                                 *|
\*******************************************************************************/

int stillonline(char *ifs)
{

    FILE    *fp;
    int     i;

    i = 0;
    fp = fopen("/proc/net/route", "r");
    if (fp) {
        char temp[BUFFER_SIZE];

        while (fgets(temp, BUFFER_SIZE, fp)) {
            if (strstr(temp, ifs)) {
                i = 1; /* Line is alive */
                break;
            }
        }
        fclose(fp);
    }
    return i;
}

/*******************************************************************************\
|* checknetdevs                                                                *|
\*******************************************************************************/

int checknetdevs(void)
{

    FILE    *fd;
    int     i = 0, j;
    int     k;
    int     devsfound = 0;
    char    foundbuffer[MAX_STAT_DEVICES][IFNAMSIZ];

    for (i = 0; i < MAX_STAT_DEVICES; i++)
        foundbuffer[i][0] = 0;

    /* foundbuffer vullen met info uit /proc/net/dev */

    fd = fopen("/proc/net/dev", "r");
    if (fd) {
        char temp[BUFFER_SIZE];

        /* Skip the first 2 lines */
        if (!fgets(temp, BUFFER_SIZE, fd)) {
            fclose(fd);
            return -1;
        }
        if (!fgets(temp, BUFFER_SIZE, fd)) {
            fclose(fd);
            return -1;
        }
        while (fgets(temp, BUFFER_SIZE, fd)) {
            char *p, *saveptr;
            char *tokens = " :\t\n";

            p = strtok_r(temp, tokens, &saveptr);
            if (p == NULL) {
                    printf("Barfed on: %s", temp);
                    break;
            }
            /* Skip dummy code */

            if (!strncmp(p, "dummy", 5))
                continue;

            /* If p == "lo", and active_interface (as given on the cmd line) != "lo",
               skip it! */

            if (strcmp(p, "lo") || (active_interface && !strcmp(active_interface, "lo"))) {
                strncpy(foundbuffer[devsfound], p, IFNAMSIZ);
                devsfound++;
            }
            if (devsfound >= MAX_STAT_DEVICES)
                break;
        }
        fclose(fd);
    }

    /* Nu foundbuffer naar stat_devices[].name kopieeren */

    for (i = 0; i < MAX_STAT_DEVICES; i++) {
        /* Loop stat_devices na, als die naam niet voorkomt in foundbuffer, kill! */

        if (stat_devices[i].name[0]) {
            k = 0;
            for (j = 0; j < MAX_STAT_DEVICES; j++) {
                if (!strcmp(stat_devices[i].name, foundbuffer[j])) {
                    k = 1;
                    foundbuffer[j][0] = 0;
                }
            }
            if (!k)
                stat_devices[i].name[0] = 0;
        }
    }

    for (i = 0, j = 0; j < MAX_STAT_DEVICES; i++, j++) {

        while (!stat_devices[j].name[0] && j < MAX_STAT_DEVICES)
            j++;

        if (j < MAX_STAT_DEVICES && i != j)
            stat_devices[i] = stat_devices[j];
    }
    i--;

    for (j = 0; j < MAX_STAT_DEVICES; j++) {
        if (foundbuffer[j][0]) {

            strncpy(stat_devices[i].name, foundbuffer[j], IFNAMSIZ);

            for (k = 0; k < 48; k++) {
                stat_devices[i].his[k][0] = 0;
                stat_devices[i].his[k][1] = 0;
            }

            i++;
        }
    }
    if (LockMode && active_interface != NULL) {
        k = 0;
        for (j = 0; j < i; j++)
            if (!strcmp(stat_devices[j].name, active_interface)) {
                k = 1;
                break;
            }
        if (!k) {
            strncpy(stat_devices[i].name, active_interface, IFNAMSIZ);
            for (k = 0; k < 48; k++) {
                stat_devices[i].his[k][0] = 0;
                stat_devices[i].his[k][1] = 0;
            }
            devsfound++;
        }

    }
    return devsfound;
}

/*******************************************************************************\
|* DrawStats                                                                   *|
\*******************************************************************************/

void DrawStats(int *his, int num, int size, int x_left, int y_bottom)
{

    int     pixels_per_byte;
    int     j, k;
    int     *p;
    int     p2, p3;

    pixels_per_byte = size;
    p = his;
    for (j = 0; j < num; j++) {
        if (p[0] + p[1] > pixels_per_byte)
            pixels_per_byte = p[0] + p[1];
        p += 2;
    }

    pixels_per_byte /= size;
    p = his;

    for (k = 0; k < num; k++) {
        int p0, p1;

        p0 = p[0];
        p1 = p[1];


        if (WaveForm) {
            p2 = 0;
            p3 = 1;
            for (j = 0; j < size; j++) {
                if (j < p0 / pixels_per_byte)
                    da_copy_xpm_area(100+2, 68, 1, 1, k+x_left, y_bottom-size/2+p2/2);
                else if (j < (p0 + p1) / pixels_per_byte)
                    da_copy_xpm_area(100+1, 68, 1, 1, k+x_left, y_bottom-size/2+p2/2);
                else
                    da_copy_xpm_area(100+0, 68, 1, 1, k+x_left, y_bottom-size/2+p2/2);

                p2 = (p2 + p3);
                p3 *= -1;
                p2 *= -1;
            }
            da_copy_xpm_area(100+3, 68, 1, 1, k+x_left, y_bottom-size/2);
        } else {
            for (j = 0; j < size; j++) {
                if (j < p0 / pixels_per_byte)
                    da_copy_xpm_area(100+2, 68, 1, 1, k+x_left, y_bottom-j);
                else if (j < (p0 + p1) / pixels_per_byte)
                    da_copy_xpm_area(100+1, 68, 1, 1, k+x_left, y_bottom-j);
                else
                    da_copy_xpm_area(100+0, 68, 1, 1, k+x_left, y_bottom-j);
            }
        }
        p += 2;
    }
}

/*******************************************************************************\
|* get_ppp_stats                                                               *|
\*******************************************************************************/

void get_ppp_stats(struct ppp_stats *cur)
{

    struct ifpppstatsreq    req;

    memset(&req, 0, sizeof(req));

    req.stats_ptr = (void *) &req.stats;

    sprintf(req.ifr__name, "ppp%d", PPP_UNIT);

    if (ioctl(ppp_h, SIOCGPPPSTATS, &req) < 0) {
        /* fprintf(stderr, "heyho!\n") */;
    }
    *cur = req.stats;
}

#define LED_SZE_X (4)
#define LED_SZE_Y (4)

#define LED_ON_NET_X (87)
#define LED_ON_NET_Y (66)
#define LED_OFF_NET_X (93)
#define LED_OFF_NET_Y (66)
#define LED_ERR_NET_X (81)
#define LED_ERR_NET_Y (66)
#define LED_ON_SW_NET_X (49)
#define LED_ON_SW_NET_Y (85)
#define LED_OFF_SW_NET_X (44)
#define LED_OFF_SW_NET_Y (85)

#define LED_PWR_X (53)
#define LED_PWR_Y (7)
#define LED_SND_X (47)
#define LED_SND_Y (7)
#define LED_RCV_X (41)
#define LED_RCV_Y (7)

#define LED_SW_X (38)
#define LED_SW_Y (14)

/*******************************************************************************\
|* SetOnLED                                                                                                                                *|
\*******************************************************************************/
void SetOnLED(int led)
{

    switch (led) {

    case LED_NET_RX:
        da_copy_xpm_area(LED_ON_NET_X, LED_ON_NET_Y, LED_SZE_X, LED_SZE_Y,  LED_RCV_X, LED_RCV_Y);
        break;
    case LED_NET_TX:
        da_copy_xpm_area(LED_ON_NET_X, LED_ON_NET_Y, LED_SZE_X, LED_SZE_Y,  LED_SND_X, LED_SND_Y);
        break;
    case LED_NET_POWER:
        da_copy_xpm_area(LED_ON_NET_X, LED_ON_NET_Y, LED_SZE_X, LED_SZE_Y,  LED_PWR_X, LED_PWR_Y);
        break;
    }
}

/*******************************************************************************\
|* SetOffLED                                                                                                                               *|
\*******************************************************************************/
void SetOffLED(int led)
{

    switch (led) {

    case LED_NET_RX:
        da_copy_xpm_area(LED_OFF_NET_X, LED_OFF_NET_Y, LED_SZE_X, LED_SZE_Y,  LED_RCV_X, LED_RCV_Y);
        break;
    case LED_NET_TX:
        da_copy_xpm_area(LED_OFF_NET_X, LED_OFF_NET_Y, LED_SZE_X, LED_SZE_Y,  LED_SND_X, LED_SND_Y);
        break;
    case LED_NET_POWER:
        da_copy_xpm_area(LED_OFF_NET_X, LED_OFF_NET_Y, LED_SZE_X, LED_SZE_Y,  LED_PWR_X, LED_PWR_Y);
        break;
    }
}

/*******************************************************************************\
|* SetErrLED                                                                                                                               *|
\*******************************************************************************/
void SetErrLED(int led)
{

    switch (led) {
    case LED_NET_POWER:
        da_copy_xpm_area(LED_ERR_NET_X, LED_ERR_NET_Y, LED_SZE_X, LED_SZE_Y,  LED_PWR_X, LED_PWR_Y);
        break;
    }
}
