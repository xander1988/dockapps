/*
 *  WMBlueMem - a memory monitor
 *
 *  Copyright (C) 2003 Draghicioiu Mihai Andrei <misuceldestept@go.ro>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/select.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "mem.h"
#include "mwm.h"
#include "menu.h"

#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define OPT_MILISECS 1000

long long int oldmem = -1, oldswp = -1;

int redraw = 1;

struct timeval tv = {0, 0};

menu_t *m;

int opt_milisecs;

char *opt_bgcolor;
char *opt_offcolor;
char *opt_oncolor;

bool opt_buffers;
bool opt_cache;

void draw_graph(int, int, int, int, int);
void proc(void);
void draw_window(void);
void process_events(void);
void routine(int, char **);
void set_buffers(menuitem_t *);
void set_cache(menuitem_t *);

void process_events(void) {
    /* Process any pending X events */
    switch (da_watch_xevent()) {
    case MOUSE_2_PRS:
        exit(0);
        break;
        
    case MOUSE_3_PRS:
        if (menu_pop(m) == 0)
            exit(0);
        break;
    }
}

void draw_graph(int x, int y, int width, int height, int val) {
    da_set_foreground(2);
    
    da_fill_rectangle(x, y, width, height);
    
    da_set_foreground(0);
    
    da_fill_rectangle(x + 1, y + 1, width - 2, height - 2);
    
    if(val > 0) {
        da_set_foreground(1);
        
        da_fill_rectangle(x + 2, y + height - val + 1, width - 4, val - 3);
        
        if(val >= 3) {
            da_set_foreground(2);
            
            da_fill_rectangle(x + 3, y + height - val + 2, width - 6, val - 5);
        }
        
        if(val >= 9) {
            da_set_foreground(1);
            
            da_fill_rectangle(x + 4, y + height - val + 3, 3, val - 7);
            da_fill_rectangle(x + 8, y + height - val + 3, 3, val - 7);
            da_fill_rectangle(x + 12, y + height - val + 3, 3, val - 7);
        }
    }
}

void draw_window(void) {
    long long int mem, swp;
    
    int mem_graph, swp_graph;

    mem = mem_used;
    
    if(!opt_buffers) 
        mem -= mem_buffers;
    
    if(!opt_cache) 
        mem -= mem_cached;
    
    swp = swp_used;
    
    mem_graph = mem * 54 / mem_total;
    
    swp_graph = (swp_total > 0) ? swp * 54 / swp_total : 0;
    
    if(mem_graph != oldmem) 
        draw_graph(19, 5, 19, 54, mem_graph);
    
    if(swp_graph != oldswp) 
        draw_graph(40, 5, 19, 54, swp_graph);
    
    redraw = 0;
    
    if(mem_graph != oldmem || swp_graph != oldswp) 
        redraw = 1;
    
    oldmem = mem_graph;
    oldswp = swp_graph;
}

void proc(void) {
    int i;
    
    fd_set fs;
    
    int fd = ConnectionNumber(da_display);

    process_events();
    
    FD_ZERO(&fs); 
    FD_SET(fd, &fs);
    
    i = select(fd + 1, &fs, 0, 0, &tv);
    
    if(i == -1) {
        fprintf(stderr, "Error with select(): %s", strerror(errno));
        exit(1);
    }
    
    if(!i) {
        mem_getusage();
        
        draw_window();
        
        if(redraw) 
            da_redraw_window();
        
        tv.tv_sec = opt_milisecs / 1000;
        tv.tv_usec = (opt_milisecs % 1000) * 1000;
    }
}

void set_buffers(menuitem_t *i) {
    opt_buffers = i->checked;
    
    tv.tv_sec = 0;
    tv.tv_usec = 0;
}

void set_cache(menuitem_t *i) {
    opt_cache = i->checked;
    
    tv.tv_sec = 0;
    tv.tv_usec = 0;
}

void routine(int argc, char *argv[]) {
    menuitem_t *i;

    da_init_xwindow();

    da_add_color(opt_bgcolor, "bg_color");
    da_add_color(opt_oncolor, "on_color");
    da_add_color(opt_offcolor, "off_color");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
    }

    menu_init(da_display);
    
    m = menu_new();

    i = menu_append(m, "Buffers");
    i->i = -1; 
    i->checked = opt_buffers; 
    i->callback = set_buffers;

    i = menu_append(m, "Cache");
    i->i = -1; 
    i->checked = opt_cache; 
    i->callback = set_cache;

    menu_append(m, "Exit");

    mem_init();
    
    while (!da_conf_changed(argc, argv))
        proc();
    
    routine(argc, argv);
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a memory monitor.\n\n"

    "The left is physical memory and the right gauge is swap space. Right-click for a menu.");

    da_init_string(&opt_bgcolor, "bg_color", "-B", "--bgcolor", "#000000", "Background color");
    da_init_string(&opt_oncolor, "on_color", "-N", "--oncolor", "#87d7ff", "Color for On leds");
    da_init_string(&opt_offcolor, "off_color", "-F", "--offcolor", "#0095e0", "Color for Off leds");
    da_init_bool(&opt_buffers, "buffers", "-b", "--buffers", false, "Consider the buffers memory");
    da_init_bool(&opt_cache, "cache", "-c", "--cache", false, "Consider the cache");
    da_init_integer(&opt_milisecs, "milisecs", "-m", "--milisecs", 0, 0, OPT_MILISECS, "The number of milisecs between updates");   
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}
