#!/bin/bash

if [[ ! $1 ]]; then
    cp wmweather+ /usr/local/bin/wmweather+;
elif [[ $1 == "--uninstall" ]]; then
    rm --interactive=never /usr/local/bin/wmweather+;
fi
