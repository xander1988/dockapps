/*
    Copyright (C) 2022 Xander

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <libdockapp4/dockapp.h>

#include "wireless.h"
#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define SIZE 310

char *front_color;
char *back_color;
char *border_dark;
char *border_light;

bool scroll;

int interval;

void routine(int, char **);
void draw_stats(struct wifi *);
void rotate_text(struct wifi *);
void draw_text(char *, int, int);
void draw_small_text(char *, int, int);
void set_sig_led(int);
void clear_wifi(struct wifi *);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp to monitor wireless signal strength.\n\n"

    PROG_NAME" is a dockapp that is supported by X window managers such as Window Maker, AfterStep, BlackBox, and Enlightenment. It monitors all wireless network interfaces on your machine.\n\n"

    "The screen displays the link (signal) strength between the currently selected card and your wireless access point as a percentage (also displayed by the indicator on the right), the current rate/speed in megabits, the noise (in dBm), the level (in dBm), and the actual link amount.\n\n"

    "The screen toggles the interface name and the current associated access point at the top every 5 seconds. Mouse1 switches the current wireless network interface.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#51c300", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&scroll, "scroll", NULL, "--scroll", false, "Scroll interface name and essid");
    //da_init_integer(&interval, "interval", "-i", "--interval", 1, 60, 1, "Number of secs between updates");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc"); 
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    struct wifi wf;

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 75, back_color, 25, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;
    }

    da_add_mouse_region(0, 5, 5, 48, 14);

    while (!da_conf_changed(argc, argv)) {
        waitpid(0, NULL, WNOHANG);
        
        get_wifi_info(&wf);
        
        //if (da_timeout(interval))
        draw_stats(&wf);
        
        da_redraw_window();

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch (da_check_mouse_region()) {
            case 0:
                //clear_wifi(&wf);
                next_if(&wf);
                
                break;
            }
            
            break;
        }
                
        usleep(200000);
    }
    
    routine(argc, argv);
}

void draw_text(char *text, int dx, int dy) {
    size_t i;
    
    int k, c;

    /* Clear area */
    //da_copy_xpm_area(70, 64, 54, 10, 5, 5);

    k = dx;
    
    for (i = 0; i < strlen(text); i++) {
        c = toupper(text[i]);
        
        if (c == ':') {
            da_copy_xpm_area(162, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == '.') {
            da_copy_xpm_area(155, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == '-') {
            da_copy_xpm_area(148, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == '_') {
            da_copy_xpm_area(141, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == ' ') {
            da_copy_xpm_area(134, 0, 7, 9, k, dy);
            
            k += 7;
           
        } else if (c >= 'A' && c <= 'Z') {
            c -= 'A';
            
            da_copy_xpm_area(64 + c * 7, 10, 7, 9, k, dy);
            
            k += 7;
        
        } else {
            c -= '0';
            
            da_copy_xpm_area(64 + c * 7, 0, 7, 9, k, dy);
            
            k += 7;
        }
    }
}

void draw_small_text(char *text, int dx, int dy) {
    size_t i;
    
    int k, c;

    /* Clear area */
    //da_copy_xpm_area(70, 64, 54, 10, 5, 5);

    k = dx;
    
    for (i = 0; i < strlen(text); i++) {
        c = toupper(text[i]);
        
        if (c == '%') {
            da_copy_xpm_area(188, 29, 8, 8, k, dy);
            
            k += 8;
            
        } else if (c == ':') {
            da_copy_xpm_area(184, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == '.') {
            da_copy_xpm_area(180, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == '-') {
            da_copy_xpm_area(176, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == '_') {
            da_copy_xpm_area(172, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == ' ') {
            da_copy_xpm_area(168, 29, 4, 8, k, dy);
            
            k += 4;
                   
        } else if (c >= 'A' && c <= 'Z') {
            c -= 'A';
            
            da_copy_xpm_area(64 + c * 4, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else {
            c -= '0';
            
            da_copy_xpm_area(64 + c * 4, 20, 4, 8, k, dy);
            
            k += 4;
        }
    }
}

void draw_stats(struct wifi *wf) {    
    char buffer[25];
    
    int num = wf->link;
    int u, percent;
    
    double rate = wf->bitrate.value;

    if (num < 0)
        num = 0;

    /* Calculate Link percentage */
    percent = num / (wf->max_qual / 100);
    num = percent;    
    
    set_sig_led(num);
    
    rotate_text(wf);
    
    /* Clear */
    for (u = 0; u < 5; u++)
        da_copy_xpm_area(105, 20, 54, 8, 5, 19 + 8 * u);

    /* Signal prc */
    sprintf(buffer, "SIGNAL: %03d%%", num);
    
    draw_small_text(buffer, 5, 19);

    /* Link */
    sprintf(buffer, "LINK: %.f", wf->link);
    
    draw_small_text(buffer, 5, 19 + 8);
    
    /* Level */
    sprintf(buffer, "LEVEL: %d", wf->level - 0x100);
    
    draw_small_text(buffer, 5, 19 + 8 * 2);
    
    /* Noise */
    sprintf(buffer, "NOISE: %d", wf->noise - 0x100);
    
    draw_small_text(buffer, 5, 19 + 8 * 3);
  
    /* Rate */
    if (rate >= GIGA) {
        sprintf(buffer, "RATE: %.fg", rate / GIGA);
        
        draw_small_text(buffer, 5, 19 + 8 * 4);
    
    } else {
        if (rate >= MEGA) {
            sprintf(buffer, "RATE: %.fm", (rate / MEGA));
            
            draw_small_text(buffer, 5, 19 + 8 * 4);
        
        } else {
            sprintf(buffer, "RATE: %.fk", rate / KILO);
            
            draw_small_text(buffer, 5, 19 + 8 * 4);
        }
    }
}

void rotate_text(struct wifi *wf) {
    char str[SIZE] = "";
    char shortened[7] = "";
    
    static int counter = 20;
    static int pos = 0;
    static int pos_0, pos_1, pos_2, pos_3, pos_4, pos_5;
    
    /* Clear */
    //da_copy_xpm_area(170, 0, 43, 10, 5, 5);
    
    if (!scroll) {
        if (counter >= 10) {
            snprintf(shortened, 7, wf->ifname);            
        } else {
            snprintf(shortened, 7, wf->essid);            
        }
        
        draw_text(shortened, 5, 5);
        
        counter--;
        
        if (counter <= 0)
            counter = 20;
    
    } else {
        sprintf(str, "      %s - %s", wf->ifname, wf->essid);
         
        if (counter % 2) {
            pos_0 = pos;
            pos_1 = pos + 1;
            pos_2 = pos + 2;
            pos_3 = pos + 3;
            pos_4 = pos + 4;
            pos_5 = pos + 5;
            
            shortened[5] = str[pos_5] ? str[pos_5] : ' ';
            shortened[4] = str[pos_4] ? str[pos_4] : ' ';
            shortened[3] = str[pos_3] ? str[pos_3] : ' ';
            shortened[2] = str[pos_2] ? str[pos_2] : ' ';
            shortened[1] = str[pos_1] ? str[pos_1] : ' ';
            shortened[0] = str[pos_0] ? str[pos_0] : ' ';
                
            pos++;

            if (pos >= (int)strlen(str))
                pos = 0;
            
            draw_text(shortened, 5, 5);
        }
        
        counter--;
        
        if (counter <= 0)
            counter = 20;
    }
}

void set_sig_led(int num) {
    if (num >= 0) {
        da_copy_xpm_area(64, 38, 5, 6, 53, 7);
    }
    
    if (num > 20) {
        da_copy_xpm_area(69, 38, 5, 6, 53, 7);
    }
    
    if (num > 40) {
        da_copy_xpm_area(74, 38, 5, 6, 53, 7);
    }
    
    if (num > 60) {
        da_copy_xpm_area(79, 38, 5, 6, 53, 7);
    }
    
    if (num > 80) {
        da_copy_xpm_area(84, 38, 5, 6, 53, 7);
    }
}

void clear_wifi(struct wifi *wf) {
    memset(&wf->ifname, 0, 255);
    memset(&wf->essid, 0, IW_ESSID_MAX_SIZE + 1);
    wf->ifnum = 0;
    wf->link = (float)0;
    wf->level = 0;
    wf->noise = 0 - 0x100;
    wf->bitrate.value = 0;
}
