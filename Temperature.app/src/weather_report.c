#include "weather_report.h"

void parse_report(FILE *file, char **r_time, char **r_temp) {
    char *line;

    char time_tmp[5] = "\0\0\0\0\0";
    char temp_tmp[5] = "\0\0\0\0\0";

    size_t len = 0;
    
    ssize_t read;

    int i, linenum = 0;

    while ((read = getline(&line, &len, file)) != -1) {
        linenum++;
        
        for (i = 0; ; i++) {
            if (line[i] == '\0') {
                break;
            }

            /* time */
            if (linenum   == 2   &&
                line[i]   == 'U' &&
                line[i+1] == 'T' &&
                line[i+2] == 'C') {
                time_tmp[0] = line[i-5];
                time_tmp[1] = line[i-4];
                time_tmp[2] = line[i-3];
                time_tmp[3] = line[i-2];
            }

            /* temperature */
            if (linenum   != 1   &&
                line[i]   == 'T' &&
                line[i+1] == 'e' &&
                line[i+2] == 'm' &&
                line[i+3] == 'p') {
                temp_tmp[0] = line[i+13];
                temp_tmp[1] = line[i+14];
                if (line[i+15] != ' ')
                temp_tmp[2] = line[i+15];
            }
        }
    }

    if (r_time) {
        *r_time = malloc(strlen(time_tmp) + 1);

        sprintf(*r_time, time_tmp);
    }

    if (r_temp) {
        *r_temp = malloc(strlen(temp_tmp) + 1);

        sprintf(*r_temp, temp_tmp);
    }

    if (line) {
        free(line);

        line = NULL;
    }
}
