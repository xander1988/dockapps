/*
 *  Temperature.app
 *
 *  Copyright (c) 2000 Per Liden
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1307,
 *  USA.
 */

#include <math.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "weather_report.h"

#include "bitmaps/master_big.xpm"
#include "bitmaps/master_big2.xpm"
#include "bitmaps/master_big3.xpm"
#include "bitmaps/master_big4.xpm"

#define WGET_CMD "wget --cache=off --tries=0 %s -O %s %s"
#define REPORT "/tmp/%s"
#define UPDATE_INTERVAL         900
#define TIMEOUT                 40
#define TIME_POS                20
#define TEMP_POS                20
#define TEMP_WITH_TIME_POS      33
#define TIME_FONT               26
#define AMPM_FONT               36
#define TEMP_FONT               0
#define UNIT_FONT               16

char *border_dark;
char *border_light;
char *station_id;
char *time_format;
char *r_time;
char *r_temp;

bool fahrenheit;
bool pm;
bool verbose;
bool show_time = false;
bool time_12_hour_format = false;

char wget_cmd[256];
char url[100];
char report[10];
char mTemperature[20];
char mTime[20];

double mTimeDiff;

struct stat filestat;

/* FOR FUTURE USE: time_t last_time;*/

void routine(int, char **);
void draw_text(char *, int, int);
void showLed(bool);
void calcTimeDiff(void);
void setTime(char *);
bool updateTemperture(void);
int file_size(char *);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a Window Maker dock application which fetches local temperature information every 15 minutes from https://tgftp.nws.noaa.gov and displays it (in Celsius or Fahrenheit).\n\n"

    "If the led is blinking (green/red), then that means Temperature.app is trying to fetch weather information. If successful the led will stop blinking and go green. If unsuccessful the led will stop blinking and go red. Also, a error message will be printed in the console, describing what went wrong. If the error message is \"wget failed\" and you can't figure out why, try using option -V, which will cause wget to be run in verbose mode.");

    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&station_id, "station", "-S", "--station", NULL, "Set station id (ICAO Location Indicator)");
    da_init_string(&time_format, "time_format", "-t", "--time-format", NULL, "Display time of temperature observation (format is 12, 24, or nothing to disable this feature)");
    da_init_bool(&fahrenheit, "fahrenheit", "-f", "--fahrenheit", false, "Display degrees in Fahrenheit");
    da_init_bool(&verbose, "verbose", "-V", "--verbose", false, "Display verbose messages from wget");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                       /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/temperatureapp.conf");  /* modern conf path */
    da_init_conf_file(LOCAL, ".temperatureapprc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    if (!station_id) {
        printf("You must supply a station id using -S <id>\n");
        da_log(ERR, "you must supply a station id using -S <id>\n");
        exit(0);
    } else {
        station_id = da_str_to_upper(station_id);
    }

    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master_big4_xpm, NULL, 192 * da_scale, 64 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master_big3_xpm, NULL, 192 * da_scale, 64 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master_big2_xpm, NULL, 192 * da_scale, 64 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_big_xpm, NULL, 192 * da_scale, 64 * da_scale);
        break;
    }

    if (!time_format) {
        show_time = false;
    
    } else {
        if (!strcmp(time_format, "12")) {
            time_12_hour_format = true;
            show_time = true;
        
        } else if (!strcmp(time_format, "24")) {
            time_12_hour_format = false;
            show_time = true;
        
        } else {
            printf("Unknown time format, use 12 or 24.\n");
            da_log(WARN, "unknown time format, use 12 or 24.\n");
            show_time = false;
        }
    }

    if (show_time) {
        calcTimeDiff();
    }

    da_redraw_window();

    const char *mverbose = (verbose ? "--verbose" : "--quiet");

    sprintf(report, REPORT, station_id);

    sprintf(url, METAR_URL, station_id);

    sprintf(wget_cmd, WGET_CMD, mverbose, report, url);

    int counter = 0;
    int timeout = 0;

	while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            /* TODO: do something */
            break;
        }

        if (counter <= 0) {
            counter = UPDATE_INTERVAL;

            showLed(true);

            da_redraw_window();

            remove(report);

            da_exec_command(wget_cmd);

            bool toggle = true;

            timeout = TIMEOUT;

            FILE *file;

            while (1) {
                timeout--;

                if (file_size(report) > 1) {
                    file = fopen(report, "r");
                    
                    parse_report(file, &r_time, &r_temp);
                    
                    if (updateTemperture()) {
                        showLed(false);

                        da_redraw_window();

                        fclose(file);

                        break;
                    }

                    fclose(file);
                }

                if (timeout <= 0) {
                    break;
                }
                
                showLed(toggle);

                toggle ^= true;

                da_redraw_window();
                
                usleep(300000);
            }

        } else {
            counter--;

            da_redraw_window();
            
            sleep(1);
        }
    }

    routine(argc, argv);
}

void showLed(bool show) {
    if (show) {
        da_copy_xpm_area(64, 59, 3, 2, 57, 59);
    } else {
        da_copy_xpm_area(67, 59, 3, 2, 57, 59);
    }
}

void calcTimeDiff() {
    struct tm* t;

    double localTime;
    double universalTime;

    time_t currentTime;

    currentTime = time(0);

    t = gmtime(&currentTime);

    universalTime = (double)t->tm_hour + (double)t->tm_min / 60.0 + (double)t->tm_sec / 3600.0;

    currentTime = time(0);

    t = localtime(&currentTime);

    localTime = (double)t->tm_hour + (double)t->tm_min / 60.0 + (double)t->tm_sec / 3600.0;

    mTimeDiff = universalTime - localTime;

    if (mTimeDiff > 24.0) {
        mTimeDiff -= 24.0;
    } else if (mTimeDiff < 0.0) {
        mTimeDiff += 24.0;
    }
}

void setTime(char *utcTime) {
    char unit[3];

    int hour = 0;
    int min = 0;

    strncpy(unit, &utcTime[0], 2);

    hour = atoi(unit);

    strncpy(unit, &utcTime[2], 2);

    min = atoi(unit);

    double time = ((double)hour + (double)min / 60.0) - mTimeDiff;

    if (time < 0.0) {
        time += 24.0;
    } else if (time > 24.0) {
        time -= 24.0;
    }

    hour = (int)time;
    min = (int)((time - (double)hour) * 60.0 + 0.5);

    if (min >= 60){
        min = 0;

        if (++hour >= 24) {
            hour = 0;
        }
    }

    if (time_12_hour_format) {
        if (hour >= 0 && hour <= 11) {
            pm = false;
        } else {
            pm = true;
        }

        if (hour == 0) {
            hour = 12;
        } else if (hour > 12) {
            hour -= 12;
        }
    }

    sprintf(mTime, "%d:%.2d", hour, min);
}

bool updateTemperture() {
    if (show_time) {
        setTime(r_time);
    }

    if (r_time) {
        free(r_time);

        r_time = NULL;
    }

    if (!r_temp) {
        printf("Could not fetch temperature (unknown file format).\n");
        da_log(WARN, "could not fetch temperature (unknown file format).\n");

        return false;
    }

    if (fahrenheit) {
        strcpy(mTemperature, r_temp);

    } else {
        sprintf(mTemperature, "%d", (int)rint((atoi(r_temp) - 32) / 1.8));
    }

    if (r_temp) {
        free(r_temp);

        r_temp = NULL;
    }

    /* Clear plotting area */
    da_copy_xpm_area(138, 15, 54, 49, 5, 5);

    if (show_time) {
        draw_text(mTime, TIME_FONT, TIME_POS);
        draw_text(mTemperature, TEMP_FONT, TEMP_WITH_TIME_POS);
    } else {
        draw_text(mTemperature, TEMP_FONT, TEMP_POS);
    }

    return true;
}

void draw_text(char *text, int font, int dest) {
    int x_dest;
    int c;
    int n;
    int m = 0;
    int text_len;
    int add_len = 0;

    text_len = strlen(text);

    /* time */
    if (font == TIME_FONT) {
        if (time_12_hour_format) {
            add_len = 2;
        }

        x_dest = (64 / 2) - (((text_len + add_len) * 6) / 2);

        for (n = 0; n < text_len; n++) {
            if (text[n] == ':') {
                da_copy_xpm_area_with_trans(64 + (10 * 6), TIME_FONT, 6, 9, x_dest + m, dest);
            } else {
                c = (int)text[n] - 48;
                
                da_copy_xpm_area_with_trans(64 + (c * 6), TIME_FONT, 6, 9, x_dest + m, dest);
            }

            m += 6;
        }

        /* AM/PM */
        if (time_12_hour_format) {
            if (pm) {
                da_copy_xpm_area_with_trans(64, 36, 14, 9, x_dest + m, dest);
            } else {
                da_copy_xpm_area_with_trans(82, 36, 14, 9, x_dest + m, dest);
            }
        }
    }

    /* temperature */
    if (font == TEMP_FONT) {
        x_dest = (64 / 2) - (((text_len + 2) * 11) / 2);

        for (n = 0; n < text_len; n++) {
            if (text[n] == '-') {
                da_copy_xpm_area_with_trans(64 + (10 * 11), TEMP_FONT, 11, 15, x_dest + m, dest);
            } else {
                c = (int)text[n] - 48;
                
                da_copy_xpm_area_with_trans(64 + (c * 11), TEMP_FONT, 11, 15, x_dest + m, dest);
            }

            m += 11;
        }

        /* Unit */
        if (fahrenheit) {
            da_copy_xpm_area_with_trans(85, 16, 15, 10, x_dest + m, dest);
        } else {
            da_copy_xpm_area_with_trans(64, 16, 15, 10, x_dest + m, dest);
        }
    }
}

int file_size(char *file_name) {
    if (stat(file_name, &filestat) != 0) {
        return 0;
    }

    return filestat.st_size;
}
