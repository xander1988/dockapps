/*
 *
 *      wmSpaceWeather-1.04 (C) 1998 Mike Henderson (mghenderson@lanl.gov)
 * 
 *          - Its a Space Weather Monitor
 * 
 * 
 * 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program (see the file COPYING); if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA
 *
 *      ToDo:
 *
 *  - The whole NOAA space weather www site is pretty screwed up! I currently have
 *    to grab data from 2 separate files to get all that I need. But it seems that
 *        sometimes one of the files shows less than it should. This seems to be related to
 *        the way they update the 2 separate files... I will have to find some way of
 *        making that more robust.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *      Changes:
 *
 *  Version 1.04 - released Feb 18, 1999
 *          Added double click capability. Double clicking on mouse button 1 sends 
 *          URL (defined via ne command line option -url) to netscape.
 *
 *  Version 1.03 - released Feb 11, 1999
 *             Changed display a bit. When no data is
 *             available, it now shows nothing (before it would
 *             show false junk...). Modified Perl Script GetKp.
 *
 *
 *  Version 1.02 - released Feb 8, 1999
 *             bug fixes...
 *
 *  Version 1.0b - released Dec 19, 1998
 *
 *
 */

/*  
 *   Includes  
 */
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "XPM/wmSpaceWeather_master.xpm"
#include "XPM/wmSpaceWeather_master2.xpm"
#include "XPM/wmSpaceWeather_master3.xpm"
#include "XPM/wmSpaceWeather_master4.xpm"
#include "XPM/wmSpaceWeather_mask.xbm"
#include "XPM/wmSpaceWeather_mask2.xbm"
#include "XPM/wmSpaceWeather_mask3.xbm"
#include "XPM/wmSpaceWeather_mask4.xbm"

/* 
 *  Delay between refreshes (in microseconds) 
 */
#define DELAY 10000L

int ForceUpdate2;

char *back_color;
char *border_dark;
char *border_light;
char *command;

void routine(int, char **);
double jd(int, int, int, double);

/*  
 *   main  
 */
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a space weather monitor.\n\n"

    PROG_NAME" is a space weather monitor. The monitor shows: 2 relativistic electron and 3 relativistic proton flux levels at geosyncronous orbit (currently from the NOAA GOES spacecraft), current Solar Flare X-ray flux, and the last 8 3-hour Kp index values.\n\n"

    "Large fluxes of relativistic (aka `Killer') electrons can cause harmful `deep-dielectric' charging/discharging events inside satellites. High proton fluxes (`Solar Energetic Proton' or SEP events) can cause single event upsets in satellite electronics. The Kp index is a measure of how disturbed the Earth's magnetic field has been over a 3-hour interval. Kp can range from 0-9, where 0 is quiet and 9 is very disturbed. The Kp values shown are the 8 previous values (i.e. the last 24 hours). If the display is up-to-date, the LED next to the Kp bars will flash blue. If the display is not up-to-date, the LED will flash orange.\n\n"

    PROG_NAME" can launch an external command via left mouse button click, which in earlier versions was an url passed to netscape. Right mouse button forces immediate update.");
    
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&command, "command", "-c", "--command", NULL, "Command to execute with mouse Button1 click");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                 /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/wmspaceweather.conf");
    da_init_conf_file(LOCAL, ".wmspaceweatherrc");

    da_parse_all(argc, argv);
    
    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    struct tm *Time;
    
    int         i, n, s, k, m, dt1, dt2;
    int         Year, Month, Day;
    int         Hours, Mins, Secs;
    long        CurrentLocalTime;
    int         height, UpToDate = 0, LEDOn;
    double      UT, hour24();

    double      CurrentJD, LatestAvailJD, tim, DeltaT;
    long int        TimeTag[8];
    int         Kp[8] = { -1, -1, -1, -1, -1, -1, -1, -1 };
    double      E1, E2, P1, P2, P3;
    char        Xray[10], digit[2];
    FILE        *fp;

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, wmSpaceWeather_master4_xpm, wmSpaceWeather_mask4_bits, wmSpaceWeather_mask4_width, wmSpaceWeather_mask4_height);
        break;
       
    case 3:
        da_open_xwindow(argc, argv, wmSpaceWeather_master3_xpm, wmSpaceWeather_mask3_bits, wmSpaceWeather_mask3_width, wmSpaceWeather_mask3_height);
        break;
    
    case 2:
        da_open_xwindow(argc, argv, wmSpaceWeather_master2_xpm, wmSpaceWeather_mask2_bits, wmSpaceWeather_mask2_width, wmSpaceWeather_mask2_height);
        break;
    
    case 1:
    default:
        da_open_xwindow(argc, argv, wmSpaceWeather_master_xpm, wmSpaceWeather_mask_bits, wmSpaceWeather_mask_width, wmSpaceWeather_mask_height);
        break;
    }

    /*
     *  Loop until we die
     */
    n = 32000;
    s = 32000;
    m = 32000;
    dt1 = 32000;
    dt2 = 32000;
    LEDOn = 0;
    ForceUpdate2 = 1;

    while (!da_conf_changed(argc, argv)) {
        /*
         *  Keep track of # of seconds
         */
        if (m > 100) {
            m = 0;
            ++dt1;
            ++dt2;
    
        } else {
            /*
             *  Increment counter
             */
            ++m;
        }

        /* 
         *   Process any pending X events.
         */
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (command) {
                da_exec_command(command);
            }
            break;
            
        case MOUSE_3_PRS:
            ForceUpdate2 = 1;
            break;
        }
    
        /* 
         *  Redraw 
         */
        da_redraw_window();

        /*
         *  Check the Kp file every (approx.) 2 seconds.
         *  Can significantly reduce this frequency later. But its
         *  easier to debug this way...
         *  Do this before trying to download again! The file may be there and it
         *  may be Up-To-Date!
         */
        if (dt2 > 2) {
            dt2 = 0;

            /*
             *  Compute Current Julian Date
             */
            CurrentLocalTime = time(CurrentTime);
            Time = gmtime(&CurrentLocalTime);
            Year  = Time->tm_year+1900;
            Month = Time->tm_mon+1;
            Day   = Time->tm_mday;
            Hours = Time->tm_hour;
            Mins  = Time->tm_min;
            Secs  = Time->tm_sec;
            UT = (double)Hours + (double)Mins/60.0 + (double)Secs/3600.0;
            CurrentJD = jd(Year, Month, Day, UT);
        
            /*
             *  Read in Kp values
             */
            if ((fp = fopen("/tmp/LatestKp.txt", "r")) != NULL) {
                for (i = 0; i < 8; ++i) {
                    fscanf(fp, "%ld %d", &TimeTag[i], &Kp[i]);

                    if (Kp[i] < 0)
                        TimeTag[i] = 190001011;
                }
                
                fscanf(fp, "%lf", &P1);
                fscanf(fp, "%lf", &P2);
                fscanf(fp, "%lf", &P3);
                fscanf(fp, "%lf", &E1);
                fscanf(fp, "%lf", &E2);
                fscanf(fp, "%10s",  Xray);
                fclose(fp);

            } else {
                for (i = 0; i < 8; ++i) {
                    Kp[i] = -1;

                    TimeTag[i] = 190001011;
                }
            }

            /*
             *  Compute Julian Date for latest available Kp
             */
            tim = TimeTag[7];
            Year = tim/100000;
            tim -= Year*100000;
            Month = tim/1000;
            tim -= Month*1000;
            Day = tim/10;
            tim -= Day*10;
            UT = tim*3.0;
            LatestAvailJD = jd(Year, Month, Day, UT);

            DeltaT = (CurrentJD - LatestAvailJD)*24.0;
            UpToDate = (DeltaT <= 3.0) ? 1 : 0;

            if (!UpToDate) {
                /*
                 * shift data back
                 */
                k = (int)(DeltaT/3.0);

                if ((k >= 0) && (k <= 7)) {
                    for (i = 0; i < 8 - k; ++i)
                        Kp[i] = Kp[i+k];
                    
                    for (i = 8 - k; i < 8; ++i)
                        Kp[i] = -1;
                }
            }
        } 

        /*
         *  Update Kp Bars etc...
         */
        if (n > 200) {
            n = 0;

            da_copy_xpm_area(5, 67, 47, 20, 5, 39);
    
            for (i = 0; i < 8; ++i) {
                if ((Kp[i] >= 0) && (Kp[i] <= 9)) {
                    height = 2 * Kp[i] + 1;
                
                    da_copy_xpm_area(53, 86-height+1, 5, height, 5+5*i+i, 58-height+1);
                }
            }

            /*
             *  Update Xray display...
             */
            if (Xray[0] != 'Z') {
                switch (Xray[0]) {
                case 'B':
                    da_copy_xpm_area(66, 17, 5, 7, 37, 25);
                    break;
                case 'C':
                    da_copy_xpm_area(72, 17, 5, 7, 37, 25);
                    break;
                case 'M':
                    da_copy_xpm_area(78, 17, 5, 7, 37, 25);
                    break;
                case 'X':
                    da_copy_xpm_area(84, 17, 5, 7, 37, 25);
                    break;
                }

                digit[0] = Xray[1];
                digit[1] = '\0';

                da_copy_xpm_area(atoi(digit)*6+66, 25, 5, 7, 43, 25);
                da_copy_xpm_area(127, 30, 3, 3, 49, 30);

                digit[0] = Xray[3];
                digit[1] = '\0';

                da_copy_xpm_area(atoi(digit)*6+66, 25, 5, 7, 53, 25);
            }

            /*
             *  Update E1 LED...
             */
            if ((E1 > 0)&&(E1 < 1e6))
                da_copy_xpm_area(66, 12, 4, 4, 25, 7);
            else if ((E1 >= 1e6)&&(E1 < 1e7))
                da_copy_xpm_area(66,  7, 4, 4, 25, 7);
            else if (E1 > 1e7)
                da_copy_xpm_area(66,  2, 4, 4, 25, 7);

            /*
             *  Update E2 LED...
             */
            if ((E2 > 0)&&(E2 < 1e3))
                da_copy_xpm_area(66, 12, 4, 4, 31, 7);
            else if ((E2 >= 1e3)&&(E2 < 1e4))
                da_copy_xpm_area(66,  7, 4, 4, 31, 7);
            else if (E2 > 1e4)
                da_copy_xpm_area(66,  2, 4, 4, 31, 7);

            /*
             *  Update P1 LED...
             */
            if ((P1 > 0)&&(P1 < 1e2))
                da_copy_xpm_area(66, 12, 4, 4, 22, 16);
            else if ((P1 >= 1e2)&&(P1 < 1e3))
                da_copy_xpm_area(66,  7, 4, 4, 22, 16);
            else if (P1 > 1e3)
                da_copy_xpm_area(66,  2, 4, 4, 22, 16);

            /*
             *  Update P2 LED...
             */
            if ((P2 > 0)&&(P2 < 0.5e0))
                da_copy_xpm_area(66, 12, 4, 4, 28, 16);
            else if ((P2 >= 0.5e0)&&(P2 < 0.5e1))
                da_copy_xpm_area(66,  7, 4, 4, 28, 16);
            else if (P2 > 0.5e1)
                da_copy_xpm_area(66,  2, 4, 4, 28, 16);

            /*
             *  Update P3 LED...
             */
            if ((P3 > 0)&&(P3 < 0.3e0))
                da_copy_xpm_area(66, 12, 4, 4, 34, 16);
            else if ((P3 >= 0.3e0)&&(P3 < 0.3e1))
                da_copy_xpm_area(66,  7, 4, 4, 34, 16);
            else if (P3 > 0.3e1)
                da_copy_xpm_area(66,  2, 4, 4, 34, 16);

        } else {
            /*
             *  Increment counter
             */
            ++n;
        }

        /*
         *  Update the blinking LED which indicates whether or not the
         *  display is up-to-date
         */
        if (s > 20) {
            s = 0;

            if (LEDOn) {
                if (UpToDate)
                    da_copy_xpm_area(65, 82, 4, 4, 54, 53);
                else
                    da_copy_xpm_area(65, 76, 4, 4, 54, 53);

                LEDOn = 0;

            } else { 
                if (UpToDate)
                    da_copy_xpm_area(60, 82, 4, 4, 54, 53);
                else
                    da_copy_xpm_area(60, 76, 4, 4, 54, 53);

                LEDOn = 1;
            }
        } else {
            /*
             *  Increment counter
             */
            ++s;
        }

        /*
         *  Check every 5 min if the values are not up to date...
         */
        if (((!UpToDate) && (dt1 > 300)) || ForceUpdate2) {
            dt1 = 0;

            /*
             *  Execute Perl script to grab the Latest Kp values
             */
            da_exec_command("GetKp");

            ForceUpdate2 = 0;
        }

        /* 
         *  Wait for next update 
         */
        usleep(DELAY);
    }

    routine(argc, argv);
}

/*
 *  Compute the Julian Day number for the given date.
 *  Julian Date is the number of days since noon of Jan 1 4713 B.C.
 */
double jd(int ny, int nm, int nd, double UT) {
    double A, B, C, D, JD, day;

    day = nd + UT/24.0;

    if ((nm == 1) || (nm == 2)){
        ny = ny - 1;
        nm = nm + 12;
    }

    if (((double)ny+nm/12.0+day/365.25)>=(1582.0+10.0/12.0+15.0/365.25)){
        A = ((int)(ny / 100.0));
        B = 2.0 - A + (int)(A/4.0);
    } else {
        B = 0.0;
    }

    if (ny < 0.0){
        C = (int)((365.25*(double)ny) - 0.75);
    } else {
        C = (int)(365.25*(double)ny);
    }

    D = (int)(30.6001*(double)(nm+1));

    JD = B + C + D + day + 1720994.5;

    return JD;
}
