/*****************************************************************************/
/*                                                                           */
/*  wmtz.c                                                                   */
/*  Shows local time in different timezones + JD + Sidereal time +           */
/*  internet time + local date and time.                                     */
/*                                                                           */
/*  Jan Lindblom <99jl@home.se> (http://www.geocities.com/~jl1n/)            */
/*                                                                           */
/*  wmtz.c was derived from:                                                 */
/*                                                                           */
/*   wminet.c                                                                */
/*                                                                           */
/*   Multi-function system monitor                                           */
/*   Dave Clark (clarkd@skynet.ca) (http://www.neotokyo.org/illusion)        */
/*   Martijn Pieterse (pieterse@xs4all.nl) (http://windowmaker.mezaway.org)  */
/*   and Antoine Nulle (warp@xs4all.nl) (http://windowmaker.mezaway.org)     */
/*                                                                           */
/*  This software is licensed through the GNU General Public Licence.        */
/*  Read the COPYING file for details.                                       */
/*                                                                           */
/*****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

/*
 * Defines
 */
#define CHAR_WIDTH 6
#define STRSIZE 100
#define LMST 1
#define GMST 0
#define ABOUT "xmessage -center -buttons \"Close\" \""PROG_NAME" - Window Maker Time Zone dockapp v "PROG_VERSION" http://www.geocities.com/jl1n/wmtz/wmtz.html\""

/*
 * Typedefs
 */
typedef struct {
    char *label;  /* Time zone designation */
    char *utdiff;
    int diff;     /* Time zone diff. from UT in hours */
    double epoch; /* Epoch (for JD) */
    char tz[256]; /* TZ environment variable string */
} timezone_t;     /* ...numbers of days to subtract from JD */

/*
 * Global variables
 */
timezone_t zone[6];

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *defedit;

bool juld;

char *month[12];
char *week_day[7];

double longitude;
double latitude;

static struct tm *clk;

extern char *tzname[2];

static char originalTZ[64];

/*
 * Function declarations
 */
void BlitString(char *name, int x, int y);
void routine(int, char **);
void range(double *val, double ran);
void siderealTime(double jde, int *result, int mode);
double julianDay(int year, int month, double day, double hour, double minute, double second, int julian);
int calendarDate(double jd, int *year, int *month, double *day);
int handleJD(void);
void handleTheMenu(int but_stat);
double jdn(time_t curtime);

/*****************************************************************************\
|* main                                          *|
\*****************************************************************************/
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "shows local time around the world and more.\n\n"

    PROG_NAME" is a Window Maker dock app derived from the WMiNET dock app. It displays the local time from different time zones defined in the configuration file. It can also display the current Julian Day Number as well as Swatch beats and sidereal time at Greenwich and local sidereal time and local time, date and weekday. "PROG_NAME" can also convert between JD and Gregorian dates via a command line interface. If "PROG_NAME" is clicked on with the mouse it shows a menu where the configuration can be changed. It can also be shut down with this menu by clicking on QUIT.\n\n"

    "This app can be useful (?) for people who have to communicate in realtime with people from different time zones. It allows you to avoid making a fool of yourself by calling someone when they are asleep...\n\n"

    "What must be present in the config is the \"time*=<string>\" token for every timeslot (1 to 5), followed by the \"utdiff*=<integer>\" for every timeslot (1 to 5) even if it is to be ignored. \"longitude=<longitude in decimal degrees>\" must also be given. The options available for \"time*=\" are the following:\n\n"

    "time*=*** -- three (or less) letter designation of timezone. utdiff for this timeslot is subtracted from UT to give time in that timezone.\n"
    "time*=JDN gives Julian Day Number. The value for utdiff for the timeslot is subtracted from the current JD, which enables you to show JD from another epoch.\n"
    "time*=GMST gives sidereal time at Greenwich.\n"
    "time*=LMST gives local sidereal time. longitude must be set.\n"
    "time*=empty gives a blank (empty) timeline. utdiff for this timeslot is ignored.\n"
    "time*=@ gives swatch beats (float format). utdiff is ignored.\n"
    "time*=DATE gives local date. utdiff is ignored.\n"
    "time*=WDAY gives local weekday. utdiff is ignored.\n"
    "time*=LOCAL gives local time (usefull to deal with DST), automatically corrected for summer/winter time. utdiff is ignored.\n"
    "time*=TZONE gives the time as specified in TZ string.\n\n"

    "Difference in hours from UT (Greenwich Civil Time) for the time zones above. Negative values are required for timezones to the east of Greenwich. A value of 0 gives UT.\n\n"

    "Automatic correction of daylight saving time changes is supported only by TZONE, WDAY, DATE and LOCAL.\n\n"

    "If TZONE was specified for a time position, you must give the TZ environment variable for the desired time zone in the corresponding utdiff*. See \"man tzset\" for at description of the TZ environment variable string.\n\n"

    "If JDN was specified for a time position, this value is subtracted from the Julian day number, giving days from another epoch. This enables you to see more decimals of the JD if you want to...\n\n"

    "Time differences in minutes is supported only for the TZONE option.\n\n"
    
    "Example config:\n\n"
    
    "time1=JDN\n"
    "time2=JDN\n"
    "time3=GMST\n"
    "time4=JST\n"
    "time5=TZONE\n\n"

    "utdiff1=0\n"
    "# This gives days from 2000-01-01 00:00 UT:\n"
    "utdiff2=2451544.5\n"
    "utdiff3=0\n"
    "utdiff4=-9\n"
    "utdiff5=XST-10XDT1,M10.5,0,M3.5.0");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&juld, "julian_date", "-j", "--julian-date", false, "Invoke JD conversion utility");
    da_init_string(&defedit, "editor", "-e", "--editor", NULL, "Set preferred text editor to use when CONFIG on the menu is choosen (e.g. xemacs or xjed)");
    da_init_float(&latitude, "latitude", "-L", "--latitude", 0.0, 0.0, 0.0, "Observers latitude; positive to the west");
    da_init_float(&longitude, "longitude", "-N", "--longitude", 0.0, 0.0, 0.0, "Observers longitude");
    da_init_string(&zone[1].label, "time1", NULL, "--time1", "empty", "First time zone label");
    da_init_string(&zone[2].label, "time2", NULL, "--time2", "empty", "Second time zone label");
    da_init_string(&zone[3].label, "time3", NULL, "--time3", "empty", "Third time zone label");
    da_init_string(&zone[4].label, "time4", NULL, "--time4", "empty", "Fourth time zone label");
    da_init_string(&zone[5].label, "time5", NULL, "--time5", "empty", "Fifth time zone label");
    da_init_string(&zone[1].utdiff, "utdiff1", NULL, "--utdiff1", "0", "First time difference");
    da_init_string(&zone[2].utdiff, "utdiff2", NULL, "--utdiff2", "0", "Second time difference");
    da_init_string(&zone[3].utdiff, "utdiff3", NULL, "--utdiff3", "0", "Third time difference");
    da_init_string(&zone[4].utdiff, "utdiff4", NULL, "--utdiff4", "0", "Fourth time difference");
    da_init_string(&zone[5].utdiff, "utdiff5", NULL, "--utdiff5", "0", "Fifth time difference");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc");
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    int j = 0, k = 0, hour = 0;
    int sid[2], clicked = 0, but_stat = -1;
    
    double jd = 0;
    
    float swatch_beats;
    
    time_t curtime;
    time_t prevtime;
    
    char *envbuf;
    
    char buf[64];
    char blitstr[STRSIZE];

    /* Store away the TZ environment variable, if set */
    if ( (envbuf = getenv("TZ")) != NULL ) {
        //Write TZ=envbuf into originalTZ
        sprintf(originalTZ, "TZ=%s", envbuf);
    } else {
        // Set originalTZ to TZ erase TZ env.
        sprintf(originalTZ, "TZ");
    }

    if (juld) {
        handleJD();
        exit(0);
    }

    month[0] = "JAN";  month[1] = "FEB";  month[2] = "MAR";
    month[3] = "APR";  month[4] = "MAY";  month[5] = "JUN";
    month[6] = "JUL";  month[7] = "AUG";  month[8] = "SEP";
    month[9] = "OUT";  month[10] = "NOV";  month[11] = "DEC";

    week_day[0] = "SUNDAY   ";
    week_day[1] = "MONDAY   ";
    week_day[2] = "TUESDAY  ";
    week_day[3] = "WEDNESDAY";
    week_day[4] = "THURSDAY ";
    week_day[5] = "FRIDAY   ";
    week_day[6] = "SATURDAY ";

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 160 * da_scale, 100 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 160 * da_scale, 100 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 160 * da_scale, 100 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 160 * da_scale, 100 * da_scale);
        break;
    }

    //da_add_mouse_region(0, 5,  6, 58, 16);
    da_add_mouse_region(0, 5, 16, 58, 26);
    da_add_mouse_region(1, 5, 26, 58, 36);
    da_add_mouse_region(2, 5, 36, 58, 46);
    da_add_mouse_region(3, 5, 46, 58, 56);

    sscanf(zone[1].utdiff, "%d", &zone[1].diff);
    sscanf(zone[2].utdiff, "%d", &zone[2].diff);
    sscanf(zone[3].utdiff, "%d", &zone[3].diff);
    sscanf(zone[4].utdiff, "%d", &zone[4].diff);
    sscanf(zone[5].utdiff, "%d", &zone[5].diff);
    
    sscanf(zone[1].utdiff, "%lf", &zone[1].epoch);
    sscanf(zone[2].utdiff, "%lf", &zone[2].epoch);
    sscanf(zone[3].utdiff, "%lf", &zone[3].epoch);
    sscanf(zone[4].utdiff, "%lf", &zone[4].epoch);
    sscanf(zone[5].utdiff, "%lf", &zone[5].epoch);

    sprintf(zone[1].tz, "TZ=%s", zone[1].utdiff);
    sprintf(zone[2].tz, "TZ=%s", zone[2].utdiff);
    sprintf(zone[3].tz, "TZ=%s", zone[3].utdiff);
    sprintf(zone[4].tz, "TZ=%s", zone[4].utdiff);
    sprintf(zone[5].tz, "TZ=%s", zone[5].utdiff);

    da_redraw_window();
    
    prevtime = time(0) - 1;

    while (!da_conf_changed(argc, argv)) {
        waitpid(0, NULL, WNOHANG);

        /* If wmtz have been mouse-clicked, show menu */
        if ( clicked ) {
            BlitString("MENU:    ", 5, (11*(0)) + 5);
            BlitString(" ABOUT   ", 5, (11*(1)) + 5);
            BlitString(" CONFIG  ", 5, (11*(2)) + 5);
            BlitString("         ", 5, (11*(3)) + 5);
            BlitString(" QUIT    ", 5, (11*(4)) + 5);
        
        } else if ( (curtime = time(0)) > prevtime) {
            prevtime = curtime;

            /* Update the display */
            for (j=1; j<6; j++) {
                /* Display empty line */
                if (strncmp( zone[j].label, "empty", 4) == 0 ) {
                    BlitString("        ", 5, (11*(j-1)) + 5);

                /* Display local day/mon/year */
                } else if (strncmp( zone[j].label, "DATE", 4) == 0 ) {
                    clk = localtime(&curtime);

                    while(clk->tm_year>99)
                        clk->tm_year-=100;

                    snprintf(blitstr, STRSIZE, "%s %02d.%02d", month[clk->tm_mon],clk->tm_mday,clk->tm_year);

                    BlitString(blitstr, 5, (11*(j-1)) + 5);

                /* Display local weekday */
                } else if (strncmp( zone[j].label, "WDAY", 4) == 0 ) {
                    clk = localtime(&curtime);

                    snprintf(blitstr, STRSIZE,"%s",week_day[clk->tm_wday]);

                    BlitString(blitstr, 4, (11*(j-1)) + 5);

                /* Display more precise internet time */
                } else if (strncmp( zone[j].label,"@", 1) == 0 ) {
                    /* Calculate Internet time */
                    swatch_beats = (float)(((curtime+3600)%86400)/86.4);

                    snprintf (blitstr, STRSIZE, "@:%7.3f", swatch_beats);

                    BlitString (blitstr, 5, (11*(j-1)) + 5);

                /* Display Julian Day Number */
                } else if (strncmp( zone[j].label, "JDN", 3) == 0 ) {
                    clk = gmtime(&curtime);

                    /* Calculate Julin Day Number */
                    jd = jdn(curtime) - zone[j].epoch;

                    snprintf(blitstr, STRSIZE, "%10f", jd );

                    BlitString(blitstr, 5, (11*(j-1)) + 5);

                /* Display Local Mean Sidereal Time */
                } else if (strncmp( zone[j].label, "LMST", 3) == 0 ) {
                    clk = gmtime(&curtime);

                    jd = jdn(curtime);

                    siderealTime( jd, sid, LMST );

                    snprintf(blitstr, STRSIZE, "%s%02i.%02i","LST:", sid[0], sid[1]);

                    BlitString(blitstr, 5, (11*(j-1)) + 5);

                /* Display Greenwich Mean Sidereal Time */
                } else if (strncmp( zone[j].label, "GMST", 3) == 0 ) {
                    clk = gmtime(&curtime);

                    jd = jdn(curtime);

                    siderealTime( jd, sid, GMST );

                    snprintf(blitstr, STRSIZE, "%s%02i.%02i","GST:", sid[0], sid[1]);

                    BlitString(blitstr, 5, (11*(j-1)) + 5);

                /* Display local time */
                } else if (strncmp( zone[j].label, "LOCAL", 5) == 0 ) {
                    clk = localtime(&curtime);

                    strncpy(buf, tzname[0], 3);

                    for (k=0; k<3; k++)
                        if (buf[k] == 0)
                            buf[k] = ' ';

                    buf[3] = ':';
                    buf[4] = 0;

                    hour = clk->tm_hour;

                    /* Print Label */
                    snprintf(blitstr, STRSIZE, "%s%02i.%02i",buf,hour,clk->tm_min);

                    BlitString(blitstr, 5, (11*(j-1)) + 5);

                /* Display time in specified time zone */
                } else if (strncmp( zone[j].label, "TZONE", 4) == 0 ) {
                    putenv(zone[j].tz);

                    tzset();

                    clk = localtime(&curtime);

                    strncpy(buf, tzname[0], 3);

                    for (k=0; k<3; k++)
                        if (buf[k] == 0)
                            buf[k] = ' ';

                    buf[3] = ':';
                    buf[4] = 0;

                    snprintf(blitstr, STRSIZE, "%s%02i.%02i", buf, clk->tm_hour, clk->tm_min);

                    BlitString(blitstr, 5, (11*(j-1)) + 5);

                    /* Reset TZ environment variable to old value */
                    putenv(originalTZ);

                /* Display time in specified time zone without TZ env. var. */
                } else {
                    clk = gmtime(&curtime);

                    strncpy(buf, zone[j].label, 3);

                    for (k=0; k<3; k++)
                        if (buf[k] == 0)
                            buf[k] = ' ';

                    buf[3] = ':';
                    buf[4] = 0;

                    hour = clk->tm_hour - zone[j].diff;

                    if (hour > 23 )
                        hour -= 24;
                    else if (hour < 0 )
                        hour += 24;

                    /* Print Label */
                    snprintf(blitstr, STRSIZE, "%s%02i.%02i", buf, hour, clk->tm_min);

                    BlitString(blitstr, 5, (11*(j-1)) + 5);
                }
            }
        }
        
        da_redraw_window();

        /* X Events */
        switch (da_watch_xevent()) {
        case MOUSE_1_REL:
            but_stat = da_check_mouse_region();
            
            if ( clicked ) /* The menu is up */ {
                handleTheMenu(but_stat);

                clicked = 0;
                
            } else /* Show the menu instead of time */ {
                clicked = 1;
            }

            break;
        }

        usleep(10000);
    }

    routine(argc, argv);
}

/*****************************************************************************\
|* handleTheMenu                                                             *|
\*****************************************************************************/
void handleTheMenu(int but_stat) {
    int i;

    char conf_file[256];
    
    char *editor;
    char *ed;

    switch (but_stat) {
    case 0:
        da_exec_command(ABOUT);
        break;
    
    case 1:
        for (i = 0; i < DA_MAX_CONF_FILES; i++) {
            if (da_config[i].path) {
                if (access(da_config[i].path, W_OK) == 0) {
                    strcpy(conf_file, da_config[i].path);

                    break;
                }
            }
        }
        
        if (conf_file[0] == 0) {
            return;
        }
        
        /* Figure out what editor to use */
        if ( defedit == NULL ) {
            ed = getenv("XEDITOR");
            
            if ( ed == NULL )
                ed = "xedit";
        
        } else {
            ed = defedit;
        }
        
        editor = malloc( strlen(ed)+strlen(conf_file)+2 );
        
        if ( editor == NULL )
            return;

        sprintf(editor, "%s %s", ed, conf_file);

        da_exec_command(editor);

        free(editor);

        break;
    
    case 2:
        break;
    
    case 3:
        exit(0);
        break;
    }
}

/*****************************************************************************\
|* BlitString - Blits a string at given coordinates.                         *|
\*****************************************************************************/
void BlitString(char *name, int x, int y) {
    int     i;
    int     c;
    int     k;

    k = x;
    
    for (i=0; name[i]; i++) {
        if (i >= 9)
            break;

        c = toupper(name[i]);

        if (c >= 'A' && c <= 'Z') {   /* its a letter */
            c -= 'A';

            da_copy_xpm_area(c * CHAR_WIDTH, 74, CHAR_WIDTH, 8, k, y);

            k += CHAR_WIDTH;
        
        } else if ( c >= '0' && c <= ':') {
            c -= '0';

            da_copy_xpm_area(c * CHAR_WIDTH, 64, CHAR_WIDTH, 8, k, y);

            k += CHAR_WIDTH;
        
        } else if (c == ';') /* used as a slim ':' */ {
            da_copy_xpm_area(60, 64, CHAR_WIDTH, 8, k, y);

            k += 4;
        
        } else if (c=='.') {
            da_copy_xpm_area(115, 64, 4, 8, k, y);

            k += 4;
        
        } else if (c=='@') {
            da_copy_xpm_area(108, 64, CHAR_WIDTH, 8, k, y);

            k += CHAR_WIDTH;
        
        } else /* print a ' ' */ {
            da_copy_xpm_area(120, 64, CHAR_WIDTH, 8, k, y);

            k += CHAR_WIDTH;
        }
    }
}

/*****************************************************************************\
|* range - Put val in 0<->ran interval.                                      *|
\*****************************************************************************/
void range (double *val, double ran)
{
      *val -= ran*floor(*val/ran);

      if (*val < 0)
          *val += ran;
}

/*****************************************************************************\
|* jdn - converts a time_t to Julian Day                                     *|
\*****************************************************************************/
double jdn(time_t curtime)
{
    return (curtime/86400.0 + 2440587.5);
}

/*****************************************************************************\
|* siderealTime - Gives sidereal time from JD.                               *|
\*****************************************************************************/
void siderealTime( double jde, int *result, int mode )
{
   double t, t2, t3, ts;

   t = (jde - 2451545.0)/36525.0;
   t2 = t*t;
   t3 = t2*t;

   /* Expression from "Astronomical Algorithms" by J. Meeus */
   ts = 280.46061837 + 360.98564736629 * ( jde - 2451545.0 )
        + 0.000387933 * t2 - t3/38710000.0;

   range( &ts, 360.0 );
   ts /= 15.0;

   /* If local time add one hour for every 15 degree in longitude */
   if ( mode == LMST )
   {
     ts += longitude/15.0;
   }

   range( &ts, 24.0 );
   result[0] = (int)ts;
   result[1] = (int)(60 *(ts - result[0]));
}

/*****************************************************************************\
|* julianDay - Gives JD from date.                                           *|
\*****************************************************************************/
double julianDay( int year, int month, double day, double hour,
                  double minute, double second, int julian )
{
   int a, b, c, d;
   double jd;

   day = day + hour/24.0 + minute/1440.0 + second/86400.0;

   if ( month < 3 )
   {
      year -= 1;
      month += 12;
   }

   /* If the date is a Julian calendar date, set julian to TRUE */
   if ( julian )
   {
      b = 0;
   }
   else /* If Gregorian calendar date, julian should be FALSE */
   {
      a = year/100;
      b = 2 - a + a/4;
   }
   c = 365.25 * (year + 4716);
   d = 30.6001 * (month + 1);
   jd = c + d + day + b - 1524.5;

   return( jd );
}

/*****************************************************************************\
|* calendarDate - Gives date from JD. Only Gregorian calendar dates.         *|
\*****************************************************************************/
int calendarDate( double jd, int *year, int *month, double *day )
{
   double a, b, frac, ij, alfa, beta, c, d, e, f;

   if ( jd < 0 )
      return 0;

   jd += 0.5;
   ij = floor(jd);
   frac = jd - ij;

   if ( ij < 2299161 )
   {
      a = ij;
   }
   else
   {
      alfa = floor((ij - 1867216.25)/36524.25);
      beta = floor(alfa/4);
      a = ij + 1 + alfa - beta;
   }

   b = a + 1524;
   c = floor((b - 122.1)/365.25);
   d = floor(365.25 * c);
   e = floor((b - d)/30.6001);
   f = floor(30.6001 * e);

   *day = b - d - f + frac;

   if (e < 14)
       *month = e - 1;
   else if (e == 14 || e == 15)
       *month = e - 13;
   else
       return 0;

   if (*month > 2)
       *year = c - 4716;
   else if (*month == 1 || *month == 2)
       *year = c - 4715;
   else
       return 0;

   return 1;
}

/*****************************************************************************\
|* handleJD                                                                  *|
\*****************************************************************************/
int handleJD( void )
{
  int conv, y, m, d, h, min, sec;
  double day, jd;

  printf(" 1 : Date to JD.\n 2 : JD to date.\n");
  printf("Choose conversion (1 or 2): ");
  scanf("%d", &conv);

  if (conv == 1 )
  {
     printf("Enter UT date with time (YYYY,MM,DD,hh:mm:ss): ");
     scanf("%d,%d,%d,%d:%d:%d", &y, &m, &d, &h, &min, &sec);
     printf("\nJulian Day: %f\n", julianDay( y, m, d, h, min, sec, 0 ) );
  }
  else if (conv == 2)
  {
     printf("Enter Julian Day Number: ");
     scanf("%lf", &jd );
     if ( !calendarDate( jd, &y, &m, &day ) )
     {
       printf("Conversion error! Negative JD not allowed.\n");
       return 0;
     }

     printf("\nGregorian date: %d-%2.2d-%2.4f\n", y, m, day);
  }
  else
  {
    printf("Invalid choice! Try again, please...\n");
    handleJD();
  }
  return 1;
}
