/* wmload - system load monitor designed for Window Maker
*  Copyright (C) 1996 Beat Christen <bchriste@iiic.ethz.ch>
*  Copyright (C) 1997 Ryan Land <rland@bc1.com>
*  Copyright (C) 2015 Window Maker Developers Team
*                     <wmaker-dev@googlegroups.com>
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License along
*  with this program; if not, write to the Free Software Foundation, Inc.,
*  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/wait.h>
#include <time.h>
#include <math.h>
#include <fcntl.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define NCPUSTATES 4

/* Global Data storage/structures ********************************************/
static long cp_time[NCPUSTATES];
static long last[NCPUSTATES];

int updatespeed;

/* X11 Variables *************************************************************/
char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *Execute;

/* XPM Structures & Variables ************************************************/
time_t actualtime;

/* Function definitions ******************************************************/
void routine(int, char **);
void ExecuteExternal(void);
void InsertLoad(void);
void GetLoad(int, int *, int *, int *, int *);
char *skip_token(const char *);

/*****************************************************************************/
/* Source Code <--> Function Implementations                                 */
/*****************************************************************************/
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "graphically display the kernel/system statistics.\n\n"

    PROG_NAME" displays a graphical representation of the kernel/system statistics on a miniwindow. There are three shades in the window: the light one, which corresponds to user statistics, second is the nice time statistics, and the darkest one is system statistics.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_integer(&updatespeed, "update_speed", "-u", "--update-speed", 1, 60, 4, "Update speed in seconds");
    da_init_string(&Execute, "command", "-c", "--command", NULL, "Command to execute on left mouse click");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

/*
 * Copied from ascpu - albert@tigr.net - 09 Mar 2000
 *
 * This function executes an external command while
 * checking whether we should drop the privileges.
 *
 * Since we might need privileges later we fork and
 * then drop privileges in one of the instances which
 * will then execute the command and die.
 *
 * This fixes the security hole for FreeBSD and AIX
 * where this program needs privileges to access
 * the system information.
 */
void ExecuteExternal(void) {
	uid_t ruid, euid;
	int pid;
#ifdef DEBUG
	printf("asload: system(%s)\n",Execute);
#endif
	if( Execute == NULL ) {
		return;
	}
	ruid = getuid();
	euid = geteuid();
	if ( ruid == euid ) {
		if (system( Execute ) == -1)
			fprintf(stderr, "system(%s) returned an error",
				Execute);
		return;
	}
	pid = fork();
	if ( pid == -1 ) {
		printf("asload : fork() failed (%s), command not executed",
				strerror(errno));
		return;
	}
	if ( pid != 0 ) {
		/* parent process simply waits for the child and continues */
		if ( waitpid(pid, 0, 0) == -1 ) {
			printf("asload : waitpid() for child failed (%s)",
				strerror(errno));
		}
		return;
	}
	/*
	 * child process drops the privileges
	 * executes the command and dies
	 */
	if ( setuid(ruid) ) {
		printf("asload : setuid failed (%s), command not executed",
				strerror(errno));
		exit(127);
	}
	if (system( Execute ) == -1)
		fprintf(stderr, "system(%s) returned an error", Execute);
	exit(0);
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
    } 

    InsertLoad();
  
    da_redraw_window();
  
    while (!da_conf_changed(argc, argv)) {
        if (actualtime != time(0)) {
	        actualtime = time(0);

	        if(actualtime % updatespeed == 0)
	            InsertLoad();

	        da_redraw_window();
	    }

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            ExecuteExternal();
            break;
        }
        
#ifdef SYSV
        poll((struct poll *)0, (size_t)0, 50);
#else
        {
            struct timespec ts;

            ts.tv_sec = 0;
            ts.tv_nsec = 50000000L;        /* 5/100 sec */
            
            nanosleep(&ts, NULL);
        }
#endif
    }
    
    routine(argc, argv);
}

/*****************************************************************************/
char *skip_token(const char *p) {
    while (isspace(*p)) 
        p++;
    
    while (*p && !isspace(*p)) 
        p++;
    
    return (char *)p;
}

void GetLoad(int Maximum, int *usr, int *nice, int *sys, int *free) {
    char buffer[100];/*[4096+1];*/
    
    int fd, len;
    int total;
    
    char *p;

    fd = open("/proc/stat", O_RDONLY);
    
    len = read(fd, buffer, sizeof(buffer)-1);
    
    close(fd);
    
    buffer[len] = '\0';

    p = skip_token(buffer);		/* "cpu" */

    cp_time[0] = strtoul(p, &p, 0);	/* user   */
    cp_time[1] = strtoul(p, &p, 0);	/* nice   */
    cp_time[2] = strtoul(p, &p, 0);	/* system */
    cp_time[3] = strtoul(p, &p, 0);	/* idle   */

    if( (*usr  = cp_time[0] - last[0]) < 0 ) 
        *usr = 0 ;
    
    if( (*nice = cp_time[1] - last[1]) < 0 ) 
        *nice = 0 ;
    
    if( (*sys  = cp_time[2] - last[2]) < 0 ) 
        *sys = 0 ;
    
    if( (*free = cp_time[3] - last[3]) < 0 ) 
        *free = 0 ;

    total = *usr + *nice + *sys + *free;

    last[0] = cp_time[0];
    last[1] = cp_time[1];
    last[2] = cp_time[2];
    last[3] = cp_time[3];

    *usr = rint(Maximum * (float)(*usr)   /total);
    *nice =rint(Maximum * (float)(*nice)  /total);
    *sys = rint(Maximum * (float)(*sys)   /total);
    *free = rint(Maximum * (float)(*free) /total);
}

void InsertLoad(void) {
    int UserTime, NiceTime, SystemTime, FreeTime, act, constrain;
    
    GetLoad( 54, &UserTime, &NiceTime, &SystemTime, &FreeTime);

    constrain = (UserTime + NiceTime + SystemTime + FreeTime);
    
    if(constrain == 55) {
        if(FreeTime > 0) 
            FreeTime--;
        
        else if(SystemTime > 0) 
            SystemTime--;
        
        else if(NiceTime > 0) 
            NiceTime--;
        
        else if(UserTime > 0) 
            UserTime--;
    
    } else if(constrain == 53) 
        FreeTime++;

    /* Move the area */
    /* The hack to make the windowed mode work */
    if (da_windowed)
        da_copy_wm_area(6, 5, 53, 54, 5, 5);
    else
        da_copy_xpm_area(6, 5, 53, 54, 5, 5);

    /* User Time */
    act = 59 - UserTime;
    
    if(UserTime > 0)
        da_copy_xpm_area(64, 5, 1, UserTime, 58, act);

    /* Nice Time */
    act = act - NiceTime;
    
    if(NiceTime > 0)
        da_copy_xpm_area(65, 5, 1, NiceTime, 58, act);

    /* System Time */
    act = act - SystemTime;
    
    if(SystemTime > 0)
        da_copy_xpm_area(66, 5, 1, SystemTime, 58, act);

    /* Free Time */
    if(FreeTime > 0)
        da_copy_xpm_area(67, 5, 1, FreeTime, 58, 5);
}
