/* vim: set noet ts=4:
 *
 * Copyright (c) 1999 Michael G. Henderson <mghenderson@lanl.gov>.
 * Copyright (c) 2002-2019 Martin A. Godisch <martin@godisch.de>.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <assert.h>
#include <getopt.h>
#include <signal.h>
#include <sys/wait.h>
#include <libdockapp4/dockapp.h>
#include <curl/curl.h>

#include "config.h"
#include "XPM/wmweather_mask.xbm"
#include "XPM/wmweather_mask2.xbm"
#include "XPM/wmweather_mask3.xbm"
#include "XPM/wmweather_mask4.xbm"
#include "XPM/wmweather_mask_lcd.xbm"
#include "XPM/wmweather_mask_lcd2.xbm"
#include "XPM/wmweather_mask_lcd3.xbm"
#include "XPM/wmweather_mask_lcd4.xbm"
#include "XPM/wmweather-master.xpm"
#include "XPM/wmweather-master2.xpm"
#include "XPM/wmweather-master3.xpm"
#include "XPM/wmweather-master4.xpm"
#include "XPM/wmweather-compat.xpm"
#include "XPM/wmweather-compat2.xpm"
#include "XPM/wmweather-compat3.xpm"
#include "XPM/wmweather-compat4.xpm"
#include "XPM/wmweather-lcd.xpm"
#include "XPM/wmweather-lcd2.xpm"
#include "XPM/wmweather-lcd3.xpm"
#include "XPM/wmweather-lcd4.xpm"

#define DEFAULT_DELAY 15
#define MAX_STRING 256
#define LCD_OFFSET 130

struct memory {
    char *memory;
    size_t size;
};

#ifdef XMESSAGE
static void display_report(void);
#endif

CURL *curl = NULL;

static struct memory header = {NULL, 0};

static int signals[] = {SIGALRM, SIGCHLD, SIGHUP, SIGINT, SIGPIPE, SIGTERM, SIGUSR1, SIGUSR2, 0};

char url[70];
char xtitle[25];

char *report = NULL;

char curl_errmsg[CURL_ERROR_SIZE];
    
pid_t xmsg_child = 0;

time_t last_update = 0;

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *gusty_wind_color;
char *data_color;
char *station_time_color;
char *station;
char *proxy;
char *proxy_user;

int delay;
int tzone;
int lcd_offset = 0;

bool lcd;
bool backlight;
bool compat;
bool pth;
bool metric;
bool showchill;
bool showheat;
bool beaufort;
bool knots;
bool utc;
bool hPa;
bool kPa;
bool mmHg;
bool mps;

static void handler(int);
static void grab_weather(void);
static void update(int);
static size_t curl_callback(void *, size_t, size_t, void *);
static void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockable weather monitor.\n\n"

    PROG_NAME" monitors local weather conditions: temperature, dew point, pressure, humidity and wind direction and speed. The user needs to specify a 4-character \"ICAO location indicator\". The standardized METAR station designations and current weather reports are designed for use by the aviation community. Since most major cities have at least one airport, there is usually one or more METAR stations in a given city. You - obviously - need to be connected to the internet and you also need to have libcurl installed.\n\n"

    PROG_NAME" will then attempt to download the latest METAR report for the station of your choice from the National Weather Sevice run by NOAA.\n\n"

    "You can toggle back and forth between imperial and metric units, i.e. between degrees Fahrenheit, inches of Mercury, and miles per hour and degrees Celsius, hectoPascal, and kilometers per hour.\n\n"

    "Also, a left click will bring up the fully decoded METAR report in xmessage. And a right click will force an immediate update, i.e. "PROG_NAME" will attempt to grab weather information.\n\n"

    "For ICAO location indicators visit https://en.wikipedia.org/wiki/ICAO_airport_code.\n\n"

    "Specify one long option per line in a conf file of choice, e.g. station=EDDC or metric. Whitespace, empty lines, and lines beginning with a # will be ignored.\n\n"

    "Files ~/.config/dockapps/???? contain the latest downloaded weather records in decoded METAR format. The file ~/.netrc contains authentication information and should be used in preference to --proxy-user.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "When toggled to compatibility mode, set the color of the labels, e.g. -f \"red\" or -f \"salmon\" or -f \"#4523ff\". Otherwise sets the main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&gusty_wind_color, "gusty_wind_color", NULL, "--gusty-wind-color", "#ff0000", "When toggled to compatibility mode, set the gusty-wind/variable-direction color. The wind speed indicator will turn this color when the wind speed is gusty. The value shown in this case is the average speed. Similarly, the wind direction indicator will change to this color when the wind direction is variable. The value shown in this case is the average direction");
    da_init_string(&data_color, "data_color", NULL, "--data-color", "#ffbf50", "When toggled to compatibility mode, set the color of the data entries");
    da_init_string(&station_time_color, "station_time_color", NULL, "--station-time-color", "#c5a6ff", "When toggled to compatibility mode, set the color of the ICAO location indicator and update time header");
    da_init_bool(&lcd, "lcd", NULL, "--lcd", false, "Use LCD style display");
    da_init_bool(&backlight, "backlight", NULL, "--backlight", false, "Turn on LCD backlight");
    da_init_string(&station, "station", "-S", "--station", NULL, "Required: this option tells "PROG_NAME" which METAR station to show data for. It can be any four-letter ICAO location indicator, which can be found at https://en.wikipedia.org/wiki/ICAO_airport_code");
    da_init_bool(&compat, "compat", "-c", "--compat", true, "Switch into compatibility mode. The old monitor style will be used, default metric pressure units are millimeters of Mercury instead of hectoPascal, and the colorization options are enabled. When the time turns red, this means that the weather report is older than 24 hours. When the wind speed turns red, this means that the wind is gusty and the listed value is an average only. Similarly, a red direction indicates a variable wind direction and the value given is the average direction. The symbolic link "PROG_NAME" is functionally equivalent to "PROG_NAME" --compat");
    da_init_integer(&delay, "delay", NULL, "--delay", 0, 0, DEFAULT_DELAY, "Set the time (in minutes) between updates");
    da_init_bool(&pth, "past_the_hour", NULL, "--past-the-hour", false, "Delay: specify minutes past the hour");
    da_init_bool(&metric, "metric", "-m", "--metric", false, "Display metric values: temperatures in degrees Celsius rather than degrees Fahrenheit, pressure in hectoPascal rather than inches of Mercury, and wind speed in kilometers per hour rather than miles per hour");
    da_init_bool(&showchill, "chill", NULL, "--chill", false, "Display wind chill (if available) instead of dew point temperature");
    da_init_bool(&showheat, "heat", "-e", "--heat", false, "Display wind heat index (if available) instead of temperature");
    da_init_bool(&beaufort, "beaufort", NULL, "--beaufort", false, "Display windspeed in Beaufort");
    da_init_bool(&knots, "knots", "-n", "--knots", false, "Display windspeed in knots (seamiles per hour)");
    da_init_bool(&utc, "utc", "-u", "--utc", false, "Display update time in UTC instead of localtime. This is equivalent to --time +0000");
    da_init_integer(&tzone, "time", "-t", "--time", 0, 0, -50000, "Convert update time according to (RFC 822 compliant) time offset rather than to localtime, e.g. --time +0100 for MET");
    da_init_string(&proxy, "proxy", "-x", "--proxy", NULL, "Proxy specification, this option is passed to libcurl (<host>[:<port>])");
    da_init_string(&proxy_user, "proxy_user", "-U", "--proxy-user", NULL, "Proxy authentication, this option is passed to libcurl. Please don't specify this option, use the file ~/.netrc for authentication instead (<user>[:<password>])");
    da_init_bool(&hPa, "hPa", "-a", "--hPa", false, "When toggled to metric, display pressure in units of hectoPascals, i.e. milliBars");
    da_init_bool(&kPa, "kPa", "-k", "--kPa", false, "When toggled to metric, display pressure in units of kiloPascals");
    da_init_bool(&mmHg, "mmHg", "-g", "--mmHg", false, "When toggled to metric, display pressure in units of millimeters of Mercury");
    da_init_bool(&mps, "mps", "-p", "--mps", false, "When toggled to metric, display wind speed in units of meters per second");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME".conf");
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                  /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */

    da_parse_all(argc, argv);

    assert(sizeof(char) == 1);

    report = malloc(strlen(getenv("HOME")) + strlen("/.config/dockapps/") + 6);
    
    assert(report);
    
    sprintf(report, "%s/.config/dockapps", getenv("HOME"));
    
    if (chdir(report)) {
        if (errno == ENOENT) {
            if (mkdir(report, 0777)) {
                fprintf(stderr, "%s: cannot create %s.\n", da_name, report);
                da_log(ERR, "cannot create %s.\n", report);
                
                exit(errno);
            }
        } else {
            fprintf(stderr, "%s: cannot chdir into %s.\n", da_name, report);
            da_log(ERR, "cannot chdir into %s.\n", report);
            
            exit(errno);
        }
    }

    strcat(report, "/");
    
    routine(argc, argv);

    return 0;
}

static void routine(int argc, char *argv[]) {
    int i;
    
    /* check options */
    if (!station) {
        fprintf(stderr, "%s: no ICAO location indicator specified\n", da_name);
        da_log(ERR, "no ICAO location indicator specified.\n");
        
        exit(1);
    }

    if (strlen(station) != 4) {
        fprintf(stderr, "%s: invalid ICAO location indicator\n", da_name);
        da_log(ERR, "invalid ICAO location indicator.\n");
        
        exit(1);
    }
    
    da_str_to_upper(station);

    assert(strlen(station) == 4);
    
    /* sorry, memcpy produced crippled result here if config got updated: */
    sprintf(url, "https://tgftp.nws.noaa.gov/data/observations/metar/decoded/%s.TXT", station);
    sprintf(xtitle, "weather report for %s", station);

    sprintf(rindex(report, '/'), "/%s", station);

    delay *= 60;

    if (hPa || kPa) {
        mmHg = false;
    }

    if (utc) {
        tzone = 0;
    }

    if (tzone < -1200 || tzone > +1200) {
        fprintf(stderr, "%s: invalid RFC 822 timezone\n", da_name);
        da_log(ERR, "invalid RFC 822 timezone.\n");
        
        exit(1);
    }
    tzone = tzone / 100 * 3600 + tzone % 100 * 60;

    if (compat) {
        if (!hPa && !kPa) {
            mmHg = true;
        }
    }

    if (lcd && backlight) {
        lcd_offset = LCD_OFFSET;
    } else {
        lcd_offset = 0;
    }
    /*****************/
    
    for (i = 0; signals[i]; i++) {
        if (signal(signals[i], handler) == SIG_ERR) {
            fprintf(stderr, "%s: cannot set handler for signal %d.\n", da_name, signals[i]);
            da_log(ERR, "cannot set handler for signal %d.\n", signals[i]);
        }
    }

    memset(curl_errmsg, 0, CURL_ERROR_SIZE);
    
    curl_global_init(CURL_GLOBAL_NOTHING);
    
    curl = curl_easy_init();
    
    assert(curl);

    if (curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curl_errmsg) != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1) != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 120) != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "wmweather app") != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_URL, url) != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_NETRC, 1) != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, curl_callback) != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, (void*)&header) != CURLE_OK ||
        (proxy && curl_easy_setopt(curl, CURLOPT_PROXY, proxy) != CURLE_OK) ||
        (proxy && proxy_user && curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, proxy_user) != CURLE_OK) ||
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1) != CURLE_OK ||
        curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 10) != CURLE_OK) {
        fprintf(stderr, "%s: %s\n", da_name, curl_errmsg);
        da_log(ERR, "%s.\n", curl_errmsg);
        
        exit(1);
    }

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    if (lcd) {
        da_add_blended_color(front_color, -24, -24, -24, 1.0, "led_color_med");
    } else {
        da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    }
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    da_add_color(gusty_wind_color, "data_color_1");
    da_add_color(data_color, "data_color_2");
    da_add_color(station_time_color, "data_color_3");

    switch (da_scale) {
    case 4:
        if (compat) {
            da_open_xwindow(argc, argv, wmweather_compat4_xpm, wmweather_mask4_bits, wmweather_mask4_width, wmweather_mask4_height);
        } else if (lcd) {
            da_open_xwindow(argc, argv, wmweather_lcd4_xpm, wmweather_mask_lcd4_bits, wmweather_mask_lcd4_width, wmweather_mask_lcd4_height);
        } else {
            da_open_xwindow(argc, argv, wmweather_master4_xpm, wmweather_mask4_bits, wmweather_mask4_width, wmweather_mask4_height);
        }
        break;

    case 3:
        if (compat) {
            da_open_xwindow(argc, argv, wmweather_compat3_xpm, wmweather_mask3_bits, wmweather_mask3_width, wmweather_mask3_height);
        } else if (lcd) {
            da_open_xwindow(argc, argv, wmweather_lcd3_xpm, wmweather_mask_lcd3_bits, wmweather_mask_lcd3_width, wmweather_mask_lcd3_height);
        } else {
            da_open_xwindow(argc, argv, wmweather_master3_xpm, wmweather_mask3_bits, wmweather_mask3_width, wmweather_mask3_height);
        }
        break;

    case 2:
        if (compat) {
            da_open_xwindow(argc, argv, wmweather_compat2_xpm, wmweather_mask2_bits, wmweather_mask2_width, wmweather_mask2_height);
        } else if (lcd) {
            da_open_xwindow(argc, argv, wmweather_lcd2_xpm, wmweather_mask_lcd2_bits, wmweather_mask_lcd2_width, wmweather_mask_lcd2_height);
        } else {
            da_open_xwindow(argc, argv, wmweather_master2_xpm, wmweather_mask2_bits, wmweather_mask2_width, wmweather_mask2_height);
        }
        break;
        
    case 1:
    default:
        if (compat) {
            da_open_xwindow(argc, argv, wmweather_compat_xpm, wmweather_mask_bits, wmweather_mask_width, wmweather_mask_height);
        } else if (lcd) {
            da_open_xwindow(argc, argv, wmweather_lcd_xpm, wmweather_mask_lcd_bits, wmweather_mask_lcd_width, wmweather_mask_lcd_height);
        } else {
            da_open_xwindow(argc, argv, wmweather_master_xpm, wmweather_mask_bits, wmweather_mask_width, wmweather_mask_height);
        }
        break;
    }

    if (time(NULL) != last_update) {
        update(1);
    }
    
    kill(getpid(), SIGALRM);

    while (!da_conf_changed(argc, argv)) {
        if (time(NULL) != last_update)
            update(0);
        
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
#ifdef XMESSAGE
            display_report();
#endif
            break;
            
        case MOUSE_2_PRS:
            if (lcd) {
                backlight = !backlight;
                if (backlight) {
                    lcd_offset = LCD_OFFSET;
                } else {
                    lcd_offset = 0;
                }
                da_redraw_window();
            }
            break;
                
        case MOUSE_3_PRS:
            grab_weather();
            break;
        }

        da_redraw_window();
        
        usleep(50000L);
    }

    routine(argc, argv);
}

#ifdef XMESSAGE
static void display_report(void)
{
    if (xmsg_child)
        return;
    
    if ((xmsg_child = fork()) < 0) {
        fprintf(stderr, "%s: cannot fork.\n", da_name);
        da_log(ERR, "cannot fork.\n");
        
        xmsg_child = 0;
        
        return;
    }
    
    if (xmsg_child)
        return;
    
    if (execl(XMESSAGE, "xmessage", "-center", "-file", report, "-title", xtitle, NULL) < 0) {
        fprintf(stderr, "%s (pid %d): error executing xmessage.\n", da_name, getpid());
        da_log(ERR, "(pid %d): error executing xmessage.\n", getpid());
        
        exit(1);
    }
}
#endif

static void grab_weather(void)
{
    static FILE
        *F    = NULL;
    static char
        *temp = NULL;
    char
        *e    = NULL;
    unsigned int
        http;

    if (F != NULL)
        return;
    if (temp == NULL) {
        temp = malloc(strlen(report) + 5);
        assert(temp);
        sprintf(temp, "%s.new", report);
    }
    if ((F = fopen(temp, "w")) == NULL) {
        fprintf(stderr, "%s: cannot open %s for writing.\n", da_name, temp);
        da_log(ERR, "cannot open %s for writing.\n", temp);
        goto GRAB_ERR;
    }
    if (curl_easy_setopt(curl, CURLOPT_FILE, F)) {
        fprintf(stderr, "%s: %s\n", da_name, curl_errmsg);
        da_log(ERR, "%s.\n", curl_errmsg);
        goto GRAB_ERR;
    }
    switch (curl_easy_perform(curl)) {
    case CURLE_OK:
        assert(header.memory);
        /* see RFC 2616 (HTTP/1.1), RFC 1945 (HTTP/1.0) for syntax definition */
        http = strtol(header.memory + 9, &e, 10);
        assert(*e == ' ' && http >= 100 && http < 600);
        if (http < 200) {        /* 1xx - Informational */
            /* FIXME: what to do here? */
        } else if (http < 300) { /* 2xx - Success       */
            if (rename(temp, report) < 0) {
                fprintf(stderr, "%s: Cannot rename %s to %s.\n", da_name, temp, report);
                da_log(ERR, "cannot rename %s to %s.\n", temp, report);
                goto GRAB_ERR;
            }
        } else if (http < 400) { /* 3xx - Redirection   */
            fprintf(stderr, "%s: Received error %u while fetching %s from NOAA, aborting\n", da_name, http, station);
            da_log(ERR, "received error %u while fetching %s from NOAA, aborting.\n", http, station);
            fprintf(stderr, "%s: Please send a bug report to <%s>\n", da_name, PACKAGE_BUGREPORT);
            da_log(ERR, "please send a bug report to <%s>.\n", PACKAGE_BUGREPORT);
            goto GRAB_ERR;
        } else if (http < 500 && http != 403) { /* 4xx - Client Error  */
            if (http == 404) {
                fprintf(stderr, "%s: ICAO location indicator %s not found at NOAA, aborting\n", da_name, station);
                da_log(ERR, "ICAO location indicator %s not found at NOAA, aborting.\n", station);
            } else {
                fprintf(stderr, "%s: Received error %u while fetching %s from NOAA, aborting\n", da_name, http, station);
                da_log(ERR, "received error %u while fetching %s from NOAA, aborting.\n", http, station);
            }
            goto GRAB_ERR;
        } else {                 /* 5xx - Server Error  */
            fprintf(stderr, "%s: Received error %u while fetching %s from NOAA\n", da_name, http, station);
            da_log(ERR, "received error %u while fetching %s from NOAA.\n", http, station);
        }
        break;
    default:
        fprintf(stderr, "%s: %s\n", da_name, curl_errmsg);
        da_log(ERR, "%s.\n", curl_errmsg);
    }
    free(header.memory);
    header.memory = NULL;
    header.size   = 0;
    fclose(F);
    F = NULL;
    unlink(temp);
    return;
GRAB_ERR:
    if (F != NULL) {
        fclose(F);
        unlink(temp);
    }
    curl_easy_cleanup(curl);
    curl_global_cleanup();
    exit(1);
}

static void handler(int signo)
{
    pid_t  pid;
    time_t now;
    int status;

    switch (signo) {
    case SIGALRM:
        grab_weather();
        time(&now);
        alarm(pth ? now / 3600 * 3600 + delay - now + (now % 3600 < delay ? 0 : 3600) : delay);
        break;
    case SIGCHLD:
        if ((pid = waitpid(-1, &status, WNOHANG)) < 0)
            return;
        if (pid == xmsg_child)
            xmsg_child = 0;
        break;
    default:
        curl_easy_cleanup(curl);
        curl_global_cleanup();
        exit(0);
    }
}

static void update(int force_read)
{
    static char
        id[4],
        winddir[3];
    static int
        timestamp        = 0,
        last24h          = 0,
        temperature      = 0,
        temperatureavail = 0,
        dewpoint         = 0,
        dewpointavail    = 0,
        windchill        = 0,
        windchillavail   = 0,
        heatindexavail   = 0,
        humidity         = 0,
        humidityavail    = 0,
        pressureavail    = 0,
        winddiravail     = 0,
        windspeedavail   = 0,
        gusting          = 0,
        showgusting      = 0,
        showwinddir      = 0,
        showerror        = 1;
    static double
        windspeed        = 0,
        heatindex        = 0,
        pressure         = 0;
    static time_t
        last_report      = 0;
    struct stat rst;
    struct tm   *tm;
    time_t utc_diff;
    FILE   *F;
    char   buffer[MAX_STRING], *i;
    int    line, n, q, sgn;
    long   l = 0;

    time(&l);
    tm = gmtime(&l);
    utc_diff = tm->tm_hour;
    tm = localtime(&l);
    utc_diff = (tm->tm_hour - utc_diff + 24) % 24 * 3600;

    if (stat(report, &rst) < 0 && errno != ENOENT) {
        if (showerror) {
            fprintf(stderr, "%s: cannot stat weather report %s.\n", da_name, report);
            da_log(ERR, "cannot stat weather report %s.\n", report);
            showerror = 0;
        }
        return;
    }

    if (rst.st_mtime > last_report || force_read) {
        F = fopen(report, "r");
        if (!F) {
            if (showerror && errno != ENOENT) {
                fprintf(stderr, "%s: cannot open weather report %s.\n", da_name, report);
                da_log(ERR, "cannot open weather report %s.\n", report);
                showerror = 0;
            }
            return;
        }
        last_report = rst.st_mtime;
        memcpy(id, "\0\0\0\0", 4);
        memcpy(winddir, "\0\0\0", 3);
        timestamp        = 0;
        last24h          = 0;
        temperature      = 0;
        temperatureavail = 0;
        dewpoint         = 0;
        dewpointavail    = 0;
        windchill        = 0;
        windchillavail   = 0;
        heatindex        = 0;
        heatindexavail   = 0;
        humidity         = 0;
        humidityavail    = 0;
        pressureavail    = 0;
        winddiravail     = 0;
        windspeedavail   = 0;
        gusting          = 0;
        showgusting      = 0;
        showwinddir      = 0;
        showerror        = 1;
        windspeed        = 0;
        pressure         = 0;
        for (line = 1;; line++) {
            i = fgets(buffer, MAX_STRING, F);
            if (feof(F))
                break;

            if (!strncmp(buffer, "ob: ", 3)) {
                i = strchr(buffer, ' ');
                i++;
                for (n = 0; n < 4; n++, i++)
                    id[n] = *i;
                continue;
            }

            if (line == 2) {
                if ((i = strrchr(buffer, ' ')) == NULL) {
                    fclose(F);
                    return;
                }
                i -= 15;
                assert(tm);
                tm->tm_year = strtol(i, &i, 10) - 1900;
                if (*i != '.')
                    continue;
                i++;
                tm->tm_mon  = strtol(i, &i, 10) - 1;
                if (*i != '.')
                    continue;
                i++;
                tm->tm_mday = strtol(i, &i, 10);
                if (*i != ' ')
                    continue;
                i++;
                l = strtol(i, &i, 10);
                if (*i != ' ' || l < 0 || l >= 2400)
                    continue;
                tm->tm_hour  = l / 100;
                tm->tm_min   = l % 100;
                tm->tm_sec   =  0;
                tm->tm_isdst = -1;
                timestamp = mktime(tm) + utc_diff;
                continue;
            }

            if (!strncmp(buffer, "Wind: ", 6)) {
                if (strstr(buffer, " Calm")) {
                    windspeed      = 0;
                    windspeedavail = 1;
                    winddiravail   = 0;
                    continue;
                }
                if (strstr(buffer, " Variable "))
                    memcpy(winddir, "VRB", 3);
                else
                    for (n = 0; n < 3; n++)
                        if (buffer[n+15] == ' ')
                            break;
                        else
                            winddir[n] = buffer[n+15];
                winddiravail = 1;
                i = strstr(buffer, " MPH");
                i--;
                while (*i != ' ')
                    i--;
                i++;
                windspeed = strtol(i, &i, 10);
                if (*i != ' ') {
                    if (showerror) {
                        fprintf(stderr, "%s: invalid windspeed: %s line %d\n", da_name, report, line);
                        da_log(ERR, "invalid windspeed: %s line %d.\n", report, line);
                    }
                    continue;
                }
                if ((i = strstr(buffer, " gusting "))) {
                    l = strtol(i + 12, &i, 10);
                    if (*i != ' ') {
                        if (showerror) {
                            fprintf(stderr, "%s: invalid gusting windspeed: %s line %d\n", da_name, report, line);
                            da_log(ERR, "invalid gusting windspeed: %s line %d.\n", report, line);
                        }
                        continue;
                    }
                    windspeed = (windspeed + l) / 2.0 + 0.5;
                    gusting = 1;
                }
                windspeedavail = 1;
                if (knots)
                    windspeed *= 0.8688; /* kt = sm/h */
                else if ((metric && mps) || beaufort)
                    windspeed *= 0.4469; /* m/s  */
                else if (metric)
                    windspeed *= 1.6090; /* km/h */
                /* else miles/h */
                if (beaufort) {
                    if (windspeed <= 0.2)
                        windspeed = 0;
                    else if (windspeed >  0.2 && windspeed <=  1.5)
                        windspeed = 1;
                    else if (windspeed >  1.5 && windspeed <=  3.3)
                        windspeed = 2;
                    else if (windspeed >  3.3 && windspeed <=  5.4)
                        windspeed = 3;
                    else if (windspeed >  5.4 && windspeed <=  7.9)
                        windspeed = 4;
                    else if (windspeed >  7.9 && windspeed <= 10.7)
                        windspeed = 5;
                    else if (windspeed > 10.7 && windspeed <= 13.8)
                        windspeed = 6;
                    else if (windspeed > 13.8 && windspeed <= 17.1)
                        windspeed = 7;
                    else if (windspeed > 17.1 && windspeed <= 20.7)
                        windspeed = 8;
                    else if (windspeed > 20.7 && windspeed <= 24.4)
                        windspeed = 9;
                    else if (windspeed > 24.4 && windspeed <= 28.4)
                        windspeed = 10;
                    else if (windspeed > 28.4 && windspeed <= 32.6)
                        windspeed = 11;
                    else if (windspeed > 32.6 && windspeed <= 36.9)
                        windspeed = 12;
                    else {
                        windspeed = 13;
                    }
                }
                windspeed = (long)(windspeed + 0.5);
                continue;
            }

            if (!strncmp(buffer, "Windchill: ", 11)) {
                i = buffer + 11;
                windchill = strtol(i, &i, 10);
                if (*i != ' ') {
                    if (showerror) {
                        fprintf(stderr, "%s: invalid wind chill temperature: %s line %d\n", da_name, report, line);
                        da_log(ERR, "invalid wind chill temperature: %s line %d.\n", report, line);
                    }
                    continue;
                }
                windchillavail = 1;
                if (metric)
                    windchill = (windchill - 32.0) * 5.0 / 9.0 + 0.5;
                continue;
            }

            if (!strncmp(buffer, "Heat index: ", 12)) {
                i = buffer + 12;
                heatindex = strtod(i, &i);
                if (*i != ' ') {
                    if (showerror) {
                        fprintf(stderr, "%s: invalid heat index temperature: %s line %d\n", da_name, report, line);
                        da_log(ERR, "invalid heat index temperature: %s line %d.\n", report, line);
                    }
                    continue;
                }
                heatindexavail = 1;
                if (metric)
                    heatindex = (heatindex - 32.0) * 5.0 / 9.0 + 0.5;
                continue;
            }

            if (!strncmp(buffer, "Temperature: ", 13)) {
                double t;
                i = buffer + 13;
                t = strtod(i, &i);
                if (*i != ' ') {
                    if (showerror) {
                        fprintf(stderr, "%s: invalid temperature: %s line %d\n", da_name, report, line);
                        da_log(ERR, "invalid temperature: %s line %d.\n", report, line);
                    }
                    continue;
                }
                temperatureavail = 1;
                if (metric)
                    t = (t - 32.0) * 5.0 / 9.0;
                temperature = (t < 0) ? t - 0.5 : t + 0.5;
                continue;
            }

            if (!strncmp(buffer, "Dew Point: ", 11)) {
                double d;
                i = buffer + 11;
                d = strtod(i, &i);
                if (*i != ' ') {
                    if (showerror) {
                        fprintf(stderr, "%s: invalid dew point: %s line %d\n", da_name, report, line);
                        da_log(ERR, "invalid dew point: %s line %d.\n", report, line);
                    }
                    continue;
                }
                dewpointavail = 1;
                if (metric)
                    d = (d - 32.0) * 5.0 / 9.0;
                dewpoint = (d < 0) ? d - 0.5 : d + 0.5;
                continue;
            }

            if (!strncmp(buffer, "Relative Humidity: ", 19)) {
                i = buffer + 19;
                humidity = strtol(i, &i, 10);
                if (*i != '%') {
                    if (showerror) {
                        fprintf(stderr, "%s: invalid humidity: %s line %d\n", da_name, report, line);
                        da_log(ERR, "invalid humidity: %s line %d.\n", report, line);
                    }
                    continue;
                }
                humidityavail = 1;
                continue;
            }

            if (!strncmp(buffer, "Pressure (altimeter): ", 22)) {
                i = buffer + 22;
                pressure = strtod(i, &i);
                if (*i != ' ') {
                    if (showerror) {
                        fprintf(stderr, "%s: invalid pressure: %s line %d\n", da_name, report, line);
                        da_log(ERR, "invalid pressure: %s line %d.\n", report, line);
                    }
                    continue;
                }
                pressureavail = 1;
                if (metric) {
                    if (mmHg)
                        pressure = pressure * 25.4;
                    else if (kPa)
                        pressure = pressure * 3.38639;
                    else /* hPa */
                        pressure = pressure * 33.8639;
                } /* else inHg */
                continue;
            }
        }
        fclose(F);
    }

    if (!id[0])
        return;
    last24h = time(NULL) - timestamp < 86400;

    if (compat) {

        da_copy_xpm_area(5, 69, 54, 54, 5, 5);

        for (n = 0; n < 4; n++)
            if (id[n] >= 'A' && id[n] <= 'Z')
                da_copy_xpm_area((int)(id[n] - 'A') * 5 + 2, 128, 5, 6, n * 5 + 7, 6);
            else if (id[n] >= '0' && id[n] <= '9')
                da_copy_xpm_area((int)(id[n] - '0') * 5 + 2, 135, 5, 6, n * 5 + 7, 6);

        l = timestamp + (tzone < -43200 ? utc_diff : tzone);
        da_copy_xpm_area(l / 3600 % 24 / 10 * 5 + (last24h ? 2 : 66), last24h ? 135 : 64, 5, 6, 36, 6);
        da_copy_xpm_area(l / 3600 % 24 % 10 * 5 + (last24h ? 2 : 66), last24h ? 135 : 64, 5, 6, 41, 6);
        da_copy_xpm_area(last24h ? 53 : 55, 135, 1, 6, 46, 6);
        da_copy_xpm_area(l /   60 % 60 / 10 * 5 + (last24h ? 2 : 66), last24h ? 135 : 64, 5, 6, 48, 6);
        da_copy_xpm_area(l /   60 % 60 % 10 * 5 + (last24h ? 2 : 66), last24h ? 135 : 64, 5, 6, 53, 6);

        if (temperatureavail) {
            q   = 0;
            l   = temperature;
            sgn = (l < 0) ? -1 : 1;
            if (sgn < 0) {
                da_copy_xpm_area(125, 57, 5, 6, 25, 15);
                q +=  5;
                l *= -1;
            }
            if (l >= 100) {
                da_copy_xpm_area(l %  1000 /  100 * 5 + 66, 57, 5, 6, 25 + q, 15);
                q += 5;
            }
            if (l >=  10) {
                da_copy_xpm_area(l %   100 /   10 * 5 + 66, 57, 5, 6, 25 + q, 15);
                q += 5;
            }
            da_copy_xpm_area(l % 10 * 5 + 66, 57, 5, 6, 25 + q, 15);
            if (metric) {
                da_copy_xpm_area(72, 34, 3, 3, 30+q, 14);
                da_copy_xpm_area(81, 35, 4, 6, 34+q, 15);
            } else {
                da_copy_xpm_area(72, 34, 8, 7, 30+q, 14);
            }
        }

        if (windchillavail && (showchill || !dewpointavail)) {
            l = windchill;
            da_copy_xpm_area(66, 87, 17, 8, 5, 23);
        } else if (dewpointavail) {
            l = dewpoint;
            da_copy_xpm_area(5, 87, 17, 8, 5, 23);
        }
        if (windchillavail || dewpointavail) {
            q   = 0;
            sgn = (l < 0) ? -1 : 1;
            if (sgn < 0) {
                da_copy_xpm_area(125, 57, 5, 6, 25, 24);
                q +=  5;
                l *= -1;
            }
            if (l >= 100) {
                da_copy_xpm_area(l % 1000 / 100 * 5 + 66, 57, 5, 6, 25 + q, 24);
                q += 5;
            }
            if (l >=  10) {
                da_copy_xpm_area(l %  100 /  10 * 5 + 66, 57, 5, 6, 25 + q, 24);
                q += 5;
            }
            da_copy_xpm_area(l % 10 * 5 + 66, 57, 5, 6, 25 + q, 24);
            if (metric) {
                da_copy_xpm_area(72, 34, 3, 3, 30 + q, 23);
                da_copy_xpm_area(81, 35, 4, 6, 34 + q, 24);
            } else {
                da_copy_xpm_area(72, 34, 8, 7, 30 + q, 23);
            }
        }

        if (pressureavail) {
            q = 0;
            if (pressure < 100)
                l = (long)(pressure * 100 + 0.5);
            else
                l = (long)(pressure * 100 + 50);
            if (l >= 100000) {
                da_copy_xpm_area(l % 1000000 / 100000 * 5 + 66, 57, 5, 6, 25 + q, 33);
                q += 5;
            }
            if (l >=  10000) {
                da_copy_xpm_area(l %  100000 /  10000 * 5 + 66, 57, 5, 6, 25 + q, 33);
                q += 5;
            }
            if (l >=   1000) {
                da_copy_xpm_area(l %   10000 /   1000 * 5 + 66, 57, 5, 6, 25 + q, 33);
                q += 5;
            }
            da_copy_xpm_area(l % 1000 / 100 * 5 + 66, 57, 5, 6, 25 + q, 33);
            q += 5;
            if (l < 10000) {
                da_copy_xpm_area(117, 57, 4, 6, 25 + q, 33);
                q += 4;
                da_copy_xpm_area(l % 100 / 10 * 5 + 66, 57, 5, 6, 25 + q, 33);
                q += 5;
                da_copy_xpm_area(l %  10      * 5 + 66, 57, 5, 6, 25 + q, 33);
            }
        }

        if (humidityavail) {
            q = 0;
            l = humidity;
            if (l >= 100) {
                da_copy_xpm_area(l % 1000 / 100 * 5 + 66, 57, 5, 6, 25 + q, 42);
                q += 5;
            }
            if (l >=  10) {
                da_copy_xpm_area(l %  100 /  10 * 5 + 66, 57, 5, 6, 25 + q, 42);
                q += 5;
            }
            da_copy_xpm_area(l % 10 * 5 + 66, 57, 5, 6, 25 + q, 42);
            da_copy_xpm_area(121, 57, 5, 6, 30 + q, 42);
        }

        q = 0;
        if (winddiravail) {
            for (n = 0; n < 3; n++) {
                switch(winddir[n]) {
                case 'N':
                    da_copy_xpm_area(66, 0 ? 50 : 43, 5, 6, 25 + q, 51);
                    break;
                case 'W':
                    da_copy_xpm_area(71, 0 ? 50 : 43, 5, 6, 25 + q, 51);
                    break;
                case 'S':
                    da_copy_xpm_area(76, 0 ? 50 : 43, 5, 6, 25 + q, 51);
                    break;
                case 'E':
                    da_copy_xpm_area(81, 0 ? 50 : 43, 5, 6, 25 + q, 51);
                    break;
                case 'V':
                    da_copy_xpm_area(86, 1 ? 50 : 43, 5, 6, 25 + q, 51);
                    break;
                case 'R':
                    da_copy_xpm_area(91, 1 ? 50 : 43, 5, 6, 25 + q, 51);
                    break;
                case 'B':
                    da_copy_xpm_area(96, 1 ? 50 : 43, 4, 6, 25 + q, 51);
                    break;
                }
                if (winddir[n])
                    q += 5;
            }
            q += 2;
        }
        if (windspeedavail)  {
            l = (long)(windspeed + 0.5);
            if (!l)
                da_copy_xpm_area(66, 4, 23, 7, 25, 51);
            else {
                if (l >= 100) {
                    da_copy_xpm_area(l % 1000 / 100 * 5 + 66, gusting ? 64 : 57, 5, 6, 25 + q, 51);
                    q += 5;
                }
                if (l >=  10) {
                    da_copy_xpm_area(l %  100 /  10 * 5 + 66, gusting ? 64 : 57, 5, 6, 25 + q, 51);
                    q += 5;
                }
                da_copy_xpm_area(l % 10 * 5 + 66, gusting ? 64 : 57, 5, 6, 25 + q, 51);
            }
        }

    } else {

        time(&l);
        tm = localtime(&l);
        if (gusting) {
            showwinddir = tm->tm_sec % 20 < 5;
            showgusting = tm->tm_sec % 20 >= 5 && tm->tm_sec % 20 < 10;
        } else {
            showwinddir = tm->tm_sec % 10 < 5;
            showgusting = 0;
        }

        if (lcd) {
            da_copy_xpm_area(63 + lcd_offset, 0, 58, 57, 3, 4);
        } else {
            da_copy_xpm_area(65, 1, 54, 54, 5, 5);
        }

        for (n = 0; n < 4; n++)
            if (id[n] >= 'A' && id[n] <= 'Z')
                da_copy_xpm_area(((int)(id[n] - 'A') * 5) + lcd_offset, 78, 5, 7, n * 6 + 6, 6);
            else if (id[n] >= '0' && id[n] <= '9')
                da_copy_xpm_area(((int)(id[n] - '0') * 5) + lcd_offset, 64, 5, 7, n * 6 + 6, 6);

        if (last24h) {
            l = timestamp + (tzone < -43200 ? utc_diff : tzone);
            da_copy_xpm_area((l / 3600 % 24 / 10 * 5 + 50) + lcd_offset, 64, 5, 7, 37, 6);
            da_copy_xpm_area((l / 3600 % 24 % 10 * 5 + 50) + lcd_offset, 64, 5, 7, 43, 6);
            da_copy_xpm_area((l /   60 % 60 / 10 * 4 + 90) + lcd_offset, 57, 4, 5, 49, 6);
            da_copy_xpm_area((l /   60 % 60 % 10 * 4 + 90) + lcd_offset, 57, 4, 5, 54, 6);
        } else {
            da_copy_xpm_area( 81 + lcd_offset, 57, 5, 7, 37, 6);
            da_copy_xpm_area( 10 + lcd_offset, 64, 5, 7, 43, 6);
            da_copy_xpm_area( 20 + lcd_offset, 64, 5, 7, 49, 6);
            da_copy_xpm_area(121 + lcd_offset, 51, 3, 5, 55, 6);
        }

        if (temperatureavail) {
            l = temperature;
            if (l < 0) {
                da_copy_xpm_area(40 + lcd_offset, 67, 5, 1, (l <= -10) ? 31 : 37, 18);
                l *= -1;
            }
            if (l >= 100)
                da_copy_xpm_area((l % 1000 / 100 * 5) + lcd_offset, 64, 5, 7, 31, 15);
            if (l >=  10)
                da_copy_xpm_area((l %  100 /  10 * 5) + lcd_offset, 64, 5, 7, 37, 15);
            da_copy_xpm_area((l % 10 * 5) + lcd_offset, 64, 5, 7, 43, 15);
            if (metric)
                da_copy_xpm_area(121 + lcd_offset, 7, 9, 7, 49, 15);
            else
                da_copy_xpm_area(121 + lcd_offset, 0, 9, 7, 49, 15);
        }

        if (windchillavail && (showchill || !dewpointavail)) {
            da_copy_xpm_area(64 + lcd_offset, 57, 17, 7, 6, 24);
            l = windchill;
        } else
            l = dewpoint;
        if (windchillavail || dewpointavail) {
            if (l < 0) {
                da_copy_xpm_area(40 + lcd_offset, 67, 5, 1, (l <= -10) ? 31 : 37, 27);
                l *= -1;
            }
            if (l >= 100)
                da_copy_xpm_area((l % 1000 / 100 * 5) + lcd_offset, 64, 5, 7, 31, 24);
            if (l >=  10)
                da_copy_xpm_area((l %  100 /  10 * 5) + lcd_offset, 64, 5, 7, 37, 24);
            da_copy_xpm_area((l % 10 * 5) + lcd_offset, 64, 5, 7, 43, 24);
            if (metric)
                da_copy_xpm_area(121 + lcd_offset, 7, 9, 7, 49, 24);
            else
                da_copy_xpm_area(121 + lcd_offset, 0, 9, 7, 49, 24);
        }

        if (heatindexavail && (showheat || (!showchill && !dewpointavail))) {
            da_copy_xpm_area(35 + lcd_offset, 71, 5, 7,  6, 24);
            da_copy_xpm_area(95 + lcd_offset, 71, 5, 7, 12, 24);
            da_copy_xpm_area(40 + lcd_offset, 71, 5, 7, 18, 24);
            l = (long)heatindex;
        } else
            l = dewpoint;
        if (heatindexavail || dewpointavail) {
            if (l < 0) {
                da_copy_xpm_area(40 + lcd_offset, 67, 5, 1, (l <= -10) ? 31 : 37, 27);
                l *= -1;
            }
            if (l >= 100)
                da_copy_xpm_area((l % 1000 / 100 * 5) + lcd_offset, 64, 5, 7, 31, 24);
            if (l >=  10)
                da_copy_xpm_area((l %  100 /  10 * 5) + lcd_offset, 64, 5, 7, 37, 24);
            da_copy_xpm_area((l % 10 * 5) + lcd_offset, 64, 5, 7, 43, 24);
            if (metric)
                da_copy_xpm_area(121 + lcd_offset, 7, 9, 7, 49, 24);
            else
                da_copy_xpm_area(121 + lcd_offset, 0, 9, 7, 49, 24);
        }

        if (pressureavail) {
            l = (long)(pressure + 0.5);
            if (l >= 1000)
                da_copy_xpm_area(9 + lcd_offset, 64, 1, 7, 29, 33);
            if (l >=  100)
                da_copy_xpm_area((l % 1000 / 100 * 5) + lcd_offset, 64, 5, 7, 31, 33);
            if (l >=   10)
                da_copy_xpm_area((l %  100 /  10 * 5) + lcd_offset, 64, 5, 7, 37, 33);
            da_copy_xpm_area((l % 10 * 5) + lcd_offset, 64, 5, 7, 43, 33);
            if (metric) {
                if (mmHg)
                    da_copy_xpm_area(121 + lcd_offset, 22, 9, 4, 49, 33);
                else if (kPa)
                    da_copy_xpm_area(121 + lcd_offset, 18, 9, 4, 49, 33);
                else /* hPa */
                    da_copy_xpm_area(121 + lcd_offset, 14, 9, 4, 49, 33);
            } else { /* inHg */
                da_copy_xpm_area(121 + lcd_offset, 26, 9, 4, 49, 33);
            }
        }

        if (humidityavail) {
            l = humidity;
            if (l >= 100)
                da_copy_xpm_area(9 + lcd_offset, 64, 1, 7, 35, 42);
            if (l >=  10)
                da_copy_xpm_area((l %  100 /  10 * 5) + lcd_offset, 64, 5, 7, 37, 42);
            da_copy_xpm_area((l % 10 * 5) + lcd_offset, 64, 5, 7, 43, 42);
            da_copy_xpm_area(125 + lcd_offset, 30, 5, 7, 53, 42);
        }

        if (winddiravail) {
            if (showwinddir || !windspeedavail) {
                for (n = 0; n < 3; n++)
                    if (winddir[n])
                        da_copy_xpm_area(((int)(winddir[n] - 'A') * 5) + lcd_offset, 71, 5, 7, n * 6 + 31, 51);
            }
            da_copy_xpm_area(123 + lcd_offset, 37, 7, 7, 51, 51);
            if      (!memcmp(winddir, "N\0\0", 3))
                da_copy_xpm_area(129 + lcd_offset, 44, 1, 4, 54, 51);
            else if (!memcmp(winddir, "NNE",   3))
                da_copy_xpm_area(122 + lcd_offset, 44, 2, 4, 54, 51);
            else if (!memcmp(winddir, "NE\0",  3))
                da_copy_xpm_area(126 + lcd_offset, 44, 3, 3, 54, 52);
            else if (!memcmp(winddir, "ENE",   3))
                da_copy_xpm_area(126 + lcd_offset, 47, 4, 2, 54, 53);
            else if (!memcmp(winddir, "E\0\0", 3))
                da_copy_xpm_area(121 + lcd_offset, 50, 4, 1, 54, 54);
            else if (!memcmp(winddir, "ESE",   3))
                da_copy_xpm_area(126 + lcd_offset, 48, 4, 2, 54, 54);
            else if (!memcmp(winddir, "SE\0",  3))
                da_copy_xpm_area(124 + lcd_offset, 44, 3, 3, 54, 54);
            else if (!memcmp(winddir, "SSE",   3))
                da_copy_xpm_area(122 + lcd_offset, 46, 2, 4, 54, 54);
            else if (!memcmp(winddir, "S\0\0", 3))
                da_copy_xpm_area(129 + lcd_offset, 44, 1, 4, 54, 54);
            else if (!memcmp(winddir, "SSW",   3))
                da_copy_xpm_area(121 + lcd_offset, 46, 2, 4, 53, 54);
            else if (!memcmp(winddir, "SW\0",  3))
                da_copy_xpm_area(126 + lcd_offset, 44, 3, 3, 52, 54);
            else if (!memcmp(winddir, "WSW",   3))
                da_copy_xpm_area(124 + lcd_offset, 48, 4, 2, 51, 54);
            else if (!memcmp(winddir, "W\0\0", 3))
                da_copy_xpm_area(121 + lcd_offset, 50, 4, 1, 51, 54);
            else if (!memcmp(winddir, "WNW",   3))
                da_copy_xpm_area(124 + lcd_offset, 47, 4, 2, 51, 53);
            else if (!memcmp(winddir, "NW\0",  3))
                da_copy_xpm_area(124 + lcd_offset, 44, 3, 3, 52, 52);
            else if (!memcmp(winddir, "NNW",   3))
                da_copy_xpm_area(121 + lcd_offset, 44, 2, 4, 53, 51);
        }
        if (windspeedavail && (!showwinddir || !winddiravail) && !showgusting) {
            l = (long)(windspeed + 0.5);
            if (l >= 100)
                da_copy_xpm_area(9 + lcd_offset, 64, 1, 7, 35, 51);
            if (l >=  10)
                da_copy_xpm_area((l %  100 /  10 * 5) + lcd_offset, 64, 5, 7, 37, 51);
            da_copy_xpm_area((l % 10 * 5) + lcd_offset, 64, 5, 7, 43, 51);
        }
        if (showgusting) {
            da_copy_xpm_area(30 + lcd_offset, 71, 5, 7, 31, 51);
            da_copy_xpm_area(90 + lcd_offset, 71, 5, 7, 37, 51);
            da_copy_xpm_area(95 + lcd_offset, 71, 5, 7, 43, 51);
        }
    }

    da_redraw_window();
    
    time(&last_update);
    
    showerror = 0;
}

static size_t curl_callback(void *ptr, size_t size, size_t n, void *data)
{
    struct memory
        *mem = (struct memory*)data;
    register unsigned long
        real;

    real = size * n;
    mem->memory = (char*)realloc(mem->memory, mem->size + real + 1);
    assert(mem->memory);
    memcpy(&(mem->memory[mem->size]), ptr, real);
    mem->size += real; 
    mem->memory[mem->size] = 0; 
    return real;
}
