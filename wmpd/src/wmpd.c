/*  wmpd - an MPD remote-controlling DockApp
 *  Copyright (C) 2020 Xander
 *  Copyright (C) 2000-2001 Bastien Nocera <hadess@hadess.net>
 *  Copyright (C) John Chapin <john+wmusic@jtan.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <libdockapp4/dockapp.h>
#include <mpd/client.h>
#include <locale.h>

#include "config.h"
#include "XPM/mask.xbm"
#include "XPM/mask2.xbm"
#include "XPM/mask3.xbm"
#include "XPM/mask4.xbm"
#include "XPM/wmpd.xpm"
#include "XPM/wmpd2.xpm"
#include "XPM/wmpd3.xpm"
#include "XPM/wmpd4.xpm"

#define SEPARATOR " ** "          /* The separator for the scrolling title */
#define SCROLL_SPEED 0.5          /* title scrolling speed */

char title_comp[512]     = " ";
char song_file_comp[512] = " ";
char mpd_info_message[512] = " ";

unsigned crossfade_seconds;
unsigned queue_length;
unsigned time_elapsed;
unsigned time_total;
unsigned length;
unsigned crossfade_on = 0;

float title_pos = 0;

bool show_kbps;
bool random_on = false;

struct mpd_connection *connection;
struct mpd_status     *status;
struct mpd_song       *current_song;

enum mpd_error mpderror;
enum mpd_state mpdstate;
enum mpd_state mpdstate_comp = MPD_STATE_UNKNOWN;

char *mpd_host;
char *run_on_song_change;
char *run_on_status_change;
char *run_on_display_act;
char *back_color;
char *front_color;
char *border_dark;
char *border_light;
char *button_color;

int position = 0;
int button_color_offset = 0;
int mpd_port;

void routine(int, char **);
int ping_mpd(void);
void DisplayRoutine(void);
void DrawPos(unsigned, unsigned);
void DrawTime(unsigned);
void DrawCrossfade(void);
void DrawRandom(void);
void DrawTitle(char *);
void DrawKbps(unsigned);
void DrawChar(wchar_t, int, int);
int DrawChars(char *, int, int);

typedef struct {
    wchar_t c;
    int x;
    int y;
} glyphdescr;

static glyphdescr glyphs[] = {
    {L'-', 67, 147}, {L'.', 73, 147}, {L'\x27', 79, 147},
    {L'(', 85, 147}, {L')', 91, 147}, {L'*', 97, 147}, {L'/', 103, 147},

    {L'0',  1, 147}, {L'1',  7, 147}, {L'2', 13, 147}, {L'3', 19, 147}, {L'4', 25, 147},
    {L'5', 31, 147}, {L'6', 37, 147}, {L'7', 43, 147}, {L'8', 49, 147}, {L'9', 55, 147},



    {L'A',  1, 137}, {L'a',  1, 137},
    {L'B',  7, 137}, {L'b',  7, 137},
    {L'C', 13, 137}, {L'c', 13, 137},
    {L'D', 19, 137}, {L'd', 19, 137},
    {L'E', 25, 137}, {L'e', 25, 137},

    {L'F', 31, 137}, {L'f', 31, 137},
    {L'G', 37, 137}, {L'g', 37, 137},
    {L'H', 43, 137}, {L'h', 43, 137},
    {L'I', 49, 137}, {L'i', 49, 137},
    {L'J', 55, 137}, {L'j', 55, 137},

    {L'K', 61, 137}, {L'k', 61, 137},
    {L'L', 67, 137}, {L'l', 67, 137},
    {L'M', 73, 137}, {L'm', 73, 137},
    {L'N', 79, 137}, {L'n', 79, 137},
    {L'O', 85, 137}, {L'o', 85, 137},

    {L'P', 91, 137}, {L'p', 91, 137},
    {L'Q', 97, 137}, {L'q', 97, 137},
    {L'R',103, 137}, {L'r',103, 137},
    {L'S',109, 137}, {L's',109, 137},
    {L'T',115, 137}, {L't',115, 137},

    {L'U',121, 137}, {L'u',121, 137},
    {L'V',127, 137}, {L'v',127, 137},
    {L'W',133, 137}, {L'w',133, 137},
    {L'X',139, 137}, {L'x',139, 137},
    {L'Y',145, 137}, {L'y',145, 137},

    {L'Z',151, 137}, {L'z',151, 137},


    {L'\x42e',  1, 157}, {L'\x44e',  1, 157}, /* cyrillic Yu */

    {L'\x410',  7, 157}, {L'\x430',  7, 157}, /* cyrillic A */
    {L'\x411', 13, 157}, {L'\x431', 13, 157}, /* cyrillic Be */
    {L'\x426', 19, 157}, {L'\x446', 19, 157}, /* cyrillic Ce */
    {L'\x414', 25, 157}, {L'\x434', 25, 157}, /* cyrillic De */
    {L'\x415', 31, 157}, {L'\x435', 31, 157}, /* cyrillic Ye */

    {L'\x424', 37, 157}, {L'\x444', 37, 157}, /* cyrillic eF */
    {L'\x413', 43, 157}, {L'\x433', 43, 157}, /* cyrillic Ge */
    {L'\x425', 49, 157}, {L'\x445', 49, 157}, /* cyrillic Ha */
    {L'\x418', 55, 157}, {L'\x438', 55, 157}, /* cyrillic I */
    {L'\x419', 61, 157}, {L'\x439', 61, 157}, /* cyrillic I-kratkoe */

    {L'\x41a', 67, 157}, {L'\x43a', 67, 157}, /* cyrillic Ka */
    {L'\x41b', 73, 157}, {L'\x43b', 73, 157}, /* cyrillic eL */
    {L'\x41c', 79, 157}, {L'\x43c', 79, 157}, /* cyrillic eM */
    {L'\x41d', 85, 157}, {L'\x43d', 85, 157}, /* cyrillic eN */
    {L'\x41e', 91, 157}, {L'\x43e', 91, 157}, /* cyrillic O */

    {L'\x41f', 97, 157}, {L'\x43f', 97, 157}, /* cyrillic Pe */
    {L'\x42f',103, 157}, {L'\x44f',103, 157}, /* cyrillic Ya */
    {L'\x420',109, 157}, {L'\x440',109, 157}, /* cyrillic eR */
    {L'\x421',115, 157}, {L'\x441',115, 157}, /* cyrillic eS */
    {L'\x422',121, 157}, {L'\x442',121, 157}, /* cyrillic Te */

    {L'\x423',127, 157}, {L'\x443',127, 157}, /* cyrillic U */
    {L'\x416',133, 157}, {L'\x436',133, 157}, /* cyrillic Je */
    {L'\x412',139, 157}, {L'\x432',139, 157}, /* cyrillic Ve */
    {L'\x42c',145, 157}, {L'\x44c',145, 157}, /* cyrillic MyagkijZnak */
    {L'\x42b',151, 157}, {L'\x44b',151, 157}, /* cyrillic Y */

    {L'\x417',157, 157}, {L'\x437',157, 157}, /* cyrillic Ze */
    {L'\x428',163, 157}, {L'\x448',163, 157}, /* cyrillic Sha */
    {L'\x42d',169, 157}, {L'\x44d',169, 157}, /* cyrillic E */
    {L'\x429',175, 157}, {L'\x449',175, 157}, /* cyrillic Scha */
    {L'\x427',181, 157}, {L'\x447',181, 157}, /* cyrillic Che */

    {L'\x42a',187, 157}, {L'\x44a',187, 157}, /* cyrillic TvyordyiZnak */
    {L'\x404',115, 147}, {L'\x454',115, 147}, /* ukrainian IE */
    {L'\x406', 49, 137}, {L'\x456', 49, 137}, /* ukrainian I */
    {L'\x407',109, 147}, {L'\x457',109, 147}, /* ukrainian YI */
    {L'\x491', 43, 157}, {L'\x490', 43, 157}, /* ukrainian GHE with upturn */

    {L'\x401',121, 147}, {L'\x451',121, 147}, /* cyrillic Yo */

    {L' ', 61, 147}
};

int ping_mpd() {
    static int previous_error_code = 0;
    static int connected = 0;

    if (connection) {
        mpd_connection_free(connection);
    }

    connection = mpd_connection_new(mpd_host, mpd_port, 0);

    mpderror = mpd_connection_get_error(connection);
    
    if (mpderror == MPD_ERROR_SUCCESS) {
        previous_error_code = 0;
        
        /* don't spam info message */
        if (!connected) {
            const unsigned *server_version = mpd_connection_get_server_version(connection);

            printf("Connected to MPD %i.%i.%i\n", server_version[0], server_version[1], server_version[2]);
            sprintf(mpd_info_message, "Connected to MPD %i.%i.%i", server_version[0], server_version[1], server_version[2]);
            da_log(INFO, "connected to MPD %i.%i.%i\n", server_version[0], server_version[1], server_version[2]);

            connected = 1;
        }
    
    } else {
        const char *error_message = mpd_connection_get_error_message(connection);
        
        /* don't spam error message */
        if ((int)mpderror != previous_error_code) {
            printf("Failed connecting to MPD (%s).\n", error_message);
            sprintf(mpd_info_message, error_message);
            da_log(ERR, "failed connecting to MPD (%s).\n", error_message);
        }
        
        previous_error_code = mpderror;
        connected = 0;
    }

    return mpderror;
}

void DisplayRoutine() {
    char *title = NULL;

    static int counter = 0;

    unsigned kbps = 0;
    
    /* Compute diplay */
    if (ping_mpd() != MPD_ERROR_SUCCESS) {
        title = strdup(mpd_info_message);

        da_copy_xpm_area(68, 4, 56, 32, 4, 4);
        
        DrawTitle(title);
    
    } else {
        status = mpd_status_begin();
        status = mpd_run_status(connection);
        
        if (status) {
            mpdstate = mpd_status_get_state(status);
        
            if (mpdstate_comp != mpdstate) {
                mpdstate_comp = mpdstate;

                if (run_on_status_change)
                    da_exec_command(run_on_status_change);
            }
        
            queue_length = mpd_status_get_queue_length(status);
            
            if ((mpdstate == MPD_STATE_PLAY) || (mpdstate == MPD_STATE_PAUSE)) {                
                current_song = mpd_run_current_song(connection);

                const char *song_title = mpd_song_get_tag(current_song, MPD_TAG_TITLE, 0);
                const char *song_file = mpd_song_get_uri(current_song);

                if (strcmp(title_comp, song_title ? song_title : "  ") != 0 || strcmp(song_file_comp, song_file) != 0) {
                    sprintf(title_comp, song_title ? song_title : "  ");
                    sprintf(song_file_comp, song_file);

                    title_pos = 0;

                    if (run_on_song_change)
                        da_exec_command(run_on_song_change);
                }

                title = strdup(song_title ? song_title : "  ");

                time_elapsed = mpd_status_get_elapsed_time(status);
                time_total   = mpd_status_get_total_time(status);

                if (time_total >= time_elapsed) {
                    length = time_total - time_elapsed;
                } else {
                    length = time_elapsed;
                }

                position = mpd_status_get_song_pos(status);
                
                if (position == -1)
                    position = 0;

                if (show_kbps) {
                    kbps = mpd_status_get_kbit_rate(status);

                    if (kbps > 0) {
                        counter++;
                    } else {
                        counter = 0;
                    }
                }

                mpd_song_free(current_song);
                
            } else { /* not playing or paused */
                title = strdup(mpd_info_message);
                
                counter = 0;
                length = 0;
            }

            /* Draw everything */
            DrawTime(length);
            DrawCrossfade();
            DrawRandom();
            DrawTitle(title);
            
            if (counter >= 30) {
                DrawKbps(kbps);
            } else {
                DrawPos(position, queue_length);
            }

            if (counter >= 60) {
                counter = 0;
            }
            /* !Draw everything */

            mpd_status_free(status);
        }
    }

    if (title) {
        free(title);
        
        title = NULL;
    }
}

void DrawPos(unsigned pos, unsigned total) {
    char posstr[16];
    char qlstr[16];
    
    char *p = posstr;
    char *pp = qlstr;
    
    int i = 1;

    pos += 1;

    if (pos > 999)
        pos = 0;
    if (total > 999)
        total = 0;
    
    sprintf(posstr, "%03d", pos);
    sprintf(qlstr, "%03d", total);

    /* Restore the '/' sign */
    da_copy_xpm_area(88, 17, 23, 8, 88 - 64, 17);

    for ( ; i < 4; i++) {
        da_copy_xpm_area((*p - '0') * 6 + 1, 65, 6, 7, i * 6, 17);
        da_copy_xpm_area((*pp - '0') * 6 + 1, 65, 6, 7, (i * 6) + 24, 17);
        
        p++;
        pp++;
    }
}

void DrawKbps(unsigned kbps) {
    char kbpstr[16];
    
    char *p = kbpstr;
    
    int i = 1;

    if (kbps > 999)
        kbps = 999;
    
    sprintf(kbpstr, "%03d", kbps);

    for ( ; i < 4; i++) {
        da_copy_xpm_area((*p - '0') * 6 + 1, 65, 6, 7, i * 6, 17);
        
        p++;
    }
    
    da_copy_xpm_area(82, 65, 23, 8, 88 - 64, 17);
}

void DrawTime(unsigned time) {
    char timestr[16];
    
    char *p = timestr;
    
    int i = 0, ii;

    /* 2 cases:
     *     up to 99 hours and 59 minutes and 59 seconds
     *     more
     */
    if (time > 360000) {
        sprintf(timestr, "%02d%02d%02d", 0, 0, 0);
    } else {
        sprintf(timestr, "%02d%02d%02d", time / 3600, time % 3600 / 60, time % 60);
    }

    for ( ; i < 6; i++) {
        if (i < 2)
            ii = (i * 7) + 6;
        if (i >= 2 && i < 4)
            ii = (i * 7) + 11;
        if (i >= 4 && i < 6)
            ii = (i * 7) + 16;
        
        da_copy_xpm_area((*p-'0')*7 + 2, 75, 7, 9, ii, 6);
        
        p++;
    }
}

void DrawCrossfade() {
    if (mpd_status_get_crossfade(status) > 0) {
        crossfade_on = 1;

        da_copy_xpm_area(71, 65, 5, 7, 51, 26);
        
    } else {
        crossfade_on = 0;

        da_copy_xpm_area(76, 65, 5, 7, 51, 26);
    }
}

void DrawRandom() {
    random_on = mpd_status_get_random(status);
    
    if (random_on) {
        da_copy_xpm_area(61, 65, 5, 7, 51, 17);
        
    } else {
        da_copy_xpm_area(66, 65, 5, 7, 51, 17);
    }
}

void DrawChar(wchar_t wc, int x, int y) {
    int i;
    
    for (i = 0; (long unsigned int)i < sizeof(glyphs)/sizeof(glyphdescr) - 1; ++i) {
        if (wc == glyphs[i].c)
            break;
    }
    
    da_copy_xpm_area(glyphs[i].x, glyphs[i].y, 6, 8, x, y);
}

int DrawChars(char *ttl, int tpos, int pos) {
    wchar_t wc;

    mbtowc(NULL, NULL, 0);
    
    while (*ttl && (pos <= (tpos + 6))) {
        int len = mbtowc(&wc, ttl, MB_CUR_MAX);
        
        ttl += len;
        
        if (pos >= tpos)
            DrawChar(wc, (pos - tpos)*6 + 6, 26);
        
        ++pos;
    }
    
    return pos;
}

void DrawTitle(char *name) {
    int len, pos, tpos = title_pos;

    if (name == NULL)
        return;

    len = pos = DrawChars(name, tpos, 0);

    if (pos < 6) {
        DrawChars("      ", tpos, pos);
        
        return;
    }

    if (pos <= tpos + 6)
        pos = DrawChars(SEPARATOR, tpos, pos);

    if (pos <= tpos + 6)
        DrawChars(name, tpos, pos);

    if (tpos >= (int)(len + strlen(SEPARATOR)))
        title_pos = 0;

    title_pos += SCROLL_SPEED;
}

void routine(int argc, char *argv[]) {
    setlocale(LC_ALL, "");

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, wmpd4_xpm, mask4_bits, mask4_width, mask4_height);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, wmpd3_xpm, mask3_bits, mask3_width, mask3_height);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, wmpd2_xpm, mask2_bits, mask2_width, mask2_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, wmpd_xpm, mask_bits, mask_width, mask_height);
        break;
    }
    
    /* button color */
    if (!strcmp(button_color, "normal")) {
        button_color_offset = 0;
        
    } else if (!strcmp(button_color, "dark")) {
        button_color_offset = 54;
        
    } else if (!strcmp(button_color, "light")) {
        button_color_offset = 108;
        
    } else {
        printf("WARN: wrong button color set, using normal.\n");
        da_log(WARN, "wrong button color set, using normal.\n");

        button_color_offset = 0;
    }

    da_copy_xpm_area(68, 4, 56, 32, 4, 4);
    da_copy_xpm_area(68, 38, 56, 22, 4, 38);
    da_copy_xpm_area(0 + button_color_offset, 115, 54, 20, 5, 39);

    /* add mouse region */
    da_add_mouse_region(0,  5, 39, 19, 48); /* prev */
    da_add_mouse_region(1, 19, 39, 33, 48); /* next */
    da_add_mouse_region(2, 33, 39, 46, 48); /* shuff */
    da_add_mouse_region(3, 46, 39, 59, 48); /* rand */
    da_add_mouse_region(4,  5, 48, 16, 59); /* crossf */
    da_add_mouse_region(5, 16, 48, 37, 59); /* play */
    da_add_mouse_region(6, 37, 48, 48, 59); /* pause */
    da_add_mouse_region(7, 48, 48, 59, 59); /* stop */
    da_add_mouse_region(8,  5,  5, 59, 35); /* displ act */

    /* Update the display */
    DisplayRoutine();

    da_redraw_window();

    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch (da_check_mouse_region()) {
            case 0:
                da_copy_xpm_area(0 + button_color_offset, 95, 14, 9, 5, 39);
                break;
                        
            case 1:
                da_copy_xpm_area(14 + button_color_offset, 95, 14, 9, 19, 39);
                break;

            case 2:
                da_copy_xpm_area(28 + button_color_offset, 95, 13, 9, 33, 39);
                break;

            case 3:
                da_copy_xpm_area(41 + button_color_offset, 95, 13, 9, 46, 39);
                break;

            case 4:
                da_copy_xpm_area(0 + button_color_offset, 104, 11, 11, 5, 48);
                break;
                        
            case 5:
                da_copy_xpm_area(11 + button_color_offset, 104, 21, 11, 16, 48);
                break;

            case 6:
                da_copy_xpm_area(32 + button_color_offset, 104, 11, 11, 37, 48);
                break;

            case 7:
                da_copy_xpm_area(43 + button_color_offset, 104, 11, 11, 48, 48);
                break;

            case 8:
            default:
                break;
            }
            
            da_redraw_window();
            
            break;

        case MOUSE_1_REL:
            switch (da_check_mouse_region()) {
            case 0:
                if (mpdstate == MPD_STATE_STOP) {
                    position--;

                    if (position < 0) {
                        position = queue_length - 1;
                    }
                }
                
                if ((mpdstate == MPD_STATE_PLAY) || (mpdstate == MPD_STATE_PAUSE)) {
                    int prev_one = position - 1;

                    if (prev_one < 0) {
                        prev_one = queue_length - 1;
                    }
    
                    mpd_send_play_pos(connection, prev_one);
                }

                break;
                        
            case 1:
                if (mpdstate == MPD_STATE_STOP) {
                    position++;

                    if (position > (int)(queue_length - 1)) {
                        position = 0;
                    }
                }

                if ((mpdstate == MPD_STATE_PLAY) || (mpdstate == MPD_STATE_PAUSE)) {
                    unsigned next_one = position + 1;

                    if (next_one > queue_length - 1) {
                        next_one = 0;
                    }
    
                    mpd_send_play_pos(connection, next_one);
                }

                break;

            case 2:
                mpd_run_shuffle(connection);
                break;

            case 3:
                random_on = !random_on;
                mpd_run_random(connection, random_on);
                break;

            case 4:
                crossfade_on = !crossfade_on;
                mpd_run_crossfade(connection, crossfade_on * crossfade_seconds);
                break;
                        
            case 5:
                mpd_send_play_pos(connection, position);
                break;

            case 6:
                mpd_send_toggle_pause(connection);
                break;

            case 7:
                mpd_send_stop(connection);
                break;

            case 8:
                if (run_on_display_act) {
                    da_exec_command(run_on_display_act);
                }
                break;
                        
            default:
                break;
            }
            
            /* reset buttons: */
            da_copy_xpm_area(0 + button_color_offset, 115, 54, 20, 5, 39);

            da_redraw_window();

            break;
        }

        DisplayRoutine();

        da_redraw_window();

        usleep(100000L);
    }

    routine(argc, argv);
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a WindowMaker dockapp that controls Music Player Demon.\n\n"
    
    "The display shows playing time (decreasing if it's a local file or increasing in case it's a stream), current item position in the playlist, total number of items, track title (or an info or error message), and indicators of the random and crossfade modes.\n\n"
    
    "The buttons run previous and next track switching, shuffling the playlist, turning the random and crossfade modes on and off, starting, pausing, unpausing, and stopping playback.\n\n"
    
    PROG_NAME" is based on wmusic, which was originally written by Bastien Nocera <hadess@hadess.net>.");
    
    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&mpd_host, "host", "-H", "--host", "localhost", "MPD host");
    da_init_integer(&mpd_port, "port", "-P", "--port", 0, 0, 6600, "MPD port");
    da_init_integer((int *)&crossfade_seconds, "crossfade_sec", "-c", "--crossfade-sec", 0, 0, 3, "Crossfade time in seconds");
    da_init_string(&button_color, "button_color", NULL, "--button-color", "normal", "Button color (can be normal, dark, or light)");
    da_init_string(&run_on_song_change, "on_song_change", NULL, "--on-song-change", NULL, "What to do on song change");
    da_init_string(&run_on_status_change, "on_status_change", NULL, "--on-status-change", NULL, "What to do on MPD status change");
    da_init_string(&run_on_display_act, "on_display_act", NULL, "--on-display-act", NULL, "What to do on display Mouse1 activation");
    da_init_bool(&show_kbps, "show_kbps", "-k", "--show-kbps", true, "Periodically show current bit rate in kbps (always off if value is unknown)");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);
    
    return 0;
}
