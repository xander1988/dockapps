/*  wmtime - Window Maker dockapp that displays the time and date
    Copyright (C) 1997, 1998 Martijn Pieterse <pieterse@xs4all.nl>
    Copyright (C) 1997, 1998 Antoine Nulle <warp@xs4all.nl>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    Code based on wmppp/wmifs

    [Orig WMMON comments]

    This code was mainly put together by looking at the
    following programs:

    asclock
        A neat piece of equip, used to display the date
        and time on the screen.
        Comes with every AfterStep installation.

        Source used:
            How do I create a not so solid window?
            How do I open a window?
            How do I use pixmaps?

    ------------------------------------------------------------

    Author: Martijn Pieterse (pieterse@xs4all.nl)

    This program is distributed under the GPL license.
    (as were asclock and pppstats)

    ----
    Changes:
    ----
   15/07/2008 (Paul Harris, harris.pc@gmail.com)
      * Minor changes to correct build warnings
    09/10/2003 (Simon Law, sfllaw@debian.org)
        * Add -geometry support
        * Add -noseconds support
        * Make the digital clock fill the space provided
        * Eliminated exploitable static buffers
    17/05/1998 (Antoine Nulle, warp@xs4all.nl)
        * Updated version number and some other minor stuff
    16/05/1998 (Antoine Nulle, warp@xs4all.nl)
        * Added Locale support, based on original diff supplied
          by Alen Salamun (snowman@hal9000.medinet.si)
    04/05/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Moved the hands one pixel down.
        * Removed the RedrawWindow out of the main loop
    02/05/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Removed a lot of code that was in the wmgeneral dir.
    02/05/1998 (Antoine Nulle, warp@xs4all.nl)
        * Updated master-xpm, hour dots where a bit 'off'
    30/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added anti-aliased hands
    23/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Changed the hand lengths.. again! ;)
        * Zombies were created, so added wait code
    21/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Added digital/analog switching support
    18/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
        * Started this project.
        * Copied the source from wmmon.
*/

#define _GNU_SOURCE
#include <iconv.h>                     /* for iconv, iconv_close, etc */
#include <langinfo.h>                  /* for nl_langinfo, ABDAY_1, etc */
#include <locale.h>                    /* for NULL, setlocale, LC_ALL */
#include <math.h>                      /* for floor, cos, sin, M_PI */
#include <stddef.h>                    /* for size_t */
#include <sys/wait.h>                  /* for waitpid, WNOHANG */
#include <time.h>                      /* for tm, time, localtime */
#include <unistd.h>                    /* for usleep */
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "XPM/wmtime-mask64.xbm"             /* for wmtime_mask_bits */
#include "XPM/wmtime-mask128.xbm"
#include "XPM/wmtime-mask192.xbm"
#include "XPM/wmtime-mask256.xbm"
#include "XPM/wmtime-master64.xpm"           /* for wmtime_master_xpm */
#include "XPM/wmtime-master128.xpm"
#include "XPM/wmtime-master192.xpm"
#include "XPM/wmtime-master256.xpm"

  /********************/
 /* Global Variables */
/********************/
char day_of_week[7][3] = { "SU", "MO", "TU", "WE", "TH", "FR", "SA" };
char mon_of_year[12][4] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *locale;
char *left_action;
char *middle_action;
char *right_action;

bool digital;
bool noseconds;

  /**************/
 /* Prototypes */
/**************/
void routine(int, char **);
void get_lang(void);
void DrawTime(int, int, int);
void DrawWijzer(int, int, int);
void DrawDate(int, int, int);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "WindowMaker dockapp that displays the time and date.\n\n"

    PROG_NAME" displays the time and date and gives you some nice additional features too. It is intended for docking in Window Maker. It currently provides:\n\n"

    "the time and date;\n"
    "a realtime morphing interface (analog <> digital mode);\n"
    "auto-scaled and anti-aliased hands;\n"
    "localization, displaying the day and date in various languages;\n"
    "three user-defined commands to launch.\n\n"

    "The "PROG_NAME" window is separated into top and bottom sections. The top section contains a clock and the bottom section contains the date.\n\n"

    "The clock can be toggled between analog and digital modes. To do this, click in the bottom section of the window.\n\n"

    PROG_NAME" can also be used to launch programs. You may click either left, middle, or right mouse buttons in the top section of the window. The pre-configured program will be launched according to the mouse button clicked.\n\n"

    PROG_NAME" will launch the appropriate command when you click on the clock.\n\n"

    "The administrator may choose to *fix* particular commands, making it impossible for users to change. These commands can be defined in /etc/"PROG_NAME"rc.fixed, although this isn't a nice thing to do.");
    
    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&locale, "locale", NULL, "--locale", NULL, "Set locale");
    da_init_bool(&digital, "digital", NULL, "--digital", false, "Display the digital clock on startup, instead of the analog clock");
    da_init_bool(&noseconds, "noseconds", "-n", "--noseconds", false, "Disables the second hand");
    da_init_string(&left_action, "left_action", NULL, "--left-action", NULL, "Set mouse left button action");
    da_init_string(&middle_action, "middle_action", NULL, "--middle-action", NULL, "Set mouse middle button action");
    da_init_string(&right_action, "right_action", NULL, "--right-action", NULL, "Set mouse right button action");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc");
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */
    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc.fixed");

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    struct tm *time_struct;

    long starttime;
    long curtime;
        
    if (setlocale(LC_ALL, locale) == NULL) {
        printf("locale \"%s\" not recognized; defaulting to \"%s\".", locale, setlocale(LC_ALL, NULL));
        da_log(ERR, "locale \"%s\" not recognized; defaulting to \"%s\".", locale, setlocale(LC_ALL, NULL));
    }
        
    get_lang();

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, wmtime_master256_xpm, wmtime_mask256_bits, wmtime_mask256_width, wmtime_mask256_height);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, wmtime_master192_xpm, wmtime_mask192_bits, wmtime_mask192_width, wmtime_mask192_height);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, wmtime_master128_xpm, wmtime_mask128_bits, wmtime_mask128_width, wmtime_mask128_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, wmtime_master64_xpm, wmtime_mask64_bits, wmtime_mask64_width, wmtime_mask64_height);
        break;
    }

    /* Mask out the right parts of the clock */
    da_copy_xpm_area(0, 0, 128, 64, 0, 98);   /* Draw the borders */

    /* add mouse region */
    da_add_mouse_region(0, 5, 48, 58, 60);
    da_add_mouse_region(1, 5, 5, 58, 46);

    starttime = time(0);

    curtime = time(0);
    time_struct = localtime(&curtime);

    while (!da_conf_changed(argc, argv)) {
        curtime = time(0);

        waitpid(0, NULL, WNOHANG);

        time_struct = localtime(&curtime);

        if (curtime >= starttime) {
            if (!digital) {
                /* Now to update the seconds */
                DrawWijzer(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);

                DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);

            } else {
                DrawTime(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);

                DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);
            }
            
            da_redraw_window();
        }

        switch (da_watch_xevent()) {
        case MOUSE_1_REL:
            switch (da_check_mouse_region()) {
            case 0:
                digital = !digital;

                if (digital) {
                    da_copy_xpm_area(68, da_windowed ? 4 : 102, 56, 42, 4, 4);
                    DrawTime(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);
                    DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);
                } else {
                    da_copy_xpm_area(4, da_windowed ? 4 : 102, 56, 42, 4, 4);
                    DrawWijzer(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);
                    DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);
                }
                
                da_redraw_window();

                break;

            case 1:
                if (left_action)
                    da_exec_command(left_action);
                break;

            default:
                break;
            }

            break;

        case MOUSE_2_REL:
            switch (da_check_mouse_region()) {
            case 0:
                break;

            case 1:
                if (middle_action)
                    da_exec_command(middle_action);
                break;

            default:
                break;
            }
            
            break;

        case MOUSE_3_REL:
            switch (da_check_mouse_region()) {
            case 0:
                break;

            case 1:
                if (right_action)
                    da_exec_command(right_action);
                break;

            default:
                break;
            }
            
            break;
        }

        /* Sleep 0.3 seconds */
        usleep(300000L);
    }

    routine(argc, argv);
}

  /************/
 /* get_lang */
/************/
void get_lang(void)
{
    char langbuf[10], outbuf[10];
    char *inp, *outp;
    iconv_t icd;
    int i, ret;
    size_t insize, outsize;

    icd = iconv_open("ASCII//TRANSLIT", nl_langinfo(CODESET));
    
    if (icd == (iconv_t) -1) {
        printf("WARN: error allocating charset conversion descriptor.\n");
        da_log(WARN, "error allocating charset conversion descriptor.\n");
        return;
    }

    for (i = 0; i < 7; i++) {
        snprintf(langbuf, 10, nl_langinfo(ABDAY_1 + i));
        insize = outsize = 10;
        inp = langbuf;
        outp = outbuf;
        do {
            ret = iconv(icd, &inp, &insize, &outp, &outsize);
        } while (outsize > 0 && ret > 0);
        if (strstr(outbuf,"?") != NULL) return;
        for (outp = outbuf, outsize = 0; *outp != 0 && outsize < 2;
            outp++, outsize++)
            day_of_week[i][outsize] = toupper(*outp);
    }
    for (i = 0; i < 12; i++) {
        snprintf(langbuf, 10, nl_langinfo(ABMON_1 + i));
        insize = outsize = 10;
        inp = langbuf;
        outp = outbuf;
        do {
            ret = iconv(icd, &inp, &insize, &outp, &outsize);
        } while (outsize > 0 && ret > 0);
        if (strstr(outbuf,"?") != NULL) return;
        for (outp = outbuf, outsize = 0; *outp != 0 && outsize < 3;
            outp++, outsize++)
            mon_of_year[i][outsize] = toupper(*outp);
    }

    iconv_close(icd);
}

/*******************************************************************************\
|* DrawTime                                                                    *|
\*******************************************************************************/

void DrawTime(int hr, int min, int sec) {
#define TIME_SIZE 16
    char    time[TIME_SIZE];
    char    *p = time;
    int     i,j,k=6;
    int numfields;

    da_copy_xpm_area(68, da_windowed ? 4 : 102, 56, 42, 4, 4);

    /* 7x13 */

    if (noseconds) {
        snprintf(time, TIME_SIZE, "%02d:%02d ", hr, min);
        numfields = 2;
    }
    else {
        snprintf(time, TIME_SIZE, "%02d:%02d:%02d ", hr, min, sec);
        numfields = 3;
    }

    for (i=0; i < numfields; i++) {
        for (j=0; j<2; j++) {
            da_copy_xpm_area((*p-'0')*7 + 1, 84, 8, 13, k, 18);
            k += 7;
            p++;
        }
        if (*p == ':') {
            da_copy_xpm_area(71, 84, 5, 13, k, 18);
            k += 4;
            p++;
        }
    }
}

/*******************************************************************************\
|* DrawDate                                                                    *|
\*******************************************************************************/

void DrawDate(int wkday, int dom, int month) {
#define DATE_SIZE 16
    char    date[DATE_SIZE];
    char    *p = date;
    int     i,k;

    /* 7x13 */

    snprintf(date, DATE_SIZE,
            "%.2s%02d%.3s  ", day_of_week[wkday], dom, mon_of_year[month]);

    k = 5;
    for (i=0; i<2; i++) {
        if (*p < 'A')
            da_copy_xpm_area((*p-'0')*6, 64, 6, 9, k, 49);
        else
            da_copy_xpm_area((*p-'A')*6, 74, 6, 9, k, 49);
        k += 6;
        p++;
    }
    k = 23;
    for (i=0; i<2; i++) {
        da_copy_xpm_area((*p-'0')*6, 64, 6, 9, k, 49);
        k += 6;
        p++;
    }
    da_copy_xpm_area(61, 64, 4, 9, k, 49);
    k += 4;
    for (i=0; i<3; i++) {
        if (*p < 'A')
            da_copy_xpm_area((*p-'0')*6, 64, 6, 9, k, 49);
        else
            da_copy_xpm_area((*p-'A')*6, 74, 6, 9, k, 49);
        k += 6;
        p++;
    }
}

/*******************************************************************************\
|* DrawWijzer                                                                  *|
\*******************************************************************************/

void DrawWijzer(int hr, int min, int sec) {

    double      psi;
    int         dx,dy;
    int         x,y;
    int         ddx,ddy;
    int         adder;
    int         k;

    int         i;

    hr %= 12;

    da_copy_xpm_area(4, da_windowed ? 4 : 102, 56, 42, 4, 4);

    /**********************************************************************/
    psi = hr * (M_PI / 6.0);
    psi += min * (M_PI / 360);

    dx = floor(sin(psi) * 22 * 0.7 + 0.5);
    dy = floor(-cos(psi) * 16 * 0.7 + 0.5);

    /* dx, dy is het punt waar we naar toe moeten.
     * Zoek alle punten die ECHT op de lijn liggen: */

    ddx = 1;
    ddy = 1;
    if (dx < 0) ddx = -1;
    if (dy < 0) ddy = -1;

    x = 0;
    y = 0;

    if (abs(dx) > abs(dy)) {
        if (dy != 0)
            adder = abs(dx) / 2;
        else
            adder = 0;
        for (i=0; i<abs(dx); i++) {
            /* laat de kleur afhangen van de adder.
             * adder loopt van abs(dx) tot 0 */

            k = 12 - adder / (abs(dx) / 12.0);
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 - ddy);

            da_copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

            k = 12-k;
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 + ddy);


            x += ddx;

            adder -= abs(dy);
            if (adder < 0) {
                adder += abs(dx);
                y += ddy;
            }
        }
    } else {
        if (dx != 0)
            adder = abs(dy) / 2;
        else
            adder = 0;
        for (i=0; i<abs(dy); i++) {
            k = 12 - adder / (abs(dy) / 12.0);
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31 - ddx, y + 24);

            da_copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

            k = 12-k;
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31 + ddx, y + 24);

            y += ddy;

            adder -= abs(dx);
            if (adder < 0) {
                adder += abs(dy);
                x += ddx;
            }
        }
    }
    /**********************************************************************/
    psi = min * (M_PI / 30.0);
    psi += sec * (M_PI / 1800);

    dx = floor(sin(psi) * 22 * 0.55 + 0.5);
    dy = floor(-cos(psi) * 16 * 0.55 + 0.5);

    /* dx, dy is het punt waar we naar toe moeten.
     * Zoek alle punten die ECHT op de lijn liggen: */

    dx += dx;
    dy += dy;

    ddx = 1;
    ddy = 1;
    if (dx < 0) ddx = -1;
    if (dy < 0) ddy = -1;

    x = 0;
    y = 0;

    if (abs(dx) > abs(dy)) {
        if (dy != 0)
            adder = abs(dx) / 2;
        else
            adder = 0;
        for (i=0; i<abs(dx); i++) {
            /* laat de kleur afhangen van de adder.
             * adder loopt van abs(dx) tot 0 */

            k = 12 - adder / (abs(dx) / 12.0);
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 - ddy);

            da_copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

            k = 12-k;
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 + ddy);


            x += ddx;

            adder -= abs(dy);
            if (adder < 0) {
                adder += abs(dx);
                y += ddy;
            }
        }
    } else {
        if (dx != 0)
            adder = abs(dy) / 2;
        else
            adder = 0;
        for (i=0; i<abs(dy); i++) {
            k = 12 - adder / (abs(dy) / 12.0);
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31 - ddx, y + 24);

            da_copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

            k = 12-k;
            da_copy_xpm_area(79+k, 67, 1, 1, x + 31 + ddx, y + 24);

            y += ddy;

            adder -= abs(dx);
            if (adder < 0) {
                adder += abs(dy);
                x += ddx;
            }
        }
    }
    /**********************************************************************/
    if (noseconds)
            return;    /* Skip drawing the seconds. */

    psi = sec * (M_PI / 30.0);

    dx = floor(sin(psi) * 22 * 0.9 + 0.5);
    dy = floor(-cos(psi) * 16 * 0.9 + 0.5);

    /* dx, dy is het punt waar we naar toe moeten.
     * Zoek alle punten die ECHT op de lijn liggen: */

    ddx = 1;
    ddy = 1;
    if (dx < 0) ddx = -1;
    if (dy < 0) ddy = -1;

    if (dx == 0) ddx = 0;
    if (dy == 0) ddy = 0;

    x = 0;
    y = 0;


    if (abs(dx) > abs(dy)) {
        if (dy != 0)
            adder = abs(dx) / 2;
        else
            adder = 0;
        for (i=0; i<abs(dx); i++) {
            /* laat de kleur afhangen van de adder.
             * adder loopt van abs(dx) tot 0 */

            k = 12 - adder / (abs(dx) / 12.0);
            da_copy_xpm_area(79+k, 70, 1, 1, x + 31, y + 24 - ddy);

            k = 12-k;
            da_copy_xpm_area(79+k, 70, 1, 1, x + 31, y + 24);


            x += ddx;

            adder -= abs(dy);
            if (adder < 0) {
                adder += abs(dx);
                y += ddy;
            }
        }
    } else {
        if (dx != 0)
            adder = abs(dy) / 2;
        else
            adder = 0;
        for (i=0; i<abs(dy); i++) {
            k = 12 - adder / (abs(dy) / 12.0);
            da_copy_xpm_area(79+k, 70, 1, 1, x + 31 - ddx, y + 24);

            k = 12-k;
            da_copy_xpm_area(79+k, 70, 1, 1, x + 31, y + 24);

            y += ddy;

            adder -= abs(dx);
            if (adder < 0) {
                adder += abs(dy);
                x += ddx;
            }
        }
    }
}

/* vim: sw=4 ts=4 columns=82
 */
