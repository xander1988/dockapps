/* wmchaosgamae - Window Maker dockapp displaying a chaos game
 * Copyright (C) 2017 Doug Torrance <dtorrance@piedmont.edu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#include <math.h>
#include <time.h>
#include <libdockapp4/dockapp.h>

#include "config.h"

#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define POINT_COLOR "light sea green"
#define AREA_WIDTH 54
#define AREA_HEIGHT 54
#define NUM_VERTICES 3
#define FRACTION 0.5
#define TIMEOUT 250
#define MAXPOINTS 2500

Pixmap pix;

XPoint *vertices;
XPoint point;

char *front_color;
char *back_color;
char *border_dark;
char *border_light;

int n;
int timeout;
int maxpoints;

double r;

void next_point(void);
void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp displaying a chaos game.\n\n"

    "AUTHORS\n\n"

    "Copyright (C) 2017 Doug Torrance <dtorrance@piedmont.edu>\n"

    "Copyright (C) 2021 Xander");

    da_init_string(&front_color, "front_color", "-f", "--front-color", POINT_COLOR, "Point color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_integer(&n, "vertices", "-n", "--vertices", 0, 0, NUM_VERTICES, "Number of vertices");
    da_init_float(&r, "fraction", "-r", "--fraction", 0.1, 0.9, FRACTION, "Fraction of distance for next pt");
    da_init_integer(&timeout, "time", "-t", "--time", 0, 0, TIMEOUT, "Time (ms) before drawing next pt");
    da_init_integer(&maxpoints, "maxpoints", "-m", "--maxpoints", 0, 0, MAXPOINTS, "Max # of points before restarting");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
	int i;

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "point_color");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
    }

    pix = XCreatePixmap(da_display, da_root_win, AREA_WIDTH * da_scale, AREA_HEIGHT * da_scale, da_display_depth);
	
	srand(time(NULL));

	vertices = malloc(n * sizeof(XPoint));
    
	for (i = 0; i < n; i++) {
		vertices[i].x = round(AREA_WIDTH / 2 * (1 + cos(M_PI * (0.5 + 2.0 * i / n))));
		vertices[i].y = round(AREA_HEIGHT / 2 * (1 - sin(M_PI * (0.5 + 2.0 * i / n))));
	}

	point.x = rand() % AREA_WIDTH;
	point.y = rand() % AREA_HEIGHT;

    da_set_foreground(0);

    da_fill_xpm_rectangle(pix, 0, 0, AREA_WIDTH, AREA_HEIGHT);

    da_set_foreground(1);

	da_fill_xpm_rectangle(pix, point.x, point.y, 1, 1);

    da_copy_from_xpm(pix, 0, 0, AREA_WIDTH, AREA_HEIGHT, 5, 5);
    
	da_redraw_window();

	while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            /* TODO: do something */
            break;
        }

        next_point();

        usleep(timeout * 1000);
    }

    XFreePixmap(da_display, pix);

    routine(argc, argv);
}

void next_point(void) {
	int i;
	static int count = 0;

	i = rand() % n;
    
	point.x = round(point.x + r * (vertices[i].x - point.x));
	point.y = round(point.y + r * (vertices[i].y - point.y));

    da_set_foreground(1);

	da_fill_xpm_rectangle(pix, point.x, point.y, 1, 1);

    da_copy_from_xpm(pix, 0, 0, AREA_WIDTH, AREA_HEIGHT, 5, 5);
    
	da_redraw_window();

	count++;
    
	if (count == maxpoints) {
        da_set_foreground(0);
        
		da_fill_xpm_rectangle(pix, 0, 0, AREA_WIDTH, AREA_HEIGHT);

        da_copy_from_xpm(pix, 0, 0, AREA_WIDTH, AREA_HEIGHT, 5, 5);
        
		count = 0;
	}
}
