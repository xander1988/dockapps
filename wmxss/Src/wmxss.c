/*
 *
 *  	wmxss-0.10 (C) 1999 Mike Henderson (mghenderson@lanl.gov)
 *
 *  		- Its a DockApp front end for xscreensaver
 *
 *
 *
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2, or (at your option)
 * 	any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program (see the file COPYING); if not, write to the
 * 	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *      Boston, MA  02111-1307, USA
 *
 *
 *      Changes:
 *
 *	Version 0.10  -	released Aug 11, 1999.
 *			just playing around right now....
 *
 *
 */

/*
 *   Includes
 */
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"
#include "bitmaps/mask.xbm"
#include "bitmaps/mask2.xbm"
#include "bitmaps/mask3.xbm"
#include "bitmaps/mask4.xbm"

/*
 *  Delay between refreshes (in microseconds)
 */
#define DELAY 10000L

char *border_dark;
char *border_light;
char *ExecuteCommand;

void routine(int, char **);

/*
 *   main
 */
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a DockApp front end for xscreensaver.\n\n"

    "Right now it only runs the separate hacks in the DockApp. Example: "PROG_NAME" -c xflame.");
    
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&ExecuteCommand, "command", "-c", "--command", NULL, "Command to execute via click of mouse button 1");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");    /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                       /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, mask4_bits, mask4_width, mask4_height);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, mask3_bits, mask3_width, mask3_height);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, mask2_bits, mask2_width, mask2_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, mask_bits, mask_width, mask_height);
        break;
    }

    char command[256];
    
    while (!da_conf_changed(argc, argv)) {
        /*
         *   Process any pending X events.
         */
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (ExecuteCommand) {
                sprintf(command, "%s -window-id 0x%x", ExecuteCommand, da_windowed ? (int)da_active_win : (int)da_icon_win);
                da_exec_command(command);
            } else {
                da_exec_command("xscreensaver-demo");
            }
            break;
        }

        usleep(DELAY);
    }

    routine(argc, argv);
}
