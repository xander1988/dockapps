/* cnslock
 * Copyright (C) 2002 Simon Hunter (lists@sprig.dyn.dhs.org)
 * Copyright (C) 2017 Window Maker Team (wmaker-dev@googlegroups.com)
 *
 * cnslock is a dock application that displays the current state of the
 * three lock keys (caps, num, and scroll)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have receive a copy of the GNU General Public License along with
 * this program; if you still want it, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Hacked from various wm applets.
 *
 */

/* general includes */
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "kleds.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"
#include "bitmaps/mask.xbm"
#include "bitmaps/mask2.xbm"
#include "bitmaps/mask3.xbm"
#include "bitmaps/mask4.xbm"

/* stuff */
void cnslock_init(void);
void cnslock_update(void);
void routine(int, char **);

/* globals */
char *front_color;
char *back_color;
char *border_dark;
char *border_light;

/* the main routine */
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dock application (dockapp) which provides a visual indication of the states of the three \"lock\" buttons (caps, num, and scroll).\n\n"

    PROG_NAME" was written so that I could see the state of those keys as I have a wireless keyboard with no leds on it. If you discover any bug in this software, please send a bugreport to wmaker-dev@googlegroups.com and describe the problem with as many details as possible.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, mask4_bits, mask4_width, mask4_height);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, mask3_bits, mask3_width, mask3_height);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, mask2_bits, mask2_width, mask2_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, mask_bits, mask_width, mask_height);
        break;
    }

    cnslock_init();
    
    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            /* TODO: do something */
            break;
        }

        cnslock_update();

        usleep(100000L);
    }

    routine(argc, argv);
}

void cnslock_init(void) {
    int status;

    status = check_kleds();

    da_copy_xpm_area(134, 6, 52, 52, 6, 6);
    
    da_redraw_window();

    if (status & 1)
        da_copy_xpm_area(70, 6, 52, 16, 6, 6);
    if (status & 2)
        da_copy_xpm_area(70, 24, 52, 16, 6, 24);
    if (status & 3)
        da_copy_xpm_area(70, 42, 52, 16, 6, 42);
}

/* update caps, num, scroll lock */
void cnslock_update(void) {
    static int status;
    int new_status;

    new_status = check_kleds();

    if ((status & 1) != (new_status & 1)) {
        if ((new_status & 1) == 1)
            da_copy_xpm_area(70, 6, 52, 16, 6, 6);
        else
            da_copy_xpm_area(134, 6, 52, 16, 6, 6);
    }
    if ((status & 2) != (new_status & 2)) {
        if ((new_status & 2) == 2)
            da_copy_xpm_area(70, 24, 52, 16, 6, 24);
        else
            da_copy_xpm_area(134, 24, 52, 16, 6, 24);
    }
    if ((status & 4) != (new_status & 4)) {
        if ((new_status & 4) == 4)
            da_copy_xpm_area(70, 42, 52, 16, 6, 42);
        else
            da_copy_xpm_area(134, 42, 52, 16, 6, 42);
    }

    status = new_status;

    da_redraw_window();
}
