/*
   This code is based upon some lines(actually two lines :-)
   in E-Leds by Mathias Meisfjordskar<mathiasm@ifi.uio.no>
   Released under GPL.
*/

#include <stdio.h>
#include <X11/XKBlib.h>
#include <libdockapp4/dockapp.h>

#include "kleds.h"

/*
  Returns the turned on leds:
   Bit 0 is Capslock
   Bit 1 is Numlock
   Bit 2 is Scrollock
*/
int check_kleds(void) {
    unsigned int states;

    if (XkbGetIndicatorState(da_display, XkbUseCoreKbd, &states) != Success) {
		printf("Error while reading Indicator status.\n");
        da_log(ERR, "error while reading Indicator status.\n");
		return -1;
    }
    
    return (states & 0x7);
}
