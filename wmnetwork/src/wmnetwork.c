/*
    Copyright (C) 2022 Xander

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define IFNAMESIZE 10
#define BUFFER_SIZE 512

struct IFR {
    char name[IFNAMESIZE];
    long inp_bytes;
    long inp_packets;
    long out_bytes;
    long out_packets;
} ifr;

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *active_ifr;
char *left_action;
char *right_action;
char *middle_action;

int sample_int;

void routine(int, char **);
int get_data(int);
bool still_online(char *);
void draw_active_iface(void);
void draw_led(void);
void draw_stats(void);
int set_active_if(void);
void clear_graph(void);
void fill_graph(int, int, bool);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockable network traffic monitor.\n\n"

    PROG_NAME" is a dockable applet for X11 that can monitor all your network interfaces. Loosely based on wmifs.\n\n"

    PROG_NAME" is a complete network monitoring dock.app, it's mainly designed for usage in WindowMaker's dock and gives you some nice & nifty features like: autosensing of *ALL* active network interfaces; separate stat regions (the upper for the incoming traffic, the lower for the outcoming); realtime cycling through active interfaces by simply clicking on the (interface) gadget; integrated RX/TX interface activity LEDs; integrated interface status LED; commandline options to force monitoring a particular interface, even 'lo' is supported; user-definable scripts for left/middle/right mouse buttons which are read from ~/."PROG_NAME"rc (optional).\n\n"

    PROG_NAME" has a special -I option, this way you can force "PROG_NAME" to monitor a particular interface like:\n\n"

    PROG_NAME" -I eth0\n"
    PROG_NAME" -I ppp0\n"
    PROG_NAME" -I lo\n\n"

    "Without the -I option ("PROG_NAME" &) "PROG_NAME" automagicly grabs the default interface and will display the name and statistics of that interface.\n\n"

    "You can cycle in realtime through all available active interfaces by simply clicking with the left mousebutton on the interface name gadget in the upper part of "PROG_NAME".");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#51c300", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&left_action, "left_action", NULL, "--left-action", NULL, "Left action");
    da_init_string(&middle_action, "middle_action", NULL, "--middle-action", NULL, "Middle action");
    da_init_string(&right_action, "right_action", NULL, "--right-action", NULL, "Right action");
    da_init_string(&active_ifr, "interface", "-I", "--interface", NULL, "Interface that should come up initially when executing "PROG_NAME"; if you use \"auto\" as interface name, the first active (\"up\") interface will be used");
    da_init_integer(&sample_int, "interval", "-i", "--interval", 0, 0, 500, "UI update interval, in ms");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc"); 
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    int current_if = 0;
    int total_ifs = 0;
    
    unsigned int curtime;
	unsigned int nexttime;
	
	struct timeval tv, tv2;

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 75, back_color, 25, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 160 * da_scale, 83 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 160 * da_scale, 83 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 160 * da_scale, 83 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 160 * da_scale, 83 * da_scale);
        break;
    }

     /* > Button */
    da_add_mouse_region(0, 5, 5, 59, 14);
    da_add_mouse_region(1, 5, 19, 59, 59);
    
    gettimeofday(&tv2, NULL);
    
	nexttime = sample_int;
	
	current_if = set_active_if();

    while (!da_conf_changed(argc, argv)) {
        gettimeofday(&tv, NULL);
        
		curtime = (tv.tv_sec - tv2.tv_sec) * 1000 + (tv.tv_usec - tv2.tv_usec) / 1000;
			
        waitpid(0, NULL, WNOHANG);
        
        total_ifs = get_data(current_if);
        
        if (curtime >= nexttime) {
			nexttime = curtime + sample_int;
            
            draw_led();
        
            draw_stats();
        }
        
        draw_active_iface();
        
        da_redraw_window();

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch (da_check_mouse_region()) {
            case 0:
                /* Next if */
                current_if++;
                
                if (current_if >= total_ifs)
                    current_if = 0;
                
                clear_graph();
                    
                get_data(current_if);
                
                draw_led();
        
                draw_stats();

                draw_active_iface();

                da_redraw_window();
                
                break;

            case 1:
                if (left_action)
                    da_exec_command(left_action);
                        
                break;
            }
            
            break;

        case MOUSE_2_PRS:
            if (da_check_mouse_region() == 1) {
                if (middle_action)
                    da_exec_command(middle_action);
            }
            break;
            
        case MOUSE_3_PRS:
            if (da_check_mouse_region() == 1) {
                if (right_action)
                    da_exec_command(right_action);
            }
            break;

        default:
            break;
        }
        
        
        
        usleep(200000);
    }
    
    routine(argc, argv);
}

void draw_active_iface(void) {
    size_t len;
    
    int i, k, c;

    /* Clear area */
    da_copy_xpm_area(70, 64, 54, 10, 5, 5);

    len = strlen(ifr.name);
    
    switch (len) {
    case 6:
        k = 11;
        
        break;
        
    case 2:
        k = 24;
        
        break;
        
    default:
        k = 5;
        
        break;
    }
    
    for (i = 0; ifr.name[i]; i++) {
        c = toupper(ifr.name[i]);
        
        if (i >= 7) {
            break;
        
        } else {
            if (c >= 'A' && c <= 'Z') {
                c -= 'A';
            
                da_copy_xpm_area(c * 7, 74, 7, 9, k, 5);
            
                k += 7;
        
            } else {
                c -= '0';
            
                da_copy_xpm_area(c * 7, 64, 7, 9, k, 5);
            
                k += 7;
            }
        }
    }
}

void draw_led(void) {
    static long inp_packets = 0;
    static long out_packets = 0;
    
    bool online;
    
    online = false;
    
    online = still_online(ifr.name);
    
    if (!online && strcmp(ifr.name, "lo")) {
        da_copy_xpm_area(82, 0, 4, 4, 54, 32);
        da_copy_xpm_area(82, 0, 4, 4, 54, 54);
        
        return;
    }
    
    /* Receive */
    if (inp_packets == ifr.inp_packets) {
        da_copy_xpm_area(74, 0, 4, 4, 54, 32);
    } else {
        da_copy_xpm_area(78, 0, 4, 4, 54, 32);
    }
    
    inp_packets = ifr.inp_packets;
    
    /* Transmit */
    if (out_packets == ifr.out_packets) {
        da_copy_xpm_area(74, 0, 4, 4, 54, 54);
    } else {
        da_copy_xpm_area(78, 0, 4, 4, 54, 54);
    }
    
    out_packets = ifr.out_packets;
}

void draw_stats(void) {    
    static long prev_in_bytes = 0;
    static long prev_out_bytes = 0;
    
    long cur_in_bytes;
    long cur_out_bytes;
    
    bool online;
    
    online = false;
    
    online = still_online(ifr.name);
    
    if (!online && strcmp(ifr.name, "lo")) {
        clear_graph();
        
        return;
    }
    
    /* Receive */
    cur_in_bytes = ifr.inp_bytes - prev_in_bytes;
    
    prev_in_bytes = ifr.inp_bytes;
    
    cur_in_bytes /= 500;
    
    if (cur_in_bytes > 9)
        cur_in_bytes = 9;
    if (cur_in_bytes < 0)
        cur_in_bytes = 0;
    
    /* Transmit */
    cur_out_bytes = ifr.out_bytes - prev_out_bytes;
    
    prev_out_bytes = ifr.out_bytes;
    
    cur_out_bytes /= 500;
    
    if (cur_out_bytes > 9)
        cur_out_bytes = 9;
    if (cur_out_bytes < 0)
        cur_out_bytes = 0;
        
    /* Draw */
    fill_graph(1, cur_in_bytes, false);
     
    fill_graph(1, cur_out_bytes, true);
    
    if (da_windowed)
        da_copy_wm_area(10, 20, 43, 17, 6, 20);
    else
        da_copy_xpm_area(10, 20, 43, 17, 6, 20);
        
    if (da_windowed)
        da_copy_wm_area(10, 42, 43, 17, 6, 42);
    else
        da_copy_xpm_area(10, 42, 43, 17, 6, 42);
}

bool still_online(char *ifs) {
    FILE *fp;
    
    bool i;

    i = false;
    
    fp = fopen("/proc/net/route", "r");
    
    if (fp) {
        char temp[BUFFER_SIZE];

        while (fgets(temp, BUFFER_SIZE, fp)) {
            if (strstr(temp, ifs)) {
                i = true; /* Line is alive */
                
                break;
            }
        }
        
        fclose(fp);
    }
    
    return i;
}

int get_data(int num) {
    FILE *file;
    
    char *line;
    
    size_t p, len = 0;
    
    ssize_t read;

    int total = 0, a, s, d, f, g, h, j, k, l, z, x, c, linenum = 0;
    
    file = fopen("/proc/net/dev", "r");

    while ((read = getline(&line, &len, file)) != -1) {
        linenum++;
        
        if (linenum > 2) {
            total++;
            
            if (linenum - 3 == num) {
                sscanf(line, "%s %li %li %i %i %i %i %i %i %li %li %i %i %i %i %i %i", ifr.name, &ifr.inp_bytes, &ifr.inp_packets, &a, &s, &d, &f, &g, &h, &ifr.out_bytes, &ifr.out_packets, &j, &k, &l, &z, &x, &c);
                
                /* Remove trailing ':' */
                p = strlen(ifr.name);
                
                if (ifr.name[p - 1] == ':')
                    ifr.name[p - 1] = '\0';
            }
        }
    }
    
    if (file) {
        fclose(file);
    }
    
    if (line) {
        free(line);

        line = NULL;
    }
    
    return total;
}

int set_active_if(void) {
    int v, total = 0;
    
    if (!active_ifr)
        return 0;
    
    total = get_data(0);
    
    for (v = 0; v < total; v++) {
        get_data(v);
        
        if (!strcmp("auto", active_ifr)) {
            if (still_online(ifr.name))
                return v;
        }
        
        if (!strcmp(ifr.name, active_ifr)) {
            return v;
        }
    }
    
    return 0;
}

void clear_graph(void) {
    int m, n = 0;
    
    for (m = 0; m < 12; m++) {
        da_copy_xpm_area(64, 0, 4, 18, 5 + n, 19);
        da_copy_xpm_area(64, 0, 4, 18, 5 + n, 41);
            
        n += 4;
    }
}

void fill_graph(int pos, int value, bool transmit) {
    int l, p = 53;
    
    for (l = 1; l <= pos; l++) {
        p -= 4;
    }
    
    /* Clear column */
    da_copy_xpm_area(64, 1, 4, 17, p, transmit ? 42 : 20);
    
    /* Fill */
    switch (value) {
    case 9:
        da_copy_xpm_area(69, 1, 4, 17, p, transmit ? 42 : 20);
        break;
    case 8:
        da_copy_xpm_area(69, 3, 4, 15, p, transmit ? 44 : 22);
        break;
    case 7:
        da_copy_xpm_area(69, 5, 4, 13, p, transmit ? 46 : 24);
        break;
    case 6:
        da_copy_xpm_area(69, 7, 4, 11, p, transmit ? 48 : 26);
        break;
    case 5:
        da_copy_xpm_area(69, 9, 4, 9, p, transmit ? 50 : 28);
        break;
    case 4:
        da_copy_xpm_area(69, 11, 4, 7, p, transmit ? 52 : 30);
        break;
    case 3:
        da_copy_xpm_area(69, 13, 4, 5, p, transmit ? 54 : 32);
        break;
    case 2:
        da_copy_xpm_area(69, 15, 4, 3, p, transmit ? 56 : 34);
        break;
    case 1:
        da_copy_xpm_area(69, 17, 4, 1, p, transmit ? 58 : 36);
        break;
    default:
        break;
    }
}
