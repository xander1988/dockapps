#!/bin/bash

CC=gcc
CC_MAIN_OPTS=(-O2 -Wall -Wextra -Wpedantic -lXpm -lX11 -lXext -ldockapp4)
CC_ADD_OPTS=(-lm)
C_FILES=(wmnetload.c utils.c)
C_ADD_FILE=""

PROG_NAME=none
PROG_VERSION=0.0

TARGET_DIR=/usr/local/bin

INSTALL=false
UNINSTALL=false

SHOW_HELP=false

# Dockapp-specific options
HAVE_ULONGLONG_T=false
HAVE_SOCKADDR_SA_LEN=false
HAVE_IPV6=false

# Get OS type
if [[ $OSTYPE == "linux-gnu" ]]; then
    C_ADD_FILE=ifstat_linux.c;
elif [[ $OSTYPE == "netbsd" ]]; then
    C_ADD_FILE=ifstat_netbsd.c;
    CC_ADD_OPTS+=(-lkvm);
elif [[ $OSTYPE == "solaris" ]]; then
    C_ADD_FILE=ifstat_solaris.c;
    CC_ADD_OPTS+=(-lkstat);
elif [[ $OSTYPE == "freebsd" ]]; then
    C_ADD_FILE=ifstat_freebsd.c;
else
    echo "Sorry, ${OSTYPE} is not supported yet.";
    exit 1;
fi

# Read PROG_NAME and PROG_VERSION from config.h
PREV=none
if [ ! -f config.h ]; then
    touch config.h;
fi
while read LINE; do
    for WORD in $LINE; do
        if [[ $PREV == "PROG_NAME" ]]; then
            PROG_NAME=${WORD//\"};
        elif [[ $PREV == "PROG_VERSION" ]]; then
            PROG_VERSION=${WORD//\"};
        fi
        PREV=$WORD;
    done
done < config.h

# Parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -c|--compiler)
            CC="$2"
            shift # past argument
            shift # past value
            ;;
        -n|--name)
            PROG_NAME="$2"
            shift # past argument
            shift # past value
            ;;
        -v|--version)
            PROG_VERSION="$2"
            shift # past argument
            shift # past value
            ;;
        -d|--directory)
            TARGET_DIR="$2"
            shift # past argument
            shift # past value
            ;;
        -i|--install)
            INSTALL=true
            shift # past argument
            ;;
        -u|--uninstall)
            UNINSTALL=true
            shift # past argument
            ;;
        -h|--help)
            SHOW_HELP=true
            shift # past argument
            ;;
        *)  # unknown option
            shift # past argument
            ;;
    esac
done

# Show help
if [[ $SHOW_HELP == true ]]; then
    echo "${PROG_NAME} ${PROG_VERSION} building and installation manual";
    echo "options:";
    echo "    -c --compiler  <compiler>     Set the compiler (default: gcc)";
    echo "    -n --name      <name>         Override program name (default: set in config.h)";
    echo "    -v --version   <version>      Override program version (default: set in config.h)";
    echo "    -d --directory <directory>    With -i or -u, set target directory (default: /usr/local/bin)";
    echo "    -i --install                  Perform installation";
    echo "    -u --uninstall                Perform uninstallation";
    echo "    -h --help                     Show this help";
    exit;
fi

# Install
if [[ $INSTALL == true ]]; then
    echo "Installing to ${TARGET_DIR}...";
    cp $PROG_NAME $TARGET_DIR;
    if [ $? -eq 0 ]
    then
        echo "Done.";
    else
        echo "Failed!";
    fi
    exit;
fi

# Uninstall
if [[ $UNINSTALL == true ]]; then
    echo "Uninstalling from ${TARGET_DIR}...";
    cd $TARGET_DIR;
    rm --interactive=never $PROG_NAME;
    if [ $? -eq 0 ]
    then
        echo "Done.";
    else
        echo "Failed!";
    fi
    exit;
fi

# Check if we have some stuff
echo "See if ulonglong_t is defined...";
$CC check/ulonglong_t.c -o /dev/null > /dev/null 2>&1;
if [ $? -eq 0 ]
then
    HAVE_ULONGLONG_T=true;
    echo "Yes.";
else
    echo "No.";
fi
echo "See if the system supports sockaddr's sa_len member...";
$CC check/sockaddr_sa_len.c -o /dev/null > /dev/null 2>&1;
if [ $? -eq 0 ]
then
    HAVE_SOCKADDR_SA_LEN=true;
    echo "Yes.";
else
    echo "No.";
fi
echo "See if the system \"supports\" IPv6 applications...";
$CC check/ipv6.c -o /dev/null > /dev/null 2>&1;
if [ $? -eq 0 ]
then
    HAVE_IPV6=true;
    echo "Yes.";
else
    echo "No.";
fi

# Write vars to config.h
echo "/* GENERATED FILE -- MANUAL EDITS MAY BE LOST */" > config.h;
echo "#define PROG_NAME \"${PROG_NAME}\"" >> config.h;
echo "#define PROG_VERSION \"${PROG_VERSION}\"" >> config.h;
if [[ $HAVE_ULONGLONG_T == false ]]; then
    echo "typedef ulonglong_t unsigned long long;" >> config.h;
fi
if [[ $HAVE_SOCKADDR_SA_LEN == true ]]; then
    echo "#define HAVE_SOCKADDR_SA_LEN" >> config.h;
fi
if [[ $HAVE_IPV6 == true ]]; then
    echo "#define HAVE_IPV6" >> config.h;
fi

# Build
echo "Building ${PROG_NAME} ${PROG_VERSION} with ${CC}...";
$CC ${C_FILES[@]} $C_ADD_FILE ${CC_MAIN_OPTS[@]} ${CC_ADD_OPTS[@]} -o $PROG_NAME;
if [ $? -eq 0 ]
then
    echo "Done.";
else
    echo "Failed!";
fi
