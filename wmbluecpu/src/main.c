/*
 *  WMBlueCPU - a cpu monitor
 *
 *  Copyright (C) 2003 Draghicioiu Mihai Andrei <misuceldestept@go.ro>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/select.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "cpu.h"
#include "mwm.h"
#include "menu.h"

#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define OPT_MILISECS 1000
#define OPT_CPUNUM 0

struct timeval tv = {0, 0};

menu_t *m;

char *opt_bgcolor;
char *opt_offcolor;
char *opt_oncolor;

int opt_milisecs;
int opt_cpunum;
int old_cpu_graph = 0;

void routine(int, char **);
void set_refresh(menuitem_t *);
void proc(void);
void draw_window(void);
void process_events(void);

void process_events(void) {
    /* Process any pending X events */
    switch (da_watch_xevent()) {
    case MOUSE_2_PRS:
        exit(0);
        break;
        
    case MOUSE_3_PRS:
        if (menu_pop(m) == 0)
            exit(0);
        break;
    }
}

void draw_window(void) {
    int r, cpu_graph;

    /* Dark-blue rect */
    da_set_foreground(2);
    
    da_fill_rectangle(19, 5, 40, 11);
    
    /* Smaller black rect */
    da_set_foreground(0);
    
    da_fill_rectangle(20, 6, 38, 9);
    
    if (cpu_total > 0)
        cpu_graph = cpu_used * 40 / cpu_total;
    else 
        cpu_graph = 0;
    
    /* Outer light-blue rect */
    if (cpu_graph > 0) {
        da_set_foreground(1);
        
        r = cpu_graph - 1;
        
        if (r < 0)
            r = 0;
        
        da_fill_rectangle(19, 5, r, 11);
    }
    
    /* Inner black rect */
    if (cpu_graph > 3 * da_scale) {
        da_set_foreground(0);
        
        r = cpu_graph - 5;
        
        if (r < 0)
            r = 0;
        
        da_fill_rectangle(21, 7, r, 7);
    }
    
    /* Inner dark-blue rect */
    if (cpu_graph > 5 * da_scale) {
        da_set_foreground(2);
        
        r = cpu_graph - 9;
        
        if (r < 0)
            r = 0;
        
        da_fill_rectangle(23, 9, r, 3);
    }
    
    /* Stat */
    /* The hack to make the windowed mode work */
    if (da_windowed)
        da_copy_wm_area(19, 18, 40, 38, 19, 21);
    else
        da_copy_xpm_area(19, 18, 40, 38, 19, 21);
    
    da_set_foreground(2);
    
    da_fill_rectangle(19, 18, 40, 2);
    
    if (old_cpu_graph > 0) {
        da_set_foreground(1);
        
        da_fill_rectangle(19, 18, old_cpu_graph, 2);
    }
    
    old_cpu_graph = cpu_graph;
    
    da_redraw_window();
}

void proc(void) {
    int i;
    
    fd_set fs;
    
    int fd = ConnectionNumber(da_display);

    process_events();
    
    FD_ZERO(&fs); 
    FD_SET(fd, &fs);
    
    i = select(fd + 1, &fs, 0, 0, &tv);
    
    if (i == -1) {
        fprintf(stderr, "Error with select(): %s", strerror(errno));
        exit(1);
    }
    
    if (!i) {
        cpu_getusage();
        draw_window();
        
        tv.tv_sec = opt_milisecs / 1000;
        tv.tv_usec = (opt_milisecs % 1000) * 1000;
    }
}

void set_refresh(menuitem_t *i) {
    opt_milisecs = i->i;
 
    for (i = m->first; i && i->i > 0; i = i->next)
        if (i->i == opt_milisecs) 
            i->checked = 1;
        else 
            i->checked = 0;

    tv.tv_sec = 0;
    tv.tv_usec = 0;
}

void routine(int argc, char *argv[]) {
    menuitem_t *i;

    da_init_xwindow();

    da_add_color(opt_bgcolor, "bg_color");
    da_add_color(opt_oncolor, "on_color");
    da_add_color(opt_offcolor, "off_color");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 128 * da_scale, 64 * da_scale);
        break;
    }
    
    menu_init(da_display);
    
    m = menu_new();
    
    i = menu_append(m, "Refresh: 250ms");
    
    i->i = 250; 
    
    i->callback = set_refresh; 
    
    if (opt_milisecs == 250) 
        i->checked = 1; 
    else 
        i->checked = 0;
    
    i = menu_append(m, "Refresh: 500ms");
    
    i->i = 500; 
    
    i->callback = set_refresh; 
    
    if (opt_milisecs == 500) 
        i->checked = 1; 
    else 
        i->checked = 0;
    
    i = menu_append(m, "Refresh: 1s");
    
    i->i = 1000; 
    
    i->callback = set_refresh; 
    
    if (opt_milisecs == 1000) 
        i->checked = 1; 
    else 
        i->checked = 0;
    
    i = menu_append(m, "Refresh: 2s");
    
    i->i = 2000; 
    
    i->callback = set_refresh; 
    
    if (opt_milisecs == 2000) 
        i->checked = 1; 
    else 
        i->checked = 0;
    
    i = menu_append(m, "Refresh: 4s");
    
    i->i = 4000; 
    
    i->callback = set_refresh; 
    
    if (opt_milisecs == 4000) 
        i->checked = 1; 
    else 
        i->checked = 0;
    
    menu_append(m, "Exit");
    
    cpu_init(opt_cpunum);
    
    while (!da_conf_changed(argc, argv))
        proc();
    
    routine(argc, argv);
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp. You'll never guess what it does.\n\n"

    PROG_NAME" displays the cpu usage at the top, and a history chart at the bottom. It can do SMP too. Right-click for a menu, middle-click to exit.");

    da_init_string(&opt_bgcolor, "bg_color", "-B", "--bgcolor", "#000000", "Background color");
    da_init_string(&opt_oncolor, "on_color", "-N", "--oncolor", "#87d7ff", "Color for On leds");
    da_init_string(&opt_offcolor, "off_color", "-F", "--offcolor", "#0095e0", "Color for Off leds");
    da_init_integer(&opt_cpunum, "cpu_number", "-c", "--cpu", 0, 0, OPT_CPUNUM, "Specify which cpu to monitor (for SMP systems only)");
    da_init_integer(&opt_milisecs, "milisecs", "-m", "--milisecs", 0, 0, OPT_MILISECS, "The number of milisecs between updates");   
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}
