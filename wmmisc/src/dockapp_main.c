/*
 *  wmmisc - WindowMaker Dockapp for monitoring misc. information.
 *  Copyright (C) 2003-2006 Jesse S. (luxorfalls@sbcglobal.net)
 *
 *  wmmisc is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  wmmisc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wmmisc; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <bits/getopt_core.h>
#include <signal.h>

#ifdef USE_MTRACE
#include <mcheck.h>
#endif /* USE_MTRACE */

#include "config.h"
#include "dockapp_main.h"
#include "dockapp_draw.h"

#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

char *front_color;
char *back_color;
char *border_dark;
char *border_light;

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a simple dockapp written to replace some of the features that gkrellm contains.\n\n"

    "It was written due to no other dockapp existing that has these features and gkrellm uses too much memory for what it does. What this dockapp does:\n\n"

    "1. Shows count of currently logged in users.\n"
    "2. Shows count of currently running processes.\n"
    "3. Shows count of currently (active) running processes.\n"
    "4. Shows uptime (HH:MM).\n"
    "5. Shows uptime (days).\n"
    "6. Shows uptime (weeks).\n"
    "7. Shows system load (5 min).\n\n"

    "This dockapp was written for GNU/Linux and has not been tested, by me, on other platforms. It may work on FreeBSD with /proc file system support, but I honestly have no idea.\n\n"

    "This dockapp is a good base if you plan on writing your own. I have provided simple functions for drawing text and numbers on the canvas. There are also functions in place for drawing bars, but it is currently unused and may contain bugs. However, I currently know of no bugs.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 200 * da_scale, 100 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 200 * da_scale, 100 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 200 * da_scale, 100 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 200 * da_scale, 100 * da_scale);
        break;
    }

#ifdef USE_MTRACE
    mtrace();
#endif /* USE_MTRACE */

    dockapp_draw_small_str("USERS", 6, 6);
    dockapp_draw_small_str("PROCS", 6, 13);
    dockapp_draw_small_str("ACTIVE", 6, 20);

    dockapp_draw_small_str("UP", 6, 29);
    dockapp_draw_small_str("DAYS", 6, 36);
    dockapp_draw_small_str("WEEKS", 6, 43);

    dockapp_draw_small_str("LOAD", 6, 52);

    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            /* TODO: do something */
            break;
        }

        dockapp_draw_data();

        usleep(250000L);
    }

    routine(argc, argv);
}
