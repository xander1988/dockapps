#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/param.h>
#include <sys/types.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define CHAR_WIDTH 5
#define CHAR_HEIGHT 7

typedef enum {
    NONE,
    ALARM,
    TIMER,
    TIMER_PAUSED,
    TIMER_DONE,
    CHRONO,
    CHRONO_PAUSED
} modeType;

/*******************************************************************************
 * Functions 
 ******************************************************************************/
// Misc functions
void execAct();
void routine(int, char **);
void process_event(int, int);

// Timer updates
void decrementTimer();
void incrementTimer();

// X11 Screen updates
void blitNum(int, int, int);
void blitString(char *, int, int);
void updateACT();
void updateClock(int, int, int);
void set_dest_time(int, int, int);
void show_main_scr();
void show_config_scr();

/*******************************************************************************
 * Globals 
 ******************************************************************************/
char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *mode_name;
char *time_string;
char *command;

bool bell;

int timeSetToZero = 0;
int hour = 0, min = 0, sec = 0;

bool conf_shown = false;

modeType mode;

/*******************************************************************************
 * main 
 ******************************************************************************/
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockable alarm clock for Window Maker which can be run in alarm, countdown timer, or chronograph mode.\n\n"

    "In alarm or timer mode, you can execute a command when the time is reached. The command cannot be set via the config screen. "PROG_NAME" can be configured either at run time via the command line or by clicking the 'C' button. The config screen allows switching between modes, setting destination time, and toggling system bell.\n\n"

    "To switch to the Chrono function simply click on the right arrow button to start the chronograph. You can pause the chronogaph by clicking on the center, rectangle button and resume again by clicking the right arrow button. You can reset the timer by clicking on the left arrow button.\n\n"

    "Time entered via the command line must be in the form of xx:xx:xx. You don't need to have 2 digits for each number but you must have at least zero's in as placeholders for hours, minutes and seconds.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&command, "command", "-c", "--command", NULL, "Command to execute when the time is reached");
    da_init_bool(&bell, "system_bell", "-B", "--bell", false, "Sound system bell when the time is reached");
    da_init_string(&mode_name, "mode", "-m", "--mode", "alarm", "Set mode (can be alarm, timer, chrono)");
    da_init_string(&time_string, "time", "-t", "--time", "00:00:00", "Set time; in the alarm mode, "PROG_NAME" will exec command at specified time, in the countdowntimer mode, "PROG_NAME" will exec command when specified time reaches 0");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc"); 

    da_parse_all(argc, argv);

    routine(argc, argv);
    
    return 0;
}

void routine(int argc, char *argv[]) {
    int prevSec = 0;

    char time_tmp[3];

    long now;

    struct tm *thisTime;

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 216 * da_scale, 116 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 216 * da_scale, 116 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 216 * da_scale, 116 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 216 * da_scale, 116 * da_scale);
        break;
    }

    /* Set mode */
    if (!strcmp(da_str_to_upper(mode_name), "ALARM")) {
        mode = ALARM;
        
    } else if (!strcmp(da_str_to_upper(mode_name), "TIMER")) {
        mode = TIMER;
        
    } else if (!strcmp(da_str_to_upper(mode_name), "CHRONO")) {
        mode = CHRONO;
        
    } else {
        printf("WARN: wrong mode set, using ALARM.\n");
        da_log(WARN, "wrong mode set, using ALARM.\n");

        mode = ALARM;
    }

    /* Parse time */
    if (!time_string || (strlen(time_string) != 8)) {
        printf("ERR: wrong time set, aborting.\n");
        da_log(ERR, "wrong time set, aborting.\n");

        exit(0);
    }
    /* hour */
    time_tmp[0] = time_string[0];
    time_tmp[1] = time_string[1];
    hour = atoi(time_tmp);
    /* min */
    time_tmp[0] = time_string[3];
    time_tmp[1] = time_string[4];
    min = atoi(time_tmp);
    /* sec */
    time_tmp[0] = time_string[6];
    time_tmp[1] = time_string[7];
    sec = atoi(time_tmp);

    /* Clickable regions */
    /* Main screen */
    da_add_mouse_region(0,  5, 45, 16, 59); /* conf button */
    da_add_mouse_region(1, 16, 45, 27, 59); /* reset button   */
    da_add_mouse_region(2, 27, 45, 48, 59); /* pause/stop button  */
    da_add_mouse_region(3, 48, 45, 59, 59); /* start button */
    /* Config screen */
    da_add_mouse_region(4,  133-128,  5, 151-128, 15); /* alarm button */
    da_add_mouse_region(5,  151-128,  5, 169-128, 15); /* timer button   */
    da_add_mouse_region(6,  169-128,  5, 187-128, 15); /* chrono button  */
    da_add_mouse_region(7,  133-128, 33, 151-128, 41); /* hour+ button */
    da_add_mouse_region(8,  151-128, 33, 169-128, 41); /* min+ button */
    da_add_mouse_region(9,  169-128, 33, 187-128, 41); /* sec+ button   */
    da_add_mouse_region(10, 133-128, 41, 151-128, 49); /* hour- button  */
    da_add_mouse_region(11, 151-128, 41, 169-128, 49); /* min- button */
    da_add_mouse_region(12, 169-128, 41, 187-128, 49); /* sec- button */
    da_add_mouse_region(13, 151-128, 49, 187-128, 59); /* ok button */
    da_add_mouse_region(14, 133-128, 49, 151-128, 59); /* bell button */
    
    show_main_scr();
    updateACT(); 

    //  if (hour == 0 && min == 0 && sec == 0) 
    //    timeSetToZero = 1;

    while (!da_conf_changed(argc, argv)) {
        now = time(0);

        waitpid(0, NULL, WNOHANG);

        thisTime = localtime(&now);

        if (!conf_shown)
            updateClock(thisTime->tm_hour, thisTime->tm_min, thisTime->tm_sec);

        da_redraw_window();

        switch (mode) {
        case TIMER:
            if (!conf_shown) {
                if (prevSec < thisTime->tm_sec) {
                    decrementTimer();
                    updateACT();

                    if (hour == 0 && min == 0 && sec == 0 && !timeSetToZero)
                        execAct();
                }

                prevSec = thisTime->tm_sec;
            }

            break;

        case CHRONO:
            if (!conf_shown) {
                if (prevSec < thisTime->tm_sec) {
                    incrementTimer();
                    updateACT();
                }

                prevSec = thisTime->tm_sec;
            }

            break;

        case ALARM:
            if (!conf_shown) {
                updateACT();
                
                if (hour == thisTime->tm_hour && min == thisTime->tm_min && sec == thisTime->tm_sec) 
                    execAct();
            }

            break;

        case NONE:
        case TIMER_DONE:
        case TIMER_PAUSED:
        case CHRONO_PAUSED:
            break;
        }

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            process_event(MOUSE_1_PRS, da_check_mouse_region());
            break;

        case MOUSE_1_REL:
            process_event(MOUSE_1_REL, da_check_mouse_region());
            break;
		}

        da_redraw_window();

        usleep(100000L);
    }

    routine(argc, argv);
}

/*******************************************************************************
 * execAct 
 ******************************************************************************/
void execAct() {
    if (command)
        da_exec_command(command);

    if (bell) {
        printf("\07");

        fflush(stdout);
    }

    if (mode == TIMER)
        mode = TIMER_DONE;      // Don't want to keep doing the Timer event
    else
        mode = NONE;
}

/*******************************************************************************
 * processEvent 
 ******************************************************************************/
void process_event(int event, int region) {
    switch (region) {
    case 0:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(54, 86, 11, 14, 5, 45);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(0, 86, 11, 14, 5, 45);

            show_config_scr();
        }
        
        da_redraw_window();
        
        break;

    case 1:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(65, 86, 11, 14, 16, 45);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(11, 86, 11, 14, 16, 45);

            if (mode == CHRONO)
                mode = CHRONO_PAUSED;

            else if (mode == TIMER)
                mode = TIMER_PAUSED;

            hour = min = sec = 0;

            updateACT();
        }
        
        da_redraw_window();
        
        break;

    case 2:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(76, 86, 21, 14, 27, 45);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(22, 86, 21, 14, 27, 45);

            if (mode == ALARM) {
                hour = min = sec = 0;

                updateACT();
            }

            if (mode == CHRONO)
                mode = CHRONO_PAUSED;

            else if (mode == TIMER)
                mode = TIMER_PAUSED;
        }
        
        da_redraw_window();
        
        break;

    case 3:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(97, 86, 11, 14, 48, 45);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(43, 86, 11, 14, 48, 45);

            if (mode == ALARM) {
                hour = min = sec = 0;

                updateACT();

                timeSetToZero = 0;

                mode = CHRONO;
            } else if (mode == TIMER || mode == TIMER_PAUSED)
                mode = TIMER;
            else 
                mode = CHRONO;
        }
        
        da_redraw_window();
        
        break;

    case 4:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(108, 90, 54, 10, 133-128, 5);
            da_copy_xpm_area(162, 90, 18, 10, 133-128, 5);
        
        } else if (event == MOUSE_1_REL) {
            mode = ALARM;
        }
        
        break;

    case 5:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(108, 90, 54, 10, 133-128, 5);
            da_copy_xpm_area(162+18, 90, 18, 10, 133-128+18, 5);
        
        } else if (event == MOUSE_1_REL) {
            mode = TIMER;
        }
        
        break;

    case 6:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(108, 90, 54, 10, 133-128, 5);
            da_copy_xpm_area(162+18+18, 90, 18, 10, 133-128+18+18, 5);
        
        } else if (event == MOUSE_1_REL) {
            mode = CHRONO;
        }
        
        break;
    
    case 7:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(54, 100, 18, 8, 133-128, 33);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(0, 100, 54, 16, 133-128, 33);

            hour++;

            if (hour > 23)
                hour = 0;

            set_dest_time(hour, min, sec);
        }
        
        break;
    
    case 8:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(54+18, 100, 18, 8, 133-128+18, 33);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(0, 100, 54, 16, 133-128, 33);

            min++;

            if (min > 59)
                min = 0;

            set_dest_time(hour, min, sec);
        }
        
        break;
    
    case 9:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(54+18+18, 100, 18, 8, 133-128+18+18, 33);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(0, 100, 54, 16, 133-128, 33);

            sec++;

            if (sec > 59)
                sec = 0;

            set_dest_time(hour, min, sec);
        }
        
        break;
    
    case 10:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(54, 100+8, 18, 8, 133-128, 33+8);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(0, 100, 54, 16, 133-128, 33);

            hour--;

            if (hour < 0)
                hour = 23;

            set_dest_time(hour, min, sec);
        }
        
        break;
    
    case 11:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(54+18, 100+8, 18, 8, 133-128+18, 33+8);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(0, 100, 54, 16, 133-128, 33);

            min--;

            if (min < 0)
                min = 59;

            set_dest_time(hour, min, sec);
        }
        
        break;
    
    case 12:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(54+18+18, 100+8, 18, 8, 133-128+18+18, 33+8);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(0, 100, 54, 16, 133-128, 33);

            sec--;

            if (sec < 0)
                sec = 59;

            set_dest_time(hour, min, sec);
        }
        
        break;
    
    case 13:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(180, 100, 36, 10, 151-128, 49);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(126, 100, 36, 10, 151-128, 49);

            if (mode == TIMER_DONE || mode == TIMER_PAUSED)
                mode = TIMER;

            if (mode == CHRONO_PAUSED)
                mode = CHRONO;

            show_main_scr();
        }
        
        break;

    case 14:
        if (event == MOUSE_1_PRS) {
            if (bell)
                da_copy_xpm_area(108, 100, 18, 10, 133-128, 49);
            else
                da_copy_xpm_area(162, 100, 18, 10, 133-128, 49);
        
        } else if (event == MOUSE_1_REL) {
            bell ^= true;
        }
        
        break;
    }
}

/*******************************************************************************
 * decrementTimer 
 ******************************************************************************/
void decrementTimer() {
    // Don't want to go past 0:0:0
    if (!(hour == 0 && min == 0 && sec == 0))     
        sec--;

    if (sec == -1) {
        sec = 59;
        min--;

        if (min == -1) {
            min = 59;
            hour--;
        }
    }
}

/*******************************************************************************
 * incrementTimer 
 ******************************************************************************/
void incrementTimer() {
    sec++;

    if (sec == 60) {
        sec = 0;
        min++;

        if (min == 60) {
            min = 0;
            hour++;
        }
    }
}

/*******************************************************************************
 * blitNum Blits a number at given co-ordinates
 ******************************************************************************/
void blitNum(int num, int x, int y) {
    char buf[1024];

    int newx = x;

    if (num > 99)
        newx -= CHAR_WIDTH;

    if (num > 999)
        newx -= CHAR_WIDTH;

    sprintf(buf, "%02i", num);

    blitString(buf, newx, y);
}

/*******************************************************************************
 * blitString Blits a string at given co-ordinates
 ******************************************************************************/
void blitString(char *name, int x, int y) {
    // da_copy_xpm_area(x_get_pos, y_get_pos, x_dist_from_x_pos, y_dist_from_y_pos, 
    //     x_placement_pos, y_placement_pos);
    // each char/num is 6u wide & 8u high, nums are 64u down, chars are 74u down

    int i;
    int c;
    int k;
  
    k = x;

    for (i = 0; name[i]; i++) {
        c = toupper(name[i]);

        if (c >= 'A' && c <= 'Z') {
            c -= 'A';

            da_copy_xpm_area(c * 6, 75, 6, 10, k, y);

            k += 6;

        } else {
            c -= '0';

            da_copy_xpm_area(c * 6, 64, 6, 10, k, y);

            k += 6;
        }
    }
}

/*******************************************************************************
 * updateACT  (AlarmChronoTimer)
 ******************************************************************************/
void updateACT() {
    blitNum(hour, 7, 30);
    blitString(":", 20, 30);
    blitNum(min, 25, 30);
    blitString(":", 38, 30);
    blitNum(sec, 43, 30);
}

/*******************************************************************************
 * updateClock
 ******************************************************************************/
void updateClock(int clockHour, int clockMin, int clockSec) {
    blitNum(clockHour, 7, 6);
    blitString(":", 20, 6);
    blitNum(clockMin, 25, 6);
    blitString(":", 38, 6);
    blitNum(clockSec, 43, 6);
}

void set_dest_time(int clockHour, int clockMin, int clockSec) {
    blitNum(clockHour, 7, 19);
    blitString(":", 20, 19);
    blitNum(clockMin, 25, 19);
    blitString(":", 38, 19);
    blitNum(clockSec, 43, 19);
}

/*******************************************************************************
 * show_main_scr 
 ******************************************************************************/
void show_main_scr() {
    int i;

    for (i = 0; i < 4; i++) {
        da_enable_mouse_region(i);
    }
    for (i = 4; i < 15; i++) {
        da_disable_mouse_region(i);
    }

    conf_shown = false;
    
    da_copy_xpm_area(64, 0, 64, 64, 0, 0);
    
    da_set_mask_xy(64, 0);

    switch (mode) {
    case ALARM:
        blitString("ALARM:", 13, 18);
        break;

    case TIMER_PAUSED:
    case TIMER:
        blitString("TIMER:", 13, 18);
        break;

    case CHRONO_PAUSED:
    case CHRONO:
        blitString("CHRONO:", 12, 18);
        break;

    default:
        blitString("WMTIMER", 10, 18);
        break;
    }

    da_redraw_window();
}

void show_config_scr() {
    int i;

    for (i = 0; i < 4; i++) {
        da_disable_mouse_region(i);
    }
    for (i = 4; i < 15; i++) {
        da_enable_mouse_region(i);
    }

    if (mode == CHRONO)
        mode = CHRONO_PAUSED;

    else if (mode == TIMER)
        mode = TIMER_PAUSED;

    hour = min = sec = 0;

    updateACT();
    
    conf_shown = true;
    
    da_copy_xpm_area(128, 0, 64, 64, 0, 0);
    
    da_set_mask_xy(128, 0);

    switch (mode) {
    case TIMER_DONE:
    case TIMER_PAUSED:
    case TIMER:
        da_copy_xpm_area(162+18, 90, 18, 10, 133-128+18, 5);
        break;

    case CHRONO_PAUSED:
    case CHRONO:
        da_copy_xpm_area(162+18+18, 90, 18, 10, 133-128+18+18, 5);
        break;

    case ALARM:
        da_copy_xpm_area(162, 90, 18, 10, 133-128, 5);
        break;

    case NONE:
        break;
    }

    if (bell) {
        da_copy_xpm_area(162, 100, 18, 10, 133-128, 49);
    }

    set_dest_time(hour, min, sec);

    da_redraw_window();
}
