// wmamixer - An ALSA mixer designed for WindowMaker with scrollwheel support
// Copyright (C) 2015  Roman Dobosz <gryf@vimja.com>
// Copyright (C) 2003  Damian Kramer <psiren@hibernaculum.net>
// Copyright (C) 1998  Sam Hawker <shawkie@geocities.com>
// WMix 3.0 -- a mixer using the OSS mixer API.
// Copyright (C) 2000, 2001 timecop@japan.co.jp
// Mixer code in version 3.0 based on mixer api library by
// Daniel Richard G. <skunk@mit.edu>, which in turn was based on
// the mixer code in WMix 2.x releases.
//
// This software comes with ABSOLUTELY NO WARRANTY
// This software is free software, and you are welcome to redistribute it
// under certain conditions
// See the README file for a more complete notice.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
//#include <stdbool.h>
#include <assert.h>
#include <math.h>
#include <alsa/asoundlib.h>

#include "common.h"
#include "misc.h"
#include "mixer.h"

/* Function to convert from percentage to volume. val = percentage */
#define convert_prange1(val, min, max) \
    ceil((val) * ((max) - (min)) * 0.01 + (min))

// Current state information
char *card = NULL;
int curchannel;

struct Selem {
    char *name;
    bool stereo;
    int currentVolRight;
    int currentVolLeft;
    short int iconIndex;
    snd_mixer_elem_t *elem;
    long min;
    long max;
    bool capture;
    snd_mixer_selem_channel_id_t channels[2];
    bool has_playback_switch;
    bool has_capture_switch;
    bool is_muted;
};

typedef struct {
    bool isvolume;
    bool capture;
    bool mono;
} slideCaptureMono;

struct NamesCount {
    short int pcm, line, lineb, mic, micb, capt, vol, aux;
} namesCount = {1, 1, 1, 1, 1, 1, 1, 1};

struct Selem *selems[32] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

struct Mixer {
    int devices_no;
    snd_mixer_t * handle;
};

struct Mixer *mix;

static int smixer_level = 0;
static struct snd_mixer_selem_regopt smixer_options;

/* prt */
struct Mixer *Mixer_create();
void Mixer_destroy(struct Mixer *mix);
slideCaptureMono Mixer_get_capabilities(snd_mixer_elem_t *elem);
int Mixer_get_volume(int current, int channelIndex);
int Mixer_read_left(int current);
int Mixer_read_right(int current);
void Mixer_set_selem_props(struct Selem *selem, const char *name);
void Mixer_set_channels(struct Selem *selem);
void Mixer_set_left(int current, int value);
void Mixer_set_right(int current, int value);
void Mixer_set_limits(snd_mixer_elem_t *elem, struct Selem *selem);
void Mixer_set_volume(int current, int channelIndex, int value);
void Selem_set_name(struct Selem *selem, const char *name, short int *count);
void Selem_destroy();

void mixer_init(const char *mixer_device) {
    card = strdup(mixer_device);
    
    mix = Mixer_create();
    
    if (mix->devices_no == 0) {
        fputs("Sorry, no supported channels found.\n", stderr);
        exit(EXIT_FAILURE);
    }
}

void mixer_print_sources() {
    printf("Mixer device: %s (%i)\n", selems[curchannel]->name, curchannel);
	puts("Available sources:");
    
	for (int i = 0; i < mix->devices_no; i++)
		printf("  %d. %s (%i%%)\n", i + 1, selems[i]->name, (Mixer_read_left(i) + Mixer_read_right(i)) / 2);
}

int mixer_get_source_count() {
    return mix->devices_no;
}

int mixer_get_source() {
    return curchannel;
}

const char *mixer_get_source_name() {
	return selems[curchannel]->name;
}

void mixer_set_source(int source) {
    assert((source >= 0) && (source < mix->devices_no));

	curchannel = source;
}

void mixer_set_source_rel(int delta_source) {}

float mixer_get_volume() {
    snd_mixer_handle_events(mix->handle);
    
    float gen_vol = ((Mixer_read_left(curchannel) + Mixer_read_right(curchannel)) / 2) * 0.01;
    
    return gen_vol;
}

void mixer_set_volume(float volume) {
    int volume_int = volume * 100;
    
    Mixer_set_left(curchannel, volume_int);
    Mixer_set_right(curchannel, volume_int);
}

void mixer_set_volume_rel(float delta_volume) {
    int delta_volume_int = delta_volume * 100;
    
    Mixer_set_left(curchannel, (Mixer_read_left(curchannel) + delta_volume_int));
    Mixer_set_right(curchannel, (Mixer_read_right(curchannel) + delta_volume_int));
}

float mixer_get_balance() {
    return 0.0;
}

void mixer_set_balance(float balance) {}

void mixer_set_balance_rel(float delta_balance) {}

void mixer_toggle_mute() {
    if (selems[curchannel]->has_playback_switch) {
        selems[curchannel]->is_muted ^= 1;
        
        bool muted = mixer_is_muted();
        
        if (muted != selems[curchannel]->is_muted) {
            snd_mixer_selem_set_playback_switch_all(selems[curchannel]->elem, selems[curchannel]->is_muted?0:1);
        }
    }
}

bool mixer_is_muted() {
    if (!selems[curchannel]->has_playback_switch)
        return false;

    int left_on;
    
    snd_mixer_selem_get_playback_switch(selems[curchannel]->elem, SND_MIXER_SCHN_FRONT_LEFT, &left_on);
    
    if (left_on)
        return false;
        
    if (selems[curchannel]->stereo) {
        int right_on;
        
        snd_mixer_selem_get_playback_switch(selems[curchannel]->elem, SND_MIXER_SCHN_FRONT_RIGHT, &right_on);
        
        if (right_on)
            return false;
    }
    
    return true;
}

struct Mixer *Mixer_create() {
    struct Mixer *mixer = malloc(sizeof(struct Mixer));
    int err;
    const char *name;
    slideCaptureMono capabilities;

    snd_mixer_selem_id_t *sid;
    snd_mixer_elem_t *elem;
    snd_mixer_selem_id_alloca(&sid);

    assert(mixer != NULL);
    mixer->devices_no = 0;

    if ((err = snd_mixer_open(&mixer->handle, 0)) < 0) {
        fprintf(stderr, "Mixer %s open error: %s", card, snd_strerror(err));
        exit(err);
    }

    if (smixer_level == 0 &&
            (err = snd_mixer_attach(mixer->handle, card)) < 0) {
        fprintf(stderr, "Mixer attach %s error: %s", card, snd_strerror(err));
        snd_mixer_close(mixer->handle);
        exit(err);
    }

    if ((err = snd_mixer_selem_register(mixer->handle,
                    smixer_level > 0 ? &smixer_options : NULL, NULL)) < 0) {
        fprintf(stderr, "Mixer register error: %s", snd_strerror(err));
        snd_mixer_close(mixer->handle);
        exit(err);
    }

    err = snd_mixer_load(mixer->handle);

    if (err < 0) {
        fprintf(stderr, "Mixer %s load error: %s", card, snd_strerror(err));
        snd_mixer_close(mixer->handle);
        exit(err);
    }
    for (elem = snd_mixer_first_elem(mixer->handle); elem;
            elem = snd_mixer_elem_next(elem)) {
        snd_mixer_selem_get_id(elem, sid);

        if (!snd_mixer_selem_is_active(elem))
            continue;

        capabilities = Mixer_get_capabilities(elem);

        if (!capabilities.isvolume)
            continue;
        name = snd_mixer_selem_id_get_name(sid);

        selems[mixer->devices_no] = malloc(sizeof(struct Selem));
        selems[mixer->devices_no]->elem = elem;
        selems[mixer->devices_no]->capture = capabilities.capture;

        Mixer_set_limits(elem, selems[mixer->devices_no]);
        Mixer_set_selem_props(selems[mixer->devices_no], name);
        Mixer_set_channels(selems[mixer->devices_no]);

        mixer->devices_no++;
        if (mixer->devices_no == 32) {
            // stop here. This is ridiculous anyway
            break;
        }
    }
    return mixer;
}

void Mixer_set_selem_props(struct Selem *selem, const char *name) {
    const char *pcm = "PCM",
          *mic = "Mic",
          *cap = "Capture",
          *boost = "Boost",
          *line = "Line",
          *aux = "Aux";

    if (strcmp("Master", name) == 0) {
        selem->name = "mstr";
        selem->iconIndex = 0;
    } else if (strstr(pcm, name)) {
        selem->iconIndex = 1;
        if (strcmp(pcm, name) == 0)
            selem->name = "pcm ";
        else
            Selem_set_name(selem, "pcm", &namesCount.pcm);

    } else if (strstr("Headphone", name)) {
        selem->name = "hdph";
        selem->iconIndex = 8;
    } else if (strstr("Beep", name) || strstr("PC Speaker", name)) {
        selem->name = "beep";
        selem->iconIndex = 7;
    } else if (strstr("Digital", name)) {
        selem->iconIndex = selem->capture ? 3 : 15;
        selem->name = "digt";
    } else if (strstr("Bass", name)) {
        selem->name = "bass";
        selem->iconIndex = 9;
    } else if (strstr("Treble", name)) {
        selem->name = "trbl";
        selem->iconIndex = 10;
    } else if (strstr("Synth", name)) {
        selem->name = "synt";
        selem->iconIndex = 11;
    } else if (strstr("CD", name)) {
        selem->name = " cd ";
        selem->iconIndex = 12;
    } else if (strstr("Phone", name)) {
        selem->name = "phne";
        selem->iconIndex = 13;
    } else if (strstr("Video", name)) {
        selem->name = "vdeo";
        selem->iconIndex = 14;
    } else if (strstr(mic, name)) {
        if (strcmp(mic, name) == 0) {
            selem->name = "mic ";
        } else {
            if (strstr(boost, name))
                Selem_set_name(selem, "mib", &namesCount.micb);
            else
                Selem_set_name(selem, "mic", &namesCount.mic);
        }
        selem->iconIndex = 4;
    } else if (strstr(aux, name)) {
        if (strcmp(aux, name) == 0) {
            selem->name = "aux ";
        } else {
            Selem_set_name(selem, "aux", &namesCount.aux);
        }
        selem->iconIndex = 5;
    } else if (strstr(line, name)) {
        if (strcmp(line, name) == 0) {
            selem->name = "line";
        } else {
            if (strstr(boost, name))
                Selem_set_name(selem, "lnb", &namesCount.lineb);
            else
                Selem_set_name(selem, "lin", &namesCount.line);
        }
        selem->iconIndex = 5;
    } else if (strstr(cap, name)) {
        if (strcmp(cap, name) == 0) {
            selem->name = "cap ";
        } else {
            Selem_set_name(selem, "cap", &namesCount.capt);
        }
        selem->iconIndex = 3;
    } else {
        namesCount.vol++;
        selem->iconIndex = 6;
        Selem_set_name(selem, name, &namesCount.vol);
    }
}

void Selem_set_name(struct Selem *selem, const char *name, short int *count) {
    char new_name[5], buf[5];

    if (*count > 10) {
        snprintf(new_name, sizeof(new_name), "%s", name);
    } else {
        snprintf(buf, sizeof(buf) - 1, "%s", name);
        snprintf(new_name, sizeof(new_name), "%s%d", buf, *count);
    }

    selem->name = strdup(new_name);
    *count = *count + 1;
}

void Mixer_set_channels(struct Selem *selem) {
    int idx;
    snd_mixer_selem_channel_id_t chn;

    selem->stereo = true;

    if (snd_mixer_selem_has_playback_volume(selem->elem)) {
        if (snd_mixer_selem_is_playback_mono(selem->elem)) {
            selem->stereo = false;
            selem->channels[0] = SND_MIXER_SCHN_MONO;
            selem->channels[1] = SND_MIXER_SCHN_MONO;
        } else {
            idx = 0;
            for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++) {
                if (!snd_mixer_selem_has_playback_channel(selem->elem, chn))
                    continue;
                selem->channels[idx] = chn;
                idx++;
                if (idx == 2)
                    break;
            }
        }
        
        selem->has_playback_switch = snd_mixer_selem_has_playback_switch(selem->elem);
    }

    if (snd_mixer_selem_has_capture_volume(selem->elem)) {
        if (snd_mixer_selem_is_capture_mono(selem->elem)) {
            selem->stereo = false;
            selem->channels[0] = SND_MIXER_SCHN_MONO;
            selem->channels[1] = SND_MIXER_SCHN_MONO;
        } else {
            idx = 0;
            for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++) {
                if (!snd_mixer_selem_has_capture_channel(selem->elem, chn))
                    continue;
                selem->channels[idx] = chn;
                idx++;
                if (idx == 2)
                    break;
            }
        }
        
        selem->has_capture_switch = snd_mixer_selem_has_capture_switch(selem->elem);
    }
}

slideCaptureMono Mixer_get_capabilities(snd_mixer_elem_t *elem) {
    slideCaptureMono retval;
    retval.mono = false;
    retval.capture = false;
    retval.isvolume = false;

    if (snd_mixer_selem_has_common_volume(elem)) {
        retval.isvolume = true;

        if (snd_mixer_selem_has_playback_volume_joined(elem) ||
                snd_mixer_selem_is_playback_mono(elem) )
            retval.mono = true;
    } else {
        if (snd_mixer_selem_has_playback_volume(elem)) {
            retval.isvolume = true;
            if (snd_mixer_selem_has_playback_volume_joined(elem) ||
                    snd_mixer_selem_is_playback_mono(elem))
                retval.mono = true;
        }

        if (snd_mixer_selem_has_capture_volume(elem)) {
            retval.isvolume = true;
            retval.capture = true;
            if (snd_mixer_selem_has_capture_volume_joined(elem) ||
                    snd_mixer_selem_is_playback_mono(elem))
                retval.mono = true;
        }
    }
    return retval;
}

void Mixer_set_limits(snd_mixer_elem_t *elem, struct Selem *selem) {
    long min = 0, max = 0;

    if (snd_mixer_selem_has_common_volume(elem)) {
        snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    } else {
        if (snd_mixer_selem_has_capture_volume(elem))
            snd_mixer_selem_get_capture_volume_range(elem, &min, &max);
        if (snd_mixer_selem_has_playback_volume(elem))
            snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    }

    selem->min = min;
    selem->max = max;
}

static int convert_prange(long val, long min, long max) {
    long range = max - min;
    int tmp;

    if (range == 0)
        return 0;

    val -= min;
    tmp = rint((double)val/(double)range * 100);
    return tmp;
}

int Mixer_get_volume(int current, int channelIndex) {
    struct Selem *selem = selems[current];
    long raw;
    int volume;

    if (!selem->capture) {
        snd_mixer_selem_get_playback_volume(selem->elem,
                selem->channels[channelIndex], &raw);
    } else {
        snd_mixer_selem_get_capture_volume(selem->elem,
                selem->channels[channelIndex], &raw);
    }

    volume = convert_prange(raw, selem->min, selem->max);
    return volume;
}

void Mixer_set_volume(int current, int channelIndex, int value) {
    struct Selem *selem = selems[current];
    long raw;

    raw = (long)convert_prange1(value, selem->min, selem->max);

    if (!selem->capture) {
        snd_mixer_selem_set_playback_volume(selem->elem,
                selem->channels[channelIndex], raw);
    } else {
        snd_mixer_selem_set_capture_volume(selem->elem,
                selem->channels[channelIndex], raw);
    }
}

void Mixer_set_left(int current, int value) {
    Mixer_set_volume(current, 0, value);
}

void Mixer_set_right(int current, int value) {
    Mixer_set_volume(current, 1, value);
}

int Mixer_read_left(int current) {
    return Mixer_get_volume(current, 0);
}

int Mixer_read_right(int current) {
    return Mixer_get_volume(current, 1);
}

void Mixer_destroy(struct Mixer *mixer) {
    assert(mixer != NULL);
    snd_mixer_close(mixer->handle);
    free(mixer);
}

void Selem_destroy() {
    int i;

    for (i = 0; i < mix->devices_no; i++) {
        free(selems[i]);
    }
}
