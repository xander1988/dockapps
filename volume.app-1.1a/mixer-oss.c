/* mixer-oss.c */

/* Volume.app -- a simple volume control
 *
 * Copyright (C) 2000
 *	Daniel Richard G. <skunk@mit.edu>,
 *	timecop <timecop@japan.co.jp>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>

#include "common.h"
#include "misc.h"
#include "mixer.h"

#define VOLUME_APP_SOURCE_NAMES \
	"Master volume", \
	"Bass", \
	"Treble", \
	"FM Synth volume", \
	"PCM Wave volume", \
	"PC Speaker", \
	"Line In level", \
	"Microphone level", \
	"CD volume", \
	"Recording monitor", \
	"PCM Wave 2 volume", \
	"Recording volume", \
	"Input gain", \
	"Output gain", \
	"Line In 1", \
	"Line In 2", \
	"Line In 3", \
	"Digital In 1", \
	"Digital In 2", \
	"Digital In 3", \
	"Phone input", \
	"Phone output", \
	"Video volume", \
	"Radio volume", \
	"Monitor volume"

#ifdef OSS_SOURCE_NAMES
#define SOURCE_NAMES SOUND_DEVICE_LABELS
#else
#define SOURCE_NAMES VOLUME_APP_SOURCE_NAMES
#endif

typedef struct {
	const char *name;		/* name of source */
	int	dev;			/* source device number */
	int	prev_dev_volume2;	/* last known left/right volume
					 * (in device format) */
	float	volume;			/* volume, in [0, 1] */
	float	balance;		/* balance, in [-1, 1] */
#if 0
	bool	can_record;		/* is it capable of recording? */
	bool	is_recording;		/* is it recording? */
#endif
	bool	is_stereo;		/* is it in stereo? */
	bool	is_muted;		/* is it muted? */
} MixerSource;

static const char *source_names[] = { SOURCE_NAMES };

static int mixer_fd;
static int prev_modify_counter = -1;

static MixerSource mixer[SOUND_MIXER_NRDEVICES];
static int n_sources;
static int cur_source = 0;

static void
get_mixer_state(void)
{
	struct mixer_info mixer_info;
	int dev_volume2, dev_left_volume, dev_right_volume;
	float left, right;
	int srcmask;
	int i;

	ioctl(mixer_fd, SOUND_MIXER_INFO, &mixer_info);

	if (mixer_info.modify_counter == prev_modify_counter)
		/*
		 * Mixer state has not changed
		 */
		return;

	/* Mixer state was changed by another program, so we need
	 * to update. As OSS cannot tell us specifically which
	 * source(s) changed, we read all of them in.
	 *
	 * prev_modify_counter was initialized to -1, so this section
	 * is guaranteed to run the first time get_mixer_state() is
	 * called.
	 */

	if (ioctl(mixer_fd, SOUND_MIXER_READ_RECSRC, &srcmask) == -1)
	{
		fputs("Mixer read failed\n", stderr);
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < n_sources; i++)
	{
		if (ioctl(mixer_fd, MIXER_READ(mixer[i].dev), &dev_volume2) == -1)
		{
			fputs("Mixer read failed\n", stderr);
			exit(EXIT_FAILURE);
		}

		if (dev_volume2 != mixer[i].prev_dev_volume2)
		{
			dev_left_volume = dev_volume2 & 0xFF;
			dev_right_volume = dev_volume2 >> 8;

			if ((dev_left_volume > 0) || (dev_right_volume > 0))
				mixer[i].is_muted = false;

			left = (float)dev_left_volume / 100.0;
			right = (float)dev_right_volume / 100.0;

			if (!mixer[i].is_muted)
			{
				if (mixer[i].is_stereo)
					lr_to_vb(
						left,
						right,
						&mixer[i].volume,
						&mixer[i].balance);
				else {
					mixer[i].volume = left;
					mixer[i].balance = 0.0;
				}

				mixer[i].prev_dev_volume2 = dev_volume2;
			}
		}

#if 0
		mixer[i].is_recording = ((1 << mixer[i].dev) & srcmask) != 0;
#endif
	}

	prev_modify_counter = mixer_info.modify_counter;
}

/* Sets mixer state for the current source only
 */
static void
set_mixer_state(void)
{
	float left, right;
	int dev_left_volume, dev_right_volume, dev_volume2;

	if (mixer[cur_source].is_muted)
	{
		left = 0.0;
		right = 0.0;
	} else if (mixer[cur_source].is_stereo)
	{
		vb_to_lr(
			mixer[cur_source].volume,
			mixer[cur_source].balance,
			&left,
			&right);
	} else
	{
		left = mixer[cur_source].volume;
		right = mixer[cur_source].volume;
	}

	dev_left_volume = (int)(100.0 * left);
	dev_right_volume = (int)(100.0 * right);
	dev_volume2 = (dev_right_volume << 8) | dev_left_volume;
	ioctl(mixer_fd, MIXER_WRITE(mixer[cur_source].dev), &dev_volume2);
}

void
mixer_init(const char *mixer_device)
{
	int devmask, srcmask, recmask, stmask;
	int count;
	int mask;
	int n = 0;

	mixer_fd = open(mixer_device, O_RDWR);

	if (mixer_fd == -1)
	{
		fprintf(stderr, "Error: cannot open mixer device %s\n", mixer_device);
		exit(EXIT_FAILURE);
	}

	if (ioctl(mixer_fd, SOUND_MIXER_READ_DEVMASK, &devmask) == -1)
	{
		fputs("Error: device mask ioctl failed\n", stderr);
		exit(EXIT_FAILURE);
	}

	if (ioctl(mixer_fd, SOUND_MIXER_READ_RECSRC, &srcmask) == -1)
	{
		fputs("Error: recording source mask ioctl failed\n", stderr);
		exit(EXIT_FAILURE);
	}

	if (ioctl(mixer_fd, SOUND_MIXER_READ_RECMASK, &recmask) == -1)
	{
		fputs("Error: recording mask ioctl failed\n", stderr);
		exit(EXIT_FAILURE);
	}

	if (ioctl(mixer_fd, SOUND_MIXER_READ_STEREODEVS, &stmask) == -1)
	{
		fputs("Error: stereo mask ioctl failed\n", stderr);
		exit(EXIT_FAILURE);
	}

	for (count = 0; count < SOUND_MIXER_NRDEVICES; count++)
	{
		mask = 1 << count;
		if (mask & devmask)
		{
			mixer[n].name = source_names[count];
			mixer[n].dev = count;
			mixer[n].prev_dev_volume2 = -1;
#if 0
			mixer[n].can_record = (mask & recmask) != 0;
			mixer[n].is_recording = (mask & srcmask) != 0;
#endif
			mixer[n].is_stereo = (mask & stmask) != 0;
			mixer[n].is_muted = false;

			++n;
		}
	}
	n_sources = n;

	get_mixer_state();
}

void
mixer_print_sources(void)
{
	struct mixer_info info;
	int i;

	if (ioctl(mixer_fd, SOUND_MIXER_INFO, &info) == -1)
	{
		fputs("Error: could not read mixer info\n", stderr);
		exit(EXIT_FAILURE);
	}

	printf("Mixer device: %s (%s)\n", info.name, info.id);
	puts("Available sources:");
	for (i = 0; i < n_sources; i++)
		printf("  %d. %s (%.0f%%)\n", i + 1, mixer[i].name, 100.0 * mixer[i].volume);
}

int
mixer_get_source_count(void)
{
	return n_sources;
}

int
mixer_get_source(void)
{
	return cur_source;
}

const char *
mixer_get_source_name(void)
{
	return mixer[cur_source].name;
}

void
mixer_set_source(int source)
{
	assert((source >= 0) && (source < n_sources));

	cur_source = source;
}

void
mixer_set_source_rel(int delta_source)
{
	cur_source = (cur_source + delta_source) % n_sources;
	if (cur_source < 0)
		cur_source += n_sources;
}

float
mixer_get_volume(void)
{
	get_mixer_state();
	return mixer[cur_source].volume;
}

void
mixer_set_volume(float volume)
{
	assert((volume >= 0.0) && (volume <= 1.0));

	mixer[cur_source].volume = volume;
	set_mixer_state();
}

void
mixer_set_volume_rel(float delta_volume)
{
	mixer[cur_source].volume += delta_volume;
	mixer[cur_source].volume = CLAMP(mixer[cur_source].volume, 0.0, 1.0);
	set_mixer_state();
}

float
mixer_get_balance(void)
{
	get_mixer_state();
	return mixer[cur_source].balance;
}

void
mixer_set_balance(float balance)
{
	assert((balance >= -1.0) && (balance <= 1.0));

	if (mixer[cur_source].is_stereo)
	{
		mixer[cur_source].balance = balance;
		set_mixer_state();
	}
}

void
mixer_set_balance_rel(float delta_balance)
{
	if (mixer[cur_source].is_stereo)
	{
		mixer[cur_source].balance += delta_balance;
		mixer[cur_source].balance = CLAMP(mixer[cur_source].balance, -1.0, 1.0);
		set_mixer_state();
	}
}

void
mixer_toggle_mute(void)
{
	mixer[cur_source].is_muted = !mixer[cur_source].is_muted;
	set_mixer_state();
}

bool
mixer_is_muted(void)
{
	return mixer[cur_source].is_muted;
}

/* end mixer-oss.c */
