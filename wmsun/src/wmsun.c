/*
 *
 *      wmSun (C) 1999 Mike Henderson (mghenderson@lanl.gov)
 *
 *          - Shows Sun Rise/Set Times....
 *
 *
 *
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program (see the file COPYING); if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *      Boston, MA  02111-1307, USA
 *
 *      Things TODO:
 *                  - clean up code!
 *                  - support for 8-bit displays.
 *                  - more detailed documentation.
 *                  - eclipses?
 *                  - add buttons to play will date and lat lon...
 *                    Could be something like this;
 *                       First click brings up buttons to change date.
 *                       Second click brings up buttons to change lat/lon.
 *                       Third goes back to display
 *                       Set time delay to go back to display if user doesnt do it...
 */

/*
 *   Includes
 */
#define _POSIX_C_SOURCE 1
#include <math.h>                      /* for cos, sin */
#include <sys/select.h>                /* for select, FD_SET, FD_ZERO, etc */
#include <sys/time.h>                  /* for timeval */
#include <libdockapp4/dockapp.h>       /* for da_copy_xpm_area, display, etc */

#include "config.h"
#include "XPM/wmSun_mask64.xbm"        /* for wmSun_mask_bits, etc */
#include "XPM/wmSun_mask128.xbm"
#include "XPM/wmSun_mask192.xbm"
#include "XPM/wmSun_mask256.xbm"
#include "XPM/wmSun_master64.xpm"      /* for wmSun_master */
#include "XPM/wmSun_master128.xpm"
#include "XPM/wmSun_master192.xpm"
#include "XPM/wmSun_master256.xpm"
#include "XPM/wmSun_master_hq64.xpm"      /* for wmSun_master_hq */
#include "XPM/wmSun_master_hq128.xpm"
#include "XPM/wmSun_master_hq192.xpm"
#include "XPM/wmSun_master_hq256.xpm"

  /***********/
 /* Defines */
/***********/
#define DELAY 1000000L /* Delay between refreshes (in microseconds) */
#define DegPerRad 57.29577951308232087680
#define RadPerDeg 0.01745329251994329576

  /********************/
 /* Global Variables */
/********************/
int ToggleWindow = 0;
int nMAX = 1;
int Flag = 1;

long UserDate;

double Glat, Glon, SinGlat, CosGlat, TimeZone, UserTimeDiff;

int xDigit[11] = {8, 18, 27, 37, 46, 55, 64, 74, 83, 92, 102};

char *border_dark;
char *border_light;

bool TwelveHour;
bool hq;

  /**************/
 /* Prototypes */
/**************/
void routine(int, char **);
void SunRise(int, int, int, double, double *, double *);

/*
 *   main
 */
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockable WindowMaker SunRise/SunSet app.\n\n"

    PROG_NAME" displays the current day's Sun Rise and Set Times. You must enter your Latitude and Longitude correctly for it to work. Examples:\n\n"

    PROG_NAME" -N 106.3 -L 35.9\n\n"

    "This would display rise/set times at Los Alamos in local time.\n\n"

    PROG_NAME" -L 19.58 -N 155.92 -t 10\n\n"

    "This would display rise/set times in Kona, Hawaii in local time (in winter - you need to take into account daylight savings at other times of the year).");
    
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&hq, "high_quality", "-H", "--high-quality", false, "Use high-quality image");
    da_init_bool(&TwelveHour, "twelve_hour", "-T", "--twelve-hour", false, "Use 12-hour clock");
    da_init_float(&Glat, "latitude", "-L", "--latitude", 0.0, 0.0, 0.0, "Observers latitude; positive to the west");
    da_init_float(&Glon, "longitude", "-N", "--longitude", 0.0, 0.0, 0.0, "Observers longitude");
    da_init_float(&UserTimeDiff, "user_time_diff", "-t", "--user-time-diff", 0.0, 0.0, 0.0, "User defined difference between UT an LT (hrs). Useful when you want to show the Sunrise/Sunset at a remote latitude/longitude without resetting your clock");
    da_init_integer((int *)&UserDate, "user_date", "-u", "--user-date", 0, 0, 0, "Set the date to show sunrise/sunset for; the format is <yyyymmdd>");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                      /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");    /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                       /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    struct tm *GMTTime, *LocalTime;
    
    int         n;
    int         Year, Month;
    int         DayOfMonth;
    long        CurrentLocalTime, CurrentGMTTime, date;
    double      UT, val, LTRise, LTSet, LocalHour, hour24();
    int         H, M;
    struct timeval  timeout;
    fd_set      xfdset;

    Glat *= RadPerDeg; SinGlat = sin( Glat ); CosGlat = cos( Glat );

    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        if (hq) {
            da_open_xwindow(argc, argv, wmSun_master_hq256_xpm, wmSun_mask256_bits, wmSun_mask256_width, wmSun_mask256_height);
        } else {
            da_open_xwindow(argc, argv, wmSun_master256_xpm, wmSun_mask256_bits, wmSun_mask256_width, wmSun_mask256_height);
        }
        break;
        
    case 3:
        if (hq) {
            da_open_xwindow(argc, argv, wmSun_master_hq192_xpm, wmSun_mask192_bits, wmSun_mask192_width, wmSun_mask192_height);
        } else {
            da_open_xwindow(argc, argv, wmSun_master192_xpm, wmSun_mask192_bits, wmSun_mask192_width, wmSun_mask192_height);
        }
        break;
        
    case 2:
        if (hq) {
            da_open_xwindow(argc, argv, wmSun_master_hq128_xpm, wmSun_mask128_bits, wmSun_mask128_width, wmSun_mask128_height);
        } else {
            da_open_xwindow(argc, argv, wmSun_master128_xpm, wmSun_mask128_bits, wmSun_mask128_width, wmSun_mask128_height);
        }
        break;
        
    case 1:
    default:
        if (hq) {
            da_open_xwindow(argc, argv, wmSun_master_hq64_xpm, wmSun_mask64_bits, wmSun_mask64_width, wmSun_mask64_height);
        } else {
            da_open_xwindow(argc, argv, wmSun_master64_xpm, wmSun_mask64_bits, wmSun_mask64_width, wmSun_mask64_height);
        }
        break;
    }

    /*
     *  Loop until we die
     */
    n = 32000;
    
    while (!da_conf_changed(argc, argv)) {
        if (Flag) {
            n = 32000;
            
            Flag = 0;
        }

        /*
         *  The Moon Ephemeris calculations are somewhat costly (the Moon is one of the most
         *  difficult objects to compute position for). So only process every nMAXth cycle of this
         *  loop. We run outer loop it faster to catch expose events, button presses, etc...
         *
         */
        if (n > nMAX) {
            struct tm result;

            n = 0;
            nMAX = 60;

            CurrentGMTTime = time(CurrentTime);
            GMTTime = gmtime_r(&CurrentGMTTime, &result);
            DayOfMonth = GMTTime->tm_mday;

            UT = GMTTime->tm_hour + GMTTime->tm_min/60.0 + GMTTime->tm_sec/3600.0;
            Year = GMTTime->tm_year+1900;
            Month = GMTTime->tm_mon+1;

            CurrentLocalTime = CurrentGMTTime;
            LocalTime = localtime_r(&CurrentLocalTime, &result);

            Flag = 0;

            if (UserDate != 0) {
                date =  UserDate;
                Year = date/10000;
                date -= Year*10000;
                Month = date/100;
                date -= Month*100;
                DayOfMonth = date;
                date = UserDate;
            } else {
                date = Year*10000 + Month*100 + DayOfMonth;
            }
                
            LocalHour = LocalTime->tm_hour + LocalTime->tm_min/60.0 + LocalTime->tm_sec/3600.0;
            TimeZone = (UserTimeDiff != 0.0) ? UserTimeDiff : UT - LocalHour;

            /*
             *  Clear Plotting area
             */
            da_copy_xpm_area(65, 5, 54, 54, 5, 5);

            /*
             *  Compute Sun Rise/Set Times in Local Time
             */
            SunRise(Year, Month, DayOfMonth, LocalHour, &LTRise, &LTSet);

            if (LTRise > 0.0) {
                val = LTRise;

                H = (int)val; val = (val-H)*60.0;

                if (TwelveHour) {
                    H = H % 12;

                    if (H == 0)
                        H = 12;
                }

                M = (int)val;

                da_copy_xpm_area(xDigit[H/10], 73, 7, 9, 17,    13);
                da_copy_xpm_area(xDigit[H%10], 73, 7, 9, 17+7,  13);
                da_copy_xpm_area(xDigit[10],   75, 3, 7, 17+15, 14);
                da_copy_xpm_area(xDigit[M/10], 73, 7, 9, 17+19, 13);
                da_copy_xpm_area(xDigit[M%10], 73, 7, 9, 17+26, 13);
            } else {
                da_copy_xpm_area(10, 84, 28, 7, 19, 15);
            }

            if (LTSet > 0.0) {
                val = LTSet;

                H = (int)val; val = (val-H)*60.0;

                if (TwelveHour) {
                    H = H % 12;

                    if (H == 0)
                        H = 12;
                }

                M = (int)val;

                da_copy_xpm_area(xDigit[H/10], 73, 7, 9, 17,    40);
                da_copy_xpm_area(xDigit[H%10], 73, 7, 9, 17+7,  40);
                da_copy_xpm_area(xDigit[10],   75, 3, 7, 17+15, 41);
                da_copy_xpm_area(xDigit[M/10], 73, 7, 9, 17+19, 40);
                da_copy_xpm_area(xDigit[M%10], 73, 7, 9, 17+26, 40);
            } else {
                da_copy_xpm_area(10, 84, 28, 7, 19, 40);
            }

        } else {
            /*
             *  Update the counter.
             */
            ++n;
        }

        /*
         *  Add X display to file descriptor set for polling.
         */
        FD_ZERO(&xfdset);
        FD_SET(ConnectionNumber(da_display), &xfdset);

        /*
         *   Process any pending X events.
         */
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            ++ToggleWindow;
   
            if (ToggleWindow > 4)
                ToggleWindow = 0;
       
            Flag = 1;
            
            break;
        }

        /*
         *  Redraw and wait for next update
         */
        da_redraw_window();

        timeout.tv_sec = DELAY / 1000000L;
        timeout.tv_usec = DELAY % 1000000L;

        select(ConnectionNumber(da_display) + 1, &xfdset, NULL, NULL, &timeout);
    }

    routine(argc, argv);
}
