/* apm/acpi dockapp - phear it 1.34
 * Copyright (C) 2000, 2001, 2002 timecop@japan.co.jp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#define _GNU_SOURCE

#include <libdockapp4/dockapp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>

#include "libacpi.h"

#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"
#include "bitmaps/master_low.xpm"
#include "bitmaps/master_low2.xpm"
#include "bitmaps/master_low3.xpm"
#include "bitmaps/master_low4.xpm"

/* Do NOT change the BASE_PERIOD without reading the code ... */
#define BASE_PERIOD 100000 /* base period, 100 ms (in usecs) */
#define DEFAULT_SCROLL_RESET 4

/* this gives us a variable scroll rate, depending on the importance of the
 * message . . . */
int scroll_reset = DEFAULT_SCROLL_RESET;
int da_tw;          /* text width inside text pixmap */
int da_percent;
int period_length;  /* length of the polling period, multiple of BASE_PERIOD */
int critical;
int battery_no;
int samplerate;
int cli_samples;
int x_fd;
bool no_blink;          /* should we blink the LED? (critical battery) */
bool bell;              /* bell on critical low, or not? */
bool no_scroll;         /* scroll message text? */
bool da_scroll_reset;   /* reset the scrolling text */
bool cli;
bool capacity;

char *front_color;
char *back_color;
char *border_dark;
char *border_light;

/* display AC power symbol */
static void display_power_glyph(void)
{
    da_copy_xpm_area(67, 38, 12, 7, 6, 17);
}

/* get rid of AC power symbol */
static void kill_power_glyph(void)
{
    da_copy_xpm_area(67, 48, 12, 7, 6, 17);
}

/* display battery symbol */
static void display_battery_glyph(void)
{
    da_copy_xpm_area(82, 38, 12, 7, 20, 17);
}

/* get rid of battery symbol */
static void kill_battery_glyph(void)
{
    da_copy_xpm_area(82, 48, 12, 7, 20, 17);
}

/* clear the time display */
static void clear_time_display(void)
{
    da_copy_xpm_area(114, 76, 31, 11, 7, 32);
}

/* set time display to -- -- */
static void invalid_time_display(void)
{
    da_copy_xpm_area(122, 14, 31, 11, 7, 32);
}

static void reset_scroll(void) {
    scroll_reset = DEFAULT_SCROLL_RESET;
}

static void clear_text_area(void) {
    da_copy_xpm_area(66, 9, 52, 7, 6, 50);
}

static void copy_to_text_buffer(int sx, int sy, int w, int h, int dx, int dy)
{
    da_copy_xpm_area(sx, sy, w, h, dx, dy);
}

static void render_text(char *string)
{
    char shortened[9];
    
    int i, c, k;
    int str_len;

    static int pos = 0;
    static int pos_0, pos_1, pos_2, pos_3, pos_4, pos_5, pos_6, pos_7;
    static int counter = 0;

    /* drop out immediately if scrolling is disabled - we don't render
     * any text at all, since there's not much else we could do
     * sensibly without scrolling. */
    if (no_scroll)
        return;

    str_len = strlen(string);

    pos_0 = pos;
    pos_1 = pos + 1;
    pos_2 = pos + 2;
    pos_3 = pos + 3;
    pos_4 = pos + 4;
    pos_5 = pos + 5;
    pos_6 = pos + 6;
    pos_7 = pos + 7;
    
    shortened[7] = string[pos_7];
    shortened[6] = string[pos_6];
    shortened[5] = string[pos_5];
    shortened[4] = string[pos_4];
    shortened[3] = string[pos_3];
    shortened[2] = string[pos_2];
    shortened[1] = string[pos_1];
    shortened[0] = string[pos_0];

    counter++;

    if (counter >= scroll_reset) {
        pos++;
        counter = 0;
    }

    if (pos == str_len) {
        pos = 0;
    }

    k = 6;

    clear_text_area();

    for (i = 0; shortened[i]; i++) {
        c = toupper(shortened[i]);

        if (c >= 'A' && c <= 'Z') { /* letter */
            c = c - 'A';
            copy_to_text_buffer(c * 6, 67, 6, 7, k, 50);
        } else if (c >= '0' && c <= '9') {  /* number */
            c = c - '0';
            copy_to_text_buffer(c * 6 + 66, 58, 6, 7, k, 50);
        } else if (c == '.') {
            copy_to_text_buffer(140, 58, 6, 7, k, 50);
        } else if (c == '-') {
            copy_to_text_buffer(126, 58, 6, 7, k, 50);
        }
        
        k += 6;
    }

    da_tw = k; /* length of text segment */

    da_redraw_window();
}

static void clear_percentage(void)
{
  /* clear the number */
  da_copy_xpm_area(95, 47, 21, 9, 37, 16);
  /* clear the bar */
  da_copy_xpm_area(66, 18, 54, 8, 5, 5);

  da_percent = -1;
}

static void display_percentage(int percent)
{
    unsigned int bar;
    int width = 54;     /* width of the bar */
    float ratio = 100.0/width;  /* ratio between the current percentage
                 * and the number of pixels in the bar */

    if (percent == -1)
    percent = 0;

    if (da_percent == percent)
    return;

    if (percent < 0)
    percent = 0;
    if (percent > 100)
    percent = 100;

    if (da_percent == -1)
    da_copy_xpm_area(127, 28, 5, 7, 52, 17);

    if (percent < 100) {    /* 0 - 99 */
    da_copy_xpm_area(95, 48, 8, 7, 37, 17);
    if (percent >= 10)
        da_copy_xpm_area((percent / 10) * 6 + 67, 28, 5, 7, 40, 17);
    da_copy_xpm_area((percent % 10) * 6 + 67, 28, 5, 7, 46, 17);
    } else
    da_copy_xpm_area(95, 37, 21, 9, 37, 16);   /* 100% */
    da_percent = percent;

    bar = (int)((float)percent / ratio);

    da_copy_xpm_area(66, 0, bar, 8, 5, 5);
    if (bar < 54)
    da_copy_xpm_area(66 + bar, 18, 54 - bar, 8, bar + 5, 5);
}

static void display_time(int minutes)
{
    static int ohour = -1, omin = -1;
    int hour, min, tmp;

    if (minutes <= 0) { /* error - clear the display */
    invalid_time_display();
    ohour = omin = -1;
    return;
    }

    /* render time on the display */
    hour = minutes / 60;
    /* our display area only fits %2d:%2d, so we need to make sure
     * what we're displaying will fit in those constraints. I don't
     * think we're likely to see any batteries that do more than
     * 100 hours any time soon, so it's fairly safe. */
    if (hour >= 100) {
    hour = 99;
    min = 59;
    } else
    min = minutes % 60;

    if (hour == ohour && min == omin)
    return;

    tmp = hour / 10;
    da_copy_xpm_area(tmp * 7 + 1, 76, 6, 11, 7, 32);
    tmp = hour % 10;
    da_copy_xpm_area(tmp * 7 + 1, 76, 6, 11, 14, 32);
    tmp = min / 10;
    da_copy_xpm_area(tmp * 7 + 1, 76, 6, 11, 25, 32);
    tmp = min % 10;
    da_copy_xpm_area(tmp * 7 + 1, 76, 6, 11, 32, 32);
    da_copy_xpm_area(71, 76, 3, 11, 21, 32);
    ohour = hour;
    omin = min;
}

/*
 * The reworked state handling stuff.
 */

/* set the current state of the power panel */
enum panel_states {
    PS_AC,
    PS_BATT,
    PS_NULL,
};

static void really_blink_power_glyph(void)
{
    static int counter = 0;

    if (counter == 10)
    display_power_glyph();
    else if (counter == 20)
    kill_power_glyph();
    else if (counter > 30)
    counter = 0;

    counter += period_length;
}

static void blink_power_glyph(void)
{
    if (!no_blink)
        really_blink_power_glyph();
}

static void really_blink_battery_glyph(void)
{
    static int counter = 0;

    if (counter == 10)
    display_battery_glyph();
    else if (counter == 20)
    kill_battery_glyph();
    else if (counter > 30)
    counter = 0;

    counter += period_length;
}

static void blink_battery_glyph(void)
{
    if (!no_blink)
        really_blink_battery_glyph();
}

static void set_power_panel(global_t *globals)
{
    static enum panel_states power = PS_NULL;
    battery_t *binfo = globals->binfo;
    adapter_t *ap = &globals->adapter;

    if (ap->power == AC) {
    if (power != PS_AC) {
        power = PS_AC;
        kill_battery_glyph();
        display_power_glyph();
    }
    } else if (ap->power == BATT) {
    if (power != PS_BATT) {
        power = PS_BATT;
        kill_power_glyph();
        display_battery_glyph();
    }
    }

    if (globals->battery_count > 0) {
    if (binfo->charge_state == CHARGE)
        blink_power_glyph();

    if ((binfo->state == CRIT) && (ap->power == BATT))
        blink_battery_glyph();
    }
}

void scroll_faster(int factor) {
    scroll_reset = factor;
}

void scroll_slower(int factor) {
    scroll_reset = factor;
}

void reset_scroll_speed(void) {
    scroll_reset = DEFAULT_SCROLL_RESET;
}

/*
 * The message that needs to be displayed needs to be decided
 * according to a heirarchy: a message like not present needs to take
 * precedence over a global thing like the current power status, and
 * something like a low battery warning should take precedence over
 * the "on battery" message. Likewise, a battery charging message
 * needs to take precedence over the on ac power message. The other
 * question is how much of a precedence local messages should take
 * over global ones . . .
 *
 * So, there are three possible sets of messages: not present, on-line
 * and off-line messages. We need to decide which of those sets is
 * appropriate right now, and then decide within them.
 */
enum messages {
    M_NB,   /* no batteries */
    M_NP,   /* not present */
    M_AC,   /* on ac power */
    M_CH,   /* battery charging */
    M_BATT, /* on battery */
    M_LB,   /* low battery */
    M_CB,   /* critical low battery */
    M_HCB,  /* battery reported critical capacity state */
    M_NULL, /* empty starting state */
};

static void set_message(global_t *globals)
{
    enum messages state = M_NULL;
    battery_t *binfo = globals->binfo;
    adapter_t *ap = &globals->adapter;

    if (globals->battery_count == 0) {
        if (state != M_NB) {
            state = M_NB;
            reset_scroll_speed();
            render_text("        no batteries");
        }

        return;
    }

    /* battery not present case */
    if (!binfo->present) {
        if (state != M_NP) {
            state = M_NP;
            reset_scroll_speed();
            render_text("        not present");
        }
    } else if (ap->power == AC) {
        if (binfo->charge_state == CHARGE) {
            if (state != M_CH) {
                state = M_CH;
                reset_scroll_speed();
                render_text("        battery charging");
            }
        } else {
            if (state != M_AC) {
                state = M_AC;
                reset_scroll_speed();
                render_text("        on ac power");
            }
        }
    } else {
        if (binfo->state == CRIT) {
            if (state != M_CB) {
                state = M_CB;
                scroll_faster(1);
                render_text("        critical low battery");
            }
        } else if (binfo->state == LOW) {
            if (state != M_LB) {
                state = M_LB;
                scroll_faster(2);
                render_text("        low battery");
            }
        } else {
            if (state != M_BATT) {
                state = M_BATT;
                reset_scroll_speed();
                render_text("        on battery");
            }
        }
    }
}

void set_time_display(global_t *globals)
{
    if (globals->battery_count == 0) {
        invalid_time_display();
    return;
    }

    if (globals->binfo->charge_state == CHARGE)
    display_time(globals->binfo->charge_time);
    else if (globals->binfo->charge_state == DISCHARGE)
    display_time(globals->rtime);
    else
    invalid_time_display();
}

void clear_batt_id_area(void)
{
    da_copy_xpm_area(125, 40, 7, 11, 51, 32);
}

void set_batt_id_area(int bno)
{
    da_copy_xpm_area((bno + 1) * 7, 76, 7, 11, 51, 32);
}

void cli_wmacpi(global_t *globals, int samples)
{
    int i, j, sleep_time = 0;
    battery_t *binfo;
    adapter_t *ap;

    pdebug("samples: %d\n", samples);
    if(samples > 1)
        sleep_time = 1000000/samples;

    /* we want to acquire samples over some period of time, so . . . */
    for(i = 0; i < samples + 2; i++) {
    for(j = 0; j < globals->battery_count; j++)
        acquire_batt_info(globals, j);
    acquire_global_info(globals);
    usleep(sleep_time);
    }

    ap = &globals->adapter;
    if(ap->power == AC) {
    printf("On AC Power");
    for(i = 0; i < globals->battery_count; i++) {
        binfo = &batteries[i];
        if(binfo->present && (binfo->charge_state == CHARGE)) {
        printf("; Battery %s charging", binfo->name);
        printf(", currently at %2d%%", binfo->percentage);
        if(binfo->charge_time >= 0)
            printf(", %2d:%02d remaining",
               binfo->charge_time/60,
               binfo->charge_time%60);
        }
    }
    printf("\n");
    } else if(ap->power == BATT) {
    printf("On Battery");
    for(i = 0; i < globals->battery_count; i++) {
        binfo = &batteries[i];
        if(binfo->present && (binfo->percentage >= 0))
        printf(", Battery %s at %d%%", binfo->name,
               binfo->percentage);
    }
    if(globals->rtime >= 0)
        printf("; %d:%02d remaining", globals->rtime/60,
           globals->rtime%60);
    printf("\n");
    }
    return;
}

battery_t *switch_battery(global_t *globals, int battno)
{
  globals->binfo = &batteries[battno];
  pinfo("changing to monitor battery %s\n", globals->binfo->name);
  set_batt_id_area(battno);
  da_redraw_window();

  return globals->binfo;
}


void routine(int argc, char **argv)
{
    int sample_count = 0;
    int batt_reinit, ac_reinit;
    int batt_count = 0;
    int ac_count = 0;
    int scroll_count = 0;
    enum rtime_mode rt_mode = RT_RATE;
    int rt_forced = 0;
    battery_t *binfo = NULL;
    global_t *globals;

    fd_set fds;
    struct timeval tv_rate;
    struct timeval tv = {0, 0};

    globals = calloc(1, sizeof(global_t));

    bell = false;
    da_scroll_reset = false;
    globals->crit_level = 10;

    /* after this many samples, we reinit the battery and AC adapter
     * information.
     * XXX: make these configurable . . . */
    batt_reinit = 100;
    ac_reinit = 1000;

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        if (da_display_depth <= 8)
            da_open_xwindow(argc, argv, master_low4_xpm, NULL, 157 * da_scale, 88 * da_scale);
        else
            da_open_xwindow(argc, argv, master4_xpm, NULL, 157 * da_scale, 88 * da_scale);
        break;
        
    case 3:
        if (da_display_depth <= 8)
            da_open_xwindow(argc, argv, master_low3_xpm, NULL, 157 * da_scale, 88 * da_scale);
        else
            da_open_xwindow(argc, argv, master3_xpm, NULL, 157 * da_scale, 88 * da_scale);
        break;
        
    case 2:
        if (da_display_depth <= 8)
            da_open_xwindow(argc, argv, master_low2_xpm, NULL, 157 * da_scale, 88 * da_scale);
        else
            da_open_xwindow(argc, argv, master2_xpm, NULL, 157 * da_scale, 88 * da_scale);
        break;
        
    case 1:
    default:
        if (da_display_depth <= 8)
            da_open_xwindow(argc, argv, master_low_xpm, NULL, 157 * da_scale, 88 * da_scale);
        else
            da_open_xwindow(argc, argv, master_xpm, NULL, 157 * da_scale, 88 * da_scale);
        break;
    }

    if (capacity) {
        rt_mode = RT_CAP;
        rt_forced = 1;
    }

    /* convert to number of base periods */
    samplerate = ((60 * 1000000) / samplerate) / BASE_PERIOD;

    if (no_scroll) {
        if (no_blink) {
            /* Adapt the period to the sample rate */
            tv_rate.tv_usec = samplerate * BASE_PERIOD;

            tv_rate.tv_sec = tv_rate.tv_usec / 1000000;
            tv_rate.tv_usec = tv_rate.tv_usec - (tv_rate.tv_sec * 1000000);

            period_length = samplerate;
        } else {
            /* blinking is every second */
            tv_rate.tv_sec = 1; /* BASE_PERIOD * 10 = 1 sec */
            tv_rate.tv_usec = 0;

            period_length = 10;
        }
    } else {
        /* scrolling is every BASE_PERIOD (100 ms) */
        tv_rate.tv_sec = 0;
        tv_rate.tv_usec = BASE_PERIOD;

        period_length = 1;
    }

    globals->crit_level = critical;

    if (battery_no >= MAXBATT) {
        fprintf(stderr, "Please specify a battery number below %d\n", MAXBATT);
        exit(0);
    }
    pinfo("Monitoring battery %d\n", battery_no);

    if (power_init(globals))
        /* power_init functions handle printing error messages */
        exit(1);

    globals->rt_mode = rt_mode;
    globals->rt_forced = rt_forced;

    if (battery_no > globals->battery_count) {
        pinfo("Battery %d not available for monitoring.\n", battery_no);
    }

    /* check for cli mode */
    if (cli) {
        cli_wmacpi(globals, cli_samples);
        exit(0);
    }
    
    /* check to see if we've got a valid DISPLAY env variable, as a simple check to see if
     * we're running under X */
    if (!getenv("DISPLAY")) {
        pdebug("Not running under X - using cli mode\n");
        cli_wmacpi(globals, cli_samples);
        exit(0);
    }

    battery_no--;

    /* get initial statistics */
    acquire_all_info(globals);

    if (globals->battery_count > 0) {
        binfo = &batteries[battery_no];
        globals->binfo = binfo;
        set_batt_id_area(battery_no);
        pinfo("monitoring battery %s\n", binfo->name);
    }

    clear_time_display();
    set_power_panel(globals);
    set_message(globals);

    x_fd = XConnectionNumber(da_display);

    /* main loop */
    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (globals->battery_count == 0)
                break;

            /* cycle through the known batteries. */
            battery_no++;
            battery_no = battery_no % globals->battery_count;

            binfo = switch_battery(globals, battery_no);
            break;
        }

        /* XXX: some laptops have problems with sampling the battery
        * regularly - apparently, the BIOS disables interrupts while
        * reading from the battery, which is generally on a slow bus
        * and is a slow device, so you get significant periods without
        * interrupts. This causes interactivity to suffer . . .
        *
        * So, the workaround/fix for this is to sample at a much
        * lower rate than we may update/refresh/expose/whatever. The
        * user specifies how many times they want us to sample per
        * minute; we use select() on our X events fd to wake up when
        * there's some display work to be done, with the timeout set
        * to whatever the time between samples is. When we hit our
        * select() timeout we update our samples, otherwise we update
        * the display.
        *
        * Note that this has a wrinkle when blinking and/or scrolling
        * is enabled, since we need to update the display more
        * frequently than we sample (most likely). In that case we
        * set the timeout based on the display update cycle. */

        /* have we completed our timeout, or were we woken up early? */
        if ((tv.tv_sec != 0) || (tv.tv_usec != 0))
            goto win_update;

        tv = tv_rate;

        sample_count += period_length;

        if (sample_count >= samplerate) {
            if (globals->battery_count == 0) {
                batt_count = 0;

                reinit_batteries(globals);

                /* battery appeared */
                if (globals->battery_count > 0) {
                    if (battery_no > globals->battery_count)
                        battery_no = 0;

                    binfo = switch_battery(globals, battery_no);
                }
            }

            acquire_all_info(globals);

            /* we need to be able to reinitialise batteries and adapters, because
            * they change - you can hotplug batteries on most laptops these days
            * and who knows what kind of shit will be happening soon . . . */
            if (batt_count++ >= batt_reinit) {
                if(reinit_batteries(globals))
                    pfatal("Oh my god, the batteries are gone!\n");
                batt_count = 0;
            }

            if (ac_count++ >= ac_reinit) {
                if(reinit_ac_adapters(globals))
                    pfatal("What happened to our AC adapters?!?\n");
                ac_count = 0;
            }
            sample_count = 0;
        }

        if (scroll_count++ >= scroll_reset) {
            reset_scroll();
            scroll_count = 0;
        }

        /* The old code had some kind of weird crap with timers and the like.
        * As far as I can tell, it's meaningless - the time we want to display
        * is the time calculated from the remaining capacity, as per the
        * ACPI spec. The only thing I'd change is the handling of a charging
        * state: my best guess, based on the behaviour I'm seeing with my
        * Lifebook, is that the present rate value when charging is the rate
        * at which the batteries are being charged, which would mean I'd just
        * need to reverse the rtime calculation to be able to work out how
        * much time remained until the batteries were fully charged . . .
        * That would be rather useful, though given it would vary rather a lot
        * it seems likely that it'd be little more than a rough guesstimate. */
        set_time_display(globals);
        set_power_panel(globals);
        set_message(globals);

        if (globals->battery_count == 0) {
            clear_percentage();
            clear_batt_id_area();
        } else
            display_percentage(binfo->percentage);

    win_update:
        /* redraw_window, if anything changed */
        da_redraw_window();

        FD_ZERO(&fds);
        FD_SET(x_fd, &fds);
        select(FD_SETSIZE, &fds, NULL, NULL, &tv);
    }
    
    routine(argc, argv);
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, VERSION, "a port of WMApm 1.1 with ACPI support.\n\n"

    PROG_NAME" is a program that displays the current battery status in a WindowMaker dock app, on systems that support Intel's Advanced Configuration and Power Interface specification (ACPI).\n\n"

    "The program monitors a battery, displaying its current percentage charge via a bar and a numeric value. It also displays the current power status for the system, the time remaining (calculated based on the remaining battery capacity and the current rate of power usage), and a scrolling message with some hopefully useful information.\n\n"

    "Clicking on the window cycles through the batteries that the ACPI system knows about.\n\n"

    "Usage:\n\n"

    "+-------------+\n"
    "|battery graph| <- visual percentage battery remaining\n"
    "|[:][=] [100%]| <- [:] - on AC (blink when charging) [=] - on battery\n"
    "|[00:00]  [bX]| <- [00:00] time remaining   [bX] battery being monitored\n"
    "|status   area| <- messages scroll here\n"
    "+-------------+\n\n"

    PROG_NAME" is a dockapp ACPI battery monitor for modern kernels (ie, 2.4.17 or later, and 2.6 kernels). Basically, it opens various files under /proc/acpi, reads status information from them, and then displays summaries.\n\n"

    "Version 1.99 and later provides full support for multiple batteries. You can tell it to monitor a particular battery with the -m option, which will display the percentage remaining and current status message for that battery. The time remaining and AC/battery status are global - the time remaining is calculated based on all batteries found on the system. When charging, the time displayed is the time remaining until the battery is fully charged - this only works sensibly if your ACPI system is implemented properly (far, far too many laptops have buggered ACPI implementations).\n\n"

    "The displayed time is averaged over 50 samples, each taken every three seconds (by default). This greatly improves the accuracy of the numbers - on my laptop, the time remaining seems to be overstated by a good hour or so if you only sample once compared to fifty times.\n\n"

    "Some ACPI implementations are stupid enough to block interrupts while reading status information from the battery over a slow bus - this means that on such b0rken laptops, running an ACPI battery monitor could affect interactivity. To provide a workaround for this, current versions of "PROG_NAME" supports setting the sample rate from the command line. The --sample-rate option specifies the number of times the battery is sampled every minute - the default is 20, and the maximum value is 600. Since --sample-rate 600 translates to sampling every 0.1 seconds, you really don't want to do that unless you're just having fun...\n\n"

    "Also provided is a command line tool to report the battery status. By default this will only sample once, but with the -a option you can specify a number. Be aware that it will try to take all those samples in the space of one second, so if your ACPI implementation is b0rken this could have adverse effects.\n\n"

    "Please report bugs to <wmaker-dev@googlegroups.com>\n\n"

    "Simon Fowler, 2007-07-13.");
    
    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&no_scroll, "no_scroll", "-r", "--no-scroll", false, "Disable scrolling message");
    da_init_bool(&no_blink, "no_blink", "-n", "--no-blink", false, "Disable blinking power glyph when charging (note that it still blinks when the battery reports its capacity state as critical)");
    da_init_bool(&cli, "cmdline", "-x", "--cmdline", false, "Run "PROG_NAME" in command line mode - this operates identically to "PROG_NAME"-cli &");
    da_init_bool(&capacity, "capacity_mode", NULL, "--capacity-mode", false, "Force the use of capacity mode for calculating time remaining. By default "PROG_NAME" will use the reported values of remaining capacity and present rate to calculate the time remaining on battery. This flag will force the use of the remaining capacity and time samples to calculate the present rate of drain, and from there the time remaining. Note that this mode of calculation generally underreports the time remaining. This mode works around certain buggy ACPI BIOSes that fail to report the current rate");
    da_init_integer(&critical, "critical", "-c", "--critical", 1, 100, 10, "Set critical low alarm at <number> percent");
    da_init_integer(&battery_no, "battery", "-m", "--battery", 0, 0, 1, "Set the battery number to monitor initially");
    da_init_integer(&samplerate, "sample_rate", NULL, "--sample-rate", 1, 600, 20, "Set the rate at which to sample the ACPI data, in number of times per minute (minimum is 1, ie once a minute)");
    da_init_integer(&verbosity, "verbosity", "-V", "--verbosity", 0, 3, 0, "Increase the verbosity of the program. Setting this to 1 will print extra error information; 2 will produce informational output; 3 will produce copious debugging output");
    da_init_integer(&cli_samples, "samples", "-a", "--samples", 0, 0, 1, "Average the time remaining over num samples - this greatly improves the accuracy of the reported time remaining (cli mode only)");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}
