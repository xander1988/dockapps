/*
 * acpi-ng: command line acpi battery status tool.
 *
 * Written by Simon Fowler <simon@dreamcraft.com.au>, 2003-06-20.
 * Copyright 2003-06-20 Dreamcraft Pty Ltd.
 *
 * This file is distributed under the GNU General Public License,
 * version 2. Please see the COPYING file for details.
 */

/*
 * 2003-06-20.
 * I'm getting sick of not having a convenient way to query battery
 * status on the command line, so I'm hacking up this - a quick little
 * command line tool to display current battery status, using the same
 * libacpi code as wmacpi-ng.
 */

#define _GNU_SOURCE

#include <libdockapp4/dockapp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>

#include "libacpi.h"

global_t *globals;

int main(int argc, char *argv[])
{
	int i, j;
	int sleep_time = 0;
	int samples;
	battery_t *binfo;
	adapter_t *ap;

    da_init_main(argv[0], VERSION, "query battery status on ACPI enabled systems.\n\n"

    PROG_NAME"-cli queries the battery status from the command line. It prints the power status, the percentage remaining for each battery found, and the time remaining if the system is on battery, or the time remaining for each battery to reach full charge if the batteries are charging.");

    da_init_integer(&samples, "samples", "-a", "--samples", 1, 1000, 1, "Average the time remaining over num samples - this greatly improves the accuracy of the reported time remaining");
    da_init_integer(&verbosity, "verbosity", "-V", "--verbosity", 0, 3, 0, "Increase the verbosity of the program. If repeated, the result is the same as setting --verbosity for "PROG_NAME" to the number of repetitions");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_parse_all(argc, argv);

    //printf("Using libacpi version %s\n", LIBACPI_VER);

    pinfo("samples: %d\n", samples);
    sleep_time = 1000000 / samples;

	globals = (global_t *) malloc(sizeof(global_t));

	power_init(globals);
    
	/* we want to acquire samples over some period of time, so . . . */
	for(i = 0; i < samples + 2; i++) {
		for(j = 0; j < globals->battery_count; j++)
			acquire_batt_info(globals, j);

        acquire_global_info(globals);

        usleep(sleep_time);
	}

	ap = &globals->adapter;
    
	if(ap->power == AC) {
		printf("On AC Power");
        
		for(i = 0; i < globals->battery_count; i++) {
			binfo = &batteries[i];

            if(binfo->present && (binfo->charge_state == CHARGE)) {
				printf("; Battery %s charging", binfo->name);
				printf(", currently at %2d%%", binfo->percentage);

                if(binfo->charge_time >= 0)
					printf(", %2d:%02d remaining", binfo->charge_time/60, binfo->charge_time%60);
			}
		}
        
		printf("\n");
    
	} else if(ap->power == BATT) {
		printf("On Battery");
        
		for(i = 0; i < globals->battery_count; i++) {
			binfo = &batteries[i];

            if(binfo->present && (binfo->percentage >= 0))
				printf(", Battery %s at %d%%", binfo->name, binfo->percentage);
		}
        
		if(globals->rtime >= 0)
			printf("; %d:%02d remaining", globals->rtime/60, globals->rtime%60);

        printf("\n");
	}
    
	return 0;
}
