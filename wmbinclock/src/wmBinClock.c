/**
* wmBinClock.c by Thomas Kuiper <tkuiper at inxsoft.net> and Sune Molgaard <sune at molgaard.org
*
* Copyright (C) 2003-2004 Thomas Kuiper <tkuiper at inxsoft.net>
* Copyright (C) 2005 - Thomas Kuiper <tkuiper at inxsoft.net> and Sune Molgaard <sune at molgaard.org> (GPLv license2)
* Copyright (C) 2015 - Thomas Kuiper <tkuiper at inxsoft.net> and Sune Molgaard <sune at molgaard.org> (BSD license)
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define GRAYX 64
#define GRAYY 2
#define NEONREDX 72
#define NEONREDY 2
#define GOLDX 80
#define GOLDY 2
#define CYANX 88
#define CYANY 2
#define PINKX 96
#define PINKY 2
#define BLUEX 104
#define BLUEY 2
#define GREENX 112
#define GREENY 2
#define DARKREDX 120
#define DARKREDY 2
#define NONEX 128
#define NONEY 2
#define MODEV 0
#define MODEH 1
#define SOFF 0
#define SON 1

/* 
 *  Delay between refreshes (in microseconds) 
 */
#define DELAY 10000L

char *led_on_color;
char *led_off_color;
char *led_on_date_color;
char *mode;
char *back_color;
char *border_dark;
char *border_light;

bool superfluous;

void IntToBinary(int, int **);
void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "shows the actual system time as binary clock.\n\n"

    "You have to add up the \"bits\" to get the time. The clock has a 24 hour format.\n\n"

    "In the default mode (vertical), the display is like this:\n\n"

    "+ + + + + +  <- 8\n"
    "+ + + + + +  <- 4\n"
    "+ + + + + +  <- 2\n"
    "+ + + + + +  <- 1\n"
    "H H M M S S\n\n"

    "For example:\n\n"

    "+ + + + + +  <- 8\n"
    "+ + * + + *  <- 4\n"
    "* + + + * *  <- 2\n"
    "+ * * + * *  <- 1\n"
    "2 1 5 0 3 7\n\n"

    "And in the horizontal mode, it's like this:\n\n"

    "+ + + + + + <- Hour\n"
    "+ + + + + + <- Minute\n"
    "+ + + + + + <- Second\n"
    "+ + + + + + <- Day\n"
    "+ + + + + + <- Month\n\n"

    "For example:\n\n"

    "+ * + * + * <- 21\n"
    "* * + + * + <- 50\n"
    "* + + * + * <- 37\n"
    "+ + * * + * <- 13\n"
    "+ + + * * + <- 06\n\n"

    "Color can be one off these colors:\n"
    "gray neonred gold cyan pink blue green darkred none\n\n"

    "Mouse left button switches between the v/h modes, mouse right button turns the superfluous mode on or off.");

    da_init_string(&led_on_color, "led_on_color", "-L", "--led-on", "neonred", "Select LED on color");
    da_init_string(&led_off_color, "led_off_color", "-O", "--led-off", "gray", "Select LED off color");
    da_init_string(&led_on_date_color, "led_on_date_color", "-D", "--led_on_date", "green", "Select color for date display");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&mode, "mode", "-m", "--mode", "vertical", "Select display mode, Where mode is: \"horizontal\" or \"vertical\"");
    da_init_bool(&superfluous, "superfluous", "-S", "--superfluous", true, "Display e.g. led for 32 in hours");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    struct tm *tmworld;
    
    int dcoloron_x = GREENX;
    int dcoloron_y = GREENY;
    int coloron_x = NEONREDX;
    int coloron_y = NEONREDY;
    int coloroff_x = GRAYX;
    int coloroff_y = GRAYY;
    
    int Mode = MODEV;
    
    time_t t;
    
    char tempstr[20];
    
    int tmp_str[6];
    
    int i, j, s, tmp_hour, tmp_minute, tmp_second, tmp_day, tmp_month;
    
    int clockarray[10][10];
    
    int iBeginLEDs[2][6] = {{2,0,1,0,1,0},{1,0,0,1,2,0}};
//    iBeginLEDs[0] = ;
//    iBeginLEDs[1] = ;

    if (strcmp(led_on_color, "gray") == 0) {
        coloron_x = GRAYX;
        coloron_y = GRAYY;
    } else if (strcmp(led_on_color, "neonred") == 0) {
        coloron_x = NEONREDX;
        coloron_y = NEONREDY;
    } else if (strcmp(led_on_color,"gold") == 0) {
        coloron_x = GOLDX;
        coloron_y = GOLDY;
    } else if (strcmp(led_on_color,"cyan") == 0) {
        coloron_x = CYANX;
        coloron_y = CYANY;
    } else if (strcmp(led_on_color,"pink") == 0) {
        coloron_x = PINKX;
        coloron_y = PINKY;
    } else if (strcmp(led_on_color,"blue") == 0) {
        coloron_x = BLUEX;
        coloron_y = BLUEY;
    } else if (strcmp(led_on_color,"green") == 0) {
        coloron_x = GREENX;
        coloron_y = GREENY;
    } else if (strcmp(led_on_color,"darkred") == 0) {
        coloron_x = DARKREDX;
        coloron_y = DARKREDY;
    } else if (strcmp(led_on_color,"none") == 0) {
        coloron_x = NONEX;
        coloron_y = NONEY;
    } else {
        printf("WARN: wrong color set, defaulting to neonred.\n");
        da_log(WARN, "wrong color set, defaulting to neonred.\n");
        coloron_x = NEONREDX;
        coloron_y = NEONREDY;
    }

    if (strcmp(led_off_color, "gray") == 0) {
        coloroff_x = GRAYX;
        coloroff_y = GRAYY;
    } else if (strcmp(led_off_color, "neonred") == 0) {
        coloroff_x = NEONREDX;
        coloroff_y = NEONREDY;
    } else if (strcmp(led_off_color,"gold") == 0) {
        coloroff_x = GOLDX;
        coloroff_y = GOLDY;
    } else if (strcmp(led_off_color,"cyan") == 0) {
        coloroff_x = CYANX;
        coloroff_y = CYANY;
    } else if (strcmp(led_off_color,"pink") == 0) {
        coloroff_x = PINKX;
        coloroff_y = PINKY;
    } else if (strcmp(led_off_color,"blue") == 0) {
        coloroff_x = BLUEX;
        coloroff_y = BLUEY;
    } else if (strcmp(led_off_color,"green") == 0) {
        coloroff_x = GREENX;
        coloroff_y = GREENY;
    } else if (strcmp(led_off_color,"darkred") == 0) {
        coloroff_x = DARKREDX;
        coloroff_y = DARKREDY;
    } else if (strcmp(led_off_color,"none") == 0) {
        coloroff_x = NONEX;
        coloroff_y = NONEY;
    } else {
        printf("WARN: wrong color set, defaulting to gray.\n");
        da_log(WARN, "wrong color set, defaulting to gray.\n");
        coloroff_x = GRAYX;
        coloroff_y = GRAYY;
    }

    if (strcmp(led_on_date_color, "gray") == 0) {
        dcoloron_x = GRAYX;
        dcoloron_y = GRAYY;
    } else if (strcmp(led_on_date_color, "neonred") == 0) {
        dcoloron_x = NEONREDX;
        dcoloron_y = NEONREDY;
    } else if (strcmp(led_on_date_color,"gold") == 0) {
        dcoloron_x = GOLDX;
        dcoloron_y = GOLDY;
    } else if (strcmp(led_on_date_color,"cyan") == 0) {
        dcoloron_x = CYANX;
        dcoloron_y = CYANY;
    } else if (strcmp(led_on_date_color,"pink") == 0) {
        dcoloron_x = PINKX;
        dcoloron_y = PINKY;
    } else if (strcmp(led_on_date_color,"blue") == 0) {
        dcoloron_x = BLUEX;
        dcoloron_y = BLUEY;
    } else if (strcmp(led_on_date_color,"green") == 0) {
        dcoloron_x = GREENX;
        dcoloron_y = GREENY;
    } else if (strcmp(led_on_date_color,"darkred") == 0) {
        dcoloron_x = DARKREDX;
        dcoloron_y = DARKREDY;
    } else if (strcmp(led_on_date_color,"none") == 0) {
        dcoloron_x = NONEX;
        dcoloron_y = NONEY;
    } else {
        printf("WARN: wrong color set, defaulting to green.\n");
        da_log(WARN, "wrong color set, defaulting to green.\n");
        dcoloron_x = GREENX;
        dcoloron_y = GREENY;
    }

    if (strcmp(mode, "vertical") == 0) {
        Mode = MODEV;
    } else if (strcmp(mode, "horizontal") == 0) {
        Mode = MODEH;
    } else {
        printf("WARN: wrong mode set, defaulting to vertical.\n");
        da_log(WARN, "wrong mode set, defaulting to vertical.\n");
        Mode = MODEV;
    }
    
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 134 * da_scale, 64 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 134 * da_scale, 64 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 134 * da_scale, 64 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 134 * da_scale, 64 * da_scale);
        break;
    }

    da_redraw_window();
    
    while (!da_conf_changed(argc, argv)) {
        /* 
         *   Process any pending X events.
         */
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (Mode == MODEV) {
                Mode = MODEH;
            } else {
                Mode = MODEV;
            }
            da_copy_xpm_area(64, 10, 54, 54, 5, 5);
            da_redraw_window();
            break;
        
        case MOUSE_3_PRS:
            if (superfluous) {
                superfluous = false;
            } else {
                superfluous = true;
            }
            da_copy_xpm_area(64, 10, 54, 54, 5, 5);
            da_redraw_window();
            break;
        }

        t = time(0);

        tmworld = localtime(&t);

        if ( Mode == MODEV ) {
            sprintf(tempstr, "%02i%02i%02i", tmworld->tm_hour, tmworld->tm_min, tmworld->tm_sec);

            for (i = 0; i < 6; i++) {
                clockarray[i][0] = 0;
                clockarray[i][1] = 0;
                clockarray[i][2] = 0;
                clockarray[i][3] = 0;

                switch (tempstr[i]) {
                case '1':
                    clockarray[i][3] = 1;
                    break;
                case '2':
                    clockarray[i][2] = 1;
                    break;
                case '3':
                    clockarray[i][3] = 1;
                    clockarray[i][2] = 1;
                    break;
                case '4':
                    clockarray[i][1] = 1;
                    break;
                case '5':
                    clockarray[i][3] = 1;
                    clockarray[i][1] = 1;
                    break;
                case '6':
                    clockarray[i][2] = 1;
                    clockarray[i][1] = 1;
                    break;
                case '7':
                    clockarray[i][3] = 1;
                    clockarray[i][2] = 1;
                    clockarray[i][1] = 1;
                    break;
                case '8':
                    clockarray[i][0] = 1;
                    break;
                case '9':
                    clockarray[i][0] = 1;
                    clockarray[i][3] = 1;
                    break;
                }
            }

            for (i = 0; i < 6; i++) {
                if(superfluous) {
                    for (j = 0; j < 4; j++) {
                        if (clockarray[i][j] == 1)
                            da_copy_xpm_area(coloron_x, coloron_y, 8, 8, 6 + (i * 9), 10 + (j * 10));
                        else
                            da_copy_xpm_area(coloroff_x, coloroff_y, 8, 8, 6 + (i * 9), 10 + (j * 10));
                    }
                } else {
                    for (j = iBeginLEDs[0][i]; j < 4; j++) {
                        if (clockarray[i][j] == 1)
                            da_copy_xpm_area(coloron_x, coloron_y, 8, 8, 6 + (i * 9), 10 + (j * 10));
                        else
                            da_copy_xpm_area(coloroff_x, coloroff_y, 8, 8, 6 + (i * 9), 10 + (j * 10));
                    }
                }
            }
        } else {
            for (i = 0; i < 6; i++) {
                clockarray[0][i] = 0;
                clockarray[1][i] = 0;
                clockarray[2][i] = 0;
                clockarray[3][i] = 0;
                clockarray[4][i] = 0;
            }

            tmp_hour = tmworld->tm_hour;

            IntToBinary(tmp_hour, (int **)&tmp_str);

            for (s = 0; s < 6; s++) {
                if (tmp_str[s] == 1) {
                    clockarray[0][5-s] = 1;
                }
            }

            tmp_minute = tmworld->tm_min;

            IntToBinary(tmp_minute, (int **)&tmp_str);

            for (s = 0; s < 6; s++) {
                if (tmp_str[s] == 1) {
                    clockarray[1][5-s] = 1;
                }
            }
    
            tmp_second = tmworld->tm_sec;

            IntToBinary(tmp_second, (int **)&tmp_str);
        
            for (s = 0; s < 6; s++) {
                if (tmp_str[s] == 1) {
                    clockarray[2][5-s] = 1;
                }
            }
        
            tmp_day = tmworld->tm_mday;

            IntToBinary(tmp_day, (int **)&tmp_str);
        
            for (s = 0; s < 6; s++) {
                if (tmp_str[s] == 1) {
                    clockarray[3][5-s] = 1;
                }
            }

            tmp_month = tmworld->tm_mon + 1;

            IntToBinary(tmp_month, (int **)&tmp_str);
        
            for (s = 0; s < 6; s++) {
                if (tmp_str[s] == 1) {
                    clockarray[4][5-s] = 1;
                }
            }

            if (superfluous) {
                for (i = 0; i < 3; i++) {
                    for (j = 0; j < 6; j++) {
                        if (clockarray[i][j] == 1)
                            da_copy_xpm_area(coloron_x, coloron_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                        else
                            da_copy_xpm_area(coloroff_x, coloroff_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                    }
                }
            } else {
                for (i = 0; i < 3; i++) {
                    for (j = iBeginLEDs[1][i]; j < 6; j++) {
                        if (clockarray[i][j] == 1)
                            da_copy_xpm_area(coloron_x, coloron_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                        else
                            da_copy_xpm_area(coloroff_x, coloroff_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                    }
                }
            }

            if (superfluous) {
                for (i = 3; i < 5; i++) {
                    for (j = 0; j < 6; j++) {
                        if (clockarray[i][j] == 1)
                            da_copy_xpm_area(dcoloron_x, dcoloron_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                        else
                            da_copy_xpm_area(coloroff_x, coloroff_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                    }
                }
            } else {
                for (i = 3; i < 5; i++) {
                    for (j = iBeginLEDs[1][i]; j < 6; j++) {
                        if (clockarray[i][j] == 1)
                            da_copy_xpm_area(dcoloron_x, dcoloron_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                        else
                            da_copy_xpm_area(coloroff_x, coloroff_y, 8, 8, 6 + (j * 9), 10 + (i * 10));
                    }
                }
            }
        }

        da_redraw_window();

        usleep(DELAY);
    }

    routine(argc, argv);
}

void IntToBinary(int x, int *str[]) {
    int i = 0;
    int counter = 0;
    
    for(i = 0; i < 6; i++) {
        str[i] = (int *)0;
    }

    while (x > 0) {
        if ((x % 2) != 0) {
            str[counter] = (int *)1;
        } else {
            str[counter] = (int *)0;
        }

        x = x / 2;

        counter++;
    }
}
