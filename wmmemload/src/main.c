/*
 *    WMMemLoad - A dockapp to monitor memory usage
 *    Copyright (C) 2002  Mark Staggs <me@markstaggs.net>
 *
 *    Based on work by Seiichi SATO <ssato@sh.rim.or.jp>
 *    Copyright (C) 2001,2002  Seiichi SATO <ssato@sh.rim.or.jp>

 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.

 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <libdockapp4/dockapp.h>

#include "config.h"
#include "mem.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define MAX_HISTORY 16

static char *light_color;       /* back-light color */
static char *border_dark;
static char *border_light;

static int update_interval;
static int alarm_mem;
static int alarm_swap;

static bool backlight;

static struct mem_options mem_opts;

static int mem_usage;
static int swap_usage;

time_t last_update = 0;

/* prototypes */
static void update(void);
static void switch_light(void);
static void draw_memdigit(int);
static void draw_swapdigit(int);
static void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a Window Maker memory/swap monitor dockapp.\n\n"

    PROG_NAME" is a program to monitor memory/swap usages. It is a dockapp that is supported by X window managers such as Window Maker, AfterStep, BlackBox, and Enlightenment.\n\n"

    "The current memory usage is displayed in the top half. The swap usage is in the bottom half. It has an LCD look-alike user interface. The back-light may be turned on/off by clicking the mouse button over the appliacation. If the usage hits a certain threshold, an alarm-mode will alert you by turning on back-light.");

    da_init_string(&light_color, "front_color", "-f", "--front-color", "#6ec63b", "Main color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&backlight, "backlight", "-b", "--backlight", false, "Turn on LCD backlight");
    da_init_integer(&update_interval, "interval", "-i", "--interval", 1, 60, 1, "Number of secs between updates");
    da_init_integer(&alarm_mem, "alarm_memory", NULL, "--alarm-mem", 0, 100, 100, "Activate alarm mode of memory, where percentage is threshold from 0 to 100");
    da_init_integer(&alarm_swap, "alarm_swap", NULL, "--alarm-swap", 0, 100, 100, "Activate alarm mode of swap, where percentage is threshold from 0 to 100");
#ifdef IGNORE_BUFFERS
    da_init_bool(&mem_opts.ignore_buffers, "ignore_buffers", "-B", "--ignore-buffers", false, "Ignore buffers (GNU/Linux)");
#endif
#ifdef IGNORE_CACHED
    da_init_bool(&mem_opts.ignore_cached, "ignore_cached", "-C", "--ignore-cached", false, "Ignore cached pages (GNU/Linux / FreeBSD)");
#endif
#ifdef IGNORE_WIRED
    da_init_bool(&mem_opts.ignore_wired, "ignore_wired", "-W", "--ignore-wired", false, "Ignore wired pages (FreeBSD)");
#endif
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

static void routine(int argc, char *argv[]) {
    /* Initialize Application */
    if (alarm_mem != 100 && alarm_swap != 100) {
        printf("ERR: select either '--alarm-mem' or '--alarm-swap'.\n");
        da_log(ERR, "select either '--alarm-mem' or '--alarm-swap'.\n");
        exit(1);
    }
    
#ifndef IGNORE_BUFFERS
    mem_opts.ignore_buffers = false;
#endif
#ifndef IGNORE_CACHED
    mem_opts.ignore_cached = false;
#endif
#ifndef IGNORE_WIRED
    mem_opts.ignore_wired = false;
#endif

    mem_init();
    
    da_init_xwindow();

    da_add_color(light_color, "led_color_high");
    da_add_blended_color(light_color, -24, -24, -24, 1.0, "led_color_med");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 192 * da_scale, 113 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 192 * da_scale, 113 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 192 * da_scale, 113 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 192 * da_scale, 113 * da_scale);
        break;
    }

    if (backlight) {
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }

    da_redraw_window();

    /* Main loop */
    while (!da_conf_changed(argc, argv)) {
        if (time(NULL) - last_update >= update_interval)
            update();

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch_light();
            break;
        }

        da_redraw_window();
    
        usleep(250000L);
    }

    routine(argc, argv);
}

/* called by timer */
static void update(void) {
    static bool pre_backlight;
    static bool in_alarm_mode = false;

    /* get current cpu usage in percent */
    mem_getusage(&mem_usage, &swap_usage, &mem_opts);

    /* alarm mode */
    if (mem_usage >= alarm_mem || swap_usage >= alarm_swap) {
        if (!in_alarm_mode) {
            in_alarm_mode = true;
            pre_backlight = backlight;
        }
        if (!backlight) {
            switch_light();
            return;
        }
    } else {
        if (in_alarm_mode) {
            in_alarm_mode = false;
            if (backlight != pre_backlight) {
                switch_light();
                return;
            }
        }
    }

    /* all clear */
    if (backlight) {
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }

    /* draw digit */
    draw_memdigit(mem_usage);
    draw_swapdigit(swap_usage);

    /* show */
    da_redraw_window();
    
    time(&last_update);
}

/* called when mouse button pressed */
static void switch_light(void) {
    if (!backlight) {
        backlight = true;
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        backlight = false;
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }

    /* redraw digit */
    mem_getusage(&mem_usage, &swap_usage, &mem_opts);
    draw_memdigit(mem_usage);
    draw_swapdigit(swap_usage);

    /* show */
    da_redraw_window();
}

static void draw_memdigit(int per) {
    int v100, v10, v1;
    int y = 0;

    if (per < 0)
        per = 0;
    if (per > 100)
        per = 100;

    v100 = per / 100;
    v10  = (per - v100 * 100) / 10;
    v1   = (per - v100 * 100 - v10 * 10);

    if (backlight)
        y = 20;

    /* draw digit */
    da_copy_xpm_area(v1 * 10, 64 + y, 10, 20, 29 + 3, 7 + 3);
    if (v10 != 0)
        da_copy_xpm_area(v10 * 10, 64 + y, 10, 20, 17 + 3, 7 + 3);
    if (v100 == 1) {
        da_copy_xpm_area(10, 64 + y, 10, 20, 5 + 3, 7 + 3);
        da_copy_xpm_area(0, 64 + y, 10, 20, 17 + 3, 7 + 3);
    }
}

static void draw_swapdigit(int per) {
    int v100, v10, v1;
    int y = 0;

    if (per < 0)
        per = 0;
    if (per > 100)
        per = 100;

    v100 = per / 100;
    v10  = (per - v100 * 100) / 10;
    v1   = (per - v100 * 100 - v10 * 10);

    if (backlight)
        y = 20;

    /* draw digit */
    da_copy_xpm_area(v1 * 10, 64 + y, 10, 20, 29 + 3, 34 + 3);
    if (v10 != 0)
        da_copy_xpm_area(v10 * 10, 64 + y, 10, 20, 17 + 3, 34 + 3);
    if (v100 == 1) {
        da_copy_xpm_area(10, 64 + y, 10, 20, 5 + 3, 34 + 3);
        da_copy_xpm_area(0, 64 + y, 10, 20, 17 + 3, 34 + 3);
    }
}
