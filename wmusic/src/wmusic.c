/*  wmusic - a xmms remote-controlling DockApp
 *  Copyright (C) 2000-2001 Bastien Nocera <hadess@hadess.net>
 *  Maintained by John Chapin <john+wmusic@jtan.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define DBL_CLICK_INTERVAL 250	/* double click interval in milliseconds */
#define ARROW_INTERVAL 100	    /* arrow update interval in milliseconds */
#define SCROLL_INTERVAL 300	    /* scroll update interval in milliseconds */
#define SEPARATOR " ** "	    /* The separator for the scrolling title */
#define DISPLAYSIZE 6		    /* width of text to display (running title) */

#include <libdockapp4/dockapp.h>
#include <playerctl/playerctl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

/*---------------------------------------------------------------------------*/
/*                             Prototypes                                    */
/*---------------------------------------------------------------------------*/

void routine(int, char **);
void ToggleVol(float);
void buttonPress(int);
void buttonRelease(int);
int PlayerConnect(void);
void DisplayRoutine(void);
void DrawPos(int);
void DrawTime(int);
void DrawArrow(void);
void DrawVolume(void);
void DrawTitle(char *);
void ExecuteXmms(void);

/*----------------------------------------------------------------------------*/
/*                             Variables                                      */
/*----------------------------------------------------------------------------*/

/* Dockapp variables */
PlayerctlPlayer *player;

bool run;
bool run_excusive;
bool t_time;
bool pause_norotate;

int volume_step;

unsigned int arrow_pos = 0;

float title_pos = 0;

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *xmms_cmd;
char *title_cmp;
char *info;

typedef struct {
    wchar_t c;
    int x;
    int y;
} glyphdescr;

static glyphdescr glyphs[] = {
    {L'-', 67, 83}, {L'.', 73, 83}, {L'\x27', 79, 83},
    {L'(', 85, 83}, {L')', 91, 83}, {L'*', 97, 83}, {L'/', 103, 83},

    {L'0',  1, 83}, {L'1',  7, 83}, {L'2', 13, 83}, {L'3', 19, 83}, {L'4', 25, 83},
    {L'5', 31, 83}, {L'6', 37, 83}, {L'7', 43, 83}, {L'8', 49, 83}, {L'9', 55, 83},



    {L'A',  1, 73}, {L'a',  1, 73},
    {L'B',  7, 73}, {L'b',  7, 73},
    {L'C', 13, 73}, {L'c', 13, 73},
    {L'D', 19, 73}, {L'd', 19, 73},
    {L'E', 25, 73}, {L'e', 25, 73},

    {L'F', 31, 73}, {L'f', 31, 73},
    {L'G', 37, 73}, {L'g', 37, 73},
    {L'H', 43, 73}, {L'h', 43, 73},
    {L'I', 49, 73}, {L'i', 49, 73},
    {L'J', 55, 73}, {L'j', 55, 73},

    {L'K', 61, 73}, {L'k', 61, 73},
    {L'L', 67, 73}, {L'l', 67, 73},
    {L'M', 73, 73}, {L'm', 73, 73},
    {L'N', 79, 73}, {L'n', 79, 73},
    {L'O', 85, 73}, {L'o', 85, 73},

    {L'P', 91, 73}, {L'p', 91, 73},
    {L'Q', 97, 73}, {L'q', 97, 73},
    {L'R',103, 73}, {L'r',103, 73},
    {L'S',109, 73}, {L's',109, 73},
    {L'T',115, 73}, {L't',115, 73},

    {L'U',121, 73}, {L'u',121, 73},
    {L'V',127, 73}, {L'v',127, 73},
    {L'W',133, 73}, {L'w',133, 73},
    {L'X',139, 73}, {L'x',139, 73},
    {L'Y',145, 73}, {L'y',145, 73},

    {L'Z',151, 73}, {L'z',151, 73},


    {L'\x42e',  1, 93}, {L'\x44e',  1, 93}, /* cyrillic Yu */

    {L'\x410',  7, 93}, {L'\x430',  7, 93}, /* cyrillic A */
    {L'\x411', 13, 93}, {L'\x431', 13, 93}, /* cyrillic Be */
    {L'\x426', 19, 93}, {L'\x446', 19, 93}, /* cyrillic Ce */
    {L'\x414', 25, 93}, {L'\x434', 25, 93}, /* cyrillic De */
    {L'\x415', 31, 93}, {L'\x435', 31, 93}, /* cyrillic Ye */

    {L'\x424', 37, 93}, {L'\x444', 37, 93}, /* cyrillic eF */
    {L'\x413', 43, 93}, {L'\x433', 43, 93}, /* cyrillic Ge */
    {L'\x425', 49, 93}, {L'\x445', 49, 93}, /* cyrillic Ha */
    {L'\x418', 55, 93}, {L'\x438', 55, 93}, /* cyrillic I */
    {L'\x419', 61, 93}, {L'\x439', 61, 93}, /* cyrillic I-kratkoe */

    {L'\x41a', 67, 93}, {L'\x43a', 67, 93}, /* cyrillic Ka */
    {L'\x41b', 73, 93}, {L'\x43b', 73, 93}, /* cyrillic eL */
    {L'\x41c', 79, 93}, {L'\x43c', 79, 93}, /* cyrillic eM */
    {L'\x41d', 85, 93}, {L'\x43d', 85, 93}, /* cyrillic eN */
    {L'\x41e', 91, 93}, {L'\x43e', 91, 93}, /* cyrillic O */

    {L'\x41f', 97, 93}, {L'\x43f', 97, 93}, /* cyrillic Pe */
    {L'\x42f',103, 93}, {L'\x44f',103, 93}, /* cyrillic Ya */
    {L'\x420',109, 93}, {L'\x440',109, 93}, /* cyrillic eR */
    {L'\x421',115, 93}, {L'\x441',115, 93}, /* cyrillic eS */
    {L'\x422',121, 93}, {L'\x442',121, 93}, /* cyrillic Te */

    {L'\x423',127, 93}, {L'\x443',127, 93}, /* cyrillic U */
    {L'\x416',133, 93}, {L'\x436',133, 93}, /* cyrillic Je */
    {L'\x412',139, 93}, {L'\x432',139, 93}, /* cyrillic Ve */
    {L'\x42c',145, 93}, {L'\x44c',145, 93}, /* cyrillic MyagkijZnak */
    {L'\x42b',151, 93}, {L'\x44b',151, 93}, /* cyrillic Y */

    {L'\x417',157, 93}, {L'\x437',157, 93}, /* cyrillic Ze */
    {L'\x428',163, 93}, {L'\x448',163, 93}, /* cyrillic Sha */
    {L'\x42d',169, 93}, {L'\x44d',169, 93}, /* cyrillic E */
    {L'\x429',175, 93}, {L'\x449',175, 93}, /* cyrillic Scha */
    {L'\x427',181, 93}, {L'\x447',181, 93}, /* cyrillic Che */

    {L'\x42a',187, 93}, {L'\x44a',187, 93}, /* cyrillic TvyordyiZnak */
    {L'\x404',115, 83}, {L'\x454',115, 83}, /* ukrainian IE */
    {L'\x406', 49, 73}, {L'\x456', 49, 73}, /* ukrainian I */
    {L'\x407',109, 83}, {L'\x457',109, 83}, /* ukrainian YI */
    {L'\x491', 43, 93}, {L'\x490', 43, 93}, /* ukrainian GHE with upturn */

    {L'\x401',121, 83}, {L'\x451',121, 83}, /* cyrillic Yo */

    {L' ', 61, 83}
};

/*----------------------------------------------------------------------------*/
/*                              Functions                                     */
/*----------------------------------------------------------------------------*/

void ToggleVol(float f) {
    double volume;
    double factor;

    if (player) {
        g_object_get(player, "volume", &volume, NULL);

        factor = f * volume_step;

        volume += factor;

        if (volume > 1)
            volume = 1;
        if (volume < 0)
            volume = 0;

        playerctl_player_set_volume(player, volume, NULL);  
    }
}

void buttonPress(int mouse_region) {
    GError *error = NULL;

    int x;

    float volume;

    switch (mouse_region) {
    case 0:
        da_copy_xpm_area(0, 95, 14, 9, 5, 39);        
        break;

    case 1:
        da_copy_xpm_area(14, 95, 14, 9, 19, 39);      
        break;

    case 2:
        da_copy_xpm_area(28, 95, 13, 9, 33, 39);      
        break;

    case 3:
        da_copy_xpm_area(41, 95, 13, 9, 46, 39);      
        break;

    case 4:
        da_copy_xpm_area(0, 104, 11, 11, 5, 48);     
        break;

    case 5:
        da_copy_xpm_area(11, 104, 21, 11, 16, 48);    
        break;

    case 6:
        da_copy_xpm_area(32, 104, 11, 11, 37, 48);     
        break;

    case 7:
        da_copy_xpm_area(43, 104, 11, 11, 48, 48);     
        break;

    case 8:
        if (!player)
            ExecuteXmms();
        break;

    case 9:
        t_time = !t_time;
        break;

    case 10:
        if (player) {
            x = da_xevent.xbutton.x - (7 * da_scale);

            if (x < 0)
                x = 0;
            if (x > 37 * da_scale)
                x = 37 * da_scale;

            volume = (float)x / (37 * da_scale);

            da_copy_xpm_area(98, 64, 37, 6, 7, 18);
            da_copy_xpm_area(61, 64, x / da_scale, 6, 7, 18);

            playerctl_player_set_volume(player, volume, &error);
        } 
        break;
    }

    da_redraw_window();
}

void buttonRelease(int mouse_region) {
    GError *error = NULL;

    da_copy_xpm_area(0, 64 + 51, 54, 20, 5, 39);

    switch (mouse_region) {
    case 0:
        if (player)
            playerctl_player_previous(player, &error);        
        break;

    case 1:
        if (player)
            playerctl_player_next(player, &error);        
        break;

    case 2:
        if (player)
            playerctl_player_seek(player, -10000000, &error);      
        break;

    case 3:
        if (player)
            playerctl_player_seek(player, 10000000, &error);      
        break;

    case 4:
        printf("Eject function is no longer supported.\n");     
        break;

    case 5:
        if (player)
            playerctl_player_play(player, &error);     
        break;

    case 6:
        if (player)
            playerctl_player_pause(player, &error);      
        break;

    case 7:
        if (player)
            playerctl_player_stop(player, &error);     
        break;
    }

    if (error != NULL)
        printf("Could not execute command: %s\n", error->message);

    da_redraw_window();
}

int PlayerConnect(void) {
    GError *error = NULL;

    static int previous_error_code = 0;

    static char *player_name = NULL;
    
    if (player != NULL) {
        g_object_unref(player);
    }
    
    player = playerctl_player_new(NULL, &error);

    if (error != NULL) {
        /* don't spam error message */
        if (error->code != previous_error_code) {
            printf("Connection to player failed: %s\n", error->message);
            
            if (info != NULL) {
                free(info);

                info = NULL;
            }
            
            info = strdup(error->message);
        }

        previous_error_code = error->code;

        player_name = NULL;

        return 0;

    } else {
        previous_error_code = 0;

        if (!player_name) {
            g_object_get(player, "player_name", &player_name, NULL);

            if (player_name) {
                printf("Connected to %s\n", player_name);
                
                if (info != NULL) {
                    free(info);

                    info = NULL;
                }
                
                info = malloc(14 + strlen(player_name));
                
                sprintf(info, "Connected to %s", player_name);
            }
        }

        return 1;
    }
}

void DisplayRoutine(void) {
    unsigned long long time = 0, length = 0;

    char *title = NULL;

    GError *error = NULL;

    PlayerConnect();
    
    if (!title_cmp) {
        title_cmp = strdup("  ");
    }

    /* Compute diplay */
    if (!player) {
        if (run_excusive)
            exit(0);

        title = strdup(info);

        arrow_pos = 5;
    } else {
        char *length_str, *status;

        g_object_get(player, "status", &status, NULL);

        if (status) {
            if (!strcmp(status, "Playing") || !strcmp(status, "Paused")) {
                g_object_get(player, "position", &time, NULL);

                title = playerctl_player_get_title(player, &error);

                if (error != NULL)
                    printf("%s\n", error->message);
                    
                if (!title) {
                    title = strdup("  ");
                }

                length_str = playerctl_player_print_metadata_prop(player, "mpris:length", &error);

                if (error != NULL)
                    printf("%s\n", error->message);

                if (length_str) {
                    sscanf(length_str, "%llu", &length);
                } else {
                    length = 0;
                }

                if (!strcmp(status, "Paused") && pause_norotate)
                    arrow_pos = 5;

            } else { /* not playing or paused */
                title = strdup(info);

                arrow_pos = 5;
            }

        } else { /* status undefined */
            title = strdup("--");

            title_pos = 0;
            arrow_pos = 5;
        }
    }
    
    if (strcmp(title_cmp, title) != 0) {
        if (title_cmp != NULL) {
            free(title_cmp);

            title_cmp = NULL;
        }
                    
        title_cmp = strdup(title);
                    
        title_pos = 0;
    }

    /*Draw everything */
    if (t_time && length)
        DrawTime((length - time) / 1000);
    else 
        DrawTime(time / 1000);

    DrawArrow();
    DrawVolume();
    DrawTitle(title);

    if (title != NULL) {
        free(title);

        title = NULL;
    }
}

void DrawPos(int pos) {
    char posstr[16];

    char *p = posstr;

    int i=1;

    if (pos > 99) 
        pos=0;

    sprintf(posstr, "%02d", pos);

    for (;i<3; i++) {
        da_copy_xpm_area((*p-'0')*6 + 1, 64 + 1, 6, 7, (i*6)+39, 7);
        p++;
    }
}

void DrawTime(int time) {
    char timestr[16];
    char secstr[16];

    char *p = timestr;
    char *pp = secstr;

    int i;

    time = time / 1000;

    /* 2 cases:
     *     up to 99 hours and 59 minutes and 59 seconds
     *     more
     */
    if (time < 360000) {
        sprintf(timestr, "%02d%02d", time / 3600, time % 3600 / 60);
        sprintf(secstr, "%02d", time % 60);
    } else {
        sprintf(timestr, "%02d%02d", 0, 0);
        sprintf(secstr, "%02d", 0);
    }

    for (i = 0; i < 2; i++) {
        da_copy_xpm_area((*pp - '0') * 6 + 1, 64 + 1, 6, 7, (i * 6) + 45, 7);
        
        pp++;
    }

    for (i = 0; i < 4; i++) {
        da_copy_xpm_area((*p - '0') * 7 + 2, 64 + 11, 7, 9, i < 2 ? (i * 7) + 7 : (i * 7) + 12, 7);
        
        p++;
    }
}

void DrawArrow(void) {
    da_copy_xpm_area((arrow_pos*8)+30, 64 + 22, 8, 9, 47, 19);

    arrow_pos++;

    if (arrow_pos > 4) 
        arrow_pos = 0;
}

void DrawVolume(void) {
    int volume;

    static double volume_d_old = -1.0;

    double volume_d = 0.0;

    if (player)
        g_object_get(player, "volume", &volume_d, NULL);

    if (volume_d_old != volume_d) {
        volume_d_old = volume_d;

        volume = 37 * volume_d_old;

        if (volume > 37)
            volume = 37;

        da_copy_xpm_area(98, 64, 37, 6, 7, 18);
        da_copy_xpm_area(61, 64, volume, 6, 7, 18);
    }
}

void DrawKbps(int bps) {
    char kbpstr[16];

    char *p = kbpstr;

    int i=1;

    if (bps > 999000) 
        bps=0;

    sprintf(kbpstr, "%03d", bps / 1000);

    for (;i<4; i++) {
        da_copy_xpm_area((*p-'0')*6 + 1, 64 + 1, 6, 7, (i*6)+1, 26);
        p++;
    }

    da_copy_xpm_area(55, 64 + 39, 18, 8, 25, 26);
}

void DrawChar(wchar_t wc, int x, int y) {
    int i;

    for(i = 0; i < (int)(sizeof(glyphs) / sizeof(glyphdescr) - 1); ++i) {
        if(wc == glyphs[i].c)
            break;
    }

    da_copy_xpm_area(glyphs[i].x, 64 + glyphs[i].y, 6, 8, x, y);
}

int DrawChars(char *title, int tpos, int pos) {
    wchar_t wc;

    mbtowc(NULL, NULL, 0);

    while(*title && (pos <= (tpos + DISPLAYSIZE))) {
        int len = mbtowc(&wc, title, MB_CUR_MAX);

        title += len;

        if(pos >= tpos)
            DrawChar(wc, (pos - tpos)*6 + 7, 26);

        ++pos;
    }

    return pos;
}

void DrawTitle(char *name) {
    int len, pos, tpos = (int)title_pos;

    if (name == NULL)
        return;

    len = pos = DrawChars(name, tpos, 0);

    if (pos < 6) {
        DrawChars("      ", tpos, pos);
        
        return;
    }

    if (pos <= tpos + DISPLAYSIZE)
        pos = DrawChars(SEPARATOR, tpos, pos);

    if (pos <= tpos + DISPLAYSIZE)
        DrawChars(name, tpos, pos);

    if (tpos >= (int)(len + strlen(SEPARATOR)))
        title_pos = 0;

    title_pos = title_pos + 0.5;
}

void ExecuteXmms(void) {
    char *command;

    int status;

    command=malloc(strlen(xmms_cmd)+5);

    sprintf(command, "%s &", xmms_cmd);

    status = system(command);

    if (status) {
        fprintf(stderr, "XMMS can't be launched, exiting...");
        exit(1);
    }

    while (!PlayerConnect())
        usleep(10000L);

    free(command);

    command = NULL;
}
/*----------------------------------------------------------------------------*/
/*                                   Main                                     */
/*----------------------------------------------------------------------------*/

void routine(int argc, char *argv[]) {
    setlocale(LC_ALL, "");

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;

    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;
    }

    da_add_mouse_region(0, 5, 39, 19, 48); //ActionPrev
    da_add_mouse_region(1, 19, 39, 33, 48); //ActionNext
    da_add_mouse_region(2, 33, 39, 46, 48); //ActionFastr
    da_add_mouse_region(3, 46, 39, 59, 48); //ActionFastf
    da_add_mouse_region(4, 5, 48, 16, 59); //ActionEject
    da_add_mouse_region(5, 16, 48, 37, 59); //ActionPlay
    da_add_mouse_region(6, 37, 48, 48, 59); //ActionPause
    da_add_mouse_region(7, 48, 48, 59, 59); //ActionStop
    da_add_mouse_region(8, 5, 25, 59, 35); //ToggleWins
    da_add_mouse_region(9, 5, 5, 59, 17); //ToggleTime
    da_add_mouse_region(10, 7, 18, 44, 24); //ChangeVol

    /* End of initialization */

    PlayerConnect();

    /* Launch xmms if it's not running and -r or -R was used */
    if ((!player) && (run || run_excusive)) {
        ExecuteXmms();
    }

    /* Update the display */
    DisplayRoutine();

    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            buttonPress(da_check_mouse_region());
            break;

        case MOUSE_1_REL:
            buttonRelease(da_check_mouse_region());
            break;

        case MOUSE_4:
            ToggleVol(0.01);
            break;

        case MOUSE_5:
            ToggleVol(-0.01);
            break;
        }

        DisplayRoutine();

        da_redraw_window();

        usleep(150000);
    }

    routine(argc, argv);
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "MPRIS-compatible media player remote control by Bastien Nocera <hadess@hadess.net>.\n\n"

    "Here is a list of the features:\n"
    "- VCR style controls including fast rewind and fast forward\n"
    "- Time and Playlist position display\n"
    "- Super stylee rotating arrow");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&xmms_cmd, "command", "-c", "--command", "xmms", "Command to launch the media player");
    da_init_bool(&run, "run", "-r", "--run", false, "Run the media player on startup");
    da_init_integer(&volume_step, "volume_step", "-V", "--volume", 1, 100, 5, "Stepping of the wheel volume control (in percent)");
    da_init_bool(&pause_norotate, "no_rotate", "-a", "--no-rotate", false, "Do not rotate the arrow, when paused");
    da_init_bool(&t_time, "time_left", "-t", "--time-left", false, "Show time left instead of time remaining by default");
    da_init_bool(&run_excusive, "run_excusive", "-R", "--run-excusive", false, "Run media player on startup, exit when it exits");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");    /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                       /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}
