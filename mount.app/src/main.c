/*
 * Copyright (c) 2021 Xander
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/vfs.h>
#include <sys/wait.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define MAX_FSYSTEMS 20

enum Types {
    CDROM,
    DOSDISK,
    FLOPPY,
    HARDDISK,
    MACDISK,
    SLOTIN,
    ZIP,
    ZIPEXT,
    OTHER
};

struct FileSystem {
    char *mount_point;
    unsigned space_left;
    bool mounted;
    int type;
} file_system[MAX_FSYSTEMS];

char *border_dark;
char *border_light;
char *fm;

bool display_mpoint;

int total_fsystems = 0;
int current_fsystem = 0;

void umount(void);
void mount(void);
void free_all(void);
bool is_mounted(char *);
void open_mountpoint(void);
void draw_free_space(void);
void draw_mpoint(void);
void draw_icon(void);
void draw_string(char *, int, int);
void draw_fsystem_info(void);
void get_fsystems_info(void);
void button_press(int);
void button_release(int);
void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dock application that handles user-mountable file systems via fstab.\n\n"

    PROG_NAME" is a \"clone\" of an old mount app using its UI; however, the code was completely rewritten because it was bloated.\n\n"
    
    "Press the \"<\" and \">\" arrow buttons to select a mount point. Press the mount button (the one with a bolt symbol) to mount or unmount. Click on the drive icon to open a program at the mount point (Midnight Commander for example).\n\n"
    
    "If you run mount.app as non-root, and none of your devices are marked as user-mountable, then mount.app will display only the \"None\" message and the buttons will be inactive and grayed-out. Read the documentation that came with your distribution about how to make devices user mountable. In general it involves adding the option \"user\" to the list of options in /etc/fstab. (The mount(8) and fstab(5) man pages are good reading as well)");

    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&fm, "file_manager", "-f", "--file-manager", NULL, "File manager used to open mountpoints via mouse click");
    da_init_bool(&display_mpoint, "display_mpoint", "-m", "--display-mpoint", false, "Display mount point instead of type");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                 /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/mountapp.conf");  /* modern conf path */
    da_init_conf_file(LOCAL, ".mountapprc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 156 * da_scale, 113 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 156 * da_scale, 113 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 156 * da_scale, 113 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 156 * da_scale, 113 * da_scale);
        break;
    }
    
    da_add_mouse_region(0,  5, 48, 33, 59); /* mount */
    da_add_mouse_region(1, 33, 48, 46, 59); /* prev */
    da_add_mouse_region(2, 46, 48, 59, 59); /* next */
    da_add_mouse_region(3, 26, 31, 58, 41); /* open */
    
    get_fsystems_info();
    
    draw_fsystem_info();

    da_redraw_window();
    
    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (total_fsystems > 0)
                button_press(da_check_mouse_region());
            break;
            
        case MOUSE_1_REL:
            if (total_fsystems > 0)
                button_release(da_check_mouse_region());
            break;
        }
        
        draw_fsystem_info();

        da_redraw_window();

        usleep(150000);
    }
    
    free_all();

    routine(argc, argv);
}

void button_press(int mouse_region) {
    switch (mouse_region) {
    case 0:
        if (!file_system[current_fsystem].mounted) {
            da_copy_xpm_area(0, 75, 28, 11, 5, 48);
            
            file_system[current_fsystem].mounted = true;
        
        } else {
            da_copy_xpm_area(0, 64, 28, 11, 5, 48);
            
            file_system[current_fsystem].mounted = false;
        }
        break;
        
    case 1:
        da_copy_xpm_area(28, 75, 13, 11, 33, 48);
        break;
        
    case 2:
        da_copy_xpm_area(41, 75, 13, 11, 46, 48);
        break;
        
    case 3:
        break;
    }
        
    da_redraw_window();
}

void button_release(int mouse_region) {
    switch (mouse_region) {
    case 0:
        if (!file_system[current_fsystem].mounted) {
            umount();
        
        } else {
            mount();
        }
        break;
        
    case 1:
        da_copy_xpm_area(28, 64, 13, 11, 33, 48);
        
        current_fsystem--;
        
        if (current_fsystem < 0)
            current_fsystem = total_fsystems - 1;
        
        break;
        
    case 2:
        da_copy_xpm_area(41, 64, 13, 11, 46, 48);
        
        current_fsystem++;
        
        if (current_fsystem >= total_fsystems)
            current_fsystem = 0;
        
        break;
        
    case 3:
        if (file_system[current_fsystem].mounted && fm)
            open_mountpoint();
            
        break;
    }

    da_redraw_window();
}

void get_fsystems_info(void) {
    FILE *fstab;
    
    int i;
    
    size_t len = 0;
    
    ssize_t read;
    
    char *line;
    
    char fsystem[256];
    char mpoint[256];
    char type[256];
    char options[256];
    char dump[256];
    char pass[256];
    
    fstab = fopen("/etc/fstab", "r");
    
    if (fstab == NULL) {
        printf("ERR: failed to open /etc/fstab.\n");
        da_log(ERR, "failed to open /etc/fstab.\n");
        exit(1);
    
    } else {
        while ((read = getline(&line, &len, fstab)) != -1) {
            if (*line == '#') {
                continue;
            }
            
            sscanf(line, "%s %s %s %s %s %s", fsystem, mpoint, type, options, dump, pass);
            
            for (i = 0; i < (int)strlen(options); i++) {
                if (options[i]     == 'u' &&
                    options[i + 1] == 's' &&
                    options[i + 2] == 'e' &&
                    options[i + 3] == 'r') {
                    file_system[total_fsystems].mount_point = strdup(mpoint);
                    
                    file_system[total_fsystems].mounted = is_mounted(mpoint);
                    
#ifdef __FreeBSD__
                    if (!strcmp(type, "cd9660"))
#else
                    if (!strcmp(type, "iso9660"))
#endif
                        file_system[total_fsystems].type = CDROM;
                    
                    else if (!strcmp(type, "ext2") ||
                             !strcmp(type, "ext3") ||
                             !strcmp(type, "ext4") ||
                             !strcmp(type, "btrfs") ||
                             !strcmp(type, "ufs") ||
                             !strcmp(type, "xfs"))
                        file_system[total_fsystems].type = HARDDISK;
                            
                    else if (!strcmp(type, "vfat") ||
                             !strcmp(type, "fat") ||
                             !strcmp(type, "msdos"))
                        file_system[total_fsystems].type = DOSDISK;
                        
                    else if (!strcmp(type, "hfs"))
                        file_system[total_fsystems].type = MACDISK;
                    
                    total_fsystems++;
                    
                    break;
                }
            }
        }
        
        fclose(fstab);
    }
    
    if (line) {
        free(line);

        line = NULL;
    }
}

void draw_fsystem_info(void) {
    da_copy_xpm_area(96, 0, 54, 37, 5, 5);
    
    if (total_fsystems == 0) {
        da_copy_xpm_area(0, 86, 54, 11, 5, 48);
        
        draw_string("None", 5, 6);
        
    } else {
        draw_mpoint();
    
        draw_icon();
    
        if (file_system[current_fsystem].mounted) {
            da_copy_xpm_area(0, 75, 28, 11, 5, 48);
            
            draw_free_space();
            
        } else {
            da_copy_xpm_area(0, 64, 28, 11, 5, 48);
        }
    }
}

void draw_mpoint(void) {
    if (display_mpoint) {
        draw_string(file_system[current_fsystem].mount_point, 5, 6);
    
    } else {
        switch (file_system[current_fsystem].type) {
        case CDROM:
            draw_string("Cdrom", 5, 6);
            break;
        
        case HARDDISK:
            draw_string("Harddisk", 5, 6);
            break;
        
        case DOSDISK:
            draw_string("Dosdisk", 5, 6);
            break;
        
        case MACDISK:
            draw_string("Macdisk", 5, 6);
            break;
        
        default:
            draw_string("Other", 5, 6);
            break;
        }
    }
}

void draw_free_space(void) {
    struct statfs sfs;
    
    char tmp[256];
    
    int avail;
    
    statfs(file_system[current_fsystem].mount_point, &sfs);
    
    avail = 100 - (int)((sfs.f_blocks - sfs.f_bfree) * 100.0 / (sfs.f_blocks - sfs.f_bfree + sfs.f_bavail) + .5);
    /*avail = (int)(sfs.f_bsize * sfs.f_bavail) / (sfs.f_blocks * sfs.f_bsize) * 100;
    total_size = (double)(sfs.f_blocks) * (sfs.f_bsize);
    avail_size = (double)(sfs.f_bavail) * (sfs.f_bsize);
    free_size = (double)(sfs.f_bfree) * (sfs.f_bsize);*/
    
    sprintf(tmp, "%i%% Free", avail);
    
    draw_string(tmp, 5, 18);
}

void draw_icon(void) {
    switch (file_system[current_fsystem].type) {
    case CDROM:
        da_copy_xpm_area(64, 70, 32, 10, 26, 31);
        break;
        
    case HARDDISK:
        da_copy_xpm_area(64, 40, 32, 10, 26, 31);
        break;
        
    case DOSDISK:
        da_copy_xpm_area(64, 60, 32, 10, 26, 31);
        break;
        
    case MACDISK:
        da_copy_xpm_area(64, 30, 32, 10, 26, 31);
        break;
        
    default:
        da_copy_xpm_area(64, 80, 32, 10, 26, 31);
        break;
    }
}

void draw_string(char *name, int x, int y) {
    char tmp[10] = "\0\0\0\0\0\0\0\0\0\0";
    
	static int i, c, k;
	
	int len;
	
	len = strlen(name);
	
	for (i = 0; i < len; i++) {
	    if (i > 8)
	        break;
	    
        tmp[i] = name[i];
    }

	k = x;
	
	for (i = 0; tmp[i]; i++) {
		c = tmp[i];
		
		if (c == '/') {
		    da_copy_xpm_area(60, 97, 6, 8, k, y);
			
    		k += 6;
		
		} else if (c == '%') {
		    da_copy_xpm_area(66, 97, 6, 8, k, y);
			
    		k += 6;
    		
		} else {
		    if (c >= 'A' && c <= 'Z') {
			    c -= 'A';
			
			    da_copy_xpm_area(c * 6, 105, 6, 8, k, y);
			
    			k += 6;
            
            } else if (c >= 'a' && c <= 'z') {
			    c -= 'a';
			
			    da_copy_xpm_area(c * 6, 113, 6, 5, k, y + 3);
			
    			k += 6;
    			
		    } else {
			    c -= '0';
			
			    da_copy_xpm_area(c * 6, 97, 6, 8, k, y);
			
			    k += 6;
		    }
		}
	}
}

void free_all(void) {
    int i;
    
    if (total_fsystems == 0)
        return;
    
    for (i = 0; i < total_fsystems; i++) {
        free(file_system[i].mount_point);
        
        file_system[i].mount_point = NULL;
    }
    
    total_fsystems = 0;
    current_fsystem = 0;
}

bool is_mounted(char *mpoint) {
    FILE *proc_mounts;
    
    char tmp[256];
    
    proc_mounts = fopen("/proc/mounts", "r");
    
    if (proc_mounts == NULL) {
        printf("ERR: failed to open /proc/mounts.\n");
        da_log(ERR, "failed to open /proc/mounts.\n");
        exit(1);
    
    } else {
        while ((fscanf(proc_mounts, "%s", tmp)) != EOF) {
            if (!strcmp(tmp, mpoint)) {
                fclose(proc_mounts);
                
                return true;
            }
        }
        
        fclose(proc_mounts);
    }
    
    return false;
}

void umount(void) {
    char command[256];
    
    sprintf(command, "umount %s", file_system[current_fsystem].mount_point);

    int ret = system(command);
    
    if (WEXITSTATUS(ret) != 0 || is_mounted(file_system[current_fsystem].mount_point)) {
        da_copy_xpm_area(0, 75, 28, 11, 5, 48);
        
        file_system[current_fsystem].mounted = true;
    }
}

void mount(void) {
    char command[256];
    
    sprintf(command, "mount %s", file_system[current_fsystem].mount_point);

    int ret = system(command);
    
    if (WEXITSTATUS(ret) != 0 || !is_mounted(file_system[current_fsystem].mount_point)) {
        da_copy_xpm_area(0, 64, 28, 11, 5, 48);
        
        file_system[current_fsystem].mounted = false;
    }
}

void open_mountpoint(void) {
    char command[256];
    
    sprintf(command, "%s %s", fm, file_system[current_fsystem].mount_point);
    
    da_exec_command(command);
}
