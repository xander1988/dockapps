/*
 * $Id: wmifinfo.c,v 1.4 2004/07/11 12:00:46 ico Exp $
 */

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>
#include <libdockapp4/dockapp.h>
#ifdef linux
#include <getopt.h>
#endif
#if defined(__OpenBSD__)
#include <net/if_dl.h>
#include <net/if_types.h>
#include <net/route.h>
#include <sys/param.h>
#include <sys/sysctl.h>
#include <netinet/if_ether.h>
#include <net/if_ieee80211.h>
#include <dev/ic/if_wi_ieee.h>
#include <dev/ic/if_wireg.h>
#include <dev/ic/if_wi_hostap.h>
#define ROUNDUP(a) ((a) > 0 ? (1 + (((a) - 1) | (sizeof(long) - 1))) : sizeof(long))
#endif
#include "config.h"
#ifdef ENABLE_NWN_SUPPORT
#include "nwn.h"
#endif

/* led */
#include "bitmaps/wmifinfo_led.xpm"
#include "bitmaps/wmifinfo_led2.xpm"
#include "bitmaps/wmifinfo_led3.xpm"
#include "bitmaps/wmifinfo_led4.xpm"
/* lcd */
#include "bitmaps/wmifinfo_lcd.xpm"
#include "bitmaps/wmifinfo_lcd2.xpm"
#include "bitmaps/wmifinfo_lcd3.xpm"
#include "bitmaps/wmifinfo_lcd4.xpm"
/* old classic */
#include "bitmaps/wmifinfo_old.xpm"
#include "bitmaps/wmifinfo_old2.xpm"
#include "bitmaps/wmifinfo_old3.xpm"
#include "bitmaps/wmifinfo_old4.xpm"

#define MAXIFS 10
#define DELAY 250000L
#define MODE_LED 1
#define MODE_LCD 2
#define MODE_OLD 3

struct ifinfo_t {
    char id[16];
    int state;
    unsigned char hw[6];
    uint32_t ip;
    uint32_t nm;
    uint32_t gw;
    int sl;
    int bytes;
    int packets;
};

struct font_t {
    char *chars;
    int sx;
    int sy;
    int dx;
    int dy;
    int charspline;
};

char *front_color;
char *border_dark;
char *back_color;
char *border_light;
char *startif;
char *exec_up;
char *exec_down;
char *mode_name;

struct font_t font1 = { " 0123456789ABCDEF", 89, 74, 4, 5, 17};
struct font_t font2 = { "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789: ", 1, 65, 6, 8, 26};

int mode;
int ifaces;
int pifno = -1;
int ifno = 0;
int fd = 0;

double interval;

bool backlight;
bool skip_lo;
bool skip_vlan;

char ifname[MAXIFS][16];

struct ifinfo_t ifinfo;

struct ifconf ifc;

void routine(int, char **);
void getifnames(void);
int getifinfo(char *, struct ifinfo_t *);
void drawipaddr(uint32_t, int);
void drawhwaddr(unsigned char *);
void addifname(char *);
void drawtext(char *, struct font_t *, int, int);
void draw_led_text(char *, int, int);
void draw_led_text_sm(char *, int, int);
void switch_light(void);
void draw_id(void);
void draw_data(void);
void draw_signal_level(void);
void draw_load_level(void);

void draw_load_level(void) {
    static int lastbytes = 0;
    static int curr_pkt = 0;
    
    int x;

    /* Meter */
    if (mode == MODE_LCD) {
        x = 0;

        da_copy_xpm_area(backlight ? 134 : 70, 14, 52, 4, 6, 14);
        
        if (ifinfo.packets) {
            if (curr_pkt != ifinfo.packets) {
                x = ifinfo.packets - curr_pkt;
                
                curr_pkt = ifinfo.packets;
            }
        }

        if (x < 0) {
            x = 0;
        }
        if (x > 13) {
            x = 13;
        }

        da_copy_xpm_area(4, backlight ? 108 : 83, 4 * x, 4, 6, 14);
    
    } else {
        /* Indicator */
        x = 0;

        if (!ifinfo.packets) {
            x = mode == MODE_OLD ? 16 : 12;
        
        } else if (da_exec_busy) {
            x = 0;
        
        } else {
            if (ifinfo.state)
                x += mode == MODE_OLD ? 8 : 6;
            
            if (lastbytes == ifinfo.bytes)
                x += mode == MODE_OLD ? 16 : 12;
        }

        if (mode == MODE_OLD) {
            da_copy_xpm_area(64 + x, 81, 8, 8, 50, 5);
        
        } else if (mode == MODE_LED) {
            da_copy_xpm_area(64 + x, 19, 6, 6, 52, 7);
        }

        lastbytes = ifinfo.bytes;
    }
}

void draw_signal_level(void) {
    int x, j;

#ifdef linux
    x = ifinfo.sl / 4;
#elif defined(__OpenBSD__)
    x = ifinfo.sl / 7;
#endif

    if (x > 13)
        x = 13;

    if (mode == MODE_LCD) {
        da_copy_xpm_area(backlight ? 134 : 70, 54, 52, 4, 6, 54);
        da_copy_xpm_area(4, backlight ? 108 : 83, 4 * x, 4, 6, 54);
    
    } else if (mode == MODE_OLD) {
        da_copy_xpm_area(69, 53, 54, 6, 5, 53);
        da_copy_xpm_area(4, 83, 4 * x, 4, 6, 54);
    
    } else if (mode == MODE_LED) {
        da_copy_xpm_area(167, 22, 54, 8, 5, 19);
        
        for (j = 0; j < 26; j++) {
            if (ifinfo.sl <= 0) {
                da_set_foreground(5);
        
            } else {
                if (j <= (ifinfo.sl * 26) / 100)
                    da_set_foreground(3);
                else
                    da_set_foreground(5);
            }
        
            da_fill_rectangle(6 + 2 * j, 20, 1, 7);
        }

        da_set_foreground(2);
    }
}

void draw_data(void) {
    if (ifinfo.ip) {
        drawipaddr(ifinfo.ip, 0);
    
    } else {
        if (mode == MODE_LCD) {
            da_copy_xpm_area(backlight ? 133 : 69, 20, 54, 5, 5, 20);
        
        } else if (mode == MODE_LED) {
            da_copy_xpm_area(64, 33, 54, 32, 5, 27);
        
        } else if (mode == MODE_OLD) {
            da_copy_xpm_area(69, 20, 54, 5, 5, 20);
        }
    }

    if (ifinfo.nm) {
        drawipaddr(ifinfo.nm, 1);
    
    } else {
        if (mode == MODE_LCD) {
            da_copy_xpm_area(backlight ? 133 : 69, 29, 54, 5, 5, 29);
        
        } else if (mode == MODE_LED) {
            da_copy_xpm_area(64, 33, 54, 32, 5, 27);
        
        } else if (mode == MODE_OLD) {
            da_copy_xpm_area(69, 29, 54, 5, 5, 29);
        }
    }

    if (ifinfo.gw) {
        drawipaddr(ifinfo.gw, 2);
    
    } else {
        if (mode == MODE_LCD) {
            da_copy_xpm_area(backlight ? 133 : 69, 38, 54, 5, 5, 38);
        
        } else if (mode == MODE_LED) {
            da_copy_xpm_area(64, 33, 54, 32, 5, 27);
        
        } else if (mode == MODE_OLD) {
            da_copy_xpm_area(69, 38, 54, 5, 5, 38);
        }
    }

    if (memcmp(ifinfo.hw, "\x00\x00\x00\x00\x00\x00", 6) != 0) {
        drawhwaddr(ifinfo.hw);
    
    } else {
        if (mode == MODE_LCD) {
            da_copy_xpm_area(backlight ? 133 : 69, 47, 54, 5, 5, 47);
        
        } else if (mode == MODE_LED) {
            da_copy_xpm_area(64, 33, 54, 32, 5, 27);
        
        } else if (mode == MODE_OLD) {
            da_copy_xpm_area(69, 47, 54, 5, 5, 47);
        }
    }
}

void draw_id(void) {
    char buf[16];

    sprintf(buf, "%-7s", ifinfo.id);
    
    da_str_to_upper(buf);

    switch (mode) {
    case MODE_LED:
        da_copy_xpm_area(167, 22, 43, 10, 5, 5);
        
        draw_led_text(buf, 5, 5);
        
        break;
    
    case MODE_LCD:
        drawtext(buf, &font2, 6, 6);
        
        break;
    
    case MODE_OLD:
        drawtext(buf, &font2, 6, 5);
        
        break;
    }
}

void switch_light(void) {
    if (!backlight) {
        backlight = true;
        
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        backlight = false;
        
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }
}

void draw_led_text_sm(char *text, int dst_x, int dst_y) {
    int length;
    int i;
    int src_x, src_y;

    text = da_str_to_upper(text);

    length = strlen(text);

    for (i = 0; i < length; i++) {
        if (text[i] == ' ') {
            src_x = 129;
            src_y = 25;
        } else if (text[i] < 'A') {
            src_x = 65 + ((int)(text[i] - '0') * 4);
            src_y = 25;
        } else {
            src_x = 105 + ((int)(text[i] - 'A') * 4);
            src_y = 25;
        }
        
        da_copy_xpm_area(src_x, src_y, 4, 7, dst_x, dst_y);

        dst_x += 4;
    }
}

void draw_led_text(char *text, int dst_x, int dst_y) {
    int length;
    int i;
    int src_x, src_y;

    text = da_str_to_upper(text);

    length = strlen(text);

    for (i = 0; i < length; i++) {
        if (text[i] < 'A') {
            src_x = 64 + ((int)(text[i] - '0') * 7);
            src_y = 0;
        } else {
            src_x = 64 + ((int)(text[i] - 'A') * 7);
            src_y = 10;
        }
        
        da_copy_xpm_area(src_x, src_y, 7, 9, dst_x, dst_y);

        dst_x += 7;
    }
}

void drawtext(char *str, struct font_t *font, int x, int y) {
    int i = 0;
    int offset_h = 0, offset_v = 0, ix, iy;
    
    char *p;

    if (mode == MODE_LCD && backlight) {
        offset_v = 25;
        offset_h = 0;

        if (font->charspline == 26) {
            font->dx = 6;
            font->dy = 8;
        }
        
    } else if (mode == MODE_OLD) {
        if (y == 5 && font->charspline == 26) {
            offset_h = 0;
            font->dx = 5;
            font->dy = 7;
        } else {
            offset_h = 25;

            if (font->charspline == 26) {
                font->dx = 6;
                font->dy = 8;
            }
        }
    }
    
    while (str[i]) {
        p = strchr(font->chars, str[i]);
        
        ix = (p) ? (p - font->chars) : 0;

        iy = (ix / font->charspline);
        ix = (ix % font->charspline);
    
        da_copy_xpm_area((font->sx - offset_h) + ix * font->dx,
                         (font->sy + offset_v) + iy * font->dy,
                         font->dx,
                         font->dy,
                         x + font->dx * i,
                         y);

        i++;
    }
}

void drawipaddr(uint32_t a, int linenum) {
    char buf[4];
    
    int i;
    
    uint32_t addr = ntohl(a);

    if (mode == MODE_LED) {
        for (i = 0; i < 4; i++) {
            snprintf(buf, 4, "%3d", (addr >> ((3-i)*8)) & 255);

            draw_led_text_sm(buf, 5 + i*14, 28 + linenum * 8);
        }
    
    } else {
        for (i = 0; i < 4; i++) {
            snprintf(buf, 4, "%3d", (addr >> ((3-i)*8)) & 255);

            drawtext(buf, &font1, 5 + i*14, 20 + linenum*9);
        }
    }
}

void drawhwaddr(unsigned char *addr) {
    char buf[4];
    
    int i;

    if (mode == MODE_LED) {
        for (i = 0; i < 6; i++) {
            snprintf(buf, 4, "%02X", addr[i]);
            
            draw_led_text_sm(buf, 6 + i*9, 52);
        }
    
    } else {
        for (i = 0; i < 6; i++) {
            snprintf(buf, 4, "%02X", addr[i]);
            
            drawtext(buf, &font1, 6 + i*9, 47);
        }
    }
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp for showing basic network info for all available interfaces.\n\n"

    PROG_NAME" shows IP address, netmask, gateway and MAC address. A bit like ifconfig. Runs on Linux and OpenBSD (for now).\n\n"

    "Left-button click moves to the next interface, right-button click calls ifup/ifdown scripts, middle-button switches backlight in the LCD mode.\n\n"

    "In the classic (old) mode, the led color means: red if interface is down, yellow if up/ifdown script is busy, dark green if interface is up with no traffic, light green if interface is up and sending (or receiving).\n\n"

    "If you have a NoWiresNeeded 1148 or Swallow NIC, compile "PROG_NAME" with --enable-nwn to enable wlan signal indication for this type of network adapter."

#ifdef ENABLE_NWN_SUPPORT
    " This copy of "PROG_NAME" has NWN support enabled.\n\n"
#else
    " This copy of "PROG_NAME" does not have NWN support.\n\n"
#endif

    "Known bugs: the link quality readout of the poldhu/swallow cards is slow and may be unreliable, due to the way this is implemented in the firmware and driver. (Bas?) Please report bugs if you find any.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&startif, "start_interface", "-i", "--start-interface", NULL, "Start with given interface, if available");
    da_init_string(&exec_up, "exec_up", "-U", "--exec-up", NULL, "Command to exec for iface-up");
    da_init_string(&exec_down, "exec_down", "-D", "--exec-down", NULL, "Command to exec for iface-down");
    da_init_string(&mode_name, "mode", "-m", "--mode", "led", "UI mode (can be led, lcd, old)");
    da_init_bool(&backlight, "backlight", NULL, "--backlight", false, "Turn on LCD backlight");
    da_init_float(&interval, "interval", "-I", "--interval", 0.1, 60.0, 0.5, "Update interval in seconds");
    da_init_bool(&skip_lo, "skip_lo", "-L", "--skip-lo", true, "Do not display lo device");
    da_init_bool(&skip_vlan, "skip_vlan", "-V", "--skip-vlan", true, "Do not display vlan device");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);
    
    return 0;
}

void routine(int argc, char *argv[]) {
    char cmd_buf[256];

    unsigned int curtime;
    unsigned int nexttime;
    
    struct timeval tv, tv2;

    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    /* set UI mode */
    if (!strcmp(da_str_to_upper(mode_name), "LED")) {
        mode = MODE_LED;

        da_add_color(back_color, "back_color");
        da_add_color(front_color, "led_color_high");
        da_add_mixed_color(front_color, 75, back_color, 25, "led_color_med");
        da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
        
    } else if (!strcmp(da_str_to_upper(mode_name), "LCD")) {
        mode = MODE_LCD;

        da_add_color(front_color, "led_color_high");
        da_add_blended_color(front_color, -24, -24, -24, 1.0, "led_color_med");
        
    } else if (!strcmp(da_str_to_upper(mode_name), "OLD")) {
        mode = MODE_OLD;
        
    } else {
        printf("WARN: wrong UI mode set, using classic.\n");
        da_log(WARN, "wrong UI mode set, using classic.\n");

        mode = MODE_OLD;
    }

    switch (da_scale) {
    case 4:
        switch(mode) {
        case MODE_LED:
            da_open_xwindow(argc, argv, wmifinfo_led4_xpm, NULL, 221 * da_scale, 65 * da_scale);
            break;
        case MODE_LCD:
            da_open_xwindow(argc, argv, wmifinfo_lcd4_xpm, NULL, 192 * da_scale, 115 * da_scale);
            break;
        case MODE_OLD:
            da_open_xwindow(argc, argv, wmifinfo_old4_xpm, NULL, 134 * da_scale, 90 * da_scale);
            break;
        }
        break;

    case 3:
        switch(mode) {
        case MODE_LED:
            da_open_xwindow(argc, argv, wmifinfo_led3_xpm, NULL, 221 * da_scale, 65 * da_scale);
            break;
        case MODE_LCD:
            da_open_xwindow(argc, argv, wmifinfo_lcd3_xpm, NULL, 192 * da_scale, 115 * da_scale);
            break;
        case MODE_OLD:
            da_open_xwindow(argc, argv, wmifinfo_old3_xpm, NULL, 134 * da_scale, 90 * da_scale);
            break;
        }
        break;
        
    case 2:
        switch(mode) {
        case MODE_LED:
            da_open_xwindow(argc, argv, wmifinfo_led2_xpm, NULL, 221 * da_scale, 65 * da_scale);
            break;
        case MODE_LCD:
            da_open_xwindow(argc, argv, wmifinfo_lcd2_xpm, NULL, 192 * da_scale, 115 * da_scale);
            break;
        case MODE_OLD:
            da_open_xwindow(argc, argv, wmifinfo_old2_xpm, NULL, 134 * da_scale, 90 * da_scale);
            break;
        }
        break;
    
    case 1:
    default:
        switch(mode) {
        case MODE_LED:
            da_open_xwindow(argc, argv, wmifinfo_led_xpm, NULL, 221 * da_scale, 65 * da_scale);
            break;
        case MODE_LCD:
            da_open_xwindow(argc, argv, wmifinfo_lcd_xpm, NULL, 192 * da_scale, 115 * da_scale);
            break;
        case MODE_OLD:
            da_open_xwindow(argc, argv, wmifinfo_old_xpm, NULL, 134 * da_scale, 90 * da_scale);
            break;
        }
        break;
    }

    gettimeofday(&tv2, NULL);

    interval *= 1000;
    
    nexttime = interval;

    /* Initialize global variables */
    fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);

    if (ifc.ifc_buf) {
        free(ifc.ifc_buf);

        ifc.ifc_buf = NULL;
    }
    
    ifc.ifc_len = sizeof(struct ifreq) * 10;
    ifc.ifc_buf = malloc(ifc.ifc_len);

    if (mode == MODE_LCD) {
        if (backlight) {
            da_copy_xpm_area(128, 0, 64, 64, 0, 0);

            da_set_mask_xy(128, 0);
        
        } else {
            da_copy_xpm_area(64, 0, 64, 64, 0, 0);

            da_set_mask_xy(64, 0);
        }
    }

    da_redraw_window();

    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            ifno++;

            break;

        case MOUSE_2_PRS:
            if (mode == MODE_LCD) {
                switch_light();

                if (ifaces > 0) {
                    draw_id();

                    draw_data();

                    draw_signal_level();

                    draw_load_level();
                }

                da_redraw_window();
            }
            break;

        case MOUSE_3_PRS:
            if (exec_up && exec_down) {
                if (ifinfo.state == 0) {
                    sprintf(cmd_buf, exec_up, ifinfo.id);
                } else {
                    sprintf(cmd_buf, exec_down, ifinfo.id);
                }
                da_exec_command(cmd_buf);
            } else {
                printf("Use the -U and -D options for setting ifup/ifdown commands.\n");
                da_log(INFO, "use the -U and -D options for setting ifup/ifdown commands.\n");
            }
            break;
        }

        gettimeofday(&tv, NULL);
        
        curtime = (tv.tv_sec - tv2.tv_sec) * 1000 + (tv.tv_usec - tv2.tv_usec) / 1000;

        waitpid(0, NULL, WNOHANG);

        if ((curtime >= nexttime) || (ifno != pifno)) {
            if (curtime >= nexttime) {
                nexttime = curtime + interval;
            }

            getifnames();

            if (ifaces > 0) {
                ifno = ifno % ifaces;

                getifinfo(ifname[ifno], &ifinfo);
            }
            
            pifno = ifno;
        }

        if (ifaces > 0) {
            draw_id();

            draw_data();

            draw_signal_level();

            draw_load_level();
        }

        da_redraw_window();
        
        usleep(DELAY);
    }

    routine(argc, argv);
}

int getifinfo(char *ifname, struct ifinfo_t *info) {
    struct ifreq ifr;
    struct sockaddr_in *sa;

#ifdef linux
    static FILE *froute = NULL;
    static FILE *fwireless = NULL;
    static FILE *fdev = NULL;
#elif defined(__OpenBSD__)
    struct ifreq ibuf[32];
    struct ifconf ifc;
    struct ifreq *ifrp, *ifend;
#endif

    char parent[16];
    char buf[1024];
    char *p;
    char a[16];
    
    int b, c, d;

#ifdef linux
    if (froute == NULL)
        froute = fopen("/proc/net/route", "r");

    if (fwireless == NULL)
        fwireless = fopen("/proc/net/wireless", "r");

    if (fdev == NULL)
        fdev = fopen("/proc/net/dev", "r");
#endif

    strcpy(parent, ifname);
    
    p = strchr(parent, ':');
    
    if(p) *p=0;

    strcpy(info->id, ifname);

    strcpy(ifr.ifr_name, ifname);

    /* Get status (UP/DOWN) */

    if(ioctl(fd, SIOCGIFFLAGS, &ifr) != -1) {
        sa = (struct sockaddr_in *)&(ifr.ifr_addr);
        info->state = (ifr.ifr_flags & 1) ? 1 : 0;
    } else {
        info->state = 0;
    }

    /* Get mac address */

#ifdef linux
    if(ioctl(fd, SIOCGIFHWADDR, &ifr) != -1) {
        memcpy(info->hw, ifr.ifr_hwaddr.sa_data, 6);
    } else {
        memset(info->hw, 0, 6);
    }
#elif defined(__OpenBSD__)
    ifc.ifc_len = sizeof(ibuf);
    ifc.ifc_buf = (caddr_t) ibuf;
    if (ioctl(fd, SIOCGIFCONF, (char *) &ifc) == -1 ||
            ifc.ifc_len < sizeof(struct ifreq)) {
        memset(info->hw, 0, 6);
    } else {
        /* Search interface configuration list for link layer address. */
        ifrp = ibuf;
        ifend = (struct ifreq *) ((char *) ibuf + ifc.ifc_len);
        while (ifrp < ifend) {
            /* Look for interface */
            if (strcmp(ifname, ifrp->ifr_name) == 0 &&
                    ifrp->ifr_addr.sa_family == AF_LINK &&
                    ((struct sockaddr_dl *) &ifrp->ifr_addr)->sdl_type == IFT_ETHER) {
                memcpy(info->hw, LLADDR((struct sockaddr_dl *) &ifrp->ifr_addr), 6);
                break;
            }
            /* Bump interface config pointer */
            r = ifrp->ifr_addr.sa_len + sizeof(ifrp->ifr_name);
            if (r < sizeof(*ifrp))
                r = sizeof(*ifrp);
            ifrp = (struct ifreq *) ((char *) ifrp + r);
        }
    }
#endif

    /* Get IP address */

    if(ioctl(fd, SIOCGIFADDR, &ifr) != -1) {
        sa = (struct sockaddr_in *)&(ifr.ifr_addr);
        info->ip = sa->sin_addr.s_addr;
    } else {
        info->ip = 0;
    }

    /* Get netmask */

    if(ioctl(fd, SIOCGIFNETMASK, &ifr) != -1) {
        sa = (struct sockaddr_in *)&(ifr.ifr_addr);
        info->nm = sa->sin_addr.s_addr;
    } else {
        info->nm = 0;
    }

    /* Get default gateway if on this interface */

    info->gw = 0;
#ifdef linux
    if(froute != NULL) {
        fseek(froute, 0, 0);

        while(fgets(buf, sizeof(buf), froute)) {
            sscanf(buf, "%s %x %x", a, (unsigned int *) &b,
                   (unsigned int *)  &c);

            if((strcmp(a, info->id) == 0) && (b == 0)) {
                info->gw = c;
            }
        }

    }
#elif defined(__OpenBSD__)
    {
    struct rt_msghdr *rtm = NULL;
    char *buf = NULL, *next, *lim = NULL;
    size_t needed;
    int mib[6];
    struct sockaddr *sa;
    struct sockaddr_in *sin;

    mib[0] = CTL_NET;
    mib[1] = PF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_INET;
    mib[4] = NET_RT_DUMP;
    mib[5] = 0;
    if (sysctl(mib, 6, NULL, &needed, NULL, 0) == -1) {
        perror("route-sysctl-estimate");
        exit(1);
    }
    if (needed > 0) {
        if ((buf = malloc(needed)) == 0) {
            printf("out of space\n");
            exit(1);
        }
        if (sysctl(mib, 6, buf, &needed, NULL, 0) == -1) {
            perror("sysctl of routing table");
            exit(1);
        }
        lim  = buf + needed;
    }

    if (buf) {
        for (next = buf; next < lim; next += rtm->rtm_msglen) {
            rtm = (struct rt_msghdr *)next;
            sa = (struct sockaddr *)(rtm + 1);
            sin = (struct sockaddr_in *)sa;

            if (sin->sin_addr.s_addr == 0) {
                sa = (struct sockaddr *)(ROUNDUP(sa->sa_len) + (char *)sa);
                sin = (struct sockaddr_in *)sa;
                info->gw = sin->sin_addr.s_addr;
                break;
            }
        }
        free(buf);
    }
    }
#endif

    /* Get wireless link status if wireless */

    info->sl = 0;
#ifdef linux
    if(fwireless != NULL) {
        fseek(fwireless, 0, 0);

        while(fgets(buf, sizeof(buf), fwireless)) {
            sscanf(buf, "%s %d %d ", a, &b, &c);
            if(strchr(a, ':'))  *(strchr(a, ':')) = 0;
            if(strcmp(a, parent) == 0) {
                info->sl = c;
            }
        }
    }

#ifdef ENABLE_NWN_SUPPORT
    if (info->sl == 0) {
        info->sl = nwn_get_link(parent);
    }
#endif
#elif defined(__OpenBSD__)
    {
    struct wi_req   wreq;
    struct ifreq    ifr;

    wreq.wi_len = WI_MAX_DATALEN;
    wreq.wi_type = WI_RID_COMMS_QUALITY;

    strlcpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
    ifr.ifr_data = (caddr_t)&wreq;

    if (ioctl(fd, SIOCGWAVELAN, &ifr) != -1)
        info->sl = letoh16(wreq.wi_val[0]);
    }
#endif

    /* Get Total tx/rx bytes */
#ifdef linux
    if (fdev != NULL) {
        /* bytes */
        fseek(fdev, 0, 0);

        while (fgets(buf, sizeof(buf), fdev)) {
            sscanf(buf, "%s %d %d %d %d %d %d %d %d %d", a, &b, &d,&d,&d,&d,&d,&d,&d, &c);

            if (strchr(a, ':'))
                *(strchr(a, ':')) = 0;

            if (strcmp(a, parent) == 0) {
                info->bytes = b + c;/*printf("bytes: %i\n", b+c);*/
            }
        }

        /* packets */
        fseek(fdev, 0, 0);

        while (fgets(buf, sizeof(buf), fdev)) {
            sscanf(buf, "%s %d %d %d %d %d %d %d %d %d %d", a, &d, &b, &d,&d,&d,&d,&d,&d,&d, &c);

            if (strchr(a, ':'))
                *(strchr(a, ':')) = 0;

            if (strcmp(a, parent) == 0) {
                info->packets = b + c;/*printf("packt: %i\n", b+c);*/
            }
        }
    }
#endif

    return(0);
}

void addifname(char *name) {
    int i;

    if (strcmp(name, "lo") == 0) {
        if (skip_lo)
            return;
    }

    if (strncmp(name, "vlan", 4) == 0) {
        if (skip_vlan)
            return;
    }

    for (i = 0; i < ifaces; i++) {
        if (strcmp(ifname[i], name) == 0)
            return;
    }

    strcpy(ifname[ifaces], name);

    ifaces++;

    return;
}

/*
 * get list of interfaces. First read /proc/net/dev, then do a SIOCGIFCONF
 */
void getifnames(void) {
    char pifname[MAXIFS][16];
    int pifaces;
    int i,j;
    int isnew;
#ifdef linux
    FILE *f;
    char buf[128];
    char *p1, *p2;
    int ifcount;
#endif

    /*
     * Copy list of interface names and clean the old list
     */

    for(i=0; i<ifaces; i++) strncpy(pifname[i], ifname[i], sizeof(pifname[i]));
    pifaces = ifaces;
    ifaces = 0;

#ifdef linux
    f = fopen("/proc/net/dev", "r");

    if(f == NULL) {
        fprintf(stderr, "Can't open /proc/net/dev\n");
        exit(1);
    }

    while(fgets(buf, sizeof(buf), f)) {
        p1=buf;
        while(*p1 == ' ') p1++;
        p2=p1;
        while(*p2 && (*p2 != ':')) p2++;
        if(*p2 == ':') {
            *p2=0;
            addifname(p1);
        }
    }

    fclose(f);

    ifc.ifc_len = sizeof(struct ifreq) * 10;

    if(ioctl(fd, SIOCGIFCONF, &ifc) == -1) {
        fprintf(stderr, "SIOCGIFCONF : Can't get list of interfaces : %s\n", strerror(errno));
        exit(1);
    }

    ifcount = ifc.ifc_len / sizeof(struct ifreq);

    for(i=0; i<ifcount; i++) {
        addifname(ifc.ifc_req[i].ifr_name);
    }
#endif
#ifdef __OpenBSD__
    struct ifreq ibuf[32];
    struct ifconf ifc;
    struct ifreq *ifrp, *ifend;
    int r;

    ifc.ifc_len = sizeof(ibuf);
    ifc.ifc_buf = (caddr_t) ibuf;
    if (ioctl(fd, SIOCGIFCONF, (char *) &ifc) == -1 ||
            ifc.ifc_len < sizeof(struct ifreq)) {
        fprintf(stderr, "SIOCGIFCONF : Can't get list of interfaces : %s\n", strerror(errno));
        exit(1);
    }
    /* Search interface configuration list for link layer address. */
    ifrp = ibuf;
    ifend = (struct ifreq *) ((char *) ibuf + ifc.ifc_len);
    while (ifrp < ifend) {
        if (ifrp->ifr_addr.sa_family == AF_LINK &&
            ((struct sockaddr_dl *) &ifrp->ifr_addr)->sdl_type == IFT_ETHER) {
            addifname(ifrp->ifr_name);
        }
        /* Bump interface config pointer */
        r = ifrp->ifr_addr.sa_len + sizeof(ifrp->ifr_name);
        if (r < sizeof(*ifrp))
            r = sizeof(*ifrp);
        ifrp = (struct ifreq *) ((char *) ifrp + r);
    }
#endif

    /*
     * Check if the new list contains interfaces that were not in the old list. If a new
     * interface is found, make it the current one to display. (-i will override)
     */

    for(i=0; i<ifaces; i++) {
        isnew = 1;
        for(j=0; j<pifaces; j++) if(strcmp(ifname[i], pifname[j]) == 0) isnew = 0;
        if(isnew) ifno = i;
    }

    for (i = 0; i < ifaces; i++) {
        if (startif && (strcasecmp(ifname[i], startif) == 0)) {
            ifno = i;
            startif[0] = 0;
        }
    }

}
