# dockapps

This is a collection of scalable dockapps, the size can be changed from the old 64x64 classic to 256x256 px. As of now, Window Maker's git NEXT branch has the support for dockapp larger than 64x64 px.

Run a dockapp with the --scale parameter (or similar, see individual dockapp's help) and set a value from 1 to 4. Floating point numbers or values beyond the limit will not work. In case you need 64x64, you can just run them without the --scale option or use vanilla dockapps.

More recent dockapps usually depend on https://gitlab.com/xander1988/libdockapp and also can be run as a separate miniwindow, and even have a user-defined xpm image set as background in that mode. See individual dockapp's README for requirements and instructions.

More dockapps to be added later.

## PRODUCTION-READY DOCKAPPS

The following dockapps were ported to libdockapp4 and all have the same selection of advanced features:

**asbeats**

**asmon**

**buttonmaker**

**cnslock**

**cputnik**

**dwgo**

**mount.app**

**Temperature.app**

**wmacpi**

**wmamixer**

**wmbinclock**

**wmblueclock**

**wmbluecpu**

**wmbluemem**

**wmCalClock**

**wmchaosgame**

**wmclock**

**wmclockmon**

**wmcore**

**wmcpuload**

**wmdots**

**wmGrabImage**

**wmifinfo**

**wmifs**

**wmload**

**wmmemload**

**wmmisc**

**wmmon**

**wmmoonclock**

**wmnetwork**

**wmpd**

**wmpulse**

**wmshutdown**

**wmsm.app**

**wmSpaceWeather**

**wmsun**

**wmsysinfo**

**wmsysmon**

**wmtetris**

**wmtictactoe**

**wmtime**

**wmtimepro**

**wmtimer**

**wmweather**

**wmwifi**

**wmtz**

**wmusic**

**wmxss**

## THE REST

The dockapps not listed in the section above may or may not be scalable, or may not work at all, depending on how much repair they need. See the TODO file for more info.
