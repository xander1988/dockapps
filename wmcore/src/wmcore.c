/*
	wmcore by bitman 	<bitman@bitmania.de>

				 http://www.bitmania.de

	based on wmlm by ben jarvis <bjarvis@bresnanlink.net>

	This is a dockapp that shows the usage of each core in the system.
	The dockapp splits into two displays, the upper one showing the common usage of the system and the
	lower display showing one graph per each core.

	It detects the number of cores and computes the usage to be represented as a bar graph.
	wmcore works with a variable number of cores, I have tested the display with 1 up to 16 (simulated) cores.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/param.h>
#include <sys/types.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define MAX_CPU 22

char *back_color;
char *border_dark;
char *border_light;

int delay;

void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp that shows the usage of each core in the system by bitman@bitmania.de\n\n"

    "The dockapp splits into two displays, the upper one showing the common usage of the system and the lower display showing one graph per each core. It detects the number of cores and computes the usage to be represented as a bar graph. "PROG_NAME" works with a variable number of cores, I have tested the display with 1 up to 16 (simulated) cores.");

    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_integer(&delay, "delay", "-D", "--delay", 0, 0, 1000000, "Number of microseconds of delay between each screen update, the default is 1000000 = 1 sec");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    unsigned char h;
	
	unsigned int t;
	
	unsigned int cpu;	      // current number of cpu when looping over all cpus
	unsigned int cpu_max = 0; // number of detected cpus in the system

	unsigned int  curr_user[MAX_CPU];
	unsigned int  curr_nice[MAX_CPU];
	unsigned int  curr_syst[MAX_CPU];
	unsigned int  curr_idle[MAX_CPU];
	unsigned int  curr_total[MAX_CPU];

	unsigned int  prev_idle[MAX_CPU];
	unsigned int  prev_total[MAX_CPU];

	unsigned int  diff_idle[MAX_CPU];
	unsigned int  diff_total[MAX_CPU];
	unsigned int  diff_usage[MAX_CPU];

	char *token;

	char tmp[200];
    
	FILE *in;
    
	da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 192 * da_scale, 67 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 192 * da_scale, 67 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 192 * da_scale, 67 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 192 * da_scale, 67 * da_scale);
        break;
    }

	/*#if DEBUG == 1
	#endif*/

	// Check how many cpu cores are present
 	if ((in = fopen("/proc/stat", "r")) == NULL) {
	    printf("ERR: unable to open file /proc/stat\n");
        da_log(ERR, "unable to open file /proc/stat\n");
	    exit(1);
	
    } else {
	    fgets(tmp,200,in);
	    
        while(strstr(tmp, "cpu")) {
	        fgets(tmp,200,in);
	        
            cpu_max++;
	    }
	    
        cpu_max--;
	    
        fclose(in);
	}
	
#ifdef DEBUG
    printf("INFO: CPU-Cores detected: %i\n", cpu_max);
    da_log(INFO, "CPU-Cores detected: %i\n", cpu_max);
#endif

	// Calculate the height per bar, to fit in the screen
	h = 42 / cpu_max;

	// Init values
	for (cpu = 0; cpu < cpu_max; cpu++) {
	    prev_idle[cpu]=0;
	    prev_total[cpu]=0;
	}
    
    da_redraw_window();

	// the main loop
	while (!da_conf_changed(argc, argv)) {
	    switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            /* TODO: do something */
            break;
        }

	    // get the cpu core usage
 	    if ((in = fopen("/proc/stat", "r")) == NULL) {
	        printf("ERR: unable to open file /proc/stat\n");
            da_log(ERR, "unable to open file /proc/stat\n");
	        exit(1);
	    
        } else {
    	    fgets(tmp,200,in);
	        
            for (cpu = 0; cpu < cpu_max; cpu++) {
    	        fgets(tmp,200,in);	// read next line
	            
                token = strtok(tmp, " ");
                
	            curr_user[cpu]  = atoi(strtok(NULL," "));
	            curr_nice[cpu]  = atoi(strtok(NULL," "));
	            curr_syst[cpu]  = atoi(strtok(NULL," "));
	            curr_idle[cpu]  = atoi(strtok(NULL," "));
	            curr_total[cpu] = curr_user[cpu] + curr_nice[cpu] + curr_syst[cpu] + curr_idle[cpu];
	            diff_idle[cpu]  = curr_idle[cpu]-prev_idle[cpu];
	            diff_total[cpu] = curr_total[cpu]-prev_total[cpu];
	            
                if (diff_total[cpu] > 0) {
	                diff_usage[cpu] = (unsigned int) (1000*(diff_total[cpu]-diff_idle[cpu])/diff_total[cpu]+5)/10 ;
	            } else {
	                diff_usage[cpu] = 0;
	            }
	        }
            
            /* Avoid warning */
            (void)token;
	        
            fclose(in);
	    }

	    // Display total usage over all cores in upper display field
	    t = 0;
        
	    for (cpu = 0; cpu < cpu_max; cpu++) {
		    t = t + diff_usage[cpu];
        }
	    
        t = t / cpu_max;
  	    t = ((t * 53) / 100);
	    
        da_copy_xpm_area(131, 0, 54, 8, 5, 5);	// blank preveous bar
	    da_copy_xpm_area(67, 0, t, 8, 5, 5);	// draw new bar

	    // Display the bar graphs for each core in lower display field
	    for(cpu=0; cpu<cpu_max; cpu++) {
	        // erase preveous bar
	        da_copy_xpm_area(131, 0, 54, h, 5, 17 + (cpu * h));

	        // calculate the length of the bar and display
  	        t = ((diff_usage[cpu] * 53) / 100);
	        
            da_copy_xpm_area(67, 0, t, h, 5, 17 + (cpu * h));

	        // Remember the total and idle CPU times for the next run.
	        prev_total[cpu]=curr_total[cpu];
	        prev_idle[cpu]=curr_idle[cpu];
	    }

	    da_redraw_window();
	    
        usleep(delay);
    }
	
    routine(argc, argv);
}
