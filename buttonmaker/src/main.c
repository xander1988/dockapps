/*
 * main.c
 * Copyright (C) Renan Vedovato Traba 2012 <rvt10@inf.ufpr.br>
 *
  		ButtonMaker is free software: you can redistribute it and/or modify it
        under the terms of the GNU General Public License as published by the
        Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        ButtonMaker is distributed in the hope that it will be useful, but
        WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along
        with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libdockapp4/dockapp.h>
#include <Imlib2.h>

#include "config.h"

char *cmd;
char *fname;

int isize;

/* Prototypes */
void routine(int, char **);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "like other button dockapps, it’s a button that runs a command.\n\n"

    PROG_NAME" has support for png, jpeg, xpm, tiff, etc. thanks to Imlib2. Single click to run a command. Right click to exit. Example:\n\n"

    PROG_NAME" -c \"sudo apt-get update\" -I TerminalGNUstep.tiff -i 32.");

    da_init_string(&cmd, "command", "-c", "--command", NULL, "Which command will be called");
    da_init_string(&fname, "icon_name", "-I", "--icon-name", NULL, "Icon displayed");
    da_init_integer(&isize, "icon_size", "-i", "--icon-size", 8, 256, 50, "Size of the icon");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    int b, w, h;

    char **xpm_data;
    
    unsigned char mask_bits[(64 * da_scale) * (64 * da_scale)];

    XImage *ximage;

    Visual *visual;
    
    Pixmap xpm;

    Imlib_Image canvas_image, icon_image, icon_image_orig;

    if (!cmd) {
        printf("Command not set, aborting...\n");
        da_log(ERR, "command not set, aborting...\n");
        exit(0);
    }

    if (!fname) {
        printf("Icon not set, aborting...\n");
        da_log(ERR, "icon not set, aborting...\n");
        exit(0);
    }

    da_init_xwindow();

    b = ((64 * da_scale) - isize) / 2;
    
    xpm = XCreatePixmap(da_display, da_root_win, 64 * da_scale, 64 * da_scale, da_display_depth);
    
    visual = DefaultVisual(da_display, DefaultScreen(da_display));

    imlib_context_set_dither(1);
    imlib_context_set_display(da_display);
    imlib_context_set_visual(visual);

    /* Prepare the canvas */
    canvas_image = imlib_create_image(64 * da_scale, 64 * da_scale);

    imlib_context_set_image(canvas_image);

    imlib_image_set_has_alpha(1);

    imlib_context_set_color(0, 0, 0, 255);

    imlib_image_fill_rectangle(0, 0, 64 * da_scale, 64 * da_scale);

    /* Load the chosen icon */
    icon_image_orig = imlib_load_image(fname);

    imlib_context_set_image(icon_image_orig);

    w = imlib_image_get_width();
    h = imlib_image_get_height();

    /* Scale the icon to isize */
    /* TODO: handle non-square images */
    icon_image = imlib_create_cropped_scaled_image(0, 0, w, h, isize, isize);

    /* Paint on canvas */
    imlib_context_set_image(canvas_image);

    imlib_context_set_operation(IMLIB_OP_COPY);

    imlib_blend_image_onto_image(icon_image, 1, 0, 0, isize, isize, b, b, isize, isize);
    
    imlib_context_set_drawable(xpm);
    
    imlib_render_image_on_drawable(0, 0);

    /* Get image data and mask */
    ximage = XGetImage(da_display, xpm, 0, 0, 64 * da_scale, 64 * da_scale, AllPlanes, ZPixmap);

    XpmCreateDataFromImage(da_display, &xpm_data, ximage, NULL, NULL);
    
    da_create_xbm_from_xpm((char *)mask_bits, xpm_data, 64 * da_scale, 64 * da_scale);

    XDestroyImage(ximage);

    da_open_xwindow(argc, argv, xpm_data, mask_bits, 64 * da_scale, 64 * da_scale);
    
    da_redraw_window();
    
    while (!da_conf_changed(argc, argv)) {
        /* Process any pending X events. */
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (!da_exec_command(cmd)) {
				printf("Failed to run command \"%s\".\n", cmd);
                da_log(WARN, "failed to run command \"%s\".\n", cmd);
            }
            break;

        case MOUSE_3_PRS:
            exit(0);
            break;
        }
    
        usleep(10000);
    }
    
    routine(argc, argv);
}
