
/*
 * Cputnik - a simple cpu and memory monitor
 *
 * Copyright (C) 2002-2005 pasp and sill
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <unistd.h>
#include <limits.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define		BUFFER_SIZE		512
#define		CPU_NAME		"cpu0"
#define		CPU_NAME_LEN	5
#define     FONT_SMALL      1
#define     FONT_NORMAL     2
#define     FONT_LARGE      3
#define		V_WIDTH			54
#define		V_HEIGHT		41
#define		V_HEIGHT_MEM	36
#define     METER_WIDTH     32
#define     MAX_STRING_LEN  32

typedef struct {
	char	name[CPU_NAME_LEN];
	int		his[V_WIDTH+1];
	int		hisaddcnt;
	long	rt_stat;
	long	statlast;
	long	rt_idle;
	long	idlelast;
} stat_dev;

void routine(int, char **);
void get_mem_statistics(int *);
void draw_memory_meter(void);
void get_cpu_statistics(char *, long *, long *, long *);
void draw_stats(int *, int, int, int, int);
void update_stat_cpu(stat_dev *);
int dcl_draw_string(int, int, char *, int, int);
int dcl_draw_char(int, int, char, int);
