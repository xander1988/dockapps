
/*
 * Cputnik - a simple cpu and memory monitor
 *
 * Copyright (C) 2002-2005 pasp and sill
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "cputnik.h"

FILE *fp_stat, *fp_loadavg, *fp_memory;

int cmd_lmb, cmd_rmb, update_period;
int free_memory;

char *border_dark;
char *border_light;
char *command_lmb;
char *command_rmb;

bool show_memory;

stat_dev cpu_device;

/*----------------------------------------------------------------------*/

void get_mem_statistics(int *free) {
    long long int m_total, m_free, m_cached;
    
    char temp[BUFFER_SIZE];

    fp_memory = freopen("/proc/meminfo", "r", fp_memory);

    while(fscanf(fp_memory, "%s", temp)!=EOF) {

        if(!strncmp(temp,"MemTotal:", 9))
           fscanf(fp_memory, "%lld", &m_total);

        if(!strncmp(temp,"MemFree:", 8))
           fscanf(fp_memory, "%lld", &m_free);

        if(!strncmp(temp,"Cached:", 7))
           fscanf(fp_memory, "%lld", &m_cached);
    }

    *free = (int)(((float)(m_total - m_free - m_cached) / m_total) * 100.0);
}

/*----------------------------------------------------------------------*/

void draw_memory_meter(void) {
    free_memory = free_memory * (METER_WIDTH / 100.0);
    
    if (free_memory > METER_WIDTH) 
        free_memory = METER_WIDTH;
    
    da_copy_xpm_area(64 + 0, 64, METER_WIDTH, 5, 26, 12);
    da_copy_xpm_area(64 + 32, 64, free_memory, 5, 26, 12);
}

/*----------------------------------------------------------------------*/

void get_cpu_statistics(char *devname, long *is, long *ds, long *idle) {
    char temp[BUFFER_SIZE], *p, *tokens = " \t\n";
    
    int i;
    
    float f;

    *is = *ds = *idle = 0;

    if (!strncmp(devname, "cpu", 3)) {

        fseek(fp_stat, 0, SEEK_SET);

        while (fgets(temp, 128, fp_stat)) {

            if (strstr(temp, "cpu")) {
                p = strtok(temp, tokens);
                /* 1..3, 4 == idle, we don't want idle! */
                for (i=0; i<3; i++) {
                    p = strtok(NULL, tokens);
                    *ds += atol(p);
                }

                p = strtok(NULL, tokens);
                *idle = atol(p);
            }

        }

        fp_loadavg = freopen("/proc/loadavg", "r", fp_loadavg);
        
        fscanf(fp_loadavg, "%f", &f);
        
        *is = (long) (100 * f);
    }
}

/*----------------------------------------------------------------------*/

void draw_stats(int *his, int num, int size, int x_left, int y_bottom) {
    int pixels_per_byte, j, k, *p, d;

    pixels_per_byte = 100;
    
    p = his;

    for (j=0; j<num; j++) {
        if (p[0] > pixels_per_byte)
            pixels_per_byte += 100;
        p++;
    }

    p = his;

    for (k=0; k<num; k++) {
        d = (1.0 * p[0] / pixels_per_byte) * size;

        for (j=0; j<size; j++) {

            if (j < d - 3)
                da_copy_xpm_area(64 + 0, 71, 1, 1, k+x_left, y_bottom-j);
            else if (j < d)
                da_copy_xpm_area(64 + 1, 71, 1, 1, k+x_left, y_bottom-j);
            else
                da_copy_xpm_area(64 + 2, 71, 1, 1, k+x_left, y_bottom-j);
        }
        p++;
    }

    /* horizontal line */
    for (j = pixels_per_byte-100; j > 0; j-=100) {
        for (k=0; k<num; k++) {
            d = (40.0 / pixels_per_byte) * j;
            
            da_copy_xpm_area(64 + 3, 71, 1, 1, k+x_left, y_bottom-d);
        }
    }

}

/*----------------------------------------------------------------------*/

void update_stat_cpu(stat_dev *st) {
    long k, istat, idle;

    get_cpu_statistics(st->name, &k, &istat, &idle);

    st->rt_idle = idle - st->idlelast;
    st->idlelast = idle;

    st->rt_stat = istat - st->statlast;
    st->statlast = istat;

    st->his[V_WIDTH] += k;
    st->hisaddcnt++;
}

/*----------------------------------------------------------------------*/

void routine(int argc, char *argv[]) {
    int xfd = 0;
    
    long start_time, current_time, next_time, istat, idle, k;
    
    unsigned long j;
    
    fd_set inputs;
    
    struct timeval timeout;

    fp_memory = fopen("/proc/meminfo", "r");
    fp_loadavg = fopen("/proc/loadavg", "r");
    fp_stat = fopen("/proc/stat", "r");

    for (j=0; j<V_WIDTH+1; j++)
        cpu_device.his[j] = 0;

    cpu_device.hisaddcnt = 0;
    
    strncpy(cpu_device.name, CPU_NAME, CPU_NAME_LEN);

    timeout.tv_sec = 0;
    timeout.tv_usec = 250000;

    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    da_add_mixed_color(border_dark, 50, border_light, 50, "border_mid");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 220 * da_scale, 89 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 220 * da_scale, 89 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 220 * da_scale, 89 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 220 * da_scale, 89 * da_scale);
        break;
    }

    /* add mouse region */
    da_add_mouse_region(0, 4, 4, 59, 59);

    start_time = time(0);
    next_time = start_time + update_period;

    get_cpu_statistics(cpu_device.name, &k, &istat, &idle);
    
    cpu_device.statlast = istat;
    cpu_device.idlelast = idle;

    if (show_memory) {

        dcl_draw_string(6,  5, "cpu", FONT_NORMAL, 3);
        dcl_draw_string(6, 12, "mem", FONT_NORMAL, 3);
        
        da_copy_xpm_area(64 + 5, 57, 54, 1, 5, 18);
        
        get_mem_statistics(&free_memory);
        
        draw_memory_meter();
        
        draw_stats(cpu_device.his, V_WIDTH, V_HEIGHT_MEM, 5, 55);

    } else {

        dcl_draw_string(6, 5, "cpu", FONT_LARGE, 3);
        
        da_copy_xpm_area(64 + 5, 57, 54, 1, 5, 13);
        
        draw_stats(cpu_device.his, V_WIDTH, V_HEIGHT, 5, 55);

    }

    while (!da_conf_changed(argc, argv)) {

        current_time = time(0);

        waitpid(0, NULL, WNOHANG);

        update_stat_cpu(&cpu_device);

        /* cpu meter */
        da_copy_xpm_area(64 + 0, 64, 32, 7-((int)show_memory*2), 26, 5);

        j = (cpu_device.rt_stat + cpu_device.rt_idle);
        
        if (j != 0) 
            j = (cpu_device.rt_stat * 100) / j;
        
        j = j * (METER_WIDTH / 100.0);
        
        if (j > METER_WIDTH) 
            j = METER_WIDTH;

        da_copy_xpm_area(64 + 32, 64, j, 7-((int)show_memory*2), 26, 5);

        if (current_time >= next_time) {
            next_time += update_period;

            if (cpu_device.his[V_WIDTH])
                cpu_device.his[V_WIDTH] /= cpu_device.hisaddcnt;

            for (j=1; j<V_WIDTH+1; j++)
                cpu_device.his[j-1] = cpu_device.his[j];

        if (show_memory) {

            get_mem_statistics(&free_memory);
            
            draw_memory_meter();
            
            draw_stats(cpu_device.his, V_WIDTH, V_HEIGHT_MEM, 5, 55);

        } else {
            draw_stats(cpu_device.his, V_WIDTH, V_HEIGHT, 5, 55);
        }

            cpu_device.his[V_WIDTH] = 0;
            cpu_device.hisaddcnt = 0;

        }

        xfd = ConnectionNumber(da_display);

        FD_ZERO(&inputs);
        FD_SET(xfd, &inputs);

        switch(select(FD_SETSIZE, &inputs, NULL, NULL, &timeout)) {

            case 0:
                timeout.tv_sec = 0;
                timeout.tv_usec = 250000;
                
                da_redraw_window();
                
                break;

            case -1:
                break;

            default:
                break;
        }

        switch (da_watch_xevent()) {
        case MOUSE_1_REL:
            switch (da_check_mouse_region()) {
            case 0:
                if (command_lmb)
                    da_exec_command(command_lmb);
                break;

            default:
                break;
            }
            
            break;
            
        case MOUSE_3_REL:
            switch (da_check_mouse_region()) {
            case 0:
                if (command_rmb)
                    da_exec_command(command_rmb);
                break;

            default:
                break;
            }
            
            break;
        }
    }
    
    routine(argc, argv);
}

/*----------------------------------------------------------------------*/

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a simple CPU and memory monitor.\n\n"

    "This is small WindowMaker dockapp which displays cpu and memory usage.");

    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_integer(&update_period, "update_period", "-U", "--update-period", 0, 0, 2, "Set update period");
    da_init_bool(&show_memory, "show_memory", "-M", "--show-memory", true, "Show memory");
    da_init_string(&command_lmb, "command_lmb", "-L", "--command-lmb", "gnome-system-monitor", "Command to execute on left mouse click");
    da_init_string(&command_rmb, "command_rmb", "-R", "--command-rmb", "xkill", "Command to execute on right mouse click");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

int dcl_draw_char(int x, int y, char z, int font_type) {
    char *ctable = { "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.[]-_:'@" };
    
    int ctable_len = 44;
    
    int k, font_line, font_width, font_height;

    switch(font_type) {

        case FONT_SMALL:
                font_width = 4;
                font_height = 5;
                font_line = 12;
                break;

        case FONT_NORMAL:
                font_width = 5;
                font_height = 5;
                font_line = 7;
                break;

        case FONT_LARGE:
                font_width = 5;
                font_height = 7;
                font_line = 0;
                break;

        default:
                fprintf(stderr, "ERROR: Unknown font type...\n");
                return 0;
    }

    for(k=0; k < ctable_len; k++)
        if(toupper(z)==ctable[k]) {

            da_copy_xpm_area(k*font_width, 72 + font_line, font_width, font_height, x, y);
            break;

        }

    return 1;
}

int dcl_draw_string(int x, int y, char *string, int font_type, int length) {
    int j, len, font_width;
    
    char a;

    if(length == -1)
        len = strlen(string);
    else
        len = length;

    if(len <= 0 || len > MAX_STRING_LEN) {

        fprintf(stderr, "ERROR: Wrong string length...\n");
        return 0;

    }

    switch(font_type) {

        case FONT_SMALL:
                font_width = 4;
                break;

        case FONT_NORMAL:
                font_width = 5;
                break;

        case FONT_LARGE:
                font_width = 5;
                break;

        default:
                fprintf(stderr, "ERROR: Unknown font type...\n");
                return 0;
    }

	for(j=0; j<len; j++) {

		if((a=string[j]) == '\0') 
		    break;
		
		dcl_draw_char(x+j*(font_width+1), y, a, font_type);

	}

    return 1;
}
