/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <math.h>
#include <alsa/asoundlib.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/default.xpm"
#include "bitmaps/default2.xpm"
#include "bitmaps/default3.xpm"
#include "bitmaps/default4.xpm"
#include "bitmaps/barross.xpm"
#include "bitmaps/barross2.xpm"
#include "bitmaps/barross3.xpm"
#include "bitmaps/barross4.xpm"

/* Function to convert from percentage to volume. val = percentage */
#define convert_prange1(val, min, max) ceil((val) * ((max) - (min)) * 0.01 + (min))
#define DELAY 10000L
int testvol;
char *border_dark;
char *border_light;
char *slider_1;
char *slider_2;
char *slider_3;
char *device;
char *command;

bool use_barross;

void routine(int, char **);
void set_slider_pos(int, int);
void set_volume(int, int);
unsigned get_volume(int);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a mixer utility for Linux systems with ALSA sound driver.\n\n"

    PROG_NAME" is a mixer utility for Linux systems with ALSA sound driver. It is designed to be docked in Window Maker (and other wms). This utility has three volume controllers that can be configured to handle any sound source, the default sources are 'Master', 'PCM' and 'CD' volume. Sound sources can be easily muted and there is also wheel mouse support.\n\n"

    "The whole GUI and command parsing was taken from Mixer.app (see old doc for URL's), only connection to ALSA driver was added.\n\n"

    "If the led on Mixer.app is red an error message has been printed to stderr and something is not working correctly. If the led is green everything is working ok. (Error led doesn't work, TODO)\n\n"

    "Right click on a volume controller to mute the sound source. The button will then have a red led in one corner. Right click again to restore the volume. If a muted sound source is modified by another application "PROG_NAME" will automaticaly release its muted state.\n\n"

    "If you have a wheel mouse (where the wheel is configured as Button4 and Button5) you can control the volume by just moving the mouse over "PROG_NAME" and roll the wheel up and down. Use the command line option -W to specify which slider that should react to the wheel movement.\n\n"

    "If you run multiple instances of "PROG_NAME" you might want to be able to tell them apart. This can easily be done by setting a label to each one of them. By using the -L option you can add a little text label at the bottom of the mixer.\n\n"

    "You can also configure "PROG_NAME" to run command (i.e. better mixer) on middle click by using -c <command> option.");

    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&slider_1, "slider_1", "-1", "--slider-1", "Master", "");
    da_init_string(&slider_2, "slider_2", "-2", "--slider-2", "PCM", "");
    da_init_string(&slider_3, "slider_3", "-3", "--slider-3", "CD", "");
    da_init_string(&device, "device", "-D", "--device", "default", "");
    da_init_string(&command, "command", "-c", "--command", NULL, "Command to execute via click of mouse button 2");
    da_init_bool(&use_barross, "use_barross", "-B", "--use-barross", false, "Start with the Barross skin");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/alsamixerapp.conf");    /* modern conf path */
    da_init_conf_file(LOCAL, ".alsamixerapprc");                       /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    //int pos = 0;

    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, use_barross ? barross4_xpm : default4_xpm, NULL, 120 * da_scale, 64 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, use_barross ? barross3_xpm : default3_xpm, NULL, 120 * da_scale, 64 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, use_barross ? barross2_xpm : default2_xpm, NULL, 120 * da_scale, 64 * da_scale);
        break;

    case 1:
    default:
        da_open_xwindow(argc, argv, use_barross ? barross_xpm : default_xpm, NULL, 120 * da_scale, 64 * da_scale);
        break;
    }

    da_add_mouse_region(1, 6,  8, 22, 52); /* slider 1 */
    da_add_mouse_region(2, 24, 8, 40, 52); /* slider 2 */
    da_add_mouse_region(3, 42, 8, 58, 52); /* slider 3 */

    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            set_slider_pos(da_check_mouse_region(), da_xevent.xbutton.y);
            break;

        case MOUSE_1_REL:
            set_slider_pos(da_check_mouse_region(), da_xevent.xbutton.y);
            break;

        case MOUSE_2_PRS:
            if (command) {
                da_exec_command(command);
            }
            break;

        case MOUSE_3_PRS:
            /* toggle mute */
            break;

        case MOUSE_4:
            set_volume(da_check_mouse_region(), testvol + 4);
            break;

        case MOUSE_5:
            set_volume(da_check_mouse_region(), testvol - 4);
            break;
        }

        da_redraw_window();

        usleep(DELAY);
    }

    routine(argc, argv);
}

void set_volume(int channel_num, int val) {
    testvol = convert_prange1(val, 0, 100);
}

void set_slider_pos(int channel_num, int pos) {
    //unsigned volume;

    int new_pos;

    new_pos = pos / da_scale;

    testvol = get_volume(new_pos);printf("%i\n", testvol);

    if (new_pos < 9) {
        new_pos = 9;
    }
    if (new_pos > 52) {
        new_pos = 52;
    }

    switch (channel_num) {
    case 1:
        da_copy_xpm_area(66, 0, 16, 52, 6, 4);
        da_copy_xpm_area(71, 52, 16, 9, 6, new_pos - 5);
        break;

    case 2:
        da_copy_xpm_area(84, 0, 16, 52, 24, 4);
        da_copy_xpm_area(71, 52, 16, 9, 24, new_pos - 5);
        break;

    case 3:
        da_copy_xpm_area(102, 0, 16, 52, 42, 4);
        da_copy_xpm_area(71, 52, 16, 9, 42, new_pos - 5);
        break;
    }

    da_redraw_window();
}

unsigned get_volume(int pos) {
    unsigned volume;

    if (pos <= 8) {
        return 100;
    }

    if (pos >= 52) {
        return 0;
    }

    switch (pos) {
    case 9:
        volume = 98;
        break;
    case 10:
        volume = 96;
        break;
    case 11:
        volume = 94;
        break;
    case 12:
        volume = 92;
        break;
    case 13:
        volume = 90;
        break;
    case 14:
        volume = 88;
        break;
    case 15:
        volume = 86;
        break;
    case 16:
        volume = 84;
        break;
    case 17:
        volume = 82;
        break;
    case 18:
        volume = 80;
        break;
    case 19:
        volume = 78;
        break;
    case 20:
        volume = 76;
        break;
    case 21:
        volume = 74;
        break;
    case 22:
        volume = 72;
        break;
    case 23:
        volume = 70;
        break;
    case 24:
        volume = 68;
        break;
    case 25:
        volume = 66;
        break;
    case 26:
        volume = 64;
        break;
    case 27:
        volume = 62;
        break;
    case 28:
        volume = 60;
        break;
    case 29:
        volume = 58;
        break;
    case 30:
        volume = 56;
        break;
    case 31:
        volume = 54;
        break;
    case 32:
        volume = 52;
        break;
    case 33:
        volume = 50;
        break;
    case 34:
        volume = 48;
        break;
    case 35:
        volume = 46;
        break;
    case 36:
        volume = 44;
        break;
    case 37:
        volume = 42;
        break;
    case 38:
        volume = 40;
        break;
    case 39:
        volume = 38;
        break;
    case 40:
        volume = 36;
        break;
    case 41:
        volume = 34;
        break;
    case 42:
        volume = 32;
        break;
    case 43:
        volume = 30;
        break;
    case 44:
        volume = 28;
        break;
    case 45:
        volume = 24;
        break;
    case 46:
        volume = 20;
        break;
    case 47:
        volume = 16;
        break;
    case 48:
        volume = 12;
        break;
    case 49:
        volume = 8;
        break;
    case 50:
        volume = 6;
        break;
    case 51:
        volume = 4;
        break;
    }

    return volume;
}
