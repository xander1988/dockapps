/*
 *    WMCPULoad - A dockapp to monitor CPU usage
 *    Copyright (C) 2001,2002  Seiichi SATO <ssato@sh.rim.or.jp>

 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.

 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <libdockapp4/dockapp.h>

#include "config.h"
#include "cpu.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#define MAX_HISTORY 16
#define CPUNUM_NONE -1

static char *light_color;   /* back-light color */
static char *border_dark;
static char *border_light;

static int update_interval;
static int alarm_threshold;
static int hindex = 0;

static bool backlight;

static cpu_options cpu_opts;
static int history[MAX_HISTORY];   /* history of cpu usage */

static time_t last_update = 0;

/* prototypes */
static void update(void);
static void redraw(void);
static void switch_light(void);
static void draw_digit(int);
static void routine(int, char **);
#ifdef USE_SMP
static void draw_cpunumber(void);
#endif

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp to monitor CPU usage.\n\n"

    PROG_NAME" is a program to monitor cpu usage. It is a dockapp that is supported by X window managers such as Window Maker, AfterStep, BlackBox, and Enlightenment.\n\n"

    "It displays the current usage, expressed as a percentile and a chart, and has an LCD look-alike user interface. The back-light may be turned on/off by clicking the mouse button over the application. If the CPU usage hits a certain threshold, an alarm-mode will alert you by turning back-light on.\n\n"

    "It runs on GNU/Linux / FreeBSD / OpenBSD / NetBSD / BSDi / Solaris / Cygwin / IRIX / Darwin.");

    da_init_string(&light_color, "front_color", "-f", "--front-color", "#6ec63b", "Main color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&backlight, "backlight", "-b", "--backlight", false, "Turn on LCD backlight");
    da_init_integer(&update_interval, "interval", "-i", "--interval", 1, 60, 1, "Number of secs between updates");
    da_init_integer(&alarm_threshold, "alarm_threshold", NULL, "--alarm-threshold", 0, 100, 100, "Activate alarm mode of cpu, where percentage is threshold from 0 to 100");
#ifdef USE_SMP
    da_init_integer(&cpu_opts.cpu_number, "cpu_number", "-c", "--cpu-number", 0, 0, CPUNUM_NONE, "Which CPU is monitored (0, 1, ... )");
#endif
#ifdef IGNORE_NICE
    da_init_bool(&cpu_opts.ignore_nice, "ignore_nice", "-n", "--ignore-nice", false, "Ignore a nice value");
#endif
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

static void routine(int argc, char *argv[]) {
#ifndef USE_SMP
    cpu_opts.cpu_number = CPUNUM_NONE;
#else
    if (cpu_opts.cpu_number < CPUNUM_NONE) {
        printf("ERR: cpu number must be >= 0, defaulting to 0.\n");
        da_log(ERR, "cpu number must be >= 0, defaulting to 0.\n");

        cpu_opts.cpu_number = 0;
    }
#endif
#ifndef IGNORE_NICE
    cpu_opts.ignore_nice = false;
#endif

    /* Initialize Application */
    cpu_init();
    
    da_init_xwindow();

    da_add_color(light_color, "led_color_high");
    da_add_blended_color(light_color, -24, -24, -24, 1.0, "led_color_med");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 192 * da_scale, 136 * da_scale);
        break;
    }

    if (backlight) {
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }

    da_redraw_window();

    /* Main loop */
    while (!da_conf_changed(argc, argv)) {
        if (time(NULL) - last_update >= update_interval)
            update();

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch_light();
            redraw();
            break;
        }

        da_redraw_window();
    
        usleep(250000L);
    }

    routine(argc, argv);
}

/* called by timer */
static void update(void) {
    int usage;
    int x, x2, h;

    static bool pre_backlight;
    static bool in_alarm_mode = false;

    if (backlight) {
        x = 2;
        x2 = 64;
    } else {
        x = 0;
        x2 = 0;
    }

    /* get current cpu usage in percent */
    usage = cpu_get_usage(&cpu_opts);
    
    hindex++;

    if (hindex >= MAX_HISTORY) {
        hindex = 0;
    }
    
    history[hindex] = usage;

    /* alarm mode */
    if (usage >= alarm_threshold) {
        if (!in_alarm_mode) {
            in_alarm_mode = true;
            pre_backlight = backlight;
        }
        if (!backlight) {
            switch_light();
            redraw();
            return;
        }
        
    } else {
        if (in_alarm_mode) {
            in_alarm_mode = false;
            if (backlight != pre_backlight) {
                switch_light();
                redraw();
                return;
            }
        }
    }

    /* slide past chart */
    da_copy_xpm_area(73 + x2, 36, 47, 21, 9, 36);
    da_copy_xpm_area(100 + x, 64, 2, 21, 54, 36);

    /* clear digit */
    da_copy_xpm_area(72 + x2, 10, 34, 20, 8, 10);

    /* draw digit */
    draw_digit(usage);

#ifdef USE_SMP
    /* draw cpu number */
    if (cpu_opts.cpu_number != CPUNUM_NONE)
        draw_cpunumber();
#endif

    /* draw current chart */
    h = (21 * usage) / 100;
    
    da_copy_xpm_area(100+x, 64 + 21-h, 2, h, 54, 57-h);

    redraw();

    /* show */
    da_redraw_window();
    
    time(&last_update);
}

/* called when mouse button pressed */
static void switch_light(void) {
    if (!backlight) {
        backlight = true;
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        backlight = false;
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }

    /* show */
    da_redraw_window();
}

static void redraw(void) {
    int h, i, j = hindex;
    int x = 0;

    if (backlight) {
        x = 2;
    } else {
        x = 0;
    }

    if (backlight) {
        da_copy_xpm_area(128, 0, 64, 64, 0, 0);

        da_set_mask_xy(128, 0);
    
    } else {
        da_copy_xpm_area(64, 0, 64, 64, 0, 0);

        da_set_mask_xy(64, 0);
    }

    /* redraw digit */
    draw_digit(history[hindex]);

#ifdef USE_SMP
    /* draw cpu number */
    if (cpu_opts.cpu_number != CPUNUM_NONE)
        draw_cpunumber();
#endif

    /* redraw chart */
    for (i = 0; i < MAX_HISTORY; i++) {
        h = (21 * history[j]) / 100;

        da_copy_xpm_area(100+x, 64 + 21-h, 2, h, 54-3*i, 57-h);

        j--;

        if (j < 0)
            j = MAX_HISTORY - 1;
    }

    da_redraw_window();
}

static void draw_digit(int per) {
    int v100, v10, v1;
    int y = 0;

    if (per < 0) per = 0;
    if (per > 100) per = 100;

    v100 = per / 100;
    v10  = (per - v100 * 100) / 10;
    v1   = (per - v100 * 100 - v10 * 10);

    if (backlight) {
        y = 20;
    }

    /* draw digit */
    da_copy_xpm_area(v1 * 10, 64 + y, 10, 20, 29 + 3, 7 + 3);

    if (v10 != 0) {
        da_copy_xpm_area(v10 * 10, 64 + y, 10, 20, 17 + 3, 7 + 3);
    }
    
    if (v100 == 1) {
        da_copy_xpm_area(10, 64 + y, 10, 20, 5 + 3, 7 + 3);
        da_copy_xpm_area(0, 64 + y, 10, 20, 17 + 3, 7 + 3);
        da_copy_xpm_area(0, 64 + y, 10, 20, 29 + 3, 7 + 3);
    }
}


#ifdef USE_SMP
static void draw_cpunumber(void) {
    int y, v1 = 0, v2 = 0;

    v2 = cpu_opts.cpu_number / 10;
    v1 = cpu_opts.cpu_number - v2 * 10;

    y = backlight ? 126 : 116;

    da_copy_xpm_area(v2 * 6, y, 7, 11, 45, 12);
    da_copy_xpm_area(v1 * 6, y, 7, 11, 51, 12);
}
#endif
