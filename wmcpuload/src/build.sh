#!/bin/bash

CC=gcc
CC_MAIN_OPTS=(-O2 -Wall -Wextra -Wpedantic -lXpm -lX11 -lXext -ldockapp4)
CC_ADD_OPTS=()
C_FILES=(main.c)
C_ADD_FILE=""

PROG_NAME=none
PROG_VERSION=0.0

TARGET_DIR=/usr/local/bin

INSTALL=false
UNINSTALL=false

SHOW_HELP=false

# Dockapp-specific options
IGNORE_NICE=false
USE_SMP=false
DEBUG=false

# Get OS type
if [[ $OSTYPE == "linux-gnu" ]]; then
    IGNORE_NICE=true;
    USE_SMP=true;
    C_ADD_FILE=cpu_linux.c;
elif [[ $OSTYPE == "openbsd" ]]; then
    IGNORE_NICE=true;
    C_ADD_FILE=cpu_openbsd.c;
elif [[ $OSTYPE == "netbsd" ]]; then
    IGNORE_NICE=true;
    C_ADD_FILE=cpu_netbsd.c;
elif [[ $OSTYPE == "bsdi" ]]; then
    IGNORE_NICE=true;
    C_ADD_FILE=cpu_bsdi.c;
elif [[ $OSTYPE == "solaris" ]]; then
    C_ADD_FILE=cpu_solaris.c;
    CC_ADD_OPTS+=(-lkstat);
elif [[ $OSTYPE == "cygwin" ]]; then
    C_ADD_FILE=cpu_cygwin.c;
elif [[ $OSTYPE == "freebsd" ]]; then
    IGNORE_NICE=true;
    C_ADD_FILE=cpu_freebsd.c;
    CC_ADD_OPTS+=(-lkvm);
elif [[ $OSTYPE == "irix" ]]; then
    USE_SMP=true;
    C_ADD_FILE=cpu_irix.c;
elif [[ $OSTYPE == "darwin" ]]; then
    C_ADD_FILE=cpu_darwin.c;
else
    echo "Sorry, ${OSTYPE} is not supported yet.";
    exit 1;
fi

# Read PROG_NAME and PROG_VERSION from config.h
PREV=none
if [ ! -f config.h ]; then
    touch config.h;
fi
while read LINE; do
    for WORD in $LINE; do
        if [[ $PREV == "PROG_NAME" ]]; then
            PROG_NAME=${WORD//\"};
        elif [[ $PREV == "PROG_VERSION" ]]; then
            PROG_VERSION=${WORD//\"};
        fi
        PREV=$WORD;
    done
done < config.h

# Parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -b|--debug)
            DEBUG=true
            shift # past argument
            ;;
        -c|--compiler)
            CC="$2"
            shift # past argument
            shift # past value
            ;;
        -n|--name)
            PROG_NAME="$2"
            shift # past argument
            shift # past value
            ;;
        -v|--version)
            PROG_VERSION="$2"
            shift # past argument
            shift # past value
            ;;
        -d|--directory)
            TARGET_DIR="$2"
            shift # past argument
            shift # past value
            ;;
        -i|--install)
            INSTALL=true
            shift # past argument
            ;;
        -u|--uninstall)
            UNINSTALL=true
            shift # past argument
            ;;
        -h|--help)
            SHOW_HELP=true
            shift # past argument
            ;;
        *)  # unknown option
            shift # past argument
            ;;
    esac
done

# Show help
if [[ $SHOW_HELP == true ]]; then
    echo "${PROG_NAME} ${PROG_VERSION} building and installation manual";
    echo "options:";
    echo "    -c --compiler  <compiler>     Set the compiler (default: gcc)";
    echo "    -n --name      <name>         Override program name (default: set in config.h)";
    echo "    -v --version   <version>      Override program version (default: set in config.h)";
    echo "    -b --debug                    Turn on debug messages (default: no)";
    echo "    -d --directory <directory>    With -i or -u, set target directory (default: /usr/local/bin)";
    echo "    -i --install                  Perform installation";
    echo "    -u --uninstall                Perform uninstallation";
    echo "    -h --help                     Show this help";
    exit;
fi

# Install
if [[ $INSTALL == true ]]; then
    echo "Installing to ${TARGET_DIR}...";
    cp $PROG_NAME $TARGET_DIR;
    if [ $? -eq 0 ]
    then
        echo "Done.";
    else
        echo "Failed!";
    fi
    exit;
fi

# Uninstall
if [[ $UNINSTALL == true ]]; then
    echo "Uninstalling from ${TARGET_DIR}...";
    cd $TARGET_DIR;
    rm --interactive=never $PROG_NAME;
    if [ $? -eq 0 ]
    then
        echo "Done.";
    else
        echo "Failed!";
    fi
    exit;
fi

# Write vars to config.h
echo "/* GENERATED FILE -- MANUAL EDITS MAY BE LOST */" > config.h;
echo "#define PROG_NAME \"${PROG_NAME}\"" >> config.h;
echo "#define PROG_VERSION \"${PROG_VERSION}\"" >> config.h;
if [[ $IGNORE_NICE == true ]]; then
    echo "#define IGNORE_NICE" >> config.h;
fi
if [[ $USE_SMP == true ]]; then
    echo "#define USE_SMP" >> config.h;
fi
if [[ $DEBUG == true ]]; then
    echo "#define DEBUG" >> config.h;
fi

# Build
echo "Building ${PROG_NAME} ${PROG_VERSION} with ${CC}...";
$CC ${C_FILES[@]} $C_ADD_FILE ${CC_MAIN_OPTS[@]} ${CC_ADD_OPTS[@]} -o $PROG_NAME;
if [ $? -eq 0 ]
then
    echo "Done.";
else
    echo "Failed!";
fi
