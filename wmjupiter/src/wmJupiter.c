/* wmjupiter - Copyright (c) 2001 Thomas Kuiper <tkuiper@inxsoft.net> */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <values.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"
#include "bitmaps/master_hq.xpm"
#include "bitmaps/master_hq2.xpm"
#include "bitmaps/master_hq3.xpm"
#include "bitmaps/master_hq4.xpm"

/* 
 *  Delay between refreshes (in microseconds) 
 */
#define DELAY 250000L
#define PI 3.141592653589793238462643383279502884197169399375105

typedef struct Coordinates {
    double x;
    double y;
} Coordinates;

double lambda, lambda1, lambda2;
double jdistance;

int spotlat;

double De;          // planetocentric ang. dist. of earth from jup. equator
// Angles of each of the Galilean satellites, in radians,
// expressed relative to each satellite's inferior conjunction:
double moonAngles[5];
// And their distances from the planet:
double moonDist[5];

char moonLabels[5];

struct tm *tmjupiter = NULL;

char *border_dark;
char *border_light;

bool hq;

void computepos(double);
void routine(int, char **);
double getJulianDate(double, double, double, double, double, double);
double oangle(double);
double angle(double);
Coordinates getRedSpotXY(int);
void findspot(void);
void drawspot(void);
double getMoonDist(int);
void drawmoons(void);

/* compute the julian date between 1901 - 2099 (Sinnott 1991, p. 183) */
double getJulianDate(double year, double month, double day, double uh, double um, double us) {
    return 367 * year - (int) (7 * (year + (int) ((month + 9) / 12)) / 4) + (int) (275 * month / 9) + day + 1721013.5 + (uh + (um + us / 60.0) / 60.0) / 24.0;
}

double oangle(double a) {
    while (a > 2 * PI)
        a -= 2. * PI;

    while (a < 0)
        a += 2. * PI;

    return a;
}

double angle(double a) {
    if (a < 10000)
        return oangle(a);

    a = a - 2. * PI * (int) (a / 2. / PI);

    if (a < 0)
        a += 2 * PI;

    return a;
}

Coordinates getRedSpotXY(int spot_in_deg) {
    double spotlong = angle(lambda2 - spot_in_deg * PI / 180);

    Coordinates coord;

    // See if the spot is visible:
    if (spotlong > PI * .5 && spotlong < PI * 1.5) {
        coord.x = coord.y = MAXDOUBLE;
    } else {
        coord.x = sin(spotlong);
        coord.y = .42;      // completely random wild-assed guess
    }

    return coord;
}

void findspot() {
    time_t t;
    
    double d;

    t = time(0);

    tmjupiter = gmtime(&t);

    d = getJulianDate(tmjupiter->tm_year + 1900, tmjupiter->tm_mon + 1, tmjupiter->tm_mday, tmjupiter->tm_hour, tmjupiter->tm_min, tmjupiter->tm_sec) - 2415020;

    computepos(d);
}

void drawspot() {
    int jsize;
    int xcenter;
    int ycenter;
    int x, y, j2width, spotwidth;
    int redSpotWidth, redSpotHeight;
    
    Coordinates coord;
    
    redSpotWidth = 3;
    redSpotHeight = 4;
    xcenter = 26;
    jsize = 10;
    ycenter = 32;

    if (tmjupiter == NULL)
        return;

    coord = getRedSpotXY(spotlat);

    if (coord.x != MAXDOUBLE && coord.y != MAXDOUBLE) {
        // We only want the spot to draw to the edge of the disk,
        // so adjust the width if necessary:
        // (approx) width/2 of the planet (pixels) at the spot's latitude:
        j2width = (int) (jsize - abs(coord.y * jsize) / 3);
        
        x = xcenter + (int) (coord.x * jsize);
        y = ycenter + (int) (coord.y * jsize) - redSpotHeight / 2;
        
        spotwidth = redSpotWidth;
        
        if (x + redSpotWidth > xcenter + j2width) {
            spotwidth = xcenter + j2width - x;
            x = xcenter + j2width - spotwidth;
        } else if (x - redSpotWidth / 2 < xcenter - j2width) {
            spotwidth = x + redSpotWidth / 2 - xcenter + j2width;
            x = xcenter - j2width;
        } else {
            x -= redSpotWidth / 2;
        }

        da_copy_xpm_area(73, 88, 4, 3, x + 2, 28);
    }
}

double getMoonDist(int whichmoon) {
    return moonDist[whichmoon];
}

void drawmoons() {
    Coordinates coord;

    int i;
    int moonsize = 5;

    char chr;

    double r;

    for (i = 0; i < 4; i++) {
        r = getMoonDist(i);
        coord.x = r * sin(moonAngles[i]);
        coord.y = -r * cos(moonAngles[i]) * sin(De);

        if (coord.x < 1. && coord.x > -1. && moonAngles[i] > PI * .5 && moonAngles[i] < PI * 1.5) {
            coord.x = coord.y = MAXDOUBLE;
        }

        if ((coord.x != MAXDOUBLE) && (coord.y != MAXDOUBLE)) {
            int x = 32 + (int) (coord.x * 10) - moonsize / 2;
            int y = 32 - (int) (coord.y * 10) - moonsize / 2;

            if ((x > 2) && (x < 55) && (y > 2) && (y < 60)) {
                da_copy_xpm_area(78, 88, 4, 4, x, y);

                chr = moonLabels[i] - 65;

                da_copy_xpm_area(chr * 5 + 2, 128, 5, 6, x, 6);
            }
        }
    }
}

void computepos(double d) {
    double psi;
    double delta;       /* Earth-Jupiter distance */
    double V, M, N, J, A, B, K, R, r, G, H;

    // Argument for the long-period term in the motion of Jupiter:
    V = angle((134.63 + .00111587 * d) * PI / 180);

    // Mean anomalies of Earth and Jupiter:
    M = angle((358.476 + .9856003 * d) * PI / 180);
    N = angle((225.328 + .0830853 * d + .33 * sin(V)) * PI / 180);

    // Diff between the mean heliocentric longitudes of Earth & Jupiter:
    J = angle((221.647 + .9025179 * d - .33 * sin(V)) * PI / 180);

    // Equations of the center of Earth and Jupiter:
    A = angle((1.916 * sin(M) + .020 * sin(2 * M)) * PI / 180);
    B = angle((5.552 * sin(N) + .167 * sin(2 * N)) * PI / 180);

    K = angle(J + A - B);

    // Distances are specified in AU:
    // Radius vector of the earth:
    R = 1.00014 - .01672 * cos(M) - .00014 * cos(2 * M);
    // Radius vector of Jupiter:
    r = 5.20867 - .25192 * cos(N) - .00610 * cos(2 * N);

    // Earth-Jupiter distance:
    delta = sqrt(r * r + R * R - 2 * r * R * cos(K));

    jdistance = delta;

    // Phase angle of Jupiter (always btw. -12 and 12 degrees):
    psi = asin(R / delta * sin(K));

    // Longitude of system 1:
    lambda1 = angle((268.28 * 877.8169088 * (d - delta / 173)) * PI / 180 + psi - B);
    // Longitude of system 2:
    lambda2 = angle((290.28 + 870.1869088 * (d - delta / 173)) * PI / 180 + psi - B);

    // calculate the angles of each of the satellites:
    moonAngles[0] = angle((84.5506 + 203.4058630 * (d - delta / 173)) * PI / 180 + psi - B);
    moonAngles[1] = angle((41.5015 + 101.2916323 * (d - delta / 173)) * PI / 180 + psi - B);
    moonAngles[2] = angle((109.9770 + 50.2345169 * (d - delta / 173)) * PI / 180 + psi - B);
    moonAngles[3] = oangle((176.3586 + 21.4879802 * (d - delta / 173)) * PI / 180 + psi - B);

    // and the planetocentric angular distance of the earth
    // from the equator of Jupiter:
    lambda = angle((238.05 + .083091 * d + .33 * sin(V)) * PI / 180 + B);
    De = ((3.07 * sin(lambda + 44.5 * PI / 180) - 2.15 * sin(psi) * cos(lambda - 24. * PI / 180) - 1.31 * (r - delta) / delta * sin(lambda - 99.4 * PI / 180)) * PI / 180);

    G = angle((187.3 + 50.310674 * (d - delta / 173)) * PI / 180);
    H = angle((311.1 + 21.569229 * (d - delta / 173)) * PI / 180);

    // Calculate the distances before any corrections are applied:
    moonDist[0] = 5.9061 - .0244 * cos(2 * (moonAngles[0] - moonAngles[1]));
    moonDist[1] = 9.3972 - .0889 * cos(2 * (moonAngles[1] - moonAngles[2]));
    moonDist[2] = 14.9894 - .0227 * cos(G);
    moonDist[3] = 26.3649 - .1944 * cos(H);

    // apply some first-order correction terms to the angles:
    moonAngles[0] = angle(moonAngles[0] + sin(2 * (moonAngles[0] - moonAngles[1])) * .472 * PI / 180);
    moonAngles[1] = angle(moonAngles[1] + sin(2 * (moonAngles[1] - moonAngles[2])) * 1.073 * PI / 180);
    moonAngles[2] = angle(moonAngles[2] + sin(G) * .174 * PI / 180);
    moonAngles[3] = angle(moonAngles[3] + sin(H) * .845 * PI / 180);
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "shows you the actual distance of Jupiter in AE and when the red spot crosses.\n\n"

    "The four Gallileo Moons are displayed too but only when they are near the planet (Io, Europa, Ganymede, and Calisto).\n\n"

    "The applet space is too small to display all moons at the same time but a future version might be able to zoom in/out.\n\n"

    "The position of the red spot changes on the surface so you need to change that value within few months or so. The current position can be found on the internet. It was 80 deg. as of 11th Jan 2002.");
    
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&hq, "high_quality", "-H", "--high-quality", false, "Use high-quality image");
    da_init_integer(&spotlat, "spot_position", "-S", "--spot-position", 0, 0, 0, "Spot position in lat. (e.g. 80)");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    int m = 101, dt1, dt2, dt3;
    int q, i;
    int digit;

    char tempstr[255];
    
    //double val;

    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, hq ? master_hq4_xpm : master4_xpm, NULL, 134 * da_scale, 148 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, hq ? master_hq3_xpm : master3_xpm, NULL, 134 * da_scale, 148 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, hq ? master_hq2_xpm : master2_xpm, NULL, 134 * da_scale, 148 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, hq ? master_hq_xpm : master_xpm, NULL, 134 * da_scale, 148 * da_scale);
        break;
    }

    moonLabels[0] = 'I';
    moonLabels[1] = 'E';
    moonLabels[2] = 'G';
    moonLabels[3] = 'C';

    findspot();

    da_redraw_window();
    
    while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            /* TODO: do something */
            break;
        }

        q = 0;
/*      da_copy_xpm_area(5, 69, 54, 54, 5, 5);
        chr = 'A' - 65;
             da_copy_xpm_area(chr*5+2, 128, 5, 6, 7+q, 6); q+= 5;
        chr = 'B' - 65;
             da_copy_xpm_area(chr*5+2, 128, 5, 6, 7+q, 6); q+= 5;
*/
        da_copy_xpm_area(5, 69, 54, 54, 5, 5);

        drawspot();

        /* hide unneeded spot parts */
        da_copy_xpm_area_with_trans(84, 69, 50, 54, 5, 5);

        drawmoons();

        sprintf(tempstr, "%2.5f", jdistance);

        q = 0;

        //val = jdistance;

        for (i = 0; i < 7; i++) {
            if (tempstr[i] == '\0')
                break;

            digit = tempstr[i] - 48;

            if (digit >= 0) {
                da_copy_xpm_area(digit * 5 + 66, 57, 5, 6, 22 + q, 52);

                q += 5;

            } else {
                da_copy_xpm_area(10 * 5 + 66, 57, 5, 6, 22 + q, 52);

                q += 5;
            }
        }

        if (m > 100) {
            findspot();

            m = 0;

            ++dt1;
            ++dt2;
            ++dt3;
        } else {
            /*
             *  Increment counter
             */
            ++m;
        }

        da_redraw_window();

        usleep(DELAY);
    }

    routine(argc, argv);
}
