/* Some code was taken from wmpd. */

#include "lang.h"

#define CURRENT_LANGS 20

/* The order in langs, weekdays, and months must NOT be changed! */
char *langs[] = {"english", "croatian", "hungarian", "polish", "spanish", "czech", "french", "french2", "indonesian", "portuguese", "swedish", "danish", "italian", "russian", "ukrainian", "breton", "dutch", "german", "norwegian", "slovene"};

char *weekdays[][7] = {
    /* default: english */
    {"mon", "tue", "wed", "thu", "fri", "sat", "sun"},
    /* croatian */
    {"pon", "uto", "sri", "čet", "pet", "sub", "ned"},
    /* hungarian */
    {"hé", "ke", "sze", "csü", "pé", "szo", "vas"},
    /* polish */
    {"pon", "wtr", "śrd", "czw", "ptk", "sob", "ndl"},
    /* spanish */
    {"lun", "mar", "mie", "jue", "vie", "sab", "dom"},
    /* czech */
    {"po", "út", "st", "čt", "pá", "so", "ne"},
    /* french */
    {"lun", "mar", "mer", "jeu", "ven", "sam", "dim"},
    /* french2 */
    {"lun", "mar", "mer", "jeu", "ven", "sam", "dim"},
    /* indonesian */
    {"sen", "sel", "rab", "kam", "jum", "sab", "min"},
    /* portuguese */
    {"seg", "ter", "qua", "qui", "sex", "sáb", "dom"},
    /* swedish */
    {"mån", "tis", "ons", "tor", "fre", "lör", "sön"},
    /* danish */
    {"man", "tir", "ons", "tor", "fre", "lør", "søn"},
    /* italian */
    {"lun", "mar", "mer", "gio", "ven", "sab", "dom"},
    /* russian */
    {"пнд", "втр", "срд", "чтв", "птн", "суб", "вос"},
    /* ukrainian */
    {"пон", "вів", "сер", "чет", "п'ят", "суб", "нед"},
    /* breton */
    {"lun", "meu", "mer", "yao", "gwe", "sad", "sul"},
    /* dutch */
    {"ma", "di", "wo", "do", "vr", "za", "zo"},
    /* german */
    {"mo", "di", "mi", "do", "fr", "sa", "so"},
    /* norwegian */
    {"man", "tir", "ons", "tor", "fre", "lør", "søn"},
    /* slovene */
    {"pon", "tor", "sre", "čet", "pet", "sob", "ned"}
};

char *months[][12] = {
    /* default: english */
    {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"},
    /* croatian */
    {"sij", "vel", "ožu", "tra", "svi", "lip", "spr", "kol", "ruj", "lis", "stu", "pro"},
    /* hungarian */
    {"jan", "feb", "már", "ápr", "máj", "jún", "júl", "aug", "szep", "okt", "nov", "dec"},
    /* polish */ 
    {"sty", "lut", "marz", "kwi", "maj", "czer", "lip", "sier", "wrz", "paź", "list", "gru"},
    /* spanish */
    {"ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"},
    /* czech */
    {"led", "úno", "bře", "dub", "kvě", "čer", "čec", "srp", "zář", "říj", "lis", "pro"},
    /* french */
    {"jan", "fev", "mar", "avr", "mai", "jun", "jul", "aou", "sep", "oct", "nov", "dec"},
    /* french2 */
    {"jan", "fev", "mar", "avr", "mai", "juin", "juil", "aou", "sep", "oct", "nov", "dec"},
    /* indonesian */
    {"jan", "feb", "mar", "apr", "mei", "jun", "jul", "agu", "sep", "okt", "nov", "des"},
    /* portuguese */
    {"jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez"},
    /* swedish */
    {"jan", "feb", "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec"},
    /* danish */
    {"jan", "feb", "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec"},
    /* italian */
    {"gen", "feb", "mar", "apr", "mag", "giu", "lug", "ago", "set", "ott", "nov", "dic"},
    /* russian */
    {"янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"},
    /* ukrainian */
    {"січ", "лют", "бер", "кві", "тра", "чер", "лип", "сер", "вер", "жов", "лис", "гру"},
    /* breton */
    {"gen", "chw", "meu", "ebr", "mae", "eve", "gou", "eos", "gwe", "her", "du", "ker"},
    /* dutch */
    {"jan", "feb", "mar", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"},
    /* german */
    {"jan", "feb", "mrz", "apr", "mai", "jun", "jul", "aug", "sep", "okt", "nov", "dez"},
    /* norwegian */
    {"jan", "feb", "mar", "apr", "mai", "jun", "jul", "aug", "sep", "okt", "nov", "des"},
    /* slovene */
    {"jan", "feb", "mar", "apr", "maj", "jun", "jul", "avg", "sep", "okt", "nov", "dec"}
};

static glyphdescr glyphs[] = {
    {L'-', 67, 147}, {L'.', 73, 147}, {L'\x27', 79, 147},
    {L'(', 85, 147}, {L')', 91, 147}, {L'*', 97, 147}, {L'/', 103, 147},

    {L'0',  1, 147}, {L'1',  7, 147}, {L'2', 13, 147}, {L'3', 19, 147}, {L'4', 25, 147},
    {L'5', 31, 147}, {L'6', 37, 147}, {L'7', 43, 147}, {L'8', 49, 147}, {L'9', 55, 147},



    {L'A',  1, 137}, {L'a',  1, 137},
    {L'B',  7, 137}, {L'b',  7, 137},
    {L'C', 13, 137}, {L'c', 13, 137},
    {L'D', 19, 137}, {L'd', 19, 137},
    {L'E', 25, 137}, {L'e', 25, 137},

    {L'F', 31, 137}, {L'f', 31, 137},
    {L'G', 37, 137}, {L'g', 37, 137},
    {L'H', 43, 137}, {L'h', 43, 137},
    {L'I', 49, 137}, {L'i', 49, 137},
    {L'J', 55, 137}, {L'j', 55, 137},

    {L'K', 61, 137}, {L'k', 61, 137},
    {L'L', 67, 137}, {L'l', 67, 137},
    {L'M', 73, 137}, {L'm', 73, 137},
    {L'N', 79, 137}, {L'n', 79, 137},
    {L'O', 85, 137}, {L'o', 85, 137},

    {L'P', 91, 137}, {L'p', 91, 137},
    {L'Q', 97, 137}, {L'q', 97, 137},
    {L'R',103, 137}, {L'r',103, 137},
    {L'S',109, 137}, {L's',109, 137},
    {L'T',115, 137}, {L't',115, 137},

    {L'U',121, 137}, {L'u',121, 137},
    {L'V',127, 137}, {L'v',127, 137},
    {L'W',133, 137}, {L'w',133, 137},
    {L'X',139, 137}, {L'x',139, 137},
    {L'Y',145, 137}, {L'y',145, 137},

    {L'Z',151, 137}, {L'z',151, 137},


    {L'\x42e',  1, 157}, {L'\x44e',  1, 157}, /* cyrillic Yu */

    {L'\x410',  7, 157}, {L'\x430',  7, 157}, /* cyrillic A */
    {L'\x411', 13, 157}, {L'\x431', 13, 157}, /* cyrillic Be */
    {L'\x426', 19, 157}, {L'\x446', 19, 157}, /* cyrillic Ce */
    {L'\x414', 25, 157}, {L'\x434', 25, 157}, /* cyrillic De */
    {L'\x415', 31, 157}, {L'\x435', 31, 157}, /* cyrillic Ye */

    {L'\x424', 37, 157}, {L'\x444', 37, 157}, /* cyrillic eF */
    {L'\x413', 43, 157}, {L'\x433', 43, 157}, /* cyrillic Ge */
    {L'\x425', 49, 157}, {L'\x445', 49, 157}, /* cyrillic Ha */
    {L'\x418', 55, 157}, {L'\x438', 55, 157}, /* cyrillic I */
    {L'\x419', 61, 157}, {L'\x439', 61, 157}, /* cyrillic I-kratkoe */

    {L'\x41a', 67, 157}, {L'\x43a', 67, 157}, /* cyrillic Ka */
    {L'\x41b', 73, 157}, {L'\x43b', 73, 157}, /* cyrillic eL */
    {L'\x41c', 79, 157}, {L'\x43c', 79, 157}, /* cyrillic eM */
    {L'\x41d', 85, 157}, {L'\x43d', 85, 157}, /* cyrillic eN */
    {L'\x41e', 91, 157}, {L'\x43e', 91, 157}, /* cyrillic O */

    {L'\x41f', 97, 157}, {L'\x43f', 97, 157}, /* cyrillic Pe */
    {L'\x42f',103, 157}, {L'\x44f',103, 157}, /* cyrillic Ya */
    {L'\x420',109, 157}, {L'\x440',109, 157}, /* cyrillic eR */
    {L'\x421',115, 157}, {L'\x441',115, 157}, /* cyrillic eS */
    {L'\x422',121, 157}, {L'\x442',121, 157}, /* cyrillic Te */

    {L'\x423',127, 157}, {L'\x443',127, 157}, /* cyrillic U */
    {L'\x416',133, 157}, {L'\x436',133, 157}, /* cyrillic Je */
    {L'\x412',139, 157}, {L'\x432',139, 157}, /* cyrillic Ve */
    {L'\x42c',145, 157}, {L'\x44c',145, 157}, /* cyrillic MyagkijZnak */
    {L'\x42b',151, 157}, {L'\x44b',151, 157}, /* cyrillic Y */

    {L'\x417',157, 157}, {L'\x437',157, 157}, /* cyrillic Ze */
    {L'\x428',163, 157}, {L'\x448',163, 157}, /* cyrillic Sha */
    {L'\x42d',169, 157}, {L'\x44d',169, 157}, /* cyrillic E */
    {L'\x429',175, 157}, {L'\x449',175, 157}, /* cyrillic Scha */
    {L'\x427',181, 157}, {L'\x447',181, 157}, /* cyrillic Che */

    {L'\x42a',187, 157}, {L'\x44a',187, 157}, /* cyrillic TvyordyiZnak */
    {L'\x404',115, 147}, {L'\x454',115, 147}, /* ukrainian IE */
    {L'\x406', 49, 137}, {L'\x456', 49, 137}, /* ukrainian I */
    {L'\x407',109, 147}, {L'\x457',109, 147}, /* ukrainian YI */
    {L'\x491', 43, 157}, {L'\x490', 43, 157}, /* ukrainian GHE with upturn */

    {L'\x401',121, 147}, {L'\x451',121, 147}, /* cyrillic Yo */

    {L' ', 61, 147},

    {L'Ø',  1, 127}, {L'ø',  1, 127},
    {L'Č',  7, 127}, {L'č',  7, 127},
    {L'É', 13, 127}, {L'é', 13, 127},
    {L'Ü', 19, 127}, {L'ü', 19, 127},
    {L'Ś', 31, 127}, {L'ś', 31, 127},
    {L'Á', 37, 127}, {L'á', 37, 127},
    {L'Ú', 25, 127}, {L'ú', 25, 127},
    {L'Å', 37, 127}, {L'å', 37, 127},
    {L'Ö', 43, 127}, {L'ö', 43, 127},
    {L'Ž', 49, 127}, {L'ž', 49, 127},
    {L'Ř', 55, 127}, {L'ř', 55, 127},
    {L'Í', 61, 127}, {L'í', 61, 127}
    /*{L'Í', 85, 137}, {L'í', 85, 137},
    {L'Í', 85, 137}, {L'í', 85, 137},
    {L'Í', 85, 137}, {L'í', 85, 137},*/
};

int chosen_lang_pos;

void DrawChar(wchar_t wc, int x, int y) {
    int i;
    
    for (i = 0; (long unsigned int)i < sizeof(glyphs)/sizeof(glyphdescr) - 1; ++i) {
        if (wc == glyphs[i].c)
            break;
    }
    
    da_copy_xpm_area(glyphs[i].x, glyphs[i].y, 6, 8, x, y);
}

void DrawChars(char *ttl, int y_pos) {
    wchar_t wc;

    int len, i = 0, ii = 3, x_pos = 23;

    size_t true_len = 0;

    char *ttl_temp = ttl;
    
    while (*ttl_temp) {
        true_len += (*ttl_temp++ & 0xC0) != 0x80;
    }
    
    switch (true_len) {
    case 4:
        x_pos = 20;
        ii = 4;
        break;
    case 2:
        x_pos = 26;
        ii = 2;
        break;
    case 3:
    default:
        x_pos = 23;
        ii = 3;
        break;
    }

    mbtowc(NULL, NULL, 0);
    
    while (*ttl && i < ii) {
        len = mbtowc(&wc, ttl, MB_CUR_MAX);
        
        ttl += len;
        
        DrawChar(wc, x_pos, y_pos);

        x_pos += 6;
        i++;
    }
}

void DrawWeekDay(int day) {
    DrawChars(weekdays[chosen_lang_pos][day], 24);
}

void DrawMonth(int month) {
    DrawChars(months[chosen_lang_pos][month], 46);
}

void SetLang() {
    int i, len;

    char *chosen_lang_l;

    len = strlen(chosen_lang);

    chosen_lang_l = strdup(chosen_lang);

    for (i = 0; i < len; i++) {
        chosen_lang_l[i] = tolower(chosen_lang[i]);
    }

    for (i = 0; i <= CURRENT_LANGS; i++) {
        if (i == CURRENT_LANGS) {
            printf("WARN: %s is unknown, defaulting to english.\n", chosen_lang_l);
            da_log(WARN, "%s is unknown, defaulting to english.\n", chosen_lang_l);

            chosen_lang_pos = 0;

            break;
        }
        
        if (!strcmp(langs[i], chosen_lang_l)) {
            chosen_lang_pos = i;

            break;
        }
    }

    if (chosen_lang_l) {
        free(chosen_lang_l);

        chosen_lang_l = NULL;
    }
}

void ShowLangs() {
    int i;

    printf("Currently avaliable languages are: ");
    
    for (i = 0; i < CURRENT_LANGS; i++) {
        if (i == CURRENT_LANGS - 1) {
            printf("%s.\n\n", langs[i]);
        } else {
            printf("%s, ", langs[i]);
        }
    }
}
