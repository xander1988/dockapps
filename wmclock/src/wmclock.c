/* wmclock.c: a dockable clock applet for Window Maker
 * created 1999-Apr-09 jmk
 *
 * by Jim Knoble <jmknoble@pobox.com>
 * Copyright (C) 1999 Jim Knoble
 *
 * Significant portions of this software are derived from asclock by
 * Beat Christen <spiff@longstreet.ch>.  Such portions are copyright
 * by Beat Christen and the other authors of asclock.
 *
 * Disclaimer:
 *
 * The software is provided "as is", without warranty of any kind,
 * express or implied, including but not limited to the warranties of
 * merchantability, fitness for a particular purpose and
 * noninfringement. In no event shall the author(s) be liable for any
 * claim, damages or other liability, whether in an action of
 * contract, tort or otherwise, arising from, out of or in connection
 * with the software or the use or other dealings in the software.
 */

#include <sys/select.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "config.h"
#include "lang.h"

#define LED_NUM_Y_OFFSET    0
#define LED_THIN_1_X_OFFSET 13
#define LED_NUM_WIDTH       9
#define LED_NUM_HEIGHT      11
#define LED_THIN_1_WIDTH    5

#define COLON_X_OFFSET      90
#define COLON_Y_OFFSET      0
#define BLANK_X_OFFSET      119
#define BLANK_Y_OFFSET      COLON_Y_OFFSET
#define COLON_WIDTH     3
#define COLON_HEIGHT        11

#define AM_X_OFFSET     94
#define AM_Y_OFFSET     5
#define PM_X_OFFSET     107
#define PM_Y_OFFSET     5
#define AM_WIDTH        12
#define AM_HEIGHT       6
#define PM_WIDTH        11
#define PM_HEIGHT       6

#define MONTH_X_POS     10
#define MONTH_Y_POS     3
#define MONTH_X_OFFSET      0
#define MONTH_WIDTH     22
#define MONTH_HEIGHT        6

#define DATE_LEFT_X_POS     7
#define DATE_CENTER_X_POS   8
#define DATE_RIGHT_X_POS    9
#define DATE_Y_POS      2
#define DATE_Y_OFFSET       0
#define DATE_NUM_WIDTH      9
#define DATE_NUM_HEIGHT     13

#define WEEKDAY_X_POS       6
#define WEEKDAY_Y_POS       1
#define WEEKDAY_X_OFFSET    0
#define WEEKDAY_WIDTH       21
#define WEEKDAY_HEIGHT      6

#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

int mytime(void);
void showYear(void);
void showTime12(void);
void showTime24(void);
void showTime(void);
void routine(int, char **);

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *command;

bool enable12HourClock;  /* default value is 24h format */
bool enableBlinking;     /* default is blinking */
bool enableYearDisplay;  /* default is to show time, not year */

int blinkInterval;       /* default is a 2-second blink cycle */

time_t actualTime;
long   actualMinutes;

static struct tm *localTime;

/* Fetch the system time and time zone */
int mytime(void) {
    struct timeval  tv;
    struct timezone tz;

    gettimeofday(&tv, &tz);

    return(tv.tv_sec);
}

/* Display the current year in the LED display */
void showYear(void) {
    int year;
    int digitXOffset;
    int digitYOffset;

    year = localTime->tm_year + 1900;

    digitYOffset = LED_NUM_Y_OFFSET;
    digitXOffset = LED_NUM_WIDTH * (year / 1000);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 12, 6);

    digitXOffset = LED_NUM_WIDTH * ((year / 100) % 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 21, 6);

    digitXOffset = LED_NUM_WIDTH * ((year / 10) % 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 34, 6);

    digitXOffset = LED_NUM_WIDTH * (year % 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 43, 6);
}

/* Display time in twelve-hour mode, with am/pm indicator */
void showTime12(void) {
    int digitXOffset;
    int digitYOffset;
    int localHour = localTime->tm_hour % 12;

    if (0 == localHour) {
        localHour = 12;
    }

    if (localTime->tm_hour < 12) {
        /* AM */
        da_copy_xpm_area(AM_X_OFFSET + 64, AM_Y_OFFSET + 13, AM_WIDTH, AM_HEIGHT, 47, 11);
    } else {
        /* PM */
        da_copy_xpm_area(PM_X_OFFSET + 64, PM_Y_OFFSET + 13, PM_WIDTH, PM_HEIGHT, 47, 11);
    }

    digitYOffset = LED_NUM_Y_OFFSET;

    if (localHour > 9) {
        digitXOffset = LED_THIN_1_X_OFFSET;

        da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_THIN_1_WIDTH, LED_NUM_HEIGHT, 6, 6);
    }

    digitXOffset = LED_NUM_WIDTH * (localHour % 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 15, 6);

    digitXOffset = LED_NUM_WIDTH * (localTime->tm_min / 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 28, 6);

    digitXOffset = LED_NUM_WIDTH * (localTime->tm_min % 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 37, 6);
}

/* Display time in 24-hour mode, without am/pm indicator */
void showTime24(void) {
    int digitXOffset;
    int digitYOffset;

    digitYOffset = LED_NUM_Y_OFFSET;
    digitXOffset = LED_NUM_WIDTH * (localTime->tm_hour / 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 12, 6);

    digitXOffset = LED_NUM_WIDTH * (localTime->tm_hour % 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 21, 6);

    digitXOffset = LED_NUM_WIDTH * (localTime->tm_min / 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 34, 6);

    digitXOffset = LED_NUM_WIDTH * (localTime->tm_min % 10);

    da_copy_xpm_area(digitXOffset + 64, digitYOffset + 13, LED_NUM_WIDTH, LED_NUM_HEIGHT, 43, 6);
}

void showTime(void) {
    int xOffset;
    int yOffset;

    /* Zeit auslesen */
    actualTime = mytime();
    actualMinutes = actualTime / 60;

    localTime = localtime(&actualTime);

    if (enableYearDisplay) {
        showYear();
    } else if (enable12HourClock) {
        showTime12();
    } else {
        showTime24();
    }

    /* Monat */
    xOffset = MONTH_X_OFFSET;
    yOffset = MONTH_HEIGHT * (localTime->tm_mon);

    DrawMonth(localTime->tm_mon);

    /* Datum */
    yOffset = DATE_Y_OFFSET;

    if (localTime->tm_mday > 9) {
        xOffset = DATE_NUM_WIDTH * (((localTime->tm_mday / 10) + 9) % 10);

        da_copy_xpm_area(xOffset + 64, yOffset, DATE_NUM_WIDTH, DATE_NUM_HEIGHT, 23, 32);

        xOffset = DATE_NUM_WIDTH * (((localTime->tm_mday % 10) + 9) % 10);

        da_copy_xpm_area(xOffset + 64, yOffset, DATE_NUM_WIDTH, DATE_NUM_HEIGHT, 31, 32);
    } else {
        xOffset = DATE_NUM_WIDTH * (localTime->tm_mday - 1);

        da_copy_xpm_area(xOffset + 64, yOffset, DATE_NUM_WIDTH, DATE_NUM_HEIGHT, 28, 32);
    }

    /* Wochentag */
    xOffset = WEEKDAY_X_OFFSET;
    yOffset = WEEKDAY_HEIGHT * ((localTime->tm_wday + 6) % 7);

    DrawWeekDay((localTime->tm_wday + 6) % 7);

    if ((!enableBlinking) && (!enableYearDisplay)) {
        /* Sekunden Doppelpunkt ein */
        xOffset = COLON_X_OFFSET;
        yOffset = COLON_Y_OFFSET;

        da_copy_xpm_area(xOffset + 64, yOffset + 13, COLON_WIDTH, COLON_HEIGHT, enable12HourClock ? 25 : 31, 6);
    }
}

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockable clock for the Window Maker window manager.\n\n"

    PROG_NAME" is an applet which displays the date and time in a dockable tile in the same style as the clock from the NEXTSTEP(tm) operating system. "PROG_NAME" is specially designed for the Window Maker window manager, by Alfredo Kojima, and features multiple language support (see below), twenty-four-hour and twelve-hour (am/pm) time display, and, optionally, can run a user-specified program on a mouse click. "PROG_NAME" is derived from asclock, a similar clock for the AfterStep window manager.\n\n"

    "Currently avaliable languages are: english, croatian, hungarian, polish, spanish, czech, french, french2, indonesian, portuguese, swedish, danish, italian, russian, ukrainian, breton, dutch, german, norwegian, slovene.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "LightSeaGreen", "Time color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&chosen_lang, "language", "-L", "--lang", "english", "Set weekday and month display language (see the list above)");
    da_init_string(&command, "command", "-c", "--command", NULL, "Command to execute on mouse click");
    da_init_bool(&enable12HourClock, "12_hour_clock", NULL, "--12-hour", false, "Display the time in either twelve-hour format (with am/pm) or twenty-four-hour format");
    da_init_bool(&enableBlinking, "enable_blinking", "-B", "--blink", true, "The separator between the hours and minutes in the time display blinks by default; this option turns off the blinking and displays a steadily lit separator instead");
    da_init_integer(&blinkInterval, "blink_interval", "-i", "--blink-interval", 1, 60, 2, "Set the blink cycle interval; the default is 2 (1 second on, 1 second off)");
    da_init_bool(&enableYearDisplay, "display_year", "-y", "--display-year", false, "Display the current year in the LED display instead of the time");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    int blinkCounter = 0;

    setlocale(LC_ALL, "");

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 193 * da_scale, 165 * da_scale);
        break;
    }

    SetLang();

    showTime();

    da_redraw_window();

    while (!da_conf_changed(argc, argv)) {
        if (actualTime != mytime()) {
            actualTime = mytime();

            if (actualMinutes != (actualTime / 60)) {
                showTime();

                if (!enableBlinking) {
                    da_redraw_window();
                }
            }

            if (0 == (actualTime % 2)) {
                /* Clean up zombie processes */
#ifdef DEBUG
                fprintf(stderr, "cleaning up zombies (time %ld)\n", actualTime);
                da_log(INFO, "cleaning up zombies (time %ld).\n", actualTime);
#endif /* DEBUG */
                if (NULL != command) {
                    waitpid(0, NULL, WNOHANG);
                }
            }
        }

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (command) {
                da_exec_command(command);
            }
            break;
        }
        
        if (enableBlinking && (!enableYearDisplay)) {
            blinkCounter++;
            
            if (blinkCounter < blinkInterval * 5) {
                da_copy_xpm_area(COLON_X_OFFSET + 64, COLON_Y_OFFSET + 13, COLON_WIDTH, COLON_HEIGHT, enable12HourClock ? 25 : 31, 6);
                
            } else {
                if (blinkCounter >= blinkInterval * 10) {
                    blinkCounter = 0;
                }
                
                da_copy_xpm_area(BLANK_X_OFFSET + 64, BLANK_Y_OFFSET + 13, COLON_WIDTH, COLON_HEIGHT, enable12HourClock ? 25 : 31, 6);
            }
        }

        da_redraw_window();
        
        usleep(100000L);
    }
    
    routine(argc, argv);
}
