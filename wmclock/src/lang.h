/* Some code was taken from wmpd. */

#include <libdockapp4/dockapp.h>
#include <locale.h>

typedef struct {
    wchar_t c;
    int x;
    int y;
} glyphdescr;

char *chosen_lang;

void DrawChar(wchar_t, int, int);
void DrawChars(char *, int);
void DrawWeekDay(int);
void DrawMonth(int);
void SetLang(void);
void ShowLangs(void);
