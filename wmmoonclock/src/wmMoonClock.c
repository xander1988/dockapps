/*
 *
 *      wmMoonClock-1.26 (C) 1998, 1999 Mike Henderson (mghenderson@lanl.gov)
 *
 *          - Shows Moon Phase....
 *
 *
 *
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program (see the file COPYING); if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *      Boston, MA 02110-1301 USA
 *
 *      Things TODO:
 *                  - clean up code!
 *                  - more detailed documentation.
 *                  - reduce size of pixmap! Dont need it in one pixmap.
 *            Aslo, does the hi-color pixmap really need all those colors?
 *                  - add rotation of moon so user sees it as they would in reality?
 *                  - eclipses. The calcs are quite acurate so this should be easily doable.
 *            (Note the Sun position calcs in CalcEphem are low precision -- high is not
 *             as costly as the Moon calcs.) Sun posiiton is calculated but not used yet...
 *          - Next new moons, next full moons, next quarters, etc...
 *          - Moon names. I.e. Harvest, Blue, etc...
 *
 *
 *
 *      Changes:
 *      Version 1.27 -  released June 7, 1999.
 *          fixed a minor bug in computation of azimuth (A in Horizon Coords). Thanks to
 *          Dan Moraru for spotting this one. (There were two SinGlat factors instead of one).
 *
 *      Version 1.26 -  released April 22, 1999 (?).
 *      Version 1.25 -  released March 22, 1999.
 *                      Now auto-detects 8-bit display and forces the LowColor pixmap to
 *          be used. The -low option still works if you need to conserve colors
 *          even on high-color displays.
 *
 *          Added 3 command line options + code to change colors of the data
 *          entries:
 *
 *              -bc <Color> to change background color.
 *              -lc <Color> to change color of labels and headers.
 *              -dc <Color> to change color of data values.
 *
 *      Version 1.24 -  released February 9, 1999.
 *                      Added low color support via the -low command line option.
 *
 *      Version 1.23 -  released February 4, 1999.
 *                      cosmetic for AfterStep users. removed spurious black line at RHS edge an mask.
 *
 *  Version 1.22 -  released January 8, 1999.
 *
 *              + Changed PI2 to TwoPi in Moon.c -- Linux Pyth. had probs because
 *                        PI2 was defined in <math.h>.
 *
 *  Version 1.21 -  released January 7, 1999.
 *
 *                     + minor bug fixes in Makefile and manpage.
 *
 *  Version 1.2 - released January 3, 1999.
 *            Added:
 *
 *          + Local Time/ Universal Time display.
 *          + Visible: Yes/No to indicate if Moon is up or not.
 *          + Frac (percent through orbit -- this is NOT a simple
 *            conversion of AGE....).
 *          + Horizon Coords. Altitude is measured up from horizon to
 *            Moon in degrees. Azimuth is in degrees from due south.
 *
 *                Also shuffled things around a bit...
 *
 *
 *
 *  Version 1.1 - released December 24, 1998.
 *                    Fixed bug in AGE calculation. It now should be highly accurate.
 *                    Note that AGE is not the same as Phase*29.530589 ...
 *                    I have checked with the Astronomical Almanac and it agrees very
 *            well....
 *
 *
 *  Version 1.0 - released December 16, 1998.
 *
 *
 *
 */

/*
 *   Includes
 */
#include <math.h>
#include <sys/select.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "CalcEphem.h"
#include "MoonRise.h"

#include "XPM/wmMoonClock_mask64.xbm"
#include "XPM/wmMoonClock_mask128.xbm"
#include "XPM/wmMoonClock_mask192.xbm"
#include "XPM/wmMoonClock_mask256.xbm"
#include "XPM/wmMoonClock_masterLow64.xpm"
#include "XPM/wmMoonClock_masterLow128.xpm"
#include "XPM/wmMoonClock_masterLow192.xpm"
#include "XPM/wmMoonClock_masterLow256.xpm"
#include "XPM/wmMoonClock_master64.xpm"
#include "XPM/wmMoonClock_master128.xpm"
#include "XPM/wmMoonClock_master192.xpm"
#include "XPM/wmMoonClock_master256.xpm"
#include "XPM/wmMoonClock_master_hq64.xpm"
#include "XPM/wmMoonClock_master_hq128.xpm"
#include "XPM/wmMoonClock_master_hq192.xpm"
#include "XPM/wmMoonClock_master_hq256.xpm"

/*
 *  Delay between refreshes (in microseconds)
 */
#define DELAY 1000000L

int ToggleWindow = 0;
int nMAX = 1;
int Flag = 1;

double Glat, Glon, SinGlat, CosGlat, TimeZone;

bool UseLowColorPixmap;
bool UseHQPixmap;

char *border_dark;
char *border_light;
char *label_color;
char *data_color;
char *back_color;

/* prototypes */
void routine(int, char **);

/*
 *   main
 */
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "dockable Moon Phase Clock.\n\n"

    PROG_NAME" displays the current phase of the moon. Clicking on the icon brings up different displays - there are 5 in all. The different \"pages\" are:\n\n"

    "First Page\n"
    "Shows the Moon phase image.\n\n"

    "Second Page\n"
    "Shows the current Local Time (LT) and Universal Time (UT), the Moon's Age (number of days since last new moon), the geometric (as opposed to temporal) fraction of the way through the current lunar cyle (e.g. 50%% for full moon), the fraction of the Moon's disc that is illuminated (ratio of area illuminated to total area of disc) and whether the Moon is (locally) visible of not (i.e. is it above the horizon?).\n\n"

    "Third Page\n"
    "Shows the Rise and Set times for yesterday (first line), today (middle line), and tommorrow (last line). If the Moon does not rise or set on a given day a `null time' is shown (--:--). Note that these times should still be good for high latitude observers. Also note that there will always be at least one (--:--) showing up per month. This is because once per month the Moon will rise (set) on a given day but will set (rise) in the very early portion of the next day.\n\n"

    "Fourth Page\n"
    "Shows the Moon's horizon coordinates (i.e. the Altitude/Azimuth system). Azimuth is measured in degrees CCW from due south, and altitude is measured in degrees from the horizon up to the Moon. Distance (Dist) is measured in units on Earth radii (1 Re is about 6370km). Note that this is a local coordinate system and will not be correct if the observer's latitude and longitude are not set correctly.\n\n"

    "Fifth Page\n"
    "Shows the Moon's ecliptic coordinates. (i.e. the Right Ascention/Declination system). Useful for astronomers?\n\n"

    "Many of the quantities shown will not be correct unless LT and UT are correct, and the user specifies the proper latitude and longitude.");

    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&label_color, "label_color", NULL, "--label-color", "#a171ff", "Set color of text labels");
    da_init_string(&data_color, "data_color", NULL, "--data-color", "#3dafff", "Set color of data values");
    da_init_bool(&UseHQPixmap, "high_quality", "-H", "--high-quality", false, "Use experimental high quality moon images");
    da_init_bool(&UseLowColorPixmap, "low_color", NULL, "--low-color", false, "Use a lower-color pixmap for 8-bit displays; you can also force its use on higher-color displays if necessary");
    da_init_float(&Glat, "latitude", "-L", "--latitude", 0.0, 0.0, 0.0, "Observers Latitude in degrees; positive in northern hemisphere, negative in southern hemisphere");
    da_init_float(&Glon, "longitude", "-N", "--longitude", 0.0, 0.0, 0.0, "Observers Longitude in degrees; Greenwich is 0.0, and longitude increases positively toward the west (alternatively, negative numbers can also be used to specify longitudes to the east of Greenwich)");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                      /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");    /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                       /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    struct tm *GMTTime, *LocalTime;

    int         i, n, j, ImageNumber, Year, Month, DayOfMonth, digit;
    long        CurrentLocalTime, CurrentGMTTime, date;
    double      UT, val, RA, DEC, UTRise, UTSet, LocalHour, hour24();
    int         D, H, M, S, sgn, A, B, q;
    CTrans              c;
    struct timeval  timeout;
    fd_set      xfdset;

    c.Glat = Glat, c.Glon = Glon;
    Glat *= RadPerDeg; SinGlat = sin( Glat ); CosGlat = cos( Glat );

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    da_add_color(label_color, "data_color_1");
    da_add_color(data_color, "data_color_2");

    switch (da_scale) {
    case 4:
        if (UseLowColorPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_masterLow256_xpm, wmMoonClock_mask256_bits, wmMoonClock_mask256_width, wmMoonClock_mask256_height);
        } else if (UseHQPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_master_hq256_xpm, wmMoonClock_mask256_bits, wmMoonClock_mask256_width, wmMoonClock_mask256_height);
        } else {
            da_open_xwindow(argc, argv, wmMoonClock_master256_xpm, wmMoonClock_mask256_bits, wmMoonClock_mask256_width, wmMoonClock_mask256_height);
        }
        break;
        
    case 3:
        if (UseLowColorPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_masterLow192_xpm, wmMoonClock_mask192_bits, wmMoonClock_mask192_width, wmMoonClock_mask192_height);
        } else if (UseHQPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_master_hq192_xpm, wmMoonClock_mask192_bits, wmMoonClock_mask192_width, wmMoonClock_mask192_height);
        } else {
            da_open_xwindow(argc, argv, wmMoonClock_master192_xpm, wmMoonClock_mask192_bits, wmMoonClock_mask192_width, wmMoonClock_mask192_height);
        }
        break;
        
    case 2:
        if (UseLowColorPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_masterLow128_xpm, wmMoonClock_mask128_bits, wmMoonClock_mask128_width, wmMoonClock_mask128_height);
        } else if (UseHQPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_master_hq128_xpm, wmMoonClock_mask128_bits, wmMoonClock_mask128_width, wmMoonClock_mask128_height);
        } else {
            da_open_xwindow(argc, argv, wmMoonClock_master128_xpm, wmMoonClock_mask128_bits, wmMoonClock_mask128_width, wmMoonClock_mask128_height);
        }
        break;
        
    case 1:
    default:
        if (UseLowColorPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_masterLow64_xpm, wmMoonClock_mask64_bits, wmMoonClock_mask64_width, wmMoonClock_mask64_height);
        } else if (UseHQPixmap) {
            da_open_xwindow(argc, argv, wmMoonClock_master_hq64_xpm, wmMoonClock_mask64_bits, wmMoonClock_mask64_width, wmMoonClock_mask64_height);
        } else {
            da_open_xwindow(argc, argv, wmMoonClock_master64_xpm, wmMoonClock_mask64_bits, wmMoonClock_mask64_width, wmMoonClock_mask64_height);
        }
        break;
    }

    /*
     *  Loop until we die
     */
    n = 32000;
    
    while (!da_conf_changed(argc, argv)) {
        if (Flag) {
            n = 32000;
            Flag = 0;
        }

        /*
         *  The Moon Ephemeris calculations are somewhat costly (the Moon is one of the most
         *  difficult objects to compute position for). So only process every nMAXth cycle of this
         *  loop. We run outer loop it faster to catch expose events, button presses, etc...
         *
         */
        if (n > nMAX) {
            n = 0;

            CurrentGMTTime = time(CurrentTime); GMTTime = gmtime(&CurrentGMTTime);
            UT = GMTTime->tm_hour + GMTTime->tm_min/60.0 + GMTTime->tm_sec/3600.0;
            Year = GMTTime->tm_year+1900;
            Month = GMTTime->tm_mon+1;
            DayOfMonth = GMTTime->tm_mday;
            date = Year*10000 + Month*100 + DayOfMonth;
            CurrentLocalTime = CurrentGMTTime; LocalTime = localtime(&CurrentLocalTime);
            LocalHour = LocalTime->tm_hour + LocalTime->tm_min/60.0 + LocalTime->tm_sec/3600.0;
            TimeZone = UT - LocalHour;

            CalcEphem(date, UT, &c);

            if (ToggleWindow == 0)  {
                /*
                 *  Update Moon Image
                 */
                nMAX = 1000;
                
                ImageNumber = (int)(c.MoonPhase * 60.0 + 0.5);

                if (ImageNumber > 59) ImageNumber = 0;

                if (Glat < 0) ImageNumber = 59 - ImageNumber;       /* add southern hemisphere support, closes: #537480 */

                j = ImageNumber/10;
                i = ImageNumber%10;

                da_copy_xpm_area(67+58*i, 2+58*j, 54, 54, 5, 5);

            } else if (ToggleWindow == 1)   {
                /*
                 *  Update Numerical Display
                 */
                /* This requires second precision for LT and UT */
                nMAX = 0;

                /*
                 * Clear plotting area
                 */
                da_copy_xpm_area(4, 69, 56, 56, 4, 4);

                /*
                 *  Paste up LT and UT.
                 */
                val = LocalHour;

                H = (int)val; val = (val - H)*60.0;
                M = (int)val; val = (val - M)*60.0;
                S = (int)val;

                digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25, 6);
                digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25+5, 6);
                da_copy_xpm_area(117, 353, 1, 6, 25+10, 6);
                digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25+12, 6);
                digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25+17, 6);

                val = UT;

                H = (int)val; val = (val - H)*60.0;
                M = (int)val; val = (val - M)*60.0;
                S = (int)val;

                digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25, 15);
                digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25+5, 15);
                da_copy_xpm_area(117, 353, 1, 6, 25+10, 15);
                digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25+12, 15);
                digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 25+17, 15);

                /*
                 *  Paste up AGE.
                 */
                val = c.MoonAge;

                q = (val < 10.0) ? 5 : 0;
                A = (int)val;

                val = (val - A)*100.0;

                B = (int)val;

                digit = A/10; if (digit != 0) da_copy_xpm_area(67+digit*5, 353, 5, 7, 26-q, 24);
                digit = A%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 26+5-q, 24);
                da_copy_xpm_area(62, 357, 3, 3, 26+11-q, 28);
                digit = B/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 26+15-q, 24);
                digit = B%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 26+20-q, 24);
                da_copy_xpm_area(143, 354, 3, 3, 26+25-q, 23);

                /*
                 *  Paste up Phase (Percent Illuminated).
                 */
                val = 0.5*( 1.0 - cos(c.MoonPhase*6.2831853) );
                val *= 100.0;

                A = (int)(val+0.5);

                if (A == 100){
                    da_copy_xpm_area(72, 353, 5, 7, 32+5, 42);
                    da_copy_xpm_area(67, 353, 5, 7, 32+10, 42);
                    da_copy_xpm_area(67, 353, 5, 7, 32+15, 42);
                    da_copy_xpm_area(147, 353, 5, 7, 32+20, 42);
                } else if (A >= 10){
                    digit = A/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 32+5, 42);
                    digit = A%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 32+10, 42);
                    da_copy_xpm_area(147, 353, 5, 7, 32+15, 42);
                } else {
                    digit = A; da_copy_xpm_area(67+digit*5, 353, 5, 7, 32+5, 42);
                    da_copy_xpm_area(147, 353, 5, 7, 32+10, 42);
                }

                /*
                 *  Paste up Frac (Percent of way through current lunar cycle).
                 */
                val = c.MoonPhase*100.0;

                A = (int)(val+0.5);

                if (A == 100){
                    da_copy_xpm_area(72, 353, 5, 7, 27+5, 33);
                    da_copy_xpm_area(67, 353, 5, 7, 27+10, 33);
                    da_copy_xpm_area(67, 353, 5, 7, 27+15, 33);
                    da_copy_xpm_area(147, 353, 5, 7, 27+20, 33);
                } else if (A >= 10){
                    digit = A/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 27+5, 33);
                    digit = A%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 27+10, 33);
                    da_copy_xpm_area(147, 353, 5, 7, 27+15, 33);
                } else {
                    digit = A; da_copy_xpm_area(67+digit*5, 353, 5, 7, 27+5, 33);
                    da_copy_xpm_area(147, 353, 5, 7, 27+10, 33);
                }

                /*
                 *  Paste up Visible Status.
                 */
                if (c.Visible)
                    da_copy_xpm_area(6,  327, 13, 6, 46, 51);
                else
                    da_copy_xpm_area(26, 327,  9, 6, 46, 51);

            } else if (ToggleWindow == 2) {
                /*
                 *  Plot up Moon Rise/Set Times
                 */
                nMAX = 60;

                /*
                 * Clear plotting area
                 */
                da_copy_xpm_area(4, 134, 56, 56, 4, 4);

                /*
                 *   Do Yesterday's first
                 */
                MoonRise(Year, Month, DayOfMonth-1, LocalHour, &UTRise, &UTSet);

                UTTohhmm(UTRise, &H, &M);

                if (H >= 0){
                    digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7, 19);
                    digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+5, 19);
                    da_copy_xpm_area(117, 354, 1, 4, 7+10, 20);
                    digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+12, 19);
                    digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+17, 19);
                } else {
                    da_copy_xpm_area(57, 355, 5, 1, 7, 22); da_copy_xpm_area(57, 355, 5, 1, 7+5, 22);
                    da_copy_xpm_area(117, 354, 1, 4, 7+10, 20);
                    da_copy_xpm_area(57, 355, 5, 1, 7+12, 22); da_copy_xpm_area(57, 355, 5, 1, 7+17, 22);
                }

                UTTohhmm(UTSet, &H, &M);

                if (H >= 0){
                    digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35, 19);
                    digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+5, 19);
                    da_copy_xpm_area(117, 354, 1, 4, 35+10, 20);
                    digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+12, 19);
                    digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+17, 19);
                } else {
                    da_copy_xpm_area(57, 355, 5, 1, 35, 22); da_copy_xpm_area(57, 355, 5, 1, 35+5, 22);
                    da_copy_xpm_area(117, 354, 1, 4, 35+10, 20);
                    da_copy_xpm_area(57, 355, 5, 1, 35+12, 22); da_copy_xpm_area(57, 355, 5, 1, 35+17, 22);
                }

                /*
                 *  Plot up todays Rise/Set times.
                 */
                MoonRise(Year, Month, DayOfMonth,   LocalHour, &UTRise, &UTSet);

                UTTohhmm(UTRise, &H, &M);

                if (H >= 0){
                    digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7, 29);
                    digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+5, 29);
                    da_copy_xpm_area(117, 354, 1, 4, 7+10, 30);
                    digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+12, 29);
                    digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+17, 29);
                } else {
                    da_copy_xpm_area(57, 355, 5, 1, 7, 32); da_copy_xpm_area(57, 355, 5, 1, 7+5, 32);
                    da_copy_xpm_area(117, 354, 1, 4, 7+10, 30);
                    da_copy_xpm_area(57, 355, 5, 1, 7+12, 32); da_copy_xpm_area(57, 355, 5, 1, 7+17, 32);
                }

                UTTohhmm(UTSet, &H, &M);

                if (H >= 0){
                    digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35, 29);
                    digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+5, 29);
                    da_copy_xpm_area(117, 354, 1, 4, 35+10, 30);
                    digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+12, 29);
                    digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+17, 29);
                } else {
                    da_copy_xpm_area(57, 355, 5, 1, 35, 32); da_copy_xpm_area(57, 355, 5, 1, 35+5, 32);
                    da_copy_xpm_area(117, 354, 1, 4, 35+10, 30);
                    da_copy_xpm_area(57, 355, 5, 1, 35+12, 32); da_copy_xpm_area(57, 355, 5, 1, 35+17, 32);
                }

                /*
                 *  Plot up tomorrow's Rise/Set times.
                 */
                MoonRise(Year, Month, DayOfMonth+1, LocalHour, &UTRise, &UTSet);

                UTTohhmm(UTRise, &H, &M);

                if (H >= 0){
                    digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7, 39);
                    digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+5, 39);
                    da_copy_xpm_area(117, 354, 1, 4, 7+10, 40);
                    digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+12, 39);
                    digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 7+17, 39);
                } else {
                    da_copy_xpm_area(57, 355, 5, 1, 7, 42); da_copy_xpm_area(57, 355, 5, 1, 7+5, 42);
                    da_copy_xpm_area(117, 354, 1, 4, 7+10, 40);
                    da_copy_xpm_area(57, 355, 5, 1, 7+12, 42); da_copy_xpm_area(57, 355, 5, 1, 7+17, 42);
                }

                UTTohhmm(UTSet, &H, &M);

                if (H >= 0){
                    digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35, 39);
                    digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+5, 39);
                    da_copy_xpm_area(117, 354, 1, 4, 35+10, 40);
                    digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+12, 39);
                    digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 35+17, 39);
                } else {
                    da_copy_xpm_area(57, 355, 5, 1, 35, 42); da_copy_xpm_area(57, 355, 5, 1, 35+5, 42);
                    da_copy_xpm_area(117, 354, 1, 4, 35+10, 40);
                    da_copy_xpm_area(57, 355, 5, 1, 35+12, 42); da_copy_xpm_area(57, 355, 5, 1, 35+17, 42);
                }

            } else if (ToggleWindow == 3) {
                /*
                 *  Plot up Horizon Coords
                 */
                nMAX = 3;

                /*
                 * Clear plotting area
                 */
                da_copy_xpm_area(4, 199, 56, 56, 4, 4);

                /*
                 *  Paste up Azimuth, A
                 */
                val = c.A_moon;
                sgn = (val < 0.0) ? -1 : 0;
                val = fabs(val);
                D = (int)val;
                val = (val-(double)D)*100.0;
                M = (int)val;

                if (sgn < 0) da_copy_xpm_area(120, 357, 2, 1, 19, 27);

                /* degrees 100's */
                digit = D/100; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22, 24);
                D -= digit*100;

                /* degrees 10's */
                digit = D/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22+5, 24);

                /* degrees 1's */
                digit = D%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22+10, 24);

                /* Decimal */
                da_copy_xpm_area(62, 357, 3, 3, 22+15, 28);

                /*  Decimal Part */
                digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22+19, 24);

                /*  mins 1's */
                digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22+24, 24);

                da_copy_xpm_area(120, 353, 3, 3, 22+29, 23);

                /*
                 *  Paste up Altitude, h
                 */
                val = c.h_moon;
                sgn = (val < 0.0) ? -1 : 0;
                val = fabs(val);
                D = (int)val;
                val = (val-(double)D)*100.0;
                M = (int)val;

                if (sgn < 0) da_copy_xpm_area(120, 357, 2, 1, 19, 39);

                /* degrees 10's */
                digit = D/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22, 36);

                /* degrees 1's */
                digit = D%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22+5, 36);

                /* Decimal */
                da_copy_xpm_area(62, 357, 3, 3, 22+10, 40);

                /*  Decimal Part */
                digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22+14, 36);

                /*  mins 1's */
                digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 22+19, 36);

                da_copy_xpm_area(120, 353, 3, 3, 22+24, 35);

                /*
                 *  Paste up Earth-Moon Distance (in units of Earth radii).
                 */
                val = c.EarthMoonDistance;
                A = (int)val;
                val = (val - A)*100.0;
                B = (int)val;
                digit = A/10; if (digit != 0) da_copy_xpm_area(67+digit*5, 353, 5, 7, 30, 47);
                digit = A%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 30+5, 47);
                da_copy_xpm_area(62, 357, 3, 3, 30+11, 51);
                digit = B/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 30+15, 47);
                digit = B%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 30+20, 47);

            } else if (ToggleWindow == 4) {
                /*
                 *  Plot up RA/DEC Coords
                 */
                nMAX = 3;

                /*
                 * Clear plotting area
                 */
                da_copy_xpm_area(4, 264, 56, 56, 4, 4);

                /*
                 *  Paste up Right Ascention
                 */
                RA = c.RA_moon/15.0;
                H = (int)RA;
                RA = (RA-(double)H)*60.0;
                M = (int)RA; RA = (RA-(double)M)*60.0;
                S = (int)(RA+0.5);

                /* hours 10's */
                digit = H/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17, 25);

                /* hours 1's */
                digit = H%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+5, 25);

                /* hour symbol */
                da_copy_xpm_area(138, 354, 3, 3, 17+10, 24);

                /*  mins 10's */
                digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+14, 25);

                /*  mins 1's */
                digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+19, 25);

                /* min symbol */
                da_copy_xpm_area(124, 353, 3, 3, 17+23, 24);

                /*  secs 10's */
                digit = S/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+27, 25);

                /*  secs 1's */
                digit = S%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+32, 25);

                /* sec symbol */
                da_copy_xpm_area(128, 353, 5, 3, 17+36, 24);

                /*
                 *  Paste up Declination
                 */
                DEC = c.DEC_moon;
                sgn = (DEC < 0.0) ? -1 : 0;
                DEC = fabs(DEC);
                D = (int)DEC;
                DEC = (DEC-(double)D)*60.0;
                M = (int)DEC;
                DEC = (DEC-(double)M)*60.0;
                S = (int)(DEC+0.5);

                if (sgn < 0) da_copy_xpm_area(120, 357, 2, 1, 14, 39);

                /* degrees 10's */
                digit = D/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17, 36);

                /* degrees 1's */
                digit = D%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+5, 36);

                /* degree symbol */
                da_copy_xpm_area(120, 353, 3, 3, 17+10, 35);

                /*  mins 10's */
                digit = M/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+14, 36);

                /*  mins 1's */
                digit = M%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+19, 36);

                /* min symbol */
                da_copy_xpm_area(124, 353, 3, 3, 17+23, 35);

                /*  secs 10's */
                digit = S/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+27, 36);

                /*  secs 1's */
                digit = S%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 17+32, 36);

                /* sec symbol */
                da_copy_xpm_area(128, 353, 5, 3, 17+36, 35);

                /*
                 *  Paste up Earth-Moon Distance (in units of Earth radii).
                 */
                val = c.EarthMoonDistance;
                A = (int)val;
                val = (val - A)*100.0;
                B = (int)val;
                digit = A/10; if (digit != 0) da_copy_xpm_area(67+digit*5, 353, 5, 7, 30, 47);
                digit = A%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 30+5, 47);
                da_copy_xpm_area(62, 357, 3, 3, 30+11, 51);
                digit = B/10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 30+15, 47);
                digit = B%10; da_copy_xpm_area(67+digit*5, 353, 5, 7, 30+20, 47);
            }

        } else {
            /*
             *  Update the counter.
             */
            ++n;
        }

        /*
         *  Add X display to file descriptor set for polling.
         */
        FD_ZERO(&xfdset);
        FD_SET(ConnectionNumber(da_display), &xfdset);

        /*
         *   Process any pending X events.
         */
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            ++ToggleWindow;
   
            if (ToggleWindow > 4)
                ToggleWindow = 0;
        
            Flag = 1;

            break;
        }

        /*
         *  Redraw and wait for next update
         */
        da_redraw_window();

        timeout.tv_sec = DELAY / 1000000L;
        timeout.tv_usec = DELAY % 1000000L;

        select(ConnectionNumber(da_display) + 1, &xfdset, NULL, NULL, &timeout);
    }

    routine(argc, argv);
}
