#!/bin/bash

CC=gcc
CC_MAIN_OPTS=(-O2 -Wall -Wextra -lXpm -lX11 -lXext -ldockapp4)
CC_ADD_OPTS=(-lm)
C_FILES=(*.c)

PROG_NAME=none
PROG_VERSION=0.0

TARGET_DIR=/usr/local/bin

INSTALL=false
UNINSTALL=false

SHOW_HELP=false

# Read vars from config.h
PREV=none
if [ ! -f config.h ]; then
    touch config.h;
fi
while read LINE; do
    for WORD in $LINE; do
        if [[ $PREV == "PROG_NAME" ]]; then
            PROG_NAME=${WORD//\"};
        elif [[ $PREV == "PROG_VERSION" ]]; then
            PROG_VERSION=${WORD//\"};
        fi
        PREV=$WORD;
    done
done < config.h

# Parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -c|--compiler)
            CC="$2"
            shift # past argument
            shift # past value
            ;;
        -n|--name)
            PROG_NAME="$2"
            shift # past argument
            shift # past value
            ;;
        -v|--version)
            PROG_VERSION="$2"
            shift # past argument
            shift # past value
            ;;
        -d|--directory)
            TARGET_DIR="$2"
            shift # past argument
            shift # past value
            ;;
        -i|--install)
            INSTALL=true
            shift # past argument
            ;;
        -u|--uninstall)
            UNINSTALL=true
            shift # past argument
            ;;
        -h|--help)
            SHOW_HELP=true
            shift # past argument
            ;;
        *)  # unknown option
            shift # past argument
            ;;
    esac
done

# Show help
if [[ $SHOW_HELP == true ]]; then
    echo "${PROG_NAME} ${PROG_VERSION} building and installation manual";
    echo "options:";
    echo "    -c --compiler  <compiler>     Set the compiler (default: gcc)";
    echo "    -n --name      <name>         Override program name (default: set in config.h)";
    echo "    -v --version   <version>      Override program version (default: set in config.h)";
    echo "    -d --directory <directory>    With -i or -u, set target directory (default: /usr/local/bin)";
    echo "    -i --install                  Perform installation";
    echo "    -u --uninstall                Perform uninstallation";
    echo "    -h --help                     Show this help";
    exit;
fi

# Install
if [[ $INSTALL == true ]]; then
    echo "Installing to ${TARGET_DIR}...";
    cp $PROG_NAME $TARGET_DIR;
    if [ $? -eq 0 ]
    then
        echo "Done.";
    else
        echo "Failed!";
    fi
    exit;
fi

# Uninstall
if [[ $UNINSTALL == true ]]; then
    echo "Uninstalling from ${TARGET_DIR}...";
    cd $TARGET_DIR;
    rm --interactive=never $PROG_NAME;
    if [ $? -eq 0 ]
    then
        echo "Done.";
    else
        echo "Failed!";
    fi
    exit;
fi

# Write vars to config.h
echo "/* GENERATED FILE -- MANUAL EDITS MAY BE LOST */" > config.h;
echo "#define PROG_NAME \"${PROG_NAME}\"" >> config.h;
echo "#define PROG_VERSION \"${PROG_VERSION}\"" >> config.h;

# Build
echo "Building ${PROG_NAME} ${PROG_VERSION} with ${CC}...";
$CC ${C_FILES[@]} ${CC_MAIN_OPTS[@]} ${CC_ADD_OPTS[@]} -o $PROG_NAME;
if [ $? -eq 0 ]
then
    echo "Done.";
else
    echo "Failed!";
fi
