/*
 * Copyright (c) 1999 Alfredo K. Kojima
 * Copyright (c) 2001, 2002 Seiichi SATO <ssato@sh.rim.or.jp>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 * This code is based on libdockapp-0.4.0
 * modified by Seiichi SATO <ssato@sh.rim.or.jp>
 */

#include "dockapp.h"

#include "bitmaps/mask_lcd.xbm"
#include "bitmaps/mask_lcd2.xbm"
#include "bitmaps/mask_lcd3.xbm"
#include "bitmaps/mask_lcd4.xbm"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"
#include "bitmaps/backlight_on1.xpm"
#include "bitmaps/backlight_on2.xpm"
#include "bitmaps/backlight_on3.xpm"
#include "bitmaps/backlight_on4.xpm"
#include "bitmaps/backlight_off1.xpm"
#include "bitmaps/backlight_off2.xpm"
#include "bitmaps/backlight_off3.xpm"
#include "bitmaps/backlight_off4.xpm"
#include "bitmaps/parts1.xpm"
#include "bitmaps/parts2.xpm"
#include "bitmaps/parts3.xpm"
#include "bitmaps/parts4.xpm"

void dockapp_init_ui(int argc, char *argv[]) {
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, mask_lcd4_bits, mask_lcd4_width, mask_lcd4_height);
        
        if (!dockapp_xpm_to_pixmap(backlight_on4_xpm, &backdrop_on, &backdrop_on_mask, da_colors)) {
            fprintf(stderr, "Error initializing backlit background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(backlight_off4_xpm, &backdrop_off, &backdrop_off_mask, da_colors)) {
            fprintf(stderr, "Error initializing background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(parts4_xpm, &parts, NULL, da_colors)) {
            fprintf(stderr, "Error initializing parts image.\n");
            exit(1);
        }
        break;
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, mask_lcd3_bits, mask_lcd3_width, mask_lcd3_height);
        
        if (!dockapp_xpm_to_pixmap(backlight_on3_xpm, &backdrop_on, &backdrop_on_mask, da_colors)) {
            fprintf(stderr, "Error initializing backlit background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(backlight_off3_xpm, &backdrop_off, &backdrop_off_mask, da_colors)) {
            fprintf(stderr, "Error initializing background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(parts3_xpm, &parts, NULL, da_colors)) {
            fprintf(stderr, "Error initializing parts image.\n");
            exit(1);
        }
        break;
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, mask_lcd2_bits, mask_lcd2_width, mask_lcd2_height);
        
        if (!dockapp_xpm_to_pixmap(backlight_on2_xpm, &backdrop_on, &backdrop_on_mask, da_colors)) {
            fprintf(stderr, "Error initializing backlit background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(backlight_off2_xpm, &backdrop_off, &backdrop_off_mask, da_colors)) {
            fprintf(stderr, "Error initializing background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(parts2_xpm, &parts, NULL, da_colors)) {
            fprintf(stderr, "Error initializing parts image.\n");
            exit(1);
        }
        break;
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, mask_lcd_bits, mask_lcd_width, mask_lcd_height);
        
        if (!dockapp_xpm_to_pixmap(backlight_on1_xpm, &backdrop_on, &backdrop_on_mask, da_colors)) {
            fprintf(stderr, "Error initializing backlit background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(backlight_off1_xpm, &backdrop_off, &backdrop_off_mask, da_colors)) {
            fprintf(stderr, "Error initializing background image.\n");
            exit(1);
        }
        if (!dockapp_xpm_to_pixmap(parts1_xpm, &parts, NULL, da_colors)) {
            fprintf(stderr, "Error initializing parts image.\n");
            exit(1);
        }
        break;
    }
}

bool dockapp_xpm_to_pixmap(char **data, Pixmap *pixmap, Pixmap *mask, XpmColorSymbol *colorSymbol) {
    XpmAttributes xpmAttr;
    xpmAttr.valuemask = XpmCloseness;
    xpmAttr.closeness = 40000;

    if (colorSymbol) {
        xpmAttr.colorsymbols = colorSymbol;
        xpmAttr.numsymbols = DA_MAX_COLORS;
        xpmAttr.valuemask |= XpmColorSymbols;
    }

    if (XpmCreatePixmapFromData(da_display, da_windowed ? da_active_win : da_icon_win, data, pixmap, mask, &xpmAttr) != 0)
        return false;

    return true;
}

void dockapp_cleanup() {
    if (backdrop_off_mask) XFreePixmap(da_display, backdrop_off_mask);
    if (backdrop_on)       XFreePixmap(da_display, backdrop_on);
    if (backdrop_off)      XFreePixmap(da_display, backdrop_off);
    if (parts)             XFreePixmap(da_display, parts);
    if (backdrop_on_mask)  XFreePixmap(da_display, backdrop_on_mask);
}

bool dockapp_nextevent_or_timeout(XEvent *event, unsigned long miliseconds) {
    struct timeval timeout;

    fd_set rset;

    if (!delete_win)
        delete_win = XInternAtom(da_display, "WM_DELETE_WINDOW", false);

    XSync(da_display, false);

    if (XPending(da_display)) {
        XNextEvent(da_display, event);

        return true;
    }

    timeout.tv_sec = miliseconds / 1000;
    timeout.tv_usec = (miliseconds % 1000) * 1000;

    FD_ZERO(&rset);
    FD_SET(ConnectionNumber(da_display), &rset);

    if (select(ConnectionNumber(da_display)+1, &rset, NULL, NULL, &timeout) > 0) {
        XNextEvent(da_display, event);

        if (event->type == ClientMessage) {
            if (event->xclient.data.l[0] == (long int)delete_win) {
                XDestroyWindow(da_display, event->xclient.window);

                XCloseDisplay(da_display);

                exit(0);
            }
        }
	
        return true;
    }

    return false;
}

void dockapp_copy_area(Pixmap src, int x_src, int y_src, int w, int h, int x_dist, int y_dist) {
    da_copy_from_xpm(src, x_src, y_src, w, h, x_dist + 3, y_dist + 3);
}

/*void
dockapp_set_eventmask(long mask)
{
    XSelectInput(display, icon_window, mask);
    XSelectInput(display, window, mask);
}

static Pixmap
create_bg_pixmap(void)
{
    Pixmap bg;

    bg = XCreatePixmap(display, icon_window, WINDOWED_SIZE_W, WINDOWED_SIZE_H,
		       depth);
    XSetForeground(display, gc, dockapp_getcolor("rgb:ae/aa/ae"));
    XFillRectangle(display, bg, gc, 0, 0, WINDOWED_SIZE_W, WINDOWED_SIZE_H);
    XSetForeground(display, gc, dockapp_getcolor("rgb:ff/ff/ff"));
    XDrawLine(display, bg, gc, 0, 0, 0, 63);
    XDrawLine(display, bg, gc, 1, 0, 1, 62);
    XDrawLine(display, bg, gc, 2, 0, 63, 0);
    XDrawLine(display, bg, gc, 2, 1, 62, 1);
    XSetForeground(display, gc, dockapp_getcolor("rgb:52/55/52"));
    XDrawLine(display, bg, gc, 1, 63, 63, 63);
    XDrawLine(display, bg, gc, 2, 62, 63, 62);
    XDrawLine(display, bg, gc, 63, 1, 63, 61);
    XDrawLine(display, bg, gc, 62, 2, 62, 61);

    return bg;
}


void
dockapp_set_background(Pixmap pixmap)
{
    if (dockapp_iswindowed) {
	Pixmap bg;
	bg = create_bg_pixmap();
	XCopyArea(display, pixmap, bg, gc, 0, 0, width, height,
		  offset_w, offset_w);
	XSetWindowBackgroundPixmap(display, icon_window, bg);
	XSetWindowBackgroundPixmap(display, window, bg);
	XFreePixmap(display, bg);
    } else {
	XSetWindowBackgroundPixmap(display, icon_window, pixmap);
	XSetWindowBackgroundPixmap(display, window, pixmap);
    }
    XClearWindow(display, icon_window);
    XFlush(display);
}


void
dockapp_show(void)
{
    if (!dockapp_iswindowed)
	XMapRaised(display, window);
    else
	XMapRaised(display, icon_window);

    XFlush(display);
}





Pixmap
dockapp_XCreatePixmap(int w, int h)
{
    return (XCreatePixmap(display, icon_window, w, h, depth));
}


void
dockapp_setshape(Pixmap mask, int x_ofs, int y_ofs)
{
    XShapeCombineMask(display, icon_window, ShapeBounding, -x_ofs, -y_ofs,
		      mask, ShapeSet);
    XShapeCombineMask(display, window, ShapeBounding, -x_ofs, -y_ofs,
		      mask, ShapeSet);
    XFlush(display);
}





void
dockapp_copy2window (Pixmap src)
{
    if (dockapp_isbrokenwm) {
	XCopyArea(display, src, window, gc, 0, 0, width, height, offset_w,
		  offset_h);
    } else {
	XCopyArea(display, src, icon_window, gc, 0, 0, width, height, offset_w,
		  offset_h);
    }
}





unsigned long
dockapp_getcolor(char *color_name)
{
    XColor color;

    if (!XParseColor(display, DefaultColormap(display, DefaultScreen(display)),
		     color_name, &color))
	fprintf(stderr, "can't parse color %s\n", color_name), exit(1);

    if (!XAllocColor(display, DefaultColormap(display, DefaultScreen(display)),
		     &color)) {
	fprintf(stderr, "can't allocate color %s. Using black\n", color_name);
	return BlackPixel(display, DefaultScreen(display));
    }

    return color.pixel;
}


unsigned long
dockapp_blendedcolor(char *color_name, int r, int g, int b, float fac)
{
    XColor color;

    if ((r < -255 || r > 255)||(g < -255 || g > 255)||(b < -255 || b > 255)){
	fprintf(stderr, "r:%d,g:%d,b:%d (r,g,b must be 0 to 255)", r, g, b);
	exit(1);
    }

    r *= 255;
    g *= 255;
    b *= 255;

    if (!XParseColor(display, DefaultColormap(display, DefaultScreen(display)),
		     color_name, &color))
	fprintf(stderr, "can't parse color %s\n", color_name), exit(1);

    if (!XAllocColor(display, DefaultColormap(display, DefaultScreen(display)),
		     &color)) {
	fprintf(stderr, "can't allocate color %s. Using black\n", color_name);
	return BlackPixel(display, DefaultScreen(display));
    }

    if (DefaultDepth(display, DefaultScreen(display)) < 16)
	return color.pixel;

    // red
    if (color.red + r > 0xffff) {
	color.red = 0xffff;
    } else if (color.red + r < 0) {
	color.red = 0;
    } else {
	color.red = (unsigned short)(fac * color.red + r);
    }

    // green
    if (color.green + g > 0xffff) {
	color.green = 0xffff;
    } else if (color.green + g < 0) {
	color.green = 0;
    } else {
	color.green = (unsigned short)(fac * color.green + g);
    }

    // blue
    if (color.blue + b > 0xffff) {
	color.blue = 0xffff;
    } else if (color.blue + b < 0) {
	color.blue = 0;
    } else {
	color.blue = (unsigned short)(fac * color.blue + b);
    }

    color.flags = DoRed | DoGreen | DoBlue;

    if (!XAllocColor(display, DefaultColormap(display, DefaultScreen(display)),
		     &color)) {
	fprintf(stderr, "can't allocate color %s. Using black\n", color_name);
	return BlackPixel(display, DefaultScreen(display));
    }

    return color.pixel;
}*/
