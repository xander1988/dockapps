#!/bin/bash

CC=gcc
CCOPTS=(-O2 -Wall -Wextra -Wpedantic -lXpm -lX11 -lXext -ldockapp4)
CFILES=(main.c dockapp.c)

PROG_NAME=wmbatteries
PROG_VERSION=0.0

INSTALL=false
UNINSTALL=false

# Read PROG_VERSION from config.h
PREV=none
while read LINE; do
    for WORD in $LINE; do
        if [[ $PREV == "PROG_VERSION" ]]; then
            PROG_VERSION=${WORD//\"};
        fi
        PREV=$WORD;
    done
done < config.h

# Parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -c|--compiler)
            CC="$2"
            shift # past argument
            shift # past value
            ;;
        -n|--name)
            PROG_NAME="$2"
            shift # past argument
            shift # past value
            ;;
        -v|--version)
            PROG_VERSION="$2"
            shift # past argument
            shift # past value
            ;;
        -i|--install)
            INSTALL=true
            shift # past argument
            ;;
        -u|--uninstall)
            UNINSTALL=true
            shift # past argument
            ;;
        *)  # unknown option
            shift # past argument
            ;;
    esac
done

# Install
if [[ $INSTALL == true ]]; then
    echo "Installing to /usr/local/bin/${PROG_NAME}...";
    cp $PROG_NAME /usr/local/bin/$PROG_NAME;
    echo "Done.";
    exit;
fi

# Uninstall
if [[ $UNINSTALL == true ]]; then
    echo "Uninstalling /usr/local/bin/${PROG_NAME}...";
    rm --interactive=never /usr/local/bin/$PROG_NAME;
    echo "Done.";
    exit;
fi

# Write vars to config.h
echo "#define PROG_NAME \"${PROG_NAME}\"" > config.h;
echo "#define PROG_VERSION \"${PROG_VERSION}\"" >> config.h;

# Build
echo "Building ${PROG_NAME} ${PROG_VERSION} with ${CC}...";
$CC ${CFILES[@]} ${CCOPTS[@]} -o $PROG_NAME;
echo "Done.";
