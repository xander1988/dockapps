// wmamixer - An ALSA mixer designed for WindowMaker with scrollwheel support
// Copyright (C) 2015  Roman Dobosz <gryf@vimja.com>
// Copyright (C) 2003  Damian Kramer <psiren@hibernaculum.net>
// Copyright (C) 1998  Sam Hawker <shawkie@geocities.com>
//
// This software comes with ABSOLUTELY NO WARRANTY
// This software is free software, and you are welcome to redistribute it
// under certain conditions
// See the README file for a more complete notice.

#include <libdockapp4/dockapp.h>
#include <assert.h>
#include <math.h>
#include <alsa/asoundlib.h>

#include "config.h"
#include "XPM/mask.xbm"
#include "XPM/mask2.xbm"
#include "XPM/mask3.xbm"
#include "XPM/mask4.xbm"
#include "XPM/wmamixer.xpm"
#include "XPM/wmamixer2.xpm"
#include "XPM/wmamixer3.xpm"
#include "XPM/wmamixer4.xpm"

#undef CLAMP
#define CLAMP(v, l, h) (((v) > (h)) ? (h) : (((v) < (l)) ? (l) : (v)))

/* Function to convert from percentage to volume. val = percentage */
#define convert_prange1(val, min, max) \
    ceil((val) * ((max) - (min)) * 0.01 + (min))

struct Selem {
    char *name;
    bool stereo;
    int currentVolRight;
    int currentVolLeft;
    short int iconIndex;
    snd_mixer_elem_t *elem;
    long min;
    long max;
    bool capture;
    snd_mixer_selem_channel_id_t channels[2];
};

typedef struct {
    bool isvolume;
    bool capture;
    bool mono;
} slideCaptureMono;

struct NamesCount {
    short int pcm, line, lineb, mic, micb, capt, vol, aux;
} namesCount = {1, 1, 1, 1, 1, 1, 1, 1};

struct Selem *selems[32] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

struct Mixer {
    int devices_no;
    snd_mixer_t * handle;
};

struct Mixer *mix;

static struct snd_mixer_selem_regopt smixer_options;

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *card;

bool no_volume_display;

int text_counter = 0;
// Current state information
int curchannel = 0;
int curleft;
int curright;
// For buttons
int btnstate = 0;
#define BTNNEXT 1
#define BTNPREV 2
// For repeating next and prev buttons
#define RPTINTERVAL 5
int rpttimer = 0;

static int smixer_level = 0;

// For draggable volume control
bool dragging = false;

int channel[32];

void routine(int argc, char *argv[]); 
void checkVol(bool forced);
void drawBtn(int x, int y, int w, int h, bool down);
void drawBtns(int btns);
void drawStereo(bool left);
void drawMono();
void drawText(char *text);
void drawVolLevel();
void update();
struct Mixer *Mixer_create();
void Mixer_destroy(struct Mixer *mix);
slideCaptureMono Mixer_get_capabilities(snd_mixer_elem_t *elem);
int Mixer_get_volume(int current, int channelIndex);
int Mixer_read_left(int current);
int Mixer_read_right(int current);
void Mixer_set_selem_props(struct Selem *selem, const char *name);
void Mixer_set_channels(struct Selem *selem);
void Mixer_set_left(int current, int value);
void Mixer_set_right(int current, int value);
void Mixer_set_limits(snd_mixer_elem_t *elem, struct Selem *selem);
void Mixer_set_volume(int current, int channelIndex, int value);
void Selem_set_name(struct Selem *selem, const char *name, short int *count);
void Selem_destroy();

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a fork of wmsmixer and is an ALSA mixer dockapp for Window Maker.\n\n"

    "Changes with comparison with wmsmixer: ALSA instead of OSS. This is the real thing, using alsa-lib, not just emulation of OSS. The code for the ALSA part was taken and adapted from amixer and alsamixer programs from alsa-utils package.\n\n"

    "By default, first device, which is called \"default\" is selected. That device is taken by pulseaudio nowadays, so you'll see only two controls: one for master volume and the other for capture volume. To be able to change all supported by soundcard controls, you need to pass right PCM device to this option. First, you need to know which device needs to be passed as an argument to the \"-d\" option. To list ALSA devices you might use the \"aplay\" program from alsa-utils:\n\n"

    "user@linux ~ $ aplay -l\n\n"

    "**** List of PLAYBACK Hardware Devices ****\n"
    " card 0: HDMI [HDA Intel HDMI], device 3: HDMI 0 [HDMI 0]\n"
    "  Subdevices: 1/1\n"   
    "  Subdevice #0: subdevice #0\n"   
    " card 0: HDMI [HDA Intel HDMI], device 7: HDMI 1 [HDMI 1]\n" 
    "  Subdevices: 1/1\n"   
    "  Subdevice #0: subdevice #0\n"   
    " card 0: HDMI [HDA Intel HDMI], device 8: HDMI 2 [HDMI 2]\n" 
    "  Subdevices: 1/1\n"   
    "  Subdevice #0: subdevice #0\n"   
    " card 1: PCH [HDA Intel PCH], device 0: 92HD91BXX Analog [92HD91BXX Analog]\n" 
    "  Subdevices: 0/1\n"   
    "  Subdevice #0: subdevice #0\n\n"

    "So in above example there are two sound devices, which one is HDMI and the other PCH. To select PCH device, it is enough to invoke "PROG_NAME" like:\n\n"
  
    "user@linux ~ $ "PROG_NAME" -D \"hw:1\"\n\n"

    "where \"hw:1\" means the second card (you can see it within the line \"card 1: PCH [HDA Intel PCH], device 0: 92HD91BXX Analog [92HD91BXX Analog]\" on \"aplay -l\" output in the example above).\n\n"

    "User interface is straightforward and shows volume level indicator of currently selected mixer, and icon for currently selected mixer. If clicked, will show abbreviated mixer name for a short time on volume level indicator. It cycles through available mixers. Clicking on the volume bar between the left and right channels will set same volume level for both of them. Mouse scroll will adjust volume for both channels at the same time. If clicked on left or right bar, volume will be adjusted for selected mixer left or right channel accordingly. Note, that not all mixers have ability to adjust volume for each channels separately. Mouse scroll will adjust volume for both channels.\n\n"

    "Limitations:\n\n"

    "Currently, "PROG_NAME" does not support switches and enum type of controls. Only volume is supported. Enum and switch based ALSA controls are simply ignored.\n\n"

    "There are controls with really small limit range, for example here is control Beep (pc speaker in other words) which is represented by amixer like this:\n\n"

    "Simple mixer control 'Beep',0\n"
    "Capabilities: pvolume pvolume-joined pswitch pswitch-joined\n"
    "Playback channels: Mono\n"
    "Limits: Playback 0 - 3\n"
    "Mono: Playback 1 [33%] [-12.00dB] [on]\n\n"

    "Under \"Limits\" section, there is a Playback capability with range 0 - 3. Using scrollwheel on such low ranges is somehow awkward. For that controls it's better to use clicking instead of scrolling.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&card, "device", "-D", "--device", "default", "Use specified device  (rather than \"default\")");
    da_init_bool(&no_volume_display, "novol", "-n", "--novol", false, "Do not display volume, show channel names instead");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                      /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);
    
    return 0;
}

void routine(int argc, char *argv[]) {
    if (strlen(card) > 255) {
        fprintf(stderr, "%s: Illegal device name.\n", da_name);
        da_log(ERR, "illegal device name.\n");

        exit(1);
    }
    
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    da_add_mouse_region(0,  5, 19, 31, 43); /* channel symb */
    da_add_mouse_region(1, 36,  5, 59, 59); /* volume slider */
    da_add_mouse_region(2,  5, 47, 18, 59); /* prev button */
    da_add_mouse_region(3, 19, 47, 31, 59); /* next button */
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, wmamixer4_xpm, mask4_bits, mask4_width, mask4_height);
        break;

    case 3:
        da_open_xwindow(argc, argv, wmamixer3_xpm, mask3_bits, mask3_width, mask3_height);
        break;

    case 2:
        da_open_xwindow(argc, argv, wmamixer2_xpm, mask2_bits, mask2_width, mask2_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, wmamixer_xpm, mask_bits, mask_width, mask_height);
        break;
    }
    
    da_copy_xpm_area(68, 4, 28, 12, 4, 4);
    da_copy_xpm_area(68, 18, 28, 26, 4, 18);
    da_copy_xpm_area(68, 46, 28, 14, 4, 46);
    da_copy_xpm_area(99, 4, 25, 56, 35, 4);

    mix = Mixer_create();

    if (mix->devices_no == 0) {
        fprintf(stderr, "%s: Sorry, no supported channels found.\n", da_name);
        da_log(ERR, "no supported channels found.\n");
        
    } else {
        checkVol(true);

        while (!da_conf_changed(argc, argv)) {
            switch (da_watch_xevent()) {
            case MOUSE_1_PRS:
                if (da_check_mouse_region() == 0) {
                    drawText(selems[curchannel]->name);
                
                } else if (da_check_mouse_region() == 1) {
                    dragging = true;
                    if (da_xevent.xbutton.x <= 50)
                        Mixer_set_left(curchannel, ((60 - da_xevent.xbutton.y) * 100) / (2 * 26));
                    if (da_xevent.xbutton.x >= 45)
                        Mixer_set_right(curchannel, ((60 - da_xevent.xbutton.y) * 100) / (2 * 26));
                    checkVol(false);
                
                } else if (da_check_mouse_region() == 2) {
                    curchannel--;
                    if (curchannel < 0)
                        curchannel = mix->devices_no -1;
                    btnstate |= BTNPREV;
                    rpttimer = 0;
                    drawBtns(BTNPREV);
                    checkVol(true);
        
                } else if (da_check_mouse_region() == 3) {
                    curchannel++;
                    if (curchannel >= mix->devices_no)
                        curchannel = 0;
                    btnstate|=BTNNEXT;
                    rpttimer = 0;
                    drawBtns(BTNNEXT);
                    checkVol(true);
                }
                da_redraw_window();
                break;
            
            case MOUSE_1_REL:
                dragging = false;
                btnstate &= ~(BTNPREV | BTNNEXT);
                drawBtns(BTNPREV | BTNNEXT);
                da_redraw_window();
                break;
            
            case MOUSE_4:
                Mixer_set_left(curchannel, CLAMP(Mixer_read_left(curchannel) + 4, 0, 100));
                checkVol(false);
                da_redraw_window();
                break;
        
            case MOUSE_5:    
                Mixer_set_right(curchannel, CLAMP(Mixer_read_right(curchannel) - 4, 0, 100));
                checkVol(false);
                da_redraw_window();
                break;
            }

            if (btnstate & (BTNPREV | BTNNEXT)) {
                rpttimer++;
                
                if (rpttimer >= RPTINTERVAL) {
                    if (btnstate & BTNNEXT)
                        curchannel++;
                    else
                        curchannel--;
                    
                    if (curchannel < 0)
                        curchannel = mix->devices_no - 1;
                    if (curchannel >= mix->devices_no)
                        curchannel = 0;
                        
                    checkVol(true);
                    
                    rpttimer = 0;
                }
                
            } else {
                checkVol(false);
            }

            if (text_counter) {
                text_counter--;
                
                if (!text_counter) {
                    drawVolLevel();
                    
                    da_redraw_window();
                }
            }

            da_redraw_window();
            
            snd_mixer_handle_events(mix->handle);
            
            usleep(50000);
        }
    }

    Selem_destroy();
    Mixer_destroy(mix);
    
    routine(argc, argv);
}

/* Mixer struct support functions */
struct Mixer *Mixer_create() {
    struct Mixer *mixer = malloc(sizeof(struct Mixer));
    int err;
    const char *name;
    slideCaptureMono capabilities;

    snd_mixer_selem_id_t *sid;
    snd_mixer_elem_t *elem;
    snd_mixer_selem_id_alloca(&sid);

    assert(mixer != NULL);
    mixer->devices_no = 0;

    if ((err = snd_mixer_open(&mixer->handle, 0)) < 0) {
        fprintf(stderr, "Mixer %s open error: %s", card, snd_strerror(err));
        exit(err);
    }

    if (smixer_level == 0 &&
            (err = snd_mixer_attach(mixer->handle, card)) < 0) {
        fprintf(stderr, "Mixer attach %s error: %s", card, snd_strerror(err));
        snd_mixer_close(mixer->handle);
        exit(err);
    }

    if ((err = snd_mixer_selem_register(mixer->handle,
                    smixer_level > 0 ? &smixer_options : NULL, NULL)) < 0) {
        fprintf(stderr, "Mixer register error: %s", snd_strerror(err));
        snd_mixer_close(mixer->handle);
        exit(err);
    }

    err = snd_mixer_load(mixer->handle);

    if (err < 0) {
        fprintf(stderr, "Mixer %s load error: %s", card, snd_strerror(err));
        snd_mixer_close(mixer->handle);
        exit(err);
    }
    for (elem = snd_mixer_first_elem(mixer->handle); elem;
            elem = snd_mixer_elem_next(elem)) {
        snd_mixer_selem_get_id(elem, sid);

        if (!snd_mixer_selem_is_active(elem))
            continue;

        capabilities = Mixer_get_capabilities(elem);

        if (!capabilities.isvolume)
            continue;
        name = snd_mixer_selem_id_get_name(sid);

        selems[mixer->devices_no] = malloc(sizeof(struct Selem));
        selems[mixer->devices_no]->elem = elem;
        selems[mixer->devices_no]->capture = capabilities.capture;

        Mixer_set_limits(elem, selems[mixer->devices_no]);
        Mixer_set_selem_props(selems[mixer->devices_no], name);
        Mixer_set_channels(selems[mixer->devices_no]);

        mixer->devices_no++;
        if (mixer->devices_no == 32) {
            // stop here. This is ridiculous anyway
            break;
        }
    }
    return mixer;
}

void Mixer_set_selem_props(struct Selem *selem, const char *name) {
    const char *pcm = "PCM",
          *mic = "Mic",
          *cap = "Capture",
          *boost = "Boost",
          *line = "Line",
          *aux = "Aux";

    if (strcmp("Master", name) == 0) {
        selem->name = "mstr";
        selem->iconIndex = 0;
    } else if (strstr(pcm, name)) {
        selem->iconIndex = 1;
        if (strcmp(pcm, name) == 0)
            selem->name = "pcm ";
        else
            Selem_set_name(selem, "pcm", &namesCount.pcm);

    } else if (strstr("Headphone", name)) {
        selem->name = "hdph";
        selem->iconIndex = 8;
    } else if (strstr("Beep", name) || strstr("PC Speaker", name)) {
        selem->name = "beep";
        selem->iconIndex = 7;
    } else if (strstr("Digital", name)) {
        selem->iconIndex = selem->capture ? 3 : 15;
        selem->name = "digt";
    } else if (strstr("Bass", name)) {
        selem->name = "bass";
        selem->iconIndex = 9;
    } else if (strstr("Treble", name)) {
        selem->name = "trbl";
        selem->iconIndex = 10;
    } else if (strstr("Synth", name)) {
        selem->name = "synt";
        selem->iconIndex = 11;
    } else if (strstr("CD", name)) {
        selem->name = " cd ";
        selem->iconIndex = 12;
    } else if (strstr("Phone", name)) {
        selem->name = "phne";
        selem->iconIndex = 13;
    } else if (strstr("Video", name)) {
        selem->name = "vdeo";
        selem->iconIndex = 14;
    } else if (strstr(mic, name)) {
        if (strcmp(mic, name) == 0) {
            selem->name = "mic ";
        } else {
            if (strstr(boost, name))
                Selem_set_name(selem, "mib", &namesCount.micb);
            else
                Selem_set_name(selem, "mic", &namesCount.mic);
        }
        selem->iconIndex = 4;
    } else if (strstr(aux, name)) {
        if (strcmp(aux, name) == 0) {
            selem->name = "aux ";
        } else {
            Selem_set_name(selem, "aux", &namesCount.aux);
        }
        selem->iconIndex = 5;
    } else if (strstr(line, name)) {
        if (strcmp(line, name) == 0) {
            selem->name = "line";
        } else {
            if (strstr(boost, name))
                Selem_set_name(selem, "lnb", &namesCount.lineb);
            else
                Selem_set_name(selem, "lin", &namesCount.line);
        }
        selem->iconIndex = 5;
    } else if (strstr(cap, name)) {
        if (strcmp(cap, name) == 0) {
            selem->name = "cap ";
        } else {
            Selem_set_name(selem, "cap", &namesCount.capt);
        }
        selem->iconIndex = 3;
    } else {
        namesCount.vol++;
        selem->iconIndex = 6;
        Selem_set_name(selem, name, &namesCount.vol);
    }
}

void Selem_set_name(struct Selem *selem, const char *name, short int *count) {
    char new_name[7], buf[5];

    if (*count > 10) {
        snprintf(new_name, sizeof(new_name), "%s", name);
    } else {
        snprintf(buf, sizeof(buf) - 1, "%s", name);
        snprintf(new_name, sizeof(new_name), "%s%d", buf, *count%100u);
    }

    selem->name = strdup(new_name);
    *count = *count + 1;
}

void Mixer_set_channels(struct Selem *selem) {
    int idx;
    snd_mixer_selem_channel_id_t chn;

    selem->stereo = true;

    if (snd_mixer_selem_has_playback_volume(selem->elem)) {
        if (snd_mixer_selem_is_playback_mono(selem->elem)) {
            selem->stereo = false;
            selem->channels[0] = SND_MIXER_SCHN_MONO;
            selem->channels[1] = SND_MIXER_SCHN_MONO;
        } else {
            idx = 0;
            for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++) {
                if (!snd_mixer_selem_has_playback_channel(selem->elem, chn))
                    continue;
                selem->channels[idx] = chn;
                idx++;
                if (idx == 2)
                    break;
            }
        }
    }

    if (snd_mixer_selem_has_capture_volume(selem->elem)) {
        if (snd_mixer_selem_is_capture_mono(selem->elem)) {
            selem->stereo = false;
            selem->channels[0] = SND_MIXER_SCHN_MONO;
            selem->channels[1] = SND_MIXER_SCHN_MONO;
        } else {
            idx = 0;
            for (chn = 0; chn <= SND_MIXER_SCHN_LAST; chn++) {
                if (!snd_mixer_selem_has_capture_channel(selem->elem, chn))
                    continue;
                selem->channels[idx] = chn;
                idx++;
                if (idx == 2)
                    break;
            }
        }
    }
}

slideCaptureMono Mixer_get_capabilities(snd_mixer_elem_t *elem) {
    slideCaptureMono retval;
    
    retval.mono = false;
    retval.capture = false;
    retval.isvolume = false;

    if (snd_mixer_selem_has_common_volume(elem)) {
        retval.isvolume = true;

        if (snd_mixer_selem_has_playback_volume_joined(elem) || snd_mixer_selem_is_playback_mono(elem))
            retval.mono = true;
    
    } else {
        if (snd_mixer_selem_has_playback_volume(elem)) {
            retval.isvolume = true;
            
            if (snd_mixer_selem_has_playback_volume_joined(elem) || snd_mixer_selem_is_playback_mono(elem))
                retval.mono = true;
        }

        if (snd_mixer_selem_has_capture_volume(elem)) {
            retval.isvolume = true;
            retval.capture = true;
            
            if (snd_mixer_selem_has_capture_volume_joined(elem) || snd_mixer_selem_is_playback_mono(elem))
                retval.mono = true;
        }
    }
    
    return retval;
}

void Mixer_set_limits(snd_mixer_elem_t *elem, struct Selem *selem) {
    long min = 0, max = 0;

    if (snd_mixer_selem_has_common_volume(elem)) {
        snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    } else {
        if (snd_mixer_selem_has_capture_volume(elem))
            snd_mixer_selem_get_capture_volume_range(elem, &min, &max);
        if (snd_mixer_selem_has_playback_volume(elem))
            snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    }

    selem->min = min;
    selem->max = max;
}

static int convert_prange(long val, long min, long max) {
    long range = max - min;
    
    int tmp;

    if (range == 0)
        return 0;

    val -= min;
    
    tmp = rint((double)val/(double)range * 100);
    
    return tmp;
}

int Mixer_get_volume(int current, int channelIndex) {
    struct Selem *selem = selems[current];
    
    long raw;
    
    int volume;

    if (!selem->capture) {
        snd_mixer_selem_get_playback_volume(selem->elem, selem->channels[channelIndex], &raw);
    } else {
        snd_mixer_selem_get_capture_volume(selem->elem, selem->channels[channelIndex], &raw);
    }

    volume = convert_prange(raw, selem->min, selem->max);
    
    return volume;
}

void Mixer_set_volume(int current, int channelIndex, int value) {
    struct Selem *selem = selems[current];
    
    long raw;

    raw = (long)convert_prange1(value, selem->min, selem->max);

    if (!selem->capture) {
        snd_mixer_selem_set_playback_volume(selem->elem, selem->channels[channelIndex], raw);
    } else {
        snd_mixer_selem_set_capture_volume(selem->elem, selem->channels[channelIndex], raw);
    }
}

void Mixer_set_left(int current, int value) {
    Mixer_set_volume(current, 0, value);
}

void Mixer_set_right(int current, int value) {
    Mixer_set_volume(current, 1, value);
}

int Mixer_read_left(int current) {
    return Mixer_get_volume(current, 0);
}

int Mixer_read_right(int current) {
    return Mixer_get_volume(current, 1);
}

void Mixer_destroy(struct Mixer *mixer) {
    assert(mixer != NULL);
    
    snd_mixer_close(mixer->handle);
    
    free(mixer);
}

void Selem_destroy() {
    int i;

    for (i = 0; i < mix->devices_no; i++) {
        free(selems[i]);
    }
}

void checkVol(bool forced) {
    int nl = Mixer_read_left(curchannel);
    int nr = Mixer_read_right(curchannel);
    if (forced) {
        curleft = nl;
        curright = nr;
        update();
        da_redraw_window();
    } else {
        if (nl != curleft || nr != curright) {
            if (nl != curleft) {
                curleft = nl;
                if (selems[curchannel]->stereo)
                    drawStereo(true);
                else
                    drawMono();
            }
            if (nr != curright) {
                curright = nr;
                if (selems[curchannel]->stereo)
                    drawStereo(false);
                else
                    drawMono();
            }
            if (!no_volume_display)
                drawVolLevel();
            da_redraw_window();
        }
    }
}

void update() {
    drawText(selems[curchannel]->name);

    da_copy_xpm_area(selems[curchannel]->iconIndex * 26, 84, 26, 24, 5, 19);
    
    if (selems[curchannel]->stereo) {
        drawStereo(true);
        drawStereo(false);
    
    } else {
        drawMono();
    }
}

void drawText(char *text) {
    char *p = text;
    char p2;
    
    int i;

    for (i = 0; i < 4; i++, p++) {
        p2 = toupper(*p);
        
        if (p2 >= 'A' && p2 <= 'Z') {
            da_copy_xpm_area(6 * ((int)p2 - 65), 66, 6, 9, 5 + (i * 6), 5);
            
        } else if (p2 >= '0' && p2 <= '9') {
            da_copy_xpm_area(6 * ((int)p2 - 48), 66, 6, 9, 5 + (i * 6), 5);
            
        } else {
            if (p2 == '\0')
                p--;
            
            da_copy_xpm_area(60, 66, 6, 9, 5 + (i * 6), 5);
        }
    }
    
    if (!no_volume_display)
        text_counter = 10;
}

void drawVolLevel() {
    int i;
    int digits[4];

    int vol = (Mixer_read_left(curchannel) + Mixer_read_right(curchannel)) / 2;

    digits[0] = (vol/100) ? 1 : 10;
    digits[1] = (vol/10) == 10 ? 0 : (vol/10);
    digits[2] = vol%10;
    digits[3] = 10;

    for (i = 0; i < 4; i++) {
        da_copy_xpm_area(6*digits[i], 75, 6, 9, 5+(i*6), 5);
    }
}

void drawStereo(bool left) {
    int i;
    
    short pos = left ? 37 : 48;

    da_set_foreground(0);
    
    da_fill_rectangle(47, 5, 1, 53);

    da_set_foreground(1);
    
    for (i = 0; i < 26; i++) {
        if (i == ((left ? curleft : curright) * 26) / 100)
            da_set_foreground(3);
        
        da_fill_rectangle(pos, 57 - 2 * i, 10, 1);
    }
}

void drawMono() {
    int i;
    
    da_set_foreground(1);
    
    for (i = 0; i < 26; i++) {
        if (i == (curright * 26) / 100)
            da_set_foreground(3);
        
        da_fill_rectangle(37, 57 - 2 * i, 21, 1);
    }
}

void drawBtns(int btns) {
    if (btns & BTNPREV)
        drawBtn(5, 47, 13, 12, (btnstate & BTNPREV));
    if (btns & BTNNEXT)
        drawBtn(18, 47, 13, 12, (btnstate & BTNNEXT));
}

void drawBtn(int x, int y, int w, int h, bool down) {
    if (!down) {
        da_copy_xpm_area(x + 64, y, w, h, x, y);
    } else {
        da_copy_xpm_area(x + 64, y, 1, h - 1, x + w - 1, y + 1);
        da_copy_xpm_area(x + 64 + w - 1, y + 1, 1, h - 1, x, y);
        da_copy_xpm_area(x + 64, y, w - 1, 1, x + 1, y + h - 1);
        da_copy_xpm_area(x + 64 + 1, y + h - 1, w - 1, 1, x, y);
    }
}
