#include "weather_report.h"

void parse_report(FILE *file, int *w_type, char **r_city, char **r_time, char **r_temp) {
    char *line;

    char city_tmp[20] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
    char time_tmp[5] = "\0\0\0\0\0";
    char temp_tmp[5] = "\0\0\0\0\0";

    size_t len = 0;
    
    ssize_t read;

    int i, linenum = 0;

    while ((read = getline(&line, &len, file)) != -1) {
        linenum++;
        
        for (i = 0; ; i++) {
            if (line[i] == '\0') {
                break;
            }

            /* city */
            if (linenum == 1) {
                if (line[i] == ',') {
                    break;
                }
                city_tmp[i] = line[i];
            }

            /* time */
            if (linenum   == 2   &&
                line[i]   == 'U' &&
                line[i+1] == 'T' &&
                line[i+2] == 'C') {
                time_tmp[0] = line[i-5];
                time_tmp[1] = line[i-4];
                time_tmp[2] = line[i-3];
                time_tmp[3] = line[i-2];
            }

            /* temperature */
            if (linenum   != 1   &&
                line[i]   == 'T' &&
                line[i+1] == 'e' &&
                line[i+2] == 'm' &&
                line[i+3] == 'p') {
                temp_tmp[0] = line[i+13];
                temp_tmp[1] = line[i+14];
                if (line[i+15] != ' ')
                temp_tmp[2] = line[i+15];
            }

            /* weather conditions */
            if (line[0] == 'o' &&
                line[1] == 'b' &&
                line[2] == ':') {
                *w_type = parse_conditions(line);
            }
        }
    }

    if (r_city) {
        *r_city = malloc(strlen(city_tmp) + 1);

        sprintf(*r_city, city_tmp);
    }
    
    if (r_time) {
        *r_time = malloc(strlen(time_tmp) + 1);

        sprintf(*r_time, time_tmp);
    }

    if (r_temp) {
        *r_temp = malloc(strlen(temp_tmp) + 1);

        sprintf(*r_temp, temp_tmp);
    }

    if (line) {
        free(line);

        line = NULL;
    }
}

int parse_conditions(char *line) {
    int i;
    
    for (i = 0; ; i++) {
        if (line[i] == '\0') {
            break;
        }

        if ((line[i]   == 'B' &&
             line[i+1] == 'K' &&
             line[i+2] == 'N') ||
            
            (line[i]   == 'O' &&
             line[i+1] == 'V' &&
             line[i+2] == 'C')) {
            
            return BRK;
        }

        if ((line[i]   == 'C' &&
             line[i+1] == 'A' &&
             line[i+2] == 'V' &&
             line[i+3] == 'O' &&
             line[i+4] == 'K') ||
            
            (line[i]   == 'S' &&
             line[i+1] == 'K' &&
             line[i+2] == 'C') ||
            
            (line[i]   == 'C' &&
             line[i+1] == 'L' &&
             line[i+2] == 'R')) {
            
            return CLEAR;
        }

        if ((line[i]   == 'F' &&
             line[i+1] == 'E' &&
             line[i+2] == 'W') ||
            
            (line[i]   == 'S' &&
             line[i+1] == 'C' &&
             line[i+2] == 'T')) {
            
            return CLOUDS;
        }

        if ((line[i]   == 'D' &&
             line[i+1] == 'U') ||
            
            (line[i]   == 'D' &&
             line[i+1] == 'S')) {
            
            return DUST;
        }

        if ((line[i]   == 'B' &&
             line[i+1] == 'R') ||
            
            (line[i]   == 'F' &&
             line[i+1] == 'G')) {
            
            return FOG;
        }

        if ((line[i]   == 'G' &&
             line[i+1] == 'S') ||
            
            (line[i]   == 'G' &&
             line[i+1] == 'R')) {
            
            return HAIL;
        }

        if ((line[i]   == 'D' &&
             line[i+1] == 'Z') ||
            
            (line[i]   == 'R' &&
             line[i+1] == 'A')) {
            
            return RAIN;
        }

        if ((line[i]   == 'S' &&
             line[i+1] == 'N') ||
            
            (line[i]   == 'S' &&
             line[i+1] == 'G')) {
            
            return SNOW;
        }

        if (line[i]   == 'T' &&
            line[i+1] == 'C' &&
            line[i+2] == 'U') {
            
            return TCU;
        }
    }

    return OTHER;
}
