/*
 *  dwgo
 *
 *  Copyright (c) 2008 Gaspar Fernández
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1307,
 *  USA.
 */

/*#include <errno.h>
#include <unistd.h>
#include <curl/curl.h>*/
#include <math.h>
#include <libdockapp4/dockapp.h>

#include "config.h"
#include "weather_report.h"

#include "bitmaps/brk.xpm"
#include "bitmaps/brk2.xpm"
#include "bitmaps/brk3.xpm"
#include "bitmaps/brk4.xpm"
#include "bitmaps/clear.xpm"
#include "bitmaps/clear2.xpm"
#include "bitmaps/clear3.xpm"
#include "bitmaps/clear4.xpm"
#include "bitmaps/clouds.xpm"
#include "bitmaps/clouds2.xpm"
#include "bitmaps/clouds3.xpm"
#include "bitmaps/clouds4.xpm"
#include "bitmaps/default.xpm"
#include "bitmaps/default2.xpm"
#include "bitmaps/default3.xpm"
#include "bitmaps/default4.xpm"
#include "bitmaps/dust.xpm"
#include "bitmaps/dust2.xpm"
#include "bitmaps/dust3.xpm"
#include "bitmaps/dust4.xpm"
#include "bitmaps/fog.xpm"
#include "bitmaps/fog2.xpm"
#include "bitmaps/fog3.xpm"
#include "bitmaps/fog4.xpm"
#include "bitmaps/hail.xpm"
#include "bitmaps/hail2.xpm"
#include "bitmaps/hail3.xpm"
#include "bitmaps/hail4.xpm"
#include "bitmaps/parts.xpm"
#include "bitmaps/parts2.xpm"
#include "bitmaps/parts3.xpm"
#include "bitmaps/parts4.xpm"
#include "bitmaps/rain.xpm"
#include "bitmaps/rain2.xpm"
#include "bitmaps/rain3.xpm"
#include "bitmaps/rain4.xpm"
#include "bitmaps/snow.xpm"
#include "bitmaps/snow2.xpm"
#include "bitmaps/snow3.xpm"
#include "bitmaps/snow4.xpm"
#include "bitmaps/tcu.xpm"
#include "bitmaps/tcu2.xpm"
#include "bitmaps/tcu3.xpm"
#include "bitmaps/tcu4.xpm"

#define REPORT "/tmp/%s"
#define UPDATE_INTERVAL 900
#define TIMEOUT 40
#define MAX_STATIONS 5
#define CITY_FONT "-*-clean-bold-*-*-*-%i-*-*-*-*-*-*-*"
#define TEMP_FONT "-*-clean-bold-*-*-*-%i-*-*-*-*-*-*-*"
#define TIME_FONT "-*-clean-bold-*-*-*-%i-*-*-*-*-*-*-*"

struct Station {
    char id[5];
    char name[16];
    char report[10];
    char url[100];
} station[MAX_STATIONS];

Pixmap def,    def_mask;
Pixmap brok,   brok_mask;
Pixmap clear,  clear_mask;
Pixmap clouds, clouds_mask;
Pixmap dust,   dust_mask;
Pixmap fog,    fog_mask;
Pixmap hail,   hail_mask;
Pixmap parts,  parts_mask;
Pixmap rain,   rain_mask;
Pixmap snow,   snow_mask;
Pixmap tcu,    tcu_mask;

char *city_color;
char *temp_color;
char *time_color;
char *border_dark;
char *border_light;
char *stations;

bool verbose;
bool fahrenheit;
bool metar_city;

char city_font[70];
char temp_font[70];
char time_font[70];
char mTemperature[20];
char mTime[20];

int total_stations = 0;

double mTimeDiff;

void init_stations(void);
void init_ui(int, char **);
void init_links(void);
void free_all(void);
void routine(int, char **);
bool show_weather(int);
void calcTimeDiff(void);
void setTime(char *);
int file_size(char *);
void easy_download_report(void);
/*int download_report(void);
size_t write_data(void *, size_t, size_t, void *);
void add_transfer(CURLM *, int);*/

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a multi station temperature dockapp.\n\n"

    "It is a Window Maker dockapp which fetches weather information of the selected cities from National Weather Service (https://tgftp.nws.noaa.gov) to display current temperature and change background image depending on the current weather conditions.\n\n"

    "There must be at least one station defined (with the maximum of five), the format is ICAO airport code and user description separated by the ':' sign (spaces are ignored), for example:\n\n"

    "ENBR:Bergen\n\n"

    "Or, for several stations:\n\n"

    "ENBR:Bergen,YSSY:Sydney,LPPT:Lisboa\n\n"

    "Mouse left button switches stations, mouse right button forces immediate update.");

    da_init_string(&time_color, "time_color", NULL, "--time-color", "#252A31", "Time text color");
    da_init_string(&temp_color, "temperature_color", NULL, "--temp-color", "#252A31", "Temperature text color");
    da_init_string(&city_color, "city_color", NULL, "--city-color", "#252A31", "City text color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&metar_city, "metar_city", "-m", "--metar-city", false, "Use METAR city info instead of user description");
    da_init_string(&stations, "stations", "-S", "--stations", NULL, "Set stations info (ICAO Location Indicators and descriptions)");
    da_init_bool(&fahrenheit, "fahrenheit", "-F", "--fahrenheit", false, "Use Fahrenheit degrees type");
    da_init_bool(&verbose, "verbose", "-V", "--verbose", false, "Display verbose messages while downloading");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(time_color, "time_color");
    da_add_color(temp_color, "temp_color");
    da_add_color(city_color, "city_color");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    da_add_mixed_color(border_dark, 60, border_light, 40, "border_middark");
    da_add_mixed_color(border_dark, 40, border_light, 60, "border_midlight");

    init_ui(argc, argv);

    init_stations();

    init_links();

    calcTimeDiff();

    da_redraw_window();

    int counter = 0;
    int timeout = 0;
    int station_switcher = 0;

	while (!da_conf_changed(argc, argv)) {
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            station_switcher++;

            if (station_switcher == total_stations) {
                station_switcher = 0;
            }

            show_weather(station_switcher);
            
            break;

        case MOUSE_3_PRS:
            counter = 0;
            break;
        }

        if (counter <= 0) {
            counter = UPDATE_INTERVAL * 4;

            /*download_report();*/
            easy_download_report();
            
            timeout = TIMEOUT;

            while (1) {
                timeout--;

                if (show_weather(station_switcher)) {
                    break;
                }

                if (timeout <= 0) {
                    break;
                }

                da_redraw_window();
                
                usleep(300000);
            }

        } else {
            counter--;

            da_redraw_window();
            
            usleep(250000);
        }
    }

    free_all();

    routine(argc, argv);
}

void init_ui(int argc, char *argv[]) {
    XpmAttributes xa;
    
    xa.valuemask = XpmCloseness;
    xa.closeness = 40000;
    xa.colorsymbols = da_colors;
    xa.numsymbols = DA_MAX_COLORS;
    xa.valuemask |= XpmColorSymbols;
    
    switch (da_scale) {    
    case 4:
        XpmCreatePixmapFromData(da_display, da_root_win, default4_xpm, &def,    &def_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, brk4_xpm,     &brok,   &brok_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clear4_xpm,   &clear,  &clear_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clouds4_xpm,  &clouds, &clouds_mask, &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, dust4_xpm,    &dust,   &dust_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, fog4_xpm,     &fog,    &fog_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, hail4_xpm,    &hail,   &hail_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, parts4_xpm,   &parts,  &parts_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, rain4_xpm,    &rain,   &rain_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, snow4_xpm,    &snow,   &snow_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, tcu4_xpm,     &tcu,    &tcu_mask,    &xa);
        da_open_xwindow(argc, argv, default4_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
        
    case 3:
        XpmCreatePixmapFromData(da_display, da_root_win, default3_xpm, &def,    &def_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, brk3_xpm,     &brok,   &brok_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clear3_xpm,   &clear,  &clear_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clouds3_xpm,  &clouds, &clouds_mask, &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, dust3_xpm,    &dust,   &dust_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, fog3_xpm,     &fog,    &fog_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, hail3_xpm,    &hail,   &hail_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, parts3_xpm,   &parts,  &parts_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, rain3_xpm,    &rain,   &rain_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, snow3_xpm,    &snow,   &snow_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, tcu3_xpm,     &tcu,    &tcu_mask,    &xa);
        da_open_xwindow(argc, argv, default3_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
        
    case 2:
        XpmCreatePixmapFromData(da_display, da_root_win, default2_xpm, &def,    &def_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, brk2_xpm,     &brok,   &brok_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clear2_xpm,   &clear,  &clear_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clouds2_xpm,  &clouds, &clouds_mask, &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, dust2_xpm,    &dust,   &dust_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, fog2_xpm,     &fog,    &fog_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, hail2_xpm,    &hail,   &hail_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, parts2_xpm,   &parts,  &parts_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, rain2_xpm,    &rain,   &rain_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, snow2_xpm,    &snow,   &snow_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, tcu2_xpm,     &tcu,    &tcu_mask,    &xa);
        da_open_xwindow(argc, argv, default2_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
        
    case 1:
    default:
        XpmCreatePixmapFromData(da_display, da_root_win, default_xpm, &def,    &def_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, brk_xpm,     &brok,   &brok_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clear_xpm,   &clear,  &clear_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, clouds_xpm,  &clouds, &clouds_mask, &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, dust_xpm,    &dust,   &dust_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, fog_xpm,     &fog,    &fog_mask,    &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, hail_xpm,    &hail,   &hail_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, parts_xpm,   &parts,  &parts_mask,  &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, rain_xpm,    &rain,   &rain_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, snow_xpm,    &snow,   &snow_mask,   &xa);
        XpmCreatePixmapFromData(da_display, da_root_win, tcu_xpm,     &tcu,    &tcu_mask,    &xa);
        da_open_xwindow(argc, argv, default_xpm, NULL, 64 * da_scale, 64 * da_scale);
        break;
    }

    sprintf(city_font, CITY_FONT, 10 * da_scale);
    sprintf(temp_font, TEMP_FONT, 16 * da_scale);
    sprintf(time_font, TIME_FONT, 11 * da_scale);
}

void free_all() {
    int o, p;
    
    XFreePixmap(da_display, def);    XFreePixmap(da_display, def_mask);
    XFreePixmap(da_display, brok);   XFreePixmap(da_display, brok_mask);
    XFreePixmap(da_display, clear);  XFreePixmap(da_display, clear_mask);
    XFreePixmap(da_display, clouds); XFreePixmap(da_display, clouds_mask);
    XFreePixmap(da_display, dust);   XFreePixmap(da_display, dust_mask);
    XFreePixmap(da_display, fog);    XFreePixmap(da_display, fog_mask);
    XFreePixmap(da_display, hail);   XFreePixmap(da_display, hail_mask);
    XFreePixmap(da_display, parts);  XFreePixmap(da_display, parts_mask);
    XFreePixmap(da_display, rain);   XFreePixmap(da_display, rain_mask);
    XFreePixmap(da_display, snow);   XFreePixmap(da_display, snow_mask);
    XFreePixmap(da_display, tcu);    XFreePixmap(da_display, tcu_mask);

    for (o = 0; o < MAX_STATIONS; o++) {
        for (p = 0; p < 5; p++) {
            station[o].id[p] = '\0';
        }

        for (p = 0; p < 16; p++) {
            station[o].name[p] = '\0';
        }

        for (p = 0; p < 10; p++) {
            station[o].report[p] = '\0';
        }

        for (p = 0; p < 100; p++) {
            station[o].url[p] = '\0';
        }
    }
}

void init_stations() {
    if (!stations) {
        printf("ERR: you must supply at least one station info using -S.\n");
        da_log(ERR, "you must supply at least one station info using -S.\n");
        exit(0);
    
    } else {
        int e, d = 0;
        int length;

        bool fill_id = true;

        length = strlen(stations);

        for (e = 0; e <= length; e++) {
            if (e == length) {
                total_stations++;
                
                break;
            }
            
            if (stations[e] == ' ') {
                continue;
            }
            
            if (stations[e] == ',') {
                total_stations++;

                fill_id = true;

                d = 0;

                if (total_stations == MAX_STATIONS) {
                    break;
                }

                continue;
            }

            if (stations[e] == ':') {
                fill_id = false;
                
                d = 0;

                continue;
            }
            
            if (fill_id) {
                station[total_stations].id[d] = toupper(stations[e]);

                d++;
            
            } else {
                station[total_stations].name[d] = stations[e];

                d++;
            }
        }
    }
}

void init_links() {
    int o;

    for (o = 0; o < total_stations; o++) {
        sprintf(station[o].report, REPORT, station[o].id);
        sprintf(station[o].url, METAR_URL, station[o].id);
    }
}

bool show_weather(int curr_station) {
    FILE *file;

    char *r_city = NULL;
    char *r_time = NULL;
    char *r_temp = NULL;

    int w_type = 0;

    bool res = false;
    
    if (file_size(station[curr_station].report) > 1) {
        file = fopen(station[curr_station].report, "r");
        
        parse_report(file, &w_type, &r_city, &r_time, &r_temp);

        /* show condition */
        switch (w_type) {
        case BRK:
            da_copy_from_xpm(brok, 0, 0, 64, 64, 0, 0);
            da_set_mask(brok_mask, 0, 0, 64, 64, 0, 0);
            break;
        case CLEAR:
            da_copy_from_xpm(clear, 0, 0, 64, 64, 0, 0);
            da_set_mask(clear_mask, 0, 0, 64, 64, 0, 0);
            break;
        case CLOUDS:
            da_copy_from_xpm(clouds, 0, 0, 64, 64, 0, 0);
            da_set_mask(clouds_mask, 0, 0, 64, 64, 0, 0);
            break;
        case DUST:
            da_copy_from_xpm(dust, 0, 0, 64, 64, 0, 0);
            da_set_mask(dust_mask, 0, 0, 64, 64, 0, 0);
            break;
        case FOG:
            da_copy_from_xpm(fog, 0, 0, 64, 64, 0, 0);
            da_set_mask(fog_mask, 0, 0, 64, 64, 0, 0);
            break;
        case HAIL:
            da_copy_from_xpm(hail, 0, 0, 64, 64, 0, 0);
            da_set_mask(hail_mask, 0, 0, 64, 64, 0, 0);
            break;
        case RAIN:
            da_copy_from_xpm(rain, 0, 0, 64, 64, 0, 0);
            da_set_mask(rain_mask, 0, 0, 64, 64, 0, 0);
            break;
        case SNOW:
            da_copy_from_xpm(snow, 0, 0, 64, 64, 0, 0);
            da_set_mask(snow_mask, 0, 0, 64, 64, 0, 0);
            break;
        case TCU:
            da_copy_from_xpm(tcu, 0, 0, 64, 64, 0, 0);
            da_set_mask(tcu_mask, 0, 0, 64, 64, 0, 0);
            break;
        case OTHER:
            da_copy_from_xpm(parts, 0, 0, 64, 64, 0, 0);
            da_set_mask(parts_mask, 0, 0, 64, 64, 0, 0);
            break;
        case DEFAULT_IMG:
        default:
            da_copy_from_xpm(def, 0, 0, 64, 64, 0, 0);
            da_set_mask(def_mask, 0, 0, 64, 64, 0, 0);
            break;
        }
       
        /* show time */
        setTime(r_time);

        da_set_foreground(0);

        da_draw_string(34, 11, time_font, mTime);

        /* show temperature */
        if (fahrenheit) {
            sprintf(mTemperature, "%sºF", r_temp);

        } else {
            sprintf(mTemperature, "%dºC", (int)rint((atoi(r_temp) - 32) / 1.8));
        }

        da_set_foreground(1);

        da_draw_string(CENTER_TEXT, 32, temp_font, mTemperature);

        /* show city */
        da_set_foreground(2);

        if (metar_city) {
            da_draw_string(CENTER_TEXT, 50, city_font, r_city);
        } else {
            da_draw_string(CENTER_TEXT, 50, city_font, station[curr_station].name);
        }

        res = true;

        fclose(file);
    }

    if (r_city) {
        free(r_city);

        r_city = NULL;
    }

    if (r_time) {
        free(r_time);

        r_time = NULL;
    }

    if (r_temp) {
        free(r_temp);

        r_temp = NULL;
    }

    return res;
}

void calcTimeDiff() {
    struct tm* t;

    double localTime;
    double universalTime;

    time_t currentTime;

    currentTime = time(0);

    t = gmtime(&currentTime);

    universalTime = (double)t->tm_hour + (double)t->tm_min / 60.0 + (double)t->tm_sec / 3600.0;

    currentTime = time(0);

    t = localtime(&currentTime);

    localTime = (double)t->tm_hour + (double)t->tm_min / 60.0 + (double)t->tm_sec / 3600.0;

    mTimeDiff = universalTime - localTime;

    if (mTimeDiff > 24.0) {
        mTimeDiff -= 24.0;
    } else if (mTimeDiff < 0.0) {
        mTimeDiff += 24.0;
    }
}

void setTime(char *utcTime) {
    char unit[3];

    int hour = 0;
    int min = 0;

    strncpy(unit, &utcTime[0], 2);

    hour = atoi(unit);

    strncpy(unit, &utcTime[2], 2);

    min = atoi(unit);

    double time = ((double)hour + (double)min / 60.0) - mTimeDiff;

    if (time < 0.0) {
        time += 24.0;
    } else if (time > 24.0) {
        time -= 24.0;
    }

    hour = (int)time;
    min = (int)((time - (double)hour) * 60.0 + 0.5);

    if (min >= 60){
        min = 0;

        if (++hour >= 24) {
            hour = 0;
        }
    }

    sprintf(mTime, "%d:%.2d", hour, min);
}

int file_size(char *file_name) {
    struct stat filestat;
    
    if (stat(file_name, &filestat) != 0) {
        return 0;
    }

    return filestat.st_size;
}

/*int download_report() {
    CURLM *cm;
    CURLMsg *msg;

    unsigned int transfers = 0;

    int msgs_left = -1;
    int still_alive = 1;
 
    curl_global_init(CURL_GLOBAL_ALL);

    cm = curl_multi_init();
 
    curl_multi_setopt(cm, CURLMOPT_MAXCONNECTS, (long)MAX_STATIONS);
 
    for (transfers = 0; transfers < MAX_STATIONS; transfers++) {
        add_transfer(cm, transfers);
    }
 
    do {
        curl_multi_perform(cm, &still_alive);
 
        while ((msg = curl_multi_info_read(cm, &msgs_left))) {
            if (msg->msg == CURLMSG_DONE) {
                char *url;

                CURL *e = msg->easy_handle;

                curl_easy_getinfo(msg->easy_handle, CURLINFO_PRIVATE, &url);

                fprintf(stderr, "R: %d - %s <%s>\n", msg->data.result, curl_easy_strerror(msg->data.result), url);

                curl_multi_remove_handle(cm, e);

                curl_easy_cleanup(e);
            
            } else {
                fprintf(stderr, "E: CURLMsg (%d)\n", msg->msg);
            }

            if (transfers < (unsigned)total_stations) {
                add_transfer(cm, transfers++);
            }
        }

        if (still_alive) {
            curl_multi_wait(cm, NULL, 0, 1000, NULL);
        }
 
    } while (still_alive || (transfers < (unsigned)total_stations));
 
    curl_multi_cleanup(cm);

    curl_global_cleanup();
 
    return EXIT_SUCCESS;
}

size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
    size_t written;

    written = fwrite(ptr, size, nmemb, (FILE *)stream);

    return written;
}
 
void add_transfer(CURLM *cm, int i) {
    FILE *file;
    
    CURL *eh = curl_easy_init();

    if (verbose) {
        curl_easy_setopt(eh, CURLOPT_VERBOSE, 1L);
    }
    curl_easy_setopt(eh, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(eh, CURLOPT_URL, station[i].url);
    curl_easy_setopt(eh, CURLOPT_PRIVATE, station[i].url);

    file = fopen(station[i].report, "w");

    if (file) {
        curl_easy_setopt(eh, CURLOPT_WRITEDATA, file);
 
        curl_easy_perform(eh);
 
        fclose(file);
    }

    curl_multi_add_handle(cm, eh);
}*/

/* I know this is not cool, but it's short and simple,
 * and libcurl is just too hard for me at the moment */
void easy_download_report() {
    int l, length;

    char *curl_cmd;

    length = ((67 + 9 + 5) * total_stations) + 10;

    curl_cmd = malloc(length);

    sprintf(curl_cmd, "curl ");

    if (verbose) {
        strcat(curl_cmd, "-v ");
    }

    for (l = 0; l < total_stations; l++) {
        strcat(curl_cmd, station[l].url);
        strcat(curl_cmd, " -o ");
        strcat(curl_cmd, station[l].report);
        strcat(curl_cmd, " ");
    }

    /* remove old report files */
    for (l = 0; l < total_stations; l++) {
        if (file_size(station[l].report) > 1) {
            remove(station[l].report);
        }
    }
    
    da_exec_command(curl_cmd);

    free(curl_cmd);

    curl_cmd = NULL;
}
