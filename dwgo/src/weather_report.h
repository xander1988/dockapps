#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum Condition {
    DEFAULT_IMG,
    BRK, /* Broken, Overcast (BKN, OVC) */
    CLEAR, /* CAVOK, SKC, CLR */
    CLOUDS, /* FEW, SCT */
    DUST, /* (DU, DS) */
    FOG, /* (BR, FG) */
    HAIL, /* Hail (GS, GR) */
    RAIN, /* Rainy (DZ, RA) */
    SNOW, /* Snowy (SN, SG) */
    TCU, /* TCU (Towering CUmulus) */
    OTHER /* (FC, FU, HZ, SA, SS, VA) */
};

void parse_report(FILE *, int *, char **, char **, char **);
int parse_conditions(char *);
