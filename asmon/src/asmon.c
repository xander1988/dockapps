#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <libdockapp4/dockapp.h>

#include "config.h"

#include "bitmaps/mask.xbm"
#include "bitmaps/mask2.xbm"
#include "bitmaps/mask3.xbm"
#include "bitmaps/mask4.xbm"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

#ifdef __solaris__
#include <utmp.h>
#endif

#define CHAR_WIDTH 5
#define CHAR_HEIGHT 7

#define BCHAR_WIDTH 6
#define BCHAR_HEIGHT 9

#define BOFFX   (9)
#define BOFFY   (111)
#define BREDX   (3)
#define BREDY   (111)
#define BGREENX (87)
#define BGREENY (66)

#define LITEW   (4)
#define LITEH   (4)

#define B_OFF   (0)
#define B_RED   (1)
#define B_GREEN (2)

/* Evil globals I haven't removed yet */
long last_pageins = 0, last_pageouts = 0;
long last_swapins = 0, last_swapouts = 0;

char *Command;
char *front_color;
char *back_color;
char *border_dark;
char *border_light;

bool draw_uptime;

/* functions */
void DrawUptime(void);
void routine(int, char **);
void DrawLite(int state, int dx, int dy);
void DrawCPU(void);
void DrawLoad(void);
#ifdef __solaris__
float DrawMemSwap(void);
extern int getLoad(float *);
extern int getSwap(unsigned long *, unsigned long *);
extern int getMem(unsigned long *, unsigned long *);
extern int getCPU(unsigned long *, unsigned long *,
		  unsigned long *, unsigned long *,
		  unsigned long *, unsigned long *,
		  unsigned long *, unsigned long *);
#else
void DrawXmem(int Xpid, float total);
float DrawMemSwap(float total);
#endif

int main(int argc, char *argv[])
{
    da_init_main(PROG_NAME, PROG_VERSION, "a system monitor for Linux systems by Brad Hall (bkh@rio.vg).\n\n"

    "I got fed up with most other wharf system monitors that didn’t show the correct memory usage, and took up too much CPU. "PROG_NAME" also shows the exact amount of memory used and load avg by the numbers. The CPU bar is fairly standard, next to it is the current load average. The second bar is memory usage, taken as a whole, it represent the amount of memory used. The area before the first tick represents shared memory, the area between the two ticks is buffered memory, the area from the second tick to the end of the bar represents cached memory. The number to the right represents the number of megs currently being used. The third bar represents the amount of swap file used, and the exact number of megs the swap is using. The bottom right is are page/swap LED’s taken from wmsysmon. The right is uptime. The original program is based off of Timecop’s wmcpu.\n\n"

    "The top bar: left is the CPU usage, right is the load average.\n\n"

    "The middle bar: left memory usage devided by ticks into shared, buffers, and cached, respectively, and the number of megs used.\n\n"

    "The lower bar: the left swap usage and the number of megs swapped avg.\n\n"

    "The bottom: the left is a set of LED's marking page's and swap's, the right is a bar representing the amount of memory that the X server is taking up, and the exact megs.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#00c9c1", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&Command, "command", "-c", "--command", NULL, "Command to execute on mouse click");
    da_init_bool(&draw_uptime, "draw_uptime", "-u", "--draw-uptime", false, "Force "PROG_NAME" to show uptime, rather than X mem use");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc"); 

    da_parse_all(argc, argv);

    routine(argc, argv);
    
    return 0;
}

void routine(int argc, char *argv[]) {
    FILE *fp;
	int Xpid = 1;
    
    if (draw_uptime) {
#ifdef __solaris__
        fprintf(stderr, "X Server memory stats unavailable for Solaris.\n");
        da_log(ERR, "X Server memory stats unavailable for Solaris.\n");
        exit(0);
#endif
        Xpid = 0;
    }
    
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 60, back_color, 40, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

#ifndef __solaris__
	if (Xpid != 0) {
		if ((fp = fopen("/var/run/server.0.pid", "r")) != NULL) {
			fscanf(fp, " %d", &Xpid);
			fclose(fp);
		} else {
			if ((fp = fopen("/tmp/.X0-lock", "r")) != NULL) {
				fscanf(fp, " %d", &Xpid);
				fclose(fp);
			} else {
				Xpid = 0;
			}
		}
	}
#endif

	/* Open window */
	switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, mask4_bits, mask4_width, mask4_height);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, mask3_bits, mask3_width, mask3_height);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, mask2_bits, mask2_width, mask2_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, mask_bits, mask_width, mask_height);
        break;
    }

	int xpm_X = 0, xpm_Y = 0, count = 0;
    
	float total = 0.0;

	while (!da_conf_changed(argc, argv)) {
		DrawCPU();
		DrawLoad();

		/* Only run every 15 iterations */
		if (count == 0 || count == 15)
#ifdef __solaris__
			total = DrawMemSwap();
#else
			total = DrawMemSwap(total);
#endif

#ifdef __solaris__
		DrawUptime();
#else
		/* X mem or Uptime? */
		if (Xpid == 0) {
			DrawUptime();
		} else {
			if (count == 5)
				DrawXmem(Xpid, total);
		}
#endif

		/* Redraw Windows */
		da_redraw_window_xy(xpm_X, xpm_Y);
        
		switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
#if 0
            fprintf(stderr, "da_exec_command(%s)\n", Command);
#endif
            if (Command)
                da_exec_command(Command);
            break;
		}
        
		count++;
		if (count > 30)
			count = 0;
        
		usleep(150000);
	}

    routine(argc, argv);
}

/**************************************************************************/

#ifdef __solaris__

/* CPU Usage Meter */
void DrawCPU(void
    ) {
	unsigned long cpuIdle, cpuUser, cpuKern, cpuWait;
	unsigned long pageIn, pageOut, swapIn, swapOut;

	/* remember the statistics read last time */
	static float cpustat[4] = { 0.0, 0.0, 0.0, 0.0 };
	float fields[4] = { 0.0, 0.0, 0.0, 0.0 };
	float cputotal = 0.0;

	getCPU(&cpuIdle, &cpuUser, &cpuKern, &cpuWait,
	       &pageIn, &pageOut, &swapIn, &swapOut);

	// Calculate CPU stuff
	fields[0] = ((float)cpuIdle - cpustat[0]);
	cpustat[0] = (float)cpuIdle;
	cputotal += fields[0];

	fields[1] = ((float)cpuUser - cpustat[1]);
	cpustat[1] = (float)cpuUser;
	cputotal += fields[1];

	fields[2] = ((float)cpuKern - cpustat[2]);
	cpustat[2] = (float)cpuKern;
	cputotal += fields[2];

	fields[3] = ((float)cpuWait - cpustat[3]);
	cpustat[3] = (float)cpuWait;
	cputotal += fields[3];

	// CPU Bar
	if (cputotal > 0) {
		cputotal = ((cputotal - (fields[0] + fields[2])) * 1.55);
		if (cputotal > 26)
			cputotal = 26;
		da_copy_xpm_area(3, 84, cputotal, 9, 5, 5);
		da_copy_xpm_area(15, 105, (27 - cputotal), 9, (5 + cputotal), 5);
		da_copy_xpm_area(16, 46, 2, 11, 32, 4);
	}
	// Page In/Out
	if (pageIn > last_pageins)
		DrawLite(B_RED, 5, 48);
	else
		DrawLite(B_OFF, 5, 48);

	if (pageOut > last_pageouts)
		DrawLite(B_RED, 10, 48);
	else
		DrawLite(B_OFF, 10, 48);

	last_pageins = pageIn;
	last_pageouts = pageOut;

	// Swap In/Out
	if (swapIn > last_swapins)
		DrawLite(B_RED, 5, 53);
	else
		DrawLite(B_OFF, 5, 53);

	if (swapOut > last_swapouts)
		DrawLite(B_RED, 10, 53);
	else
		DrawLite(B_OFF, 10, 53);

	last_swapins = swapIn;
	last_swapouts = swapOut;
}

#else

/* CPU Usage Meter */
void DrawCPU(void)
{
	FILE *fp;
	static double cpustat[7];	/* remember the statistics read last time */
	//double fields[7], info[7], cputotal=0.0,idlee=0.0;
	double fields[7], info[7], cputotal = 0.0;
	long pageins = 0, pageouts = 0, swapins = 0, swapouts = 0;
	char buf[128];
	int i;

	if ((fp = fopen("/proc/stat", "r")) != NULL) {
		// CPU data
		fscanf(fp, "cpu %lf %lf %lf %lf %lf %lf %lf", info,
		       info + 1, info + 2, info + 3, info + 4, info + 5,
		       info + 6);

		fclose(fp);

		if ((fp = fopen("/proc/vmstat", "r")) != NULL) {
			// gather data for LED's
			while (fgets(buf, 127, fp)) {
				if (strstr(buf, "pgpgin"))
					sscanf(buf, "pgpgin %ld",
					       &pageins);

				if (strstr(buf, "pgpgout"))
					sscanf(buf, "pgpgout %ld",
					       &pageouts);

				if (strstr(buf, "pswpin"))
					sscanf(buf, "pswpin %ld",
					       &swapins);

				if (strstr(buf, "pswpout"))
					sscanf(buf, "pswpout %ld",
					       &swapouts);
			}
			fclose(fp);
		}


		// Calculate CPU stuff
		for (i = 0; i < 7; i++) {
			fields[i] = info[i] - cpustat[i];
			cputotal += fields[i];
			cpustat[i] = info[i];
		}

		//idlee=info[3]-old;
		//old=info[3];

		//cputotal = 100 * l1 ;
		//cputotal=(100-(idlee*100/16))*26/100;
		if (cputotal > 0) {
			cputotal = (cputotal - (fields[3] + fields[4])) * 1.55;
			if (cputotal > 26)
				cputotal = 26;
			da_copy_xpm_area(3, 84, cputotal, 9, 5, 5);
			da_copy_xpm_area(15, 105, (27 - cputotal), 9, (5 + cputotal),
				    5);
			da_copy_xpm_area(16, 46, 2, 11, 32, 4);
		}
		// Page In/Out
		if (pageins > last_pageins) {
			DrawLite(B_RED, 5, 48);
		} else {
			DrawLite(B_OFF, 5, 48);
		}

		if (pageouts > last_pageouts) {
			DrawLite(B_RED, 10, 48);
		} else {
			DrawLite(B_OFF, 10, 48);
		}
		last_pageins = pageins;
		last_pageouts = pageouts;

		// Swap In/Out
		if (swapins > last_swapins) {
			DrawLite(B_RED, 5, 53);
		} else {
			DrawLite(B_OFF, 5, 53);
		}

		if (swapouts > last_swapouts) {
			DrawLite(B_RED, 10, 53);
		} else {
			DrawLite(B_OFF, 10, 53);
		}
		last_swapins = swapins;
		last_swapouts = swapouts;
	}
}

#endif

/**************************************************************************/

/* Load Average */
void DrawLoad(void)
{
	int tempy, tempa;
	static float oldv = -1.0;
	float ftmp;

#ifdef __solaris__
	if (getLoad(&ftmp) != -1) {
#else
	FILE *fp;
	if ((fp = fopen("/proc/loadavg", "r")) != NULL) {
		fscanf(fp, "%f", &ftmp);
		fclose(fp);
#endif
		if (oldv != ftmp) {
			oldv = ftmp;
			tempa = (ftmp + 0.005) * 100;
			tempy = tempa % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 50, 5);
			tempy = tempa / 10;
			tempy = tempy % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 44, 5);
			da_copy_xpm_area(65, 66, 3, 9, 41, 5);
			tempy = tempa / 100;
			if (tempy > 9) {
				tempy = (tempy - 10);
				da_copy_xpm_area(3 + (tempy * 6), 95, 6, 9, 34, 5);
			} else {
				da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 34, 5);
			}
		}
	}
}

/**************************************************************************/

#ifdef __solaris__

/* Mem/Swap Meter */
float DrawMemSwap(void
    ) {
	unsigned long memMax, memFree, swapMax, swapFree;
	unsigned long MEMmem, MEMswap;
	float memUsed, swapUsed;
	int tempy, tempa;

	getMem(&memMax, &memFree);
	memUsed = (float)(memMax - memFree);

	getSwap(&swapMax, &swapFree);
	swapUsed = (float)(swapMax - swapFree);

	/* MEM Meter */
	if (memMax == 0)
		MEMmem = 0;
	else {
		if (((float)memMax / 1048576) >= 1)
			MEMmem = ((memUsed * 31) / (float)memMax);
		else
			MEMmem = ((memUsed * 36) / (float)memMax);
	}

	// refresh
	da_copy_xpm_area(4, 115, 55, 11, 4, 18);

	// Bar
	da_copy_xpm_area(3, 75, MEMmem, 9, 5, 19);
	da_copy_xpm_area(15, 105, (36 - MEMmem), 9, (5 + MEMmem), 19);
	// Numbers
	tempa = (memUsed / 1048576);
	tempy = (tempa % 10);
	da_copy_xpm_area((3 + (tempy * 6)), 66, 6, 9, 50, 19);
	tempy = ((tempa / 10) % 10);
	da_copy_xpm_area((3 + (tempy * 6)), 66, 6, 9, 44, 19);
	tempy = ((tempa / 100) % 10);
	if (tempy != 0) {
		da_copy_xpm_area((3 + (tempy * 6)), 66, 6, 9, 38, 19);
		da_copy_xpm_area(16, 46, 2, 11, 35, 18);
	} else {
		da_copy_xpm_area(16, 46, 2, 11, 41, 18);
	}

	// refresh
	da_copy_xpm_area(4, 115, 55, 11, 4, 32);

	/* SWAP Meter */
	if (swapMax == 0)
		MEMswap = 0;
	else {
		if (((float)swapMax / 1048576) >= 1)
			MEMswap = ((swapUsed * 31) / (float)swapMax);
		else
			MEMswap = ((swapUsed * 36) / (float)swapMax);
	}
	// Bar
	da_copy_xpm_area(3, 75, MEMswap, 9, 5, 33);
	da_copy_xpm_area(15, 105, (36 - MEMswap), 9, (5 + MEMswap), 33);
	// Numbers
	tempa = (swapUsed / 1048576);
	tempy = (tempa % 10);
	da_copy_xpm_area((3 + (tempy * 6)), 66, 6, 9, 50, 33);
	tempy = ((tempa / 10) % 10);
	da_copy_xpm_area((3 + (tempy * 6)), 66, 6, 9, 44, 33);
	tempy = ((tempa / 100) % 10);
	if (tempy != 0) {
		da_copy_xpm_area((3 + (tempy * 6)), 66, 6, 9, 38, 33);
		da_copy_xpm_area(16, 46, 2, 11, 42, 18);
	} else {
		da_copy_xpm_area(16, 46, 2, 11, 41, 32);
	}

	return (float)memMax;
}

#else

/* Mem/Swap Meter */
float DrawMemSwap(float total)
{
	FILE *fp;
	if ((fp = fopen("/proc/meminfo", "r")) != NULL) {
		char junk[128];
		float used = 0.0, freeM, buffers = 0.0, cached = 0.0, swaptotal = 0.0,
		    swapused = 0.0, swapfreeM;
		unsigned long MEMshar, MEMbuff, MEMswap;
		int tempy, tempa;

		float scratch;
		while (!feof(fp)) {
			fgets(junk, 120, fp);
			if (strstr(junk, "MemTotal")) {
				sscanf(junk, "MemTotal: %f kB",
				       &scratch);
				total = scratch * 1024;
			}
			if (strstr(junk, "MemFree")) {
				sscanf(junk, "MemFree: %f kB",
				       &scratch);
				freeM = scratch * 1024;
				used = total - freeM;
			}
			if (strstr(junk, "Buffers")) {
				sscanf(junk, "Buffers: %f kB",
				       &scratch);
				buffers = scratch * 1024;
			}
			if (strstr(junk, "Cached")) {
				sscanf(junk, "Cached: %f kB", &scratch);
				cached = scratch * 1024;
			}
			if (strstr(junk, "SwapTotal")) {
				sscanf(junk, "SwapTotal: %f kB",
				       &scratch);
				swaptotal = scratch * 1024;
			}
			if (strstr(junk, "SwapFree")) {
				sscanf(junk, "SwapFree: %f kB",
				       &scratch);
				swapfreeM = scratch * 1024;
				swapused = swaptotal - swapfreeM;
			}
		}

		fclose(fp);

		/* All mem areas */
		if ((total / 101048576) >= 1) {
			MEMshar =
				((used - buffers - cached) / total) * 27;
			MEMbuff = (buffers / total) * 27;
		} else {
			MEMshar =
				((used - buffers - cached) / total) * 33;
			MEMbuff = (buffers / total) * 33;
		}
		// refresh
		da_copy_xpm_area(4, 115, 55, 11, 4, 18);
		// Bar
		if ((total / 101048576) >= 1) {
			da_copy_xpm_area(3, 75, ((used / total) * 28), 9, 5,
				    19);
		} else {
			da_copy_xpm_area(3, 75, ((used / total) * 34), 9, 5,
				    19);
		}
		// Separators
		da_copy_xpm_area(15, 105, 1, 9, 5 + MEMshar, 19);
		da_copy_xpm_area(15, 105, 1, 9, 7 + MEMshar + MEMbuff, 19);
		da_copy_xpm_area(15, 105, (36 - (used / total) * 34), 9,
			    (5 + (used / total) * 34), 19);
		// Numbers                      
		tempa = used / 1048576;
		tempy = tempa % 10;
		da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 50, 19);
		tempy = (tempa / 10) % 10;
		da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 44, 19);
		tempy = (tempa / 100) % 10;
		if ((total / 101048576) >= 1) {
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 38, 19);
			da_copy_xpm_area(16, 46, 2, 11, 35, 18);
		} else {
			da_copy_xpm_area(16, 46, 2, 11, 41, 18);
		}

		/* SWAP Meter */
		if (swaptotal == 0)
			MEMswap = 0;
		else {
			if ((total / 101048576) >= 1)
				MEMswap = (swapused * 31) / swaptotal;
			else
				MEMswap = (swapused * 36) / swaptotal;
		}
		// refresh
		da_copy_xpm_area(4, 115, 55, 11, 4, 32);
		// Bar
		da_copy_xpm_area(3, 75, MEMswap, 9, 5, 33);
		da_copy_xpm_area(15, 105, (36 - (MEMswap)), 9, 5 + MEMswap, 33);
		// Numbers
		tempa = swapused / 1048576;
		tempy = tempa % 10;
		da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 50, 33);
		tempy = (tempa / 10) % 10;
		da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 44, 33);
		tempy = tempa / 100;
		if (tempy != 0) {
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 38, 33);
			da_copy_xpm_area(16, 46, 2, 11, 35, 32);
		} else {
			da_copy_xpm_area(16, 46, 2, 11, 41, 32);
		}

	}
	return (total);
}

#endif

/**************************************************************************/

#ifndef __solaris__

/* X Mem Usage */
void DrawXmem(int Xpid, float total)
{
	FILE *fp;
	char buf[128], XFileName[256];
	float ratio;
	long old_Xsize = -1, Xsize = 0;

	sprintf(XFileName, "/proc/%d/status", Xpid);

	if ((fp = fopen(XFileName, "r")) != NULL) {
		while (fgets(buf, 127, fp)) {
			if (strstr(buf, "VmSize"))
				sscanf(buf, "VmSize: %ld", &Xsize);
		}
		if (old_Xsize != Xsize) {
			int tempy, tempa, tempb;
			old_Xsize = Xsize;
			ratio = Xsize / (total / 1024);
			if (Xsize > (total / 1024))
				Xsize = total / 1024;
			Xsize = Xsize / 1024;
			tempy = Xsize % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 50, 48);
			tempa = Xsize / 10;
			tempy = tempa % 10;
			tempb = Xsize / 100;
			if (Xsize > 100) {
				da_copy_xpm_area(3, 84, ((ratio) * 17), 11, 18, 47);
				da_copy_xpm_area(15, 105, (23 - ((ratio) * 17)), 11,
					    (18 + (ratio * 22)), 47);
				da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 44, 48);
				da_copy_xpm_area(3 + (tempb * 6), 66, 6, 9, 38, 48);
				da_copy_xpm_area(16, 46, 2, 14, 36, 46);
			} else {
				da_copy_xpm_area(3, 84, ((ratio) * 22), 11, 18, 47);
				da_copy_xpm_area(15, 105, (23 - ((ratio) * 22)), 11,
					    (18 + (ratio * 22)), 47);
				da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 44, 48);
				da_copy_xpm_area(16, 46, 2, 14, 41, 46);
			}
		}
		fclose(fp);
	}
}

#endif

/**************************************************************************/

/* Uptime */
void DrawUptime(void)
{
	int upt, days = 0, hours = 0, mins = 0, old_mins = -1, old_hours = -1;

#ifdef __solaris__
	struct utmp *pUtmp;
	struct utmp idUtmp;
	idUtmp.ut_type = BOOT_TIME;
	setutent();
	pUtmp = getutid(&idUtmp);
	upt = (time(0) - pUtmp->ut_time);
#else
	FILE *fp;
	if ((fp = fopen("/proc/uptime", "r")) != NULL)
		fscanf(fp, "%d", &upt);
	fclose(fp);
#endif
	mins = (upt / 60) % 60;
	hours = (upt / 3600) % 24;
	days = (upt / 86400);
	if (old_hours != hours)
		old_hours = hours;
	if (old_mins != mins) {
		int tempy;
		old_mins = mins;
		if (days > 9) {
			da_copy_xpm_area(20, 105, 36, 9, 18, 48);
			tempy = hours % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 50, 48);
			tempy = hours / 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 44, 48);
			da_copy_xpm_area(63, 66, 3, 9, 41, 48);
			tempy = days % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 34, 48);
			tempy = (days / 10) % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 28, 48);
			tempy = days / 100;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 22, 48);
		} else {
			tempy = mins % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 50, 48);
			tempy = mins / 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 44, 48);
			da_copy_xpm_area(63, 66, 3, 9, 41, 48);
			tempy = hours % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 34, 48);
			tempy = hours / 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 28, 48);
			da_copy_xpm_area(63, 66, 3, 9, 25, 48);
			tempy = days % 10;
			da_copy_xpm_area(3 + (tempy * 6), 66, 6, 9, 18, 48);

		}
	}
}

/**************************************************************************/

/* Drawing LED's */
void DrawLite(int state, int dx, int dy)
{
	switch (state) {
	case B_RED:
		da_copy_xpm_area(BREDX, BREDY, LITEW, LITEH, dx, dy);
		break;
	case B_GREEN:
		da_copy_xpm_area(BGREENX, BGREENY, LITEW, LITEH, dx, dy);
		break;
	default:
	case B_OFF:
		da_copy_xpm_area(BOFFX, BOFFY, LITEW, LITEH, dx, dy);
		break;
	}

}

/* EOF */
