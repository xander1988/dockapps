/* wmshutdown - dockapp to shutdown or reboot your machine
 *
 * Copyright 2001, 2002 Rafael V. Aroca <rafael@linuxqos.cjb.net>
 * Copyright 2014 Doug Torrance <dtorrance@piedmont.edu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

char *border_dark;
char *border_light;
char *halt_method;
char *reboot_method;
char *suspend_method;
char *hibernate_method;

bool halt = false;
bool reboot = false;
bool suspend = false;
bool hibernate = false;

void routine(int, char **);
void process_event(int, int);
void show_main_scr(void);
void show_config_scr(void);
void draw_button(int, int, int, int, bool);
void reset_buttons(void);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a graphical dock application to shutdown computer.\n\n"

    PROG_NAME" is a dock application (for Window Maker) that can shut down or reboot the workstation by clicking a button on the desktop. It's useful just for desktop machines, so there's no need to keep typing shutdown now -h every time you have to go away for some reason.\n\n"

    "Prior to version 1.1, "PROG_NAME" used a setuid program (Shutdown or wmshutdown-run) to actually shut down the computer, introducing potential security risks. From version 1.1 and prior to version 1.7, "PROG_NAME" used logind instead.\n\n"

    "From version 1.7, "PROG_NAME" accepts user-defined commands to shut down, reboot, suspend or hibernate computer. They can be set in config files or via command-line arguments.");

    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&halt_method, "halt_method", "-H", "--halt-method", NULL, "Command to halt your machine");
    da_init_string(&reboot_method, "reboot_method", "-R", "--reboot-method", NULL, "Command to reboot your machine");
    da_init_string(&suspend_method, "suspend_method", "-S", "--suspend-method", NULL, "Command to suspend your machine");
    da_init_string(&hibernate_method, "hibernate_method", "-B", "--hibernate-method", NULL, "Command to hibernate your machine");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                     /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");   /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc"); 

    da_parse_all(argc, argv);

    routine(argc, argv);
    
    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 192 * da_scale, 120 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 192 * da_scale, 120 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 192 * da_scale, 120 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 192 * da_scale, 120 * da_scale);
        break;
    }

    /* Clickable regions */
    /* Main screen */
    da_add_mouse_region(0,  4, 4, 60, 60);
    /* Config screen */
    da_add_mouse_region(1,  133-128,  5, 187-128, 18); /* halt button */
    da_add_mouse_region(2,  133-128, 18, 187-128, 31); /* reboot button   */
    da_add_mouse_region(3,  133-128, 31, 160-128, 44); /* susp button  */
    da_add_mouse_region(4,  160-128, 31, 187-128, 44); /* hibr button */
    da_add_mouse_region(5,  133-128, 44, 160-128, 59); /* deny button */
    da_add_mouse_region(6,  160-128, 44, 187-128, 59); /* ok button   */
    
    show_main_scr();

    while (!da_conf_changed(argc, argv)) {
        process_event(da_watch_xevent(), da_check_mouse_region());

        da_redraw_window();

        usleep(100000L);
    }

    routine(argc, argv);
}

void process_event(int event, int region) {
    switch (region) {
    case 0:
        if (event == MOUSE_1_PRS) {
            da_copy_xpm_area(0, 64, 56, 56, 4, 4);
        
        } else if (event == MOUSE_1_REL) {
            da_copy_xpm_area(68, 4, 56, 56, 4, 4);

            show_config_scr();
        }
        
        da_redraw_window();
        
        break;

    case 1:
        if (event == MOUSE_1_PRS) {
            reset_buttons();
            draw_button(5, 5, 54, 13, true);
        
        } else if (event == MOUSE_1_REL) {
            halt = true;
            reboot = false;
            suspend = false;
            hibernate = false;
        }
        
        da_redraw_window();
        
        break;

    case 2:
        if (event == MOUSE_1_PRS) {
            reset_buttons();
            draw_button(5, 18, 54, 13, true);
        
        } else if (event == MOUSE_1_REL) {
            halt = false;
            reboot = true;
            suspend = false;
            hibernate = false;
        }
        
        da_redraw_window();
        
        break;

    case 3:
        if (event == MOUSE_1_PRS) {
            reset_buttons();
            draw_button(5, 31, 27, 13, true);
        
        } else if (event == MOUSE_1_REL) {
            halt = false;
            reboot = false;
            suspend = true;
            hibernate = false;
        }
        
        da_redraw_window();
        
        break;

    case 4:
        if (event == MOUSE_1_PRS) {
            reset_buttons();
            draw_button(32, 31, 27, 13, true);
        
        } else if (event == MOUSE_1_REL) {
            halt = false;
            reboot = false;
            suspend = false;
            hibernate = true;
        }

        da_redraw_window();
        
        break;

    case 5:
        if (event == MOUSE_1_PRS) {
            reset_buttons();
            draw_button(5, 44, 27, 15, true);
        
        } else if (event == MOUSE_1_REL) {
            halt = false;
            reboot = false;
            suspend = false;
            hibernate = false;

            show_main_scr();
        }

        da_redraw_window();
        
        break;

    case 6:
        if (event == MOUSE_1_PRS) {
            reset_buttons();
            draw_button(32, 44, 27, 15, true);
        
        } else if (event == MOUSE_1_REL) {
            show_main_scr();

            if (halt) {
                halt = false;

                if (halt_method) {
                    da_exec_command(halt_method);
                }
            }
            
            if (reboot) {
                reboot = false;

                if (reboot_method) {
                    da_exec_command(reboot_method);
                }
            }
            
            if (suspend) {
                suspend = false;

                if (suspend_method) {
                    da_exec_command(suspend_method);
                }
            }
            
            if (hibernate) {
                hibernate = false;

                if (hibernate_method) {
                    da_exec_command(hibernate_method);
                }
            }
        }
        
        break;
    }
}

void show_main_scr() {
    int i;

    da_enable_mouse_region(0);
    
    for (i = 1; i < 7; i++) {
        da_disable_mouse_region(i);
    }
    
    da_copy_xpm_area(68, 4, 56, 56, 4, 4);

    da_redraw_window();
}

void show_config_scr() {
    int i;

    da_disable_mouse_region(0);
    
    for (i = 1; i < 7; i++) {
        da_enable_mouse_region(i);
    }
    
    da_copy_xpm_area(132, 4, 56, 56, 4, 4);

    da_redraw_window();
}

void reset_buttons() {
    draw_button(5,   5, 54, 13, false);
    draw_button(5,  18, 54, 13, false);
    draw_button(5,  31, 27, 13, false);
    draw_button(32, 31, 27, 13, false);
    draw_button(5,  44, 27, 15, false);
    draw_button(32, 44, 27, 15, false);
}

void draw_button(int x, int y, int w, int h, bool down) {
    if (!down) {
        da_copy_xpm_area(x + 128, y, w, h, x, y);
    
    } else {
        da_copy_xpm_area(x + 128,         y,         1,     h - 1, x + w - 1, y + 1    );
        da_copy_xpm_area(x + 128 + w - 1, y + 1,     1,     h - 1, x,         y        );
        da_copy_xpm_area(x + 128,         y,         w - 1, 1,     x + 1,     y + h - 1);
        da_copy_xpm_area(x + 128 + 1,     y + h - 1, w - 1, 1,     x,         y        );
    }
}
