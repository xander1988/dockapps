/*
    Copyright (C) 2022 Xander

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

char day_of_week[7][4] = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
char mon_of_year[12][4] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *alarm_time;
char *alarm_command;
char *command;

bool alarm_done = false;
bool alarm_on;
bool am_pm_time;
bool show_seconds;

int alarm_h;
int alarm_m;
int alarm_s;

struct tm *time_struct;

long curtime;

time_t last_update = 0;

void routine(int, char **);
void draw_text(char *, int, int);
void draw_small_text(char *, int, int);
void parse_alarm_time(void);
void update_time(void);
void draw_time(void);
void draw_alarm(void);
void draw_dots(int, int);
void draw_opts(void);
void draw_date(void);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp that shows time and date.\n\n"

    "The upper screen displays current time, the middle screen displays and sets current alarm time. Use mouse buttons 1 and 3 to set the required time.\n\n"

    "The lower left screen shows and toggles options, which are: alarm being set, 12 hour time, and seconds being displayed. The lower right screen shows current date.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#51c300", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&alarm_time, "alarm_time", "-a", "--alarm-time", "00:00:00", "Time to display alarm (always in the 24-hour format)");
    da_init_string(&alarm_command, "alarm_command", "-c", "--alarm-command", NULL, "Command to execute on alarm");
    da_init_string(&command, "command", "-C", "--command", NULL, "Command to execute when date is activated");
    da_init_bool(&alarm_on, "alarm_on", NULL, "--alarm-on", false, "Activate alarm or not");
    da_init_bool(&am_pm_time, "12_hour_time", NULL, "--12-hour-time", false, "Show time in the 12-hour format (does not affect alarm time)");
    da_init_bool(&show_seconds, "show_seconds", "-S", "--show-seconds", true, "Display seconds counter or not");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc"); 
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    

    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 75, back_color, 25, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");
    
    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;
    }

    da_add_mouse_region(0, 6, 34, 41, 41); /* alarm on/off */
    da_add_mouse_region(1, 6, 43, 41, 50); /* 12 hour on/off */
    da_add_mouse_region(2, 6, 52, 41, 59); /* seconds on/off */
    da_add_mouse_region(3, 5, 19, 22, 28); /* alarm hour */
    da_add_mouse_region(4, 23, 19, 40, 28); /* alarm min */
    da_add_mouse_region(5, 41, 19, 58, 28); /* alarm sec */
    da_add_mouse_region(6, 15, 19, 27, 28); /* alarm hour */
    da_add_mouse_region(7, 36, 19, 48, 28); /* alarm min */
    da_add_mouse_region(8, 46, 33, 59, 59); /* some command */
    
    if (show_seconds) {
        da_disable_mouse_region(6);
        da_disable_mouse_region(7);
    } else {
        da_disable_mouse_region(3);
        da_disable_mouse_region(4);
        da_disable_mouse_region(5);
    }
    
    parse_alarm_time();

    while (!da_conf_changed(argc, argv)) {
        if (time(0) - last_update >= 1) {
            update_time();
        }
        
        waitpid(0, NULL, WNOHANG);        
        
        draw_time();
        
        draw_alarm();
        
        draw_opts();
        
        draw_date();
        
        da_redraw_window();

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            switch (da_check_mouse_region()) {
            case 0:
                alarm_on ^= true;
                
                break;
            
            case 1:
                am_pm_time ^= true;
                
                break;
            
            case 2:
                show_seconds ^= true;
                
                if (show_seconds) {
                    da_enable_mouse_region(3);
                    da_enable_mouse_region(4);
                    da_enable_mouse_region(5);
                    da_disable_mouse_region(6);
                    da_disable_mouse_region(7);
                } else {
                    da_disable_mouse_region(3);
                    da_disable_mouse_region(4);
                    da_disable_mouse_region(5);
                    da_enable_mouse_region(6);
                    da_enable_mouse_region(7);
                }
                
                break;
                
            case 3:
            case 6:
                alarm_h++;

                if (alarm_h > 23)
                    alarm_h = 0;
                    
                alarm_done = false;

                break;
            
            case 4:
            case 7:
                alarm_m++;

                if (alarm_m > 59)
                    alarm_m = 0;
                
                alarm_done = false;
                
                break;
            
            case 5:
                alarm_s++;

                if (alarm_s > 59)
                    alarm_s = 0;

                alarm_done = false;
                
                break;
                
            case 8:
                if (command)
                    da_exec_command(command);

                break;
            }
            
            break;

        case MOUSE_3_PRS:
            switch (da_check_mouse_region()) {
            case 3:
            case 6:
                alarm_h--;

                if (alarm_h < 0)
                    alarm_h = 23;

                alarm_done = false;
                
                break;
            
            case 4:
            case 7:
                alarm_m--;

                if (alarm_m < 0)
                    alarm_m = 59;
                
                alarm_done = false;
                
                break;
            
            case 5:
                alarm_s--;

                if (alarm_s < 0)
                    alarm_s = 59;

                alarm_done = false;
                
                break;
            }
            
            break;
            
        default:
            break;
        }
                
        usleep(200000);
    }
    
    routine(argc, argv);
}

void draw_text(char *text, int dx, int dy) {
    size_t i;
    
    int k, c;

    /* Clear area */
    //da_copy_xpm_area(70, 64, 54, 10, 5, 5);

    k = dx;
    
    for (i = 0; i < strlen(text); i++) {
        c = toupper(text[i]);
        
        if (c == ':') {
            da_copy_xpm_area(162, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == '.') {
            da_copy_xpm_area(155, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == '-') {
            da_copy_xpm_area(148, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == '_') {
            da_copy_xpm_area(141, 0, 7, 9, k, dy);
            
            k += 7;
        
        } else if (c == ' ') {
            da_copy_xpm_area(134, 0, 7, 9, k, dy);
            
            k += 7;
           
        } else if (c >= 'A' && c <= 'Z') {
            c -= 'A';
            
            da_copy_xpm_area(64 + c * 7, 10, 7, 9, k, dy);
            
            k += 7;
        
        } else {
            c -= '0';
            
            da_copy_xpm_area(64 + c * 7, 0, 7, 9, k, dy);
            
            k += 7;
        }
    }
}

void draw_small_text(char *text, int dx, int dy) {
    size_t i;
    
    int k, c;

    /* Clear area */
    //da_copy_xpm_area(70, 64, 54, 10, 5, 5);

    k = dx;
    
    for (i = 0; i < strlen(text); i++) {
        c = toupper(text[i]);
        
        if (c == '%') {
            da_copy_xpm_area(188, 29, 8, 8, k, dy);
            
            k += 8;
            
        } else if (c == ':') {
            da_copy_xpm_area(184, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == '.') {
            da_copy_xpm_area(180, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == '-') {
            da_copy_xpm_area(176, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == '_') {
            da_copy_xpm_area(172, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else if (c == ' ') {
            da_copy_xpm_area(168, 29, 4, 8, k, dy);
            
            k += 4;
                   
        } else if (c >= 'A' && c <= 'Z') {
            c -= 'A';
            
            da_copy_xpm_area(64 + c * 4, 29, 4, 8, k, dy);
            
            k += 4;
        
        } else {
            c -= '0';
            
            da_copy_xpm_area(64 + c * 4, 20, 4, 8, k, dy);
            
            k += 4;
        }
    }
}

void parse_alarm_time(void) {
    char time_tmp[3];
    
    if (!alarm_time || (strlen(alarm_time) != 8)) {
        printf("ERR: wrong time set, aborting.\n");
        da_log(ERR, "wrong time set, aborting.\n");

        exit(0);
    }
    
    /* hour */
    time_tmp[0] = alarm_time[0];
    time_tmp[1] = alarm_time[1];
    alarm_h = atoi(time_tmp);
    /* min */
    time_tmp[0] = alarm_time[3];
    time_tmp[1] = alarm_time[4];
    alarm_m = atoi(time_tmp);
    /* sec */
    time_tmp[0] = alarm_time[6];
    time_tmp[1] = alarm_time[7];
    alarm_s = atoi(time_tmp);
}

void update_time(void) {
    curtime = time(0);

    time_struct = localtime(&curtime);

    time(&last_update);
}

void draw_time(void) {
    char hour[3];
    char min[3];
    char sec[3];
    
    int hr;
    
    hr = time_struct->tm_hour;
    
    if (am_pm_time) {
        if (hr == 0) {
            hr = 12;
        } else {
            hr = (hr > 12) ? hr - 12 : hr;
        }
    }

    sprintf(hour, "%02d", hr);
    sprintf(min, "%02d", time_struct->tm_min);
    sprintf(sec, "%02d", time_struct->tm_sec);
    
    /* Clear */
    da_copy_xpm_area(105, 19, 54, 10, 5, 5);
    
    if (show_seconds) {
        draw_text(hour, 5, 5);
        
        draw_dots(21, 5);
        
        draw_text(min, 24, 5);
        
        draw_dots(40, 5);
        
        draw_text(sec, 43, 5);
        
    } else {
        draw_text(hour, 14, 5);
        
        draw_dots(31, 5);
        
        draw_text(min, 35, 5);
    }
}

void draw_alarm(void) {
    char hour[3];
    char min[3];
    char sec[3];

    sprintf(hour, "%02d", alarm_h);
    sprintf(min, "%02d", alarm_m);
    sprintf(sec, "%02d", alarm_s);
    
    /* Clear */
    da_copy_xpm_area(105, 19, 54, 10, 5, 19);
    
    if (show_seconds) {
        draw_text(hour, 5, 19);
        
        draw_dots(21, 19);
        
        draw_text(min, 24, 19);
        
        draw_dots(40, 19);
        
        draw_text(sec, 43, 19);
        
    } else {
        draw_text(hour, 14, 19);
        
        draw_dots(31, 19);
        
        draw_text(min, 35, 19);
    }
    
    if (alarm_on && !alarm_done) {
        /* alarm */
        if (time_struct->tm_hour == alarm_h && time_struct->tm_min == alarm_m && time_struct->tm_sec == alarm_s) {
            alarm_done = true;
            
            if (alarm_command)
                da_exec_command(alarm_command);
        }
    }
}

void draw_dots(int dx, int dy) {
    da_copy_xpm_area(170, 0, 2, 9, dx, dy);
}

void draw_opts(void) {
    if (alarm_on)
        draw_small_text("ALRM:  ON", 5, 33);
    else
        draw_small_text("ALRM: OFF", 5, 33);
    
    if (am_pm_time)
        draw_small_text("AMPM:  ON", 5, 42);
    else
        draw_small_text("AMPM: OFF", 5, 42);
    
    if (show_seconds)
        draw_small_text("SCND:  ON", 5, 51);
    else
        draw_small_text("SCND: OFF", 5, 51);
}

void draw_date(void) {
    char mday[4];

    sprintf(mday, "%02d", time_struct->tm_mday);
    
    draw_small_text(day_of_week[time_struct->tm_wday], 46, 33);
    draw_small_text(mday, 48, 42);
    draw_small_text(mon_of_year[time_struct->tm_mon], 46, 51);
}
