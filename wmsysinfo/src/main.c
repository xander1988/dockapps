/*
    Copyright (C) 2022 Xander

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <libdockapp4/dockapp.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"

char *front_color;
char *back_color;
char *border_dark;
char *border_light;
char *user_info;
char *kernel;

bool show_buffers;

void routine(int, char **);
void draw_text(char *, int, int);
void draw_small_text(char *, int, int);
void draw_user_info(void);
void draw_uptime(void);
void draw_version(void);
void draw_stats(void);
void get_version(void);
void get_user_info(void);
void free_all(void);

int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "a dockapp that shows system information.\n\n"

    PROG_NAME" is a dockapp that provides system information, which is current user and host name, machine uptime, kernel version in use.\n\n"

    "The lower part of the screen shows system load bars for cpu, memory, and swap.");

    da_init_string(&front_color, "front_color", "-f", "--front-color", "#51c300", "Main color");
    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_bool(&show_buffers, "show_buffers", "-B", "--show-buffers", false, "Include buffers and cache in memory usage calculations; the trend in recent years has been to not include this in memory load monitors, but it can be useful information");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");

    da_init_conf_file(SYSTEM, "/etc/"PROG_NAME"rc");
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                   /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf"); /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                    /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(front_color, "led_color_high");
    da_add_mixed_color(front_color, 75, back_color, 25, "led_color_med");
    da_add_mixed_color(front_color, 25, back_color, 75, "led_color_low");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;

    case 3:
        da_open_xwindow(argc, argv, master3_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;

    case 2:
        da_open_xwindow(argc, argv, master2_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;

    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, NULL, 247 * da_scale, 65 * da_scale);
        break;
    }

    get_user_info();

    get_version();

    while (!da_conf_changed(argc, argv)) {
        waitpid(0, NULL, WNOHANG);

        draw_user_info();

        draw_uptime();

        draw_version();

        draw_stats();

        da_redraw_window();

        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            /* TODO: do something */

            break;
        }

        usleep(200000);
    }

    free_all();

    routine(argc, argv);
}

void draw_text(char *text, int dx, int dy) {
    size_t i;

    int k, c;

    /* Clear area */
    //da_copy_xpm_area(70, 64, 54, 10, 5, 5);

    k = dx;

    for (i = 0; i < strlen(text); i++) {
        c = toupper(text[i]);

        if (c == ':') {
            da_copy_xpm_area(162, 0, 7, 9, k, dy);

            k += 7;

        } else if (c == '.') {
            da_copy_xpm_area(155, 0, 7, 9, k, dy);

            k += 7;

        } else if (c == '@') {
            da_copy_xpm_area(169, 0, 7, 9, k, dy);

            k += 7;

        } else if (c == '-') {
            da_copy_xpm_area(148, 0, 7, 9, k, dy);

            k += 7;

        } else if (c == '_') {
            da_copy_xpm_area(141, 0, 7, 9, k, dy);

            k += 7;

        } else if (c == ' ') {
            da_copy_xpm_area(134, 0, 7, 9, k, dy);

            k += 7;

        } else if (c >= 'A' && c <= 'Z') {
            c -= 'A';

            da_copy_xpm_area(64 + c * 7, 10, 7, 9, k, dy);

            k += 7;

        } else {
            c -= '0';

            da_copy_xpm_area(64 + c * 7, 0, 7, 9, k, dy);

            k += 7;
        }
    }
}

void draw_small_text(char *text, int dx, int dy) {
    size_t i;

    int k, c;

    /* Clear area */
    //da_copy_xpm_area(70, 64, 54, 10, 5, 5);

    k = dx;

    for (i = 0; i < strlen(text); i++) {
        c = toupper(text[i]);

        if (c == '%') {
            da_copy_xpm_area(188, 29, 8, 8, k, dy);

            k += 8;

        } else if (c == ':') {
            da_copy_xpm_area(184, 29, 4, 8, k, dy);

            k += 4;

        } else if (c == '.') {
            da_copy_xpm_area(180, 29, 4, 8, k, dy);

            k += 4;

        } else if (c == '-') {
            da_copy_xpm_area(176, 29, 4, 8, k, dy);

            k += 4;

        } else if (c == '_') {
            da_copy_xpm_area(172, 29, 4, 8, k, dy);

            k += 4;

        } else if (c == ' ') {
            da_copy_xpm_area(168, 29, 4, 8, k, dy);

            k += 4;

        } else if (c >= 'A' && c <= 'Z') {
            c -= 'A';

            da_copy_xpm_area(64 + c * 4, 29, 4, 8, k, dy);

            k += 4;

        } else {
            c -= '0';

            da_copy_xpm_area(64 + c * 4, 20, 4, 8, k, dy);

            k += 4;
        }
    }
}

void draw_stats(void) {
#define WIDTH 18
#define DX 23
    long mem_use, mem_total, swap_use, swap_total;

    long unsigned mem_free, mem_buffers, mem_cached, swap_free;

    static int last_idle = 0, last_use = 0;

    int width, diff, diff1, user, nice, system, idle;
    int j;

    FILE *fp;

    fp = fopen("/proc/stat", "r");

    if (fp) {
        char line[512];

        while (fgets(line, 512, fp)) {
            if (strstr(line, "cpu"))
                sscanf(line,"cpu %d %d %d %d", &user, &nice, &system, &idle);
        }

        fclose(fp);
    }

    diff = idle - last_idle;
    diff1 = user + nice + system - last_use;
    width = diff1 * WIDTH / (diff + diff1 + 1);
    last_idle = idle;
    last_use = user + nice + system;

    fp = fopen("/proc/meminfo", "r");

    if (fp) {
        char line[512];

        while (fgets(line, 512, fp)) {
            if (strstr(line, "MemTotal:"))
                sscanf(line, "MemTotal: %ld", &mem_total);
            else if (strstr(line, "MemFree:"))
                sscanf(line, "MemFree: %lu", &mem_free);
            //else if (strstr(line, "MemShared:"))
                //sscanf(line, "MemShared: %lu", &shared);
            else if (strstr(line, "Buffers:"))
                sscanf(line, "Buffers: %lu", &mem_buffers);
            else if (strstr(line, "Cached:"))
                sscanf(line, "Cached: %lu", &mem_cached);
            else if (strstr(line, "SwapTotal:"))
                sscanf(line, "SwapTotal: %ld", &swap_total);
            else if (strstr(line, "SwapFree:"))
                sscanf(line, "SwapFree: %lu", &swap_free);
        }

        fclose(fp);
    }

    mem_use = mem_total - mem_free;
    swap_use = swap_total - swap_free;

    if (!show_buffers)
        mem_use -= mem_buffers + mem_cached;

    draw_small_text("CPU", 5, 19 + 8 * 2);
    draw_small_text("MEM", 5, 19 + 8 * 3);
    draw_small_text("SWP", 5, 19 + 8 * 4);

    for (j = 0; j < WIDTH; j++) {
        if (j <= width)
            da_set_foreground(1);
        else
            da_set_foreground(3);

        da_fill_rectangle(DX + 2 * j, 36, 1, 7);
    }

    for (j = 0; j < WIDTH; j++) {
        if (j <= (mem_use * WIDTH) / mem_total)
            da_set_foreground(1);
        else
            da_set_foreground(3);

        da_fill_rectangle(DX + 2 * j, 44, 1, 7);
    }

    for (j = 0; j < WIDTH; j++) {
        if (swap_total == 0) {
            da_set_foreground(3);

        } else {
            if (j <= (swap_use * WIDTH) / swap_total)
                da_set_foreground(1);
            else
                da_set_foreground(3);
        }

        da_fill_rectangle(DX + 2 * j, 52, 1, 7);
    }
}

void draw_version(void) {
    size_t len;

    char str[255] = "";
    char shortened[11] = "";

    static int counter = 20;
    static int pos = 0;
    static int pos_0, pos_1, pos_2, pos_3, pos_4, pos_5, pos_6, pos_7, pos_8;

    len = strlen(kernel);

    draw_small_text("KRN", 5, 19 + 8);

    if (len <= 9) {
        draw_small_text(kernel, 22, 19 + 8);

    } else {
        sprintf(str, "         %s", kernel);

        if (counter % 2) {
            pos_0 = pos;
            pos_1 = pos + 1;
            pos_2 = pos + 2;
            pos_3 = pos + 3;
            pos_4 = pos + 4;
            pos_5 = pos + 5;
            pos_6 = pos + 6;
            pos_7 = pos + 7;
            pos_8 = pos + 8;

            shortened[8] = str[pos_8] ? str[pos_8] : ' ';
            shortened[7] = str[pos_7] ? str[pos_7] : ' ';
            shortened[6] = str[pos_6] ? str[pos_6] : ' ';
            shortened[5] = str[pos_5] ? str[pos_5] : ' ';
            shortened[4] = str[pos_4] ? str[pos_4] : ' ';
            shortened[3] = str[pos_3] ? str[pos_3] : ' ';
            shortened[2] = str[pos_2] ? str[pos_2] : ' ';
            shortened[1] = str[pos_1] ? str[pos_1] : ' ';
            shortened[0] = str[pos_0] ? str[pos_0] : ' ';

            pos++;

            if (pos >= (int)strlen(str))
                pos = 0;

            draw_small_text(shortened, 22, 19 + 8);
        }

        counter--;

        if (counter <= 0)
            counter = 20;
    }
}

void draw_uptime(void) {
    char days[10];
    char hrs[10];
    char min[10];
    char sec[10];

    long uptime;

    int d, h, m, s;

    FILE *f;

    f = fopen("/proc/uptime", "r");

    if (f) {
        fscanf(f, "%li", &uptime);

        fclose(f);

    } else {
        exit(1);
    }

    d = (int)uptime / (24 * 3600);

    uptime -= d * 24 * 3600;

    h = (int)uptime / 3600;

    uptime -= h * 3600;

    m = (int)uptime / 60;

    uptime -= m * 60;

    s = uptime;

    if (d > 9999)
        d = 9999;

    sprintf(days, "%04i", d);
    sprintf(hrs, "%02i", h);
    sprintf(min, "%02i", m);
    sprintf(sec, "%02i", s);

    draw_small_text(days, 5, 19);
    draw_small_text(hrs, 26, 19);
    draw_small_text(".", 34, 19);
    draw_small_text(min, 38, 19);
    draw_small_text(".", 46, 19);
    draw_small_text(sec, 50, 19);
}

void draw_user_info(void) {
    size_t len;

    char str[255] = "";
    char shortened[8] = "";

    static int counter = 20;
    static int pos = 0;
    static int pos_0, pos_1, pos_2, pos_3, pos_4, pos_5, pos_6;

    len = strlen(user_info);

    if (len <= 7) {
        draw_text(user_info, 7, 5);

    } else {
        sprintf(str, "       %s", user_info);

        if (counter % 2) {
            pos_0 = pos;
            pos_1 = pos + 1;
            pos_2 = pos + 2;
            pos_3 = pos + 3;
            pos_4 = pos + 4;
            pos_5 = pos + 5;
            pos_6 = pos + 6;

            shortened[6] = str[pos_6] ? str[pos_6] : ' ';
            shortened[5] = str[pos_5] ? str[pos_5] : ' ';
            shortened[4] = str[pos_4] ? str[pos_4] : ' ';
            shortened[3] = str[pos_3] ? str[pos_3] : ' ';
            shortened[2] = str[pos_2] ? str[pos_2] : ' ';
            shortened[1] = str[pos_1] ? str[pos_1] : ' ';
            shortened[0] = str[pos_0] ? str[pos_0] : ' ';

            pos++;

            if (pos >= (int)strlen(str))
                pos = 0;

            draw_text(shortened, 7, 5);
        }

        counter--;

        if (counter <= 0)
            counter = 20;
    }
}

void get_user_info(void) {
    size_t len1, len2;

    char *username;

    char hostname[255];

    FILE *f;

    username = getlogin();

    f = fopen("/etc/hostname", "r");

    if (f) {
        fscanf(f, "%s", hostname);

        fclose(f);

    } else {
        exit(1);
    }

    len1 = strlen(username);
    len2 = strlen(hostname);

    user_info = malloc(len1 + len2 + 2);

    sprintf(user_info, "%s@%s", username, hostname);
}

void get_version(void) {
    size_t len;

    char version[255];

    FILE *f;

    f = fopen("/proc/version", "r");

    if (f) {
        fscanf(f, "%*s %*s %s", version);

        fclose(f);

    } else {
        exit(1);
    }

    len = strlen(version);

    kernel = malloc(len + 1);

    sprintf(kernel, "%s", version);
}

void free_all(void) {
    if (user_info) {
        free(user_info);

        user_info = NULL;
    }

    if (kernel) {
        free(kernel);

        kernel = NULL;
    }
}
