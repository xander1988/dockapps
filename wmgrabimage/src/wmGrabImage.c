/*
 *
 *      wmGrabImage-1.00 (C) 1999 Mike Henderson (mghenderson@lanl.gov)
 *
 *          - Monitors an image on the WWW in a WindowMaker DockApp
 *
 *
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program (see the file COPYING); if not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 *      Boston, MA  02111-1307, USA
 *
 *
 *  Platforms tested on: Linux, Solaris 2.6, IRIX 6.5
 *
 *
 *  ToDo:   Add inicator to show if image is uptodate or not
 *      Get rid of GrabImage and put all that stuff in wmGrabImage.c
 *      Center image in icon - this info should be in the returned Attributes struct.
 *      etc...
 *
 *      I am still not sure we are handling color resources properly... Seems to work OK on my 24-bit displays...
 *
 *
 *
 *
 *      Changes:
 *  Version 0.72  - May 27, 2001
 *          Fixed resource leak (XFreeColors)
 *
 *  Version 0.71  - May 24, 2001
 *          Added support for "zooming" the icon display
 *          on a region
 *
 *  Version 0.70  - March 28, 1999.
 *          Added support for local images (e.g. file:///home/mgh/junk.gif.)
 *
 *  Version 0.66  - February 23, 1999.
 *          Added help line for -delay option. And added a man page.
 *
 *  Version 0.65  - February 11, 1999.
 *                      Now starts netscape if not already running...
 *
 *  Version 0.64  - February 9, 1999.
 *                      Added command-line option for "Time between updates"
 *
 *  Version 0.63  - February 9, 1999.
 *          Fixed (potential) memory leak in the ShapeMask Pixmap
 *                      (gets returned only if color None is used in xpm file)
 *          Added XFreeColors call.
 *
 *  Version 0.62  - February 4, 1999.
 *          buf fixes. Added -c option to center vertically.
 *
 *
 *  Version 0.61  - February 2, 1999.
 *          Added Attributes support for Xpm Creation.
 *              Also, added a kludge to GrabImage to stream edit
 *          any "Transparent" colors that may get generated. (Change them
 *          to black.)
 *
 *  Version 0.6   - February 2, 1999.
 *          Some bug fixes. Added Double click support.
 *          Now, DoubleClick's do the following:
 *
 *              Button1: display original size image via xv.
 *
 *              Button2: send another URL (specified with the -http
 *                       command line option) to netscape.
 *
 *              Button3: Force Update.
 *
 *
 *  Version 0.5   - initial (very) beta release February 1, 1999.
 *
 *
 */

/*
 *   Includes
 */
#include <libdockapp4/dockapp.h>
#include <fcntl.h>

#include "config.h"
#include "bitmaps/master.xpm"
#include "bitmaps/master2.xpm"
#include "bitmaps/master3.xpm"
#include "bitmaps/master4.xpm"
#include "bitmaps/mask.xbm"
#include "bitmaps/mask2.xbm"
#include "bitmaps/mask3.xbm"
#include "bitmaps/mask4.xbm"

/*
 *  Delay between refreshes (in microseconds)
 */
#define DELAY 10000L

void routine(int, char **);

char *back_color;
char *border_dark;
char *border_light;
char *ImageURL;
char *HttpURL;
char *ConvertGeometry;
char *image_viewer;
char *web_browser;

int UpdateDELAY;

bool CenterImage;

char XpmFileName[1024], ImageFileName[1024];

int UpToDate = 0;
int ForceUpdate = 1;
int ForceUpdate2 = 1;

/*
 *   main
 */
int main(int argc, char *argv[]) {
    da_init_main(PROG_NAME, PROG_VERSION, "monitor your favorite web image.\n\n"

    "Click on Mouse Button 1 will display full size image with a chosen viewer. Mouse Button 2: send http_url to a chosen browser. Mouse Button 3: update image right away (i.e. grab a new one). If you have memory leak problems: upgrade your ImageMagick package (its likely to be the cause).");

    da_init_string(&back_color, "back_color", "-b", "--back-color", "#202020", "Background color");
    da_init_string(&border_dark, "border_dark", "-d", "--border-dark", "#000000", "Dark (upper and left) border color");
    da_init_string(&border_light, "border_light", "-l", "--border-light", "#ffffff", "Light (lower and right) border color");
    da_init_string(&ImageURL, "image_url", "-u", "--image-url", NULL, "The URL of image to display");
    da_init_string(&HttpURL, "http_url", NULL, "--http-url", NULL, "The URL of the WWW page to send to your browser with Button2 click");
    da_init_string(&ConvertGeometry, "convert_geometry", NULL, "--convert-geometry", NULL, "The geometry to pass to convert when scaling the image");
    da_init_string(&image_viewer, "image_viewer", "-i", "--image-viewer", NULL, "Set image viewer");
    da_init_string(&web_browser, "web_browser", NULL, "--web-browser", NULL, "Set web browser");
    da_init_integer(&UpdateDELAY, "update_delay", NULL, "--update-delay", 10, 14400, 600, "Set time (in seconds) between updates");
    da_init_bool(&CenterImage, "center_image", "-c", "--center-image", false, "Center the image vertically in the icon");
    da_init_integer(&da_scale, "scale", "-s", "--scale", 1, 4, 1, "Set dockapp scale");
    da_init_string(&da_display_name, "display", NULL, "--display", NULL, "Select different target display");
    da_init_string(&da_geometry, "geometry", NULL, "--geometry", NULL, "Set window geometry");
    da_init_bool(&da_windowed, "windowed", "-w", "--windowed", false, "Start in windowed mode");
    da_init_string(&da_back_image, "back_image", NULL, "--back-image", NULL, "In the windowed mode, use this as background (otherwise it will be transparent)");
    da_init_bool(&da_dock_win, "dock", NULL, "--dock", false, "In the windowed mode, behave like a panel window");
    da_init_bool(&da_showver, "version", "-v", "--version", false, "Show version and exit");
    da_init_bool(&da_showhelp, "help", "-h", "--help", false, "Show this help and exit");
    
    da_init_conf_file(LOCAL, DA_GENERAL_CONFIG);                    /* options shared between dockapps */
    da_init_conf_file(LOCAL, ".config/dockapps/"PROG_NAME".conf");  /* modern conf path */
    da_init_conf_file(LOCAL, "."PROG_NAME"rc");                     /* old homedir-based style */

    da_parse_all(argc, argv);

    routine(argc, argv);

    return 0;
}

void routine(int argc, char *argv[]) {
    struct tm *gmt;
    
    struct stat fi;
    
    Pixmap NewPixmap, NewShapeMask;
    
    XpmAttributes Attributes;
    
    Colormap cmap;
    
    int m, dt1, dt2, dt3, len;
    int i, j, Width, Height, yoff, fd, Flag;
    int havePixmap = 0;

    double hour24(), jd(), OldFileUT, FileUT;

    char command[1040], command2[1040], ImageName[256];

    if (!ImageURL) {
        fprintf(stderr, "You must specify an Image URL.\n");
        da_log(ERR, "you must specify an Image URL.\n");
        exit(-1);
    }
    
    da_init_xwindow();

    da_add_color(back_color, "back_color");
    da_add_color(border_dark, "border_dark");
    da_add_color(border_light, "border_light");

    switch (da_scale) {    
    case 4:
        da_open_xwindow(argc, argv, master4_xpm, mask4_bits, mask4_width, mask4_height);
        break;
        
    case 3:
        da_open_xwindow(argc, argv, master3_xpm, mask3_bits, mask3_width, mask3_height);
        break;
        
    case 2:
        da_open_xwindow(argc, argv, master2_xpm, mask2_bits, mask2_width, mask2_height);
        break;
        
    case 1:
    default:
        da_open_xwindow(argc, argv, master_xpm, mask_bits, mask_width, mask_height);
        break;
    }

    /*
     *  Figure out what the name of the image xpm file should be...
     */
    len = strlen(ImageURL);
    
    for (j = 0, i=0; i<len; ++i) {
        if (ImageURL[i] == '/')
            j = i;
    }
    
    strcpy(ImageName, ImageURL+j+1);
    
    sprintf(XpmFileName, "%s/.wmGrabImage/%s.xpm", getenv("HOME"), ImageName);
    sprintf(ImageFileName, "%s/.wmGrabImage/%s", getenv("HOME"), ImageName);

    cmap = DefaultColormap(da_display, DefaultScreen(da_display));

    /*
     *  Loop until we die
     */
    m = 32000;
    dt1 = 32000;
    dt2 = 32000;
    dt3 = 32000;
    UpToDate = 0;
    FileUT = -999.0;
    Flag = 1;
    NewShapeMask = 0;
    Attributes.nalloc_pixels = 0;
    
    while (!da_conf_changed(argc, argv)) {
        /*
         *  Keep track of # of seconds
         */
        if (m > 100){
            m = 0;
            ++dt1;
            ++dt2;
            ++dt3;
        } else {
            /*
             *  Increment counter
             */
            ++m;
        }

        /*
         *  This routine handles button presses.
         *
         *  Click on
         *      Mouse Button 1: Display full size image with a chosen viewer.
         *      Mouse Button 2: Send HttpURL to a chosen browser.
         *      Mouse Button 3: Update image right away (i.e. grab a new one).
         *
         */
        switch (da_watch_xevent()) {
        case MOUSE_1_PRS:
            if (image_viewer) {
                sprintf(command2, "%s %s &", image_viewer, ImageFileName);
                da_exec_command(command2);
            }
            break;

        case MOUSE_2_PRS:
            if (web_browser) {
                sprintf(command2, "%s %s &", web_browser, HttpURL);
                da_exec_command(command2);
            }
            break;

        case MOUSE_3_PRS:
            ForceUpdate2 = 1;
            break;
        }

        /*
         *  Draw window.
         */
        if (ForceUpdate || Flag) {
            /*
             * Clear window.
             */
            da_copy_xpm_area(5, 69, 54, 54, 5, 5);

            if (havePixmap) {
                /*
                 * free up the colors, if we alloc'd some before
                 */
                if (Attributes.nalloc_pixels > 0)
                    XFreeColors(da_display, cmap,  Attributes.alloc_pixels, Attributes.nalloc_pixels, 0);

                /*
                 *  Free last pixmap -- we dont need it anymore...
                 *  A ShapeMask is returned if the Pixmap had the color None used.
                 *  We could probably change Transparent to None to make use of this, but for now,
                 *  lets just ignore it...
                 */
                if ( NewShapeMask != 0 )
                    XFreePixmap(da_display, NewShapeMask);

                XFreePixmap(da_display, NewPixmap);

                XpmFreeAttributes(&Attributes);

                havePixmap= 0;
            }

            /*
             *   Grab new pixmap. Accept a reasonable color match.
             */
            Attributes.valuemask   = XpmExactColors | XpmCloseness | XpmReturnAllocPixels;
            Attributes.exactColors = 0;
            Attributes.closeness   = 40000;

            if (XpmReadFileToPixmap(da_display, da_root_win, XpmFileName, &NewPixmap, &NewShapeMask, &Attributes) >= 0){
                Height = Attributes.height;
                Width  = Attributes.width;
                yoff = (CenterImage) ? ((54 * da_scale) - Height)/2 : 0;
                
                da_copy_from_xpm(NewPixmap, 0, 0, Width, Height, 5, 5 + yoff);
                
                Flag = 0;
                ForceUpdate = 0;
                havePixmap= 1;
            }

            /*
             * Make changes visible
             */
            da_redraw_window();
        }

        /*
         *  Check xpm file status
         */
        if (dt2 > 1){
            dt2 = 0;

            if ((fd = open(XpmFileName, O_RDONLY)) >= 0 ) {
                fstat(fd, &fi);
                close(fd);
                gmt = gmtime(&fi.st_mtime);
                OldFileUT = FileUT;
                FileUT = (double)gmt->tm_hour + (double)gmt->tm_min/60.0 + (double)gmt->tm_sec/3600.0;
        
                if (FileUT != OldFileUT)
                    ForceUpdate = 1;
            }
        }

        /*
         *  Check every 5 min if the values are not up to date...
         */
        if (ForceUpdate2||(dt3 > UpdateDELAY)){
            dt3 = 0;

            /*
             *  Execute Perl script to grab the Latest METAR Report
             */
            if (ConvertGeometry != NULL)
                sprintf(command, "GrabImage %ix%i %s %s &", 54 * da_scale, 54 * da_scale, ImageURL, ConvertGeometry);
            else
                sprintf(command, "GrabImage %ix%i %s &", 54 * da_scale, 54 * da_scale, ImageURL);

            da_exec_command(command);

            ForceUpdate = 1;
            ForceUpdate2 = 0;
        }

        usleep(DELAY);
    }

    routine(argc, argv);
}

/*
 *  Compute the Julian Day number for the given date.
 *  Julian Date is the number of days since noon of Jan 1 4713 B.C.
 */
double jd(int ny, int nm, int nd, double UT) {
    double A, B, C, D, JD, day;

    day = nd + UT/24.0;

    if ((nm == 1) || (nm == 2)){
        ny = ny - 1;
        nm = nm + 12;
    }

    if (((double)ny+nm/12.0+day/365.25)>=(1582.0+10.0/12.0+15.0/365.25)){
        A = ((int)(ny / 100.0));
        B = 2.0 - A + (int)(A/4.0);
    } else {
        B = 0.0;
    }

    if (ny < 0.0){
        C = (int)((365.25*(double)ny) - 0.75);
    } else {
        C = (int)(365.25*(double)ny);
    }

    D = (int)(30.6001*(double)(nm+1));

    JD = B + C + D + day + 1720994.5;

    return(JD);
}
